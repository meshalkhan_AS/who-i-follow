<!DOCTYPE html>
<html>
@include('sports.head')
<body class="sign-in">
    <div class="wrapper">
        <div class="sign-in-page">
            <div class="signin-popup">
                <div class="signin-pop">
                    <div class="row">
                        <div class="col-lg-6 no-pdd">
                            <div class="cmp-info">
                                <img src="{{ asset('public/sports/images/resources/login.png') }}" alt="" >
                            </div>
                        </div>
                        <div class="col-lg-6">
                            <div class="cmp-info">
                                <div class="cm-logo">
                                    <img src="{{ asset('public/sports/images/login images/logo.svg') }}"  alt="">
                                </div>
                            </div>
                            <div class="login-sec">
                                <ul>
                                    @if($message =='Password Reset Successfully')
                                        <li id="mydiv">
                                            <div class="alert alert-success alert-dismissible fade show">
                                                <strong >{{$message}}</strong>
                                            </div>
                                        </li>
                                    @endif

                                    @error('error')
                                        <li id="mydiv">
                                            <div class="alert alert-warning alert-dismissible fade show credentials">
                                                <strong >{{$message}}</strong>
                                            </div>
                                        </li>
                                    @enderror
                                </ul>
                                <div class="sign_in_sec">
                                    <h3>Welcome Back</h3>
                                    {!! Form::open(['method' => 'GET', 'route' => ['login.check'], 'data-parsley-validate']) !!}
                                    <div class="row">
                                        <div class="col-lg-12 no-pdd">
                                            <div class="sn-field">
                                                <label class="label-">Email</label>
                                                {!! Form::text('email', old('email'), ['placeholder'=>'Email address','required'=>'true']) !!}
                                            </div>
                                        </div>
                                        <div class="col-lg-12 no-pdd">
                                            <div class="sn-field">
                                                <label class="label-">Password</label>
                                                {!! Form::input('password','password', old('password'), ['placeholder'=>'Password','required'=>'true']) !!}
                                            </div>
                                        </div>
                                        <div class="col-lg-12 no-pdd">
                                            <button class="btn login" type="submit">Sign In</button>
                                        </div>
                                        <div class="col-lg-12 no-pdd password-reset">
                                            <a href="{{route("auth.password.reset")}}}">Forgotton Account?</a>
                                        </div>
                                        <div class="col-lg-12 no-pdd">
                                            <hr class="p-0 w-100 m-0">
                                        </div>
                                        <div class="create-new-account">
                                            {{--<p>Don't have an account? </p>--}}
                                            {{--<a href="{{route("register")}}" title="">Sign Up</a>--}}
                                            <a class="text-white" href="{{route("register")}}">Create New Account</a>
                                        </div>
                                    </div>
                                    {!! Form::close() !!}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    {{--@include('sports.script')--}}
    @include('auth.js')
</body>
</html>