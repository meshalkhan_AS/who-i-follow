<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.0/js/select2.min.js"></script>

<script type="text/javascript">

    $("#tab-2").hide();

    var myvalidation = function () {
        var password = $('#password').val();
        var confirmPassword = $('#password1').val();
        var fname = $('#fname').val();
        var lname = $('#lname').val();
        var email = $('#email').val();

        if ($("#c2").prop("checked")) {
            if (fname == '' || password == '' || confirmPassword == '' || lname == ''|| email == '') {
                swal("Type Again", "Please fill the required fields", "error");
            }
            else {
                var check = checkvalidation();
            }
        }
        else {
            swal("Policies", "Please Agree to the Terms and Conditions", "error");
        }
    };

    function checkvalidation() {
        var password = $('#password').val();
        var confirmPassword = $('#password1').val();
        var email = $('#email').val();
        text = password.split(' ');

        if(email!='') {
            if (!validateEmail(email)) {
                var title='Please Enter Valid Email.!';
                swal("Type Again", title, "error");
            }
            else {
                $.ajax({
                    type: 'POST',
                    url: '{{ route('login.checkemail') }}',
                    data: {
                        _token: '{{ csrf_token() }}',
                        email: email,
                        id: ''
                    },
                    success: function (response) {
                        if(response=='repeat') {
                            var title='Email Already Exists.!';
                            swal("Type Again", title, "error");
                        }
                        else if(response=='success') {
                            if(password!='' && confirmPassword!='') {
                                if(text.length>1) {
                                    swal( "","Please not to use space in Password field.", "error");
                                }
                                else if(password.length<8) {
                                    swal( "","Passwords Length should be equal or greater than 8.", "error");
                                }
                                else if (password != confirmPassword) {
                                    swal( "Type Again","Passwords do not match", "error");
                                    $('#password1').val('');
                                }
                                else {
                                    $('html, body').animate({ scrollTop: 0 }, 'fast');
                                    $("#tab-1").hide();
                                    $("#tab-2").show();
                                }
                            }
                        }
                    }
                });
            }
        }
    }

    function validateEmail(email) {
        const re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        return re.test(email);
    }

    //    Check First
    var validationcheck = function ()
    {
        var day = $('#day').val();
        var month = $('#month').val();
        var year = $('#year').val();

        if (day == '' || month == '' || year == '' )
        {
            swal("Type Again", "Please fill your Date of Birth", "error");
        }
        else {
//            var date = datecheck();
            $("#myForm").submit();
        }
    };

    //    Check First
    var sendotp = function ()
    {
        var mob = $('#mob').val();
        if (mob == '' )
        {
            swal("", "Please Enter Mobile Number", "error");
        }
        else
        {
            $.ajax({
                type: 'POST',
                url: '{{ route('login.checkmobile') }}',
                data: {
                    _token: '{{ csrf_token() }}',
                    mob: mob,
                    id: ''
                },

                success: function (response) {
                    if(response=='repeat')
                    {
                        var title='Phone Number Already Exists.!';
                        swal("Type Again", title, "error");
                    }
                    else if(response=='success')
                    {

                        swal( "OTP Send","You will receive OTP within few seconds", "success");
                        $("#otpfield").css("display", "block");
                        $("#otpbutton").css("display", "block");
                    }

                }
            });
        }

    };

    //    Check Second
    var verifyotp = function ()
    {
        var mob = $('#mob').val();
        if (mob == '' )
        {
            swal("", "Please Enter Mobile Number", "error");
        }
        else
        {
            $.ajax({
                type: 'POST',
                url: '{{ route('login.checkmobile') }}',
                data: {
                    _token: '{{ csrf_token() }}',
                    mob: mob,
                    id: ''
                },

                success: function (response) {
                    if(response=='repeat')
                    {
                        var title='Phone Number Already Exists.!';
                        swal("Type Again", title, "error");
                    }
                    else if(response=='success')
                    {
                        $("#verificationform").submit();
//                        swal( "OTP Send","You will receive OTP within few seconds", "success");
//                        $("#otpfield").css("display", "block");
//                        $("#otpbutton").css("display", "block");
                    }

                }
            });
        }
    };

</script>
<script>

            {{------ Upload Image ----------}}
    var readpicURL =function(input)
        {
//    //        Alert Messages
//            $("#message-popup-div").show();
//            $("#alert-text").html('Uploading Profile Picture..');
//            setTimeout(function()
//            {
//                $("#message-popup-div").hide();
//                $("#alert-text").html('');
//            }, 1000);
//    //        Alert Messages

            swal({
                title: 'Uploading!',
                text: 'Image Uploaded Successfully!',
                icon: 'success',
                timer: 1000,
                button: false
            }).then(function() {
            });

            if (input.files && input.files[0]) {
                var reader = new FileReader();

                reader.onload = function (e) {
                    $('#id-picture').attr('src' , e.target.result);
                };

                reader.readAsDataURL(input.files[0]);
            }
            document.getElementById("profilepicdivone").style.display = "none";
            document.getElementById("profilepicdivtwo").style.display = "block";
        };
</script>
