<!DOCTYPE html>
<html>
@include('sports.head')
<body class="sign-in">
    <div class="wrapper">
        <div class="signin-popup">
            <div class="signin-pop">
                <div class="signin-pop">
                    <div class="row">
                        <div class="col-lg-6 no-pdd">
                            <div class="cmp-info">
                                <img src="{{ asset('public/sports/images/resources/login.png') }}" alt="" >
                            </div>
                        </div>
                        <div class="col-lg-6">
                            <div class="cmp-info">
                                <div class="cm-logo">
                                    <img src="{{ asset('public/sports/images/login images/logo.svg') }}"  alt="">
                                </div>
                            </div>
                            <div class="login-sec">
                                <div class="sign_in_sec">
                                    <h3>Verification</h3>
                                    {!! Form::open(['method' => 'POST', 'id'=>'verificationform','route' => ['register.verify'], 'data-parsley-validate']) !!}
                                    <div class="row">
                                        <div class="col-lg-12 no-pdd">
                                            <div class="sn-field">
                                                <label class="label-">Mobile number</label>
                                                {!! Form::number('mob', old('mob'), [ 'id'=>'mob','placeholder' => 'Enter Mobile Number','required'=>'true']) !!}
                                            </div>
                                        </div>
                                        <div class="col-lg-12 no-pdd" id="otpbutton" >
                                            <a class="btn verify-btn" onclick="verifyotp()">Register</a>
                                        </div>
                                    </div>
                                    {!! Form::close() !!}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    {{--@include('sports.script')--}}
    @include('auth.js')
</body>
</html>