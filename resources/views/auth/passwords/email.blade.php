<!DOCTYPE html>
<html>
@include('sports.head')
<body class="sign-in">
    <div class="wrapper">
        <div class="sign-in-page">
            <div class="signin-popup">
                <div class="signin-pop">
                    <div class="row">
                        <div class="col-lg-6 no-pdd">
                            <div class="cmp-info">
                                <img src="{{ asset('public/sports/images/resources/login.png') }}" alt="" >
                            </div>
                        </div>
                        <div class="col-lg-6">
                            <div class="cmp-info">
                                <div class="cm-logo">
                                    <img src="{{ asset('public/sports/images/login images/logo.svg') }}"  alt="">
                                </div>
                            </div>
                            <div class="login-sec">
                                <ul>
                                    @if (count($errors) > 0)
                                        <li  class="alert alert-warning alert-dismissible fade show password-emails">
                                            @foreach ($errors->all() as $error)
                                                <strong>{{ $error }} </strong>
                                            @endforeach
                                        </li>
                                    @endif

                                    @if($error_!='')
                                        <li class="alert alert-success alert-dismissible fade show password-emails">
                                            <strong>{{ $error_ }} </strong>
                                        </li>
                                    @endif
                                    @if (session('status'))
                                        <li  class="alert alert-success alert-dismissible fade show password-emails">
                                            <strong> {{ session('status') }} </strong>
                                        </li>
                                    @endif
                                </ul>
                                {{--<div class="col-lg-12 no-pdd">--}}
                                    {{--<a class="btn back-to-login" href="{{route('login')}}">Back to Login</a>--}}
                                {{--</div>--}}
                                <div class="sign_in_sec">
                                    <h3 class="mb-2">Find Your Account</h3>
                                    <hr class="w-100 p-0 m-0 mb-2">
                                    <form role="form" method="POST" action="{{ url('password/email') }}">
                                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                        <div class="row">
                                            <div class="col-lg-12 no-pdd mb-3">
                                                <p class="text-dark">Please enter your email address to search for your account.</p>
                                            </div>
                                            <div class="col-lg-12 no-pdd">
                                                <div class="sn-field">
                                                    {{--<label class="label-">Email</label>--}}
                                                    {!! Form::text('email', old('email'), [ 'placeholder' => 'Enter Email','required'=>'true']) !!}
                                                </div>
                                            </div>
                                            <hr class="w-100 p-0 m-0 mb-2 mt-2">
                                            <div class="col-lg-12 no-pdd">
                                                <div class="row">
                                                    <div class="col-lg-6 col-md-6 col-sm-6 col-6">
                                                        <a class="btn sent-email mt-0 bg-dark" href="{{route('login')}}">Cancel</a>
                                                    </div>
                                                    <div class="col-lg-6 col-md-6 col-sm-6 col-6">
                                                        <button class="btn sent-email mt-0" type="submit">Reset</button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    {{--@include('sports.script')--}}
    @include('auth.js')
</body>
</html>
