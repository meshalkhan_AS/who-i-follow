<!DOCTYPE html>
<html>
@include('sports.head')
<body class="sign-in">
    <div class="wrapper">
        <div class="sign-in-page">
            <div class="signin-popup">
                <div class="signin-pop">
                    <div class="row">
                        <div class="col-lg-6 no-pdd">
                            <div class="cmp-info">
                                <img src="{{ asset('public/sports/images/resources/login.png') }}" alt="" >
                            </div>
                        </div>
                        <div class="col-lg-6">
                            <div class="cmp-info">
                                <div class="cm-logo">
                                    <img src="{{ asset('public/sports/images/login images/logo.svg') }}"  alt="">
                                </div>
                            </div>
                            <div class="login-sec">
                                <ul>
                                    @if (count($errors) > 0)
                                        <li id="mydiv">
                                            <div  class="alert alert-warning alert-dismissible fade show reset-link">
                                                @foreach ($errors->all() as $error)
                                                    <strong>{{ $error }}</strong>
                                                @endforeach
                                            </div>
                                        </li>
                                    @endif
                                    @if (session('status'))
                                        <li id="mydiv">
                                            <div  class="alert alert-success alert-dismissible fade show reset-link">
                                                <strong> {{ session('status') }}</strong>
                                            </div>
                                        </li>
                                    @endif
                                </ul>
                                <div class="col-lg-12 no-pdd">
                                    <a class="btn back-to-login" href="{{route('login')}}">Back to Login</a>
                                </div>
                                <div class="sign_in_sec">
                                    <h3>Reset Password</h3>
                                    <hr class="w-100 p-0 m-0 mb-2">
                                    <form role="form" method="POST" action="{{ url('password/reset') }}">
                                        <input type="hidden" name="_token" value="{{ csrf_token() }}" />
                                        <input type="hidden" name="token" value="{{ $token }}" />
                                        <div class="row">
                                            <div class="col-lg-12 no-pdd">
                                                <div class="sn-field">
                                                    <label class="label-">Email</label>
                                                    <input type="email" name="email" value="{{ $email }}" readonly />
                                                </div>
                                            </div>
                                            <div class="col-lg-12 no-pdd">
                                                <div class="sn-field">
                                                    <label class="label-">New Password</label>
                                                    <input type="password" name="password" placeholder="New password" />
                                                </div>
                                            </div>
                                            <div class="col-lg-12 no-pdd">
                                                <div class="sn-field">
                                                    <label class="label-">Confirm Password</label>
                                                    <input type="password" name="password_confirmation"  placeholder="Confirm password"/>
                                                </div>
                                            </div>
                                            <hr class="w-100 p-0 m-0 mb-2 mt-2">
                                            <div class="col-lg-12 no-pdd">
                                                <div class="row">
                                                    <div class="col-lg-4 col-md-4 col-sm-4 col-4 no-pdd"></div>
                                                    <div class="col-lg-4 col-md-4 col-sm-4 col-4">
                                                        <a class="btn sent-email mt-0 bg-dark" href="{{route('login')}}">Cancel</a>
                                                    </div>
                                                    <div class="col-lg-4 col-md-4 colsm-4 col-4">
                                                        <button class="btn sent-email mt-0" type="submit">Reset</button>
                                                    </div>
                                                </div>
                                                {{--<button class="btn sent-email" type="submit">Reset password</button>--}}
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    {{--@include('sports.script')--}}
    @include('auth.js')
</body>
</html>