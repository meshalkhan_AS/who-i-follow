<!DOCTYPE html>
<html>
@include('sports.head')
<body class="sign-in">
    <div class="wrapper">
        <div class="sign-in-page">
            <div class="signin-popup">
                <div class="signin-pop">
                    <div class="row">
                        <div class="col-lg-6 no-pdd">
                            <div class="cmp-info">
                                <img src="{{ asset('public/sports/images/resources/login.png') }}" alt="" >
                            </div>
                        </div>
                        <div class="col-lg-6">
                            <div class="cmp-info">
                                <div class="cm-logo">
                                    <img src="{{ asset('public/sports/images/login images/logo.svg') }}"  alt="">
                                </div>
                            </div>
                            <div class="login-sec">
                                <div class="sign_in_sec">
                                    <h3>Create account</h3>
                                    {!! Form::open(['method' => 'POST','id'=>'myForm', 'route' => ['register.store'],'data-parsley-validate','files' => 'true','enctype'=>'multipart/form-data']) !!}
                                    <div class="row current" id="tab-1">
                                        <div class="col-lg-12 no-pdd">
                                            <div class="row">
                                                <div class="col-xl-6 col-lg-12 col-md-12 col-sm-12 col-12 no-pdd fname">
                                                    <div class="sn-field">
                                                        <label class="label-">First Name</label>
                                                        {!! Form::text('fname', '', [ 'id'=>'fname','placeholder'=>'First name','required'=>'true','maxlength'=>'10']) !!}
                                                    </div>
                                                </div>
                                                <div class="col-xl-6 col-lg-12 col-md-12 col-sm-12 col-12 no-pdd">
                                                    <div class="sn-field">
                                                        <label class="label-">Last Name</label>
                                                        {!! Form::text('lname', '', [ 'id'=>'lname','placeholder'=>'Surname','required'=>'true','maxlength'=>'10']) !!}
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-lg-12 no-pdd">
                                            <div class="sn-field">
                                                <label class="label-">Email</label>
                                                {!! Form::input('email', 'email', '', ['id'=>'email','placeholder'=>'Email Account','required'=>'true','maxlength'=>'35']) !!}
                                            </div>
                                        </div>
                                        <div class="col-lg-12 no-pdd">
                                            <div class="sn-field">
                                                <label class="label-">Password</label>
                                                {!! Form::input('password', 'password', '', [ 'id'=>'password','placeholder'=>'New password','required'=>'true']) !!}
                                            </div>
                                        </div>
                                        <div class="col-lg-12 no-pdd">
                                            <div class="sn-field">
                                                <label class="label-">Confirm Password</label>
                                                {!! Form::input('password', 'cpassword', '', [ 'id'=>'password1','placeholder'=>'Confirm password','required'=>'true']) !!}
                                            </div>
                                        </div>
                                        <div class="col-lg-12 no-pdd">
                                            <div class="checky-sec st2">
                                                <div class="fgt-sec">
                                                    <input type="checkbox" id="c2">
                                                    <label for="c2">
                                                        <span></span>
                                                    </label>
                                                    &nbsp;&nbsp;&nbsp;Yes, I understand and agree to the<br>&nbsp;&nbsp;&nbsp;Terms & Conditions.
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-lg-12 no-pdd">
                                            <a data-tab="tab-2" class="btn continue" onclick="myvalidation()">Continue </a>
                                        </div>
                                        <div class="col-lg-12 no-pdd have-account">
                                            <p class="darkgrey">Already have an account?</p>
                                            <a href="{{route("login")}}">Sign In</a>
                                        </div>
                                    </div>
                                    <div class="row" id="tab-2">
                                        <div class="col-lg-12 no-pdd">
                                            <div class="sn-field">
                                                <label class="label-">Profile Picture</label>
                                                <div class="box-profile">
                                                    <div id="picture_div">
                                                        <img id="id-picture" src="{{ asset('public/uploads/white.jpg') }}" alt="">
                                                    </div>

                                                    <div class="upload-btn-wrappers p-2">
                                                        <a class="btns">
                                                            {{--<img src="{{ asset('public/sports/images/login images/camera.svg') }}" alt="" >--}}
                                                            <i class="fas fa-camera"></i>
                                                        </a>
                                                        {!! Form::file('picture', [
                                                            'onchange'=>'readpicURL(this,"picture")','accept'=>'image/*','id'=>'picture'
                                                        ]) !!}
                                                        @if($errors->has('picture'))
                                                            <span class="help-block">
                                                            {{ $errors->first('picture') }}
                                                        </span>
                                                        @endif
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-lg-12 no-pdd">
                                            <div class="row birthday">
                                                <div class="col-lg-12 no-pdd">
                                                    <div class="sn-field">
                                                        <label class="label-">Date of Birth</label>
                                                    </div>
                                                    <div class="col-xl-4 col-lg-12 col-md-12 col-sm-12 col-12 no-pdd day">
                                                        <div class="sn-field">
                                                            <label class="label- day-title">Day</label>
                                                            {!! Form::select('day',config('status.day'),'', [ 'id'=>'day','required'=>'true']) !!}
                                                        </div>
                                                    </div>
                                                    <div class="col-xl-4 col-lg-12 col-md-12 col-sm-12 col-12 no-pdd month">
                                                        <div class="sn-field">
                                                            <label class="label- month-title">Month</label>
                                                            {!! Form::select('month',config('status.month'),'', [ 'id'=>'month','required'=>'true']) !!}
                                                        </div>
                                                    </div>
                                                    <div class="col-xl-4 col-lg-12 col-md-12 col-sm-12 col-12 no-pdd year">
                                                        <div class="sn-field">
                                                            <label class="label- year-title">Year</label>
                                                            {!! Form::select('year',config('status.year'),'', [ 'id'=>'year','required'=>'true']) !!}
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-lg-12 no-pdd pt-2">
                                            <a class="btn register-btn" onclick="validationcheck()">Register Now</a>
                                        </div>
                                        <div class="col-lg-12 no-pdd have-account">
                                            <p class="darkgrey">Already have an account?</p>
                                            <a href="{{route("login")}}">Sign In</a>
                                        </div>
                                    </div>
                                    {!! Form::close() !!}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    {{--@include('sports.script')--}}
    @include('auth.js')
</body>
</html>