<head>
    <meta charset="UTF-8" />
    <title>WHO I FOLLOW</title>
    <meta http-equiv="refresh" content="{{ config('session.lifetime') * 61}}">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="viewport" content="width=device-width, height=device-height, initial-scale=1.0, minimum-scale=1.0">

    <meta name="description" content="" />
    <meta name="keywords" content="" />
    <link rel="shortcut icon" href="{{ asset('public/sports/images') }}/favicon.png" type="image/x-icon">

    <!-- emoji css -->
    <link href="{{ asset('public/newchat/css') }}/emojionearea.min.css" rel="stylesheet" type="text/css">
    <link href="{{ asset('public/newchat/css') }}/image-uploader.min.css" rel="stylesheet" type="text/css">

    <!-- magnific-popup css -->
    <link href="{{ asset('public/newchat/libs/magnific-popup') }}/magnific-popup.css" rel="stylesheet" type="text/css">

    <!-- Select 2 library -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.0/css/select2.min.css"/>

    <!-- owl.carousel css -->
    <link rel="stylesheet" href="{{ asset('public/newchat/libs/owl.carousel/assets') }}/owl.carousel.min.css">

    <link rel="stylesheet" href="{{ asset('public/newchat/libs/owl.carousel/assets') }}/owl.theme.default.min.css">

    <!-- Bootstrap Css -->
    <link href="{{ asset('public/newchat/css') }}/bootstrap-dark.min.css" id="bootstrap-dark-style" rel="stylesheet" type="text/css">
    <link href="{{ asset('public/newchat/css') }}/bootstrap.min.css" id="bootstrap-style" rel="stylesheet" type="text/css">

    <!-- Icons Css -->
    <link href="{{ asset('public/newchat/css') }}/icons.min.css" rel="stylesheet" type="text/css">

    <!-- App Css-->
    <link href="{{ asset('public/newchat/css') }}/app-dark.min.css" id="app-dark-style" rel="stylesheet" type="text/css">
    <link href="{{ asset('public/newchat/css') }}/app.min.css" id="app-style" rel="stylesheet" type="text/css">
    <link rel="stylesheet" href="{{ asset('public/sports/css') }}/images-grid.css">
    {{-- Custom Style Css --}}
    {{--<link rel="stylesheet" type="text/css" href="{{ asset('public/sports/css') }}/style.css">--}}
    {{--<link rel="stylesheet" type="text/css" href="{{ asset('public/sports/css') }}/mystyle.css">--}}
    {{--<link rel="stylesheet" type="text/css" href="{{ asset('public/sports/css') }}/responsive.css">--}}
    <link href="{{ asset('public/newchat/css') }}/customstyle.css" rel="stylesheet" type="text/css">
    <link href="{{ asset('public/newchat/css') }}/commonstyle.css" rel="stylesheet" type="text/css">


    {{-- Fontawesome --}}
    



    <script src="{{ asset('public/newchat/libs/jquery') }}/jquery.min.js"></script>

    <!-- Emoji JS -->
    {{--<script src="https://code.jquery.com/jquery-3.6.0.min.js" integrity="sha256-/xUj+3OJU5yExlq6GSYGSHk7tPXikynS7ogEvDej/m4=" crossorigin="anonymous"></script>--}}
    <script src="{{ asset('public/newchat/js') }}/emojionearea.min.js"></script>

    <script src="{{ asset('public/newchat/libs/bootstrap/js') }}/bootstrap.bundle.min.js"></script>
    <link rel="stylesheet" type="text/css" href="{{ asset('public/sports/css') }}/line-awesome.css">
    <link rel="stylesheet" type="text/css" href="{{ asset('public/sports/css') }}/line-awesome-font-awesome.min.css">
    
    <link rel="stylesheet" type="text/css" href="{{ asset('public/sports/css') }}/font-awesome.min.css">
    <script src="{{ asset('public/newchat/libs/simplebar') }}/simplebar.min.js"></script>
    <script src="{{ asset('public/newchat/libs/nodes-waves') }}/waves.min.js"></script>

    <!-- Select 2 js -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.0/js/select2.min.js"></script>

    <!-- Magnific Popup-->
    <script src="{{ asset('public/newchat/libs/magnific-popup') }}/jquery.magnific-popup.min.js"></script>

    <!-- owl.carousel js -->
    <script src="{{ asset('public/newchat/libs/owl.carousel') }}/owl.carousel.min.js"></script>

    <!-- page init -->
    <script src="{{ asset('public/newchat/js/pages') }}/index.init.js"></script>
    <script src="{{ asset('public/newchat/js') }}/image-uploader.min.js"></script>
    <script src="{{ asset('public/sports/js') }}/images-grid.js"></script>
    {{-- Custom Script --}}
    <script src="{{ asset('public/newchat/js') }}/app.js"></script>

    {{-- Sweet Alert --}}
    <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>

</head>