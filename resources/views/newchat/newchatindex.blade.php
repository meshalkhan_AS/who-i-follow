<!DOCTYPE html>
<html>

@include('newchat.newchathead')

<body class="main-body" >
<style>

    body {
        /* Disables pull-to-refresh but allows overscroll glow effects. */
        overscroll-behavior-y: contain;
    }
    /* Chrome, Safari, Edge, Opera */
    input::-webkit-outer-spin-button,
    input::-webkit-inner-spin-button {
        -webkit-appearance: none;
        margin: 0;
    }

    /* Firefox */
    input[type=number] {
        -moz-appearance: textfield;
    }
    #no-chat-message{
        display: none;
    }


    .topCount
    {
        display: none;
        background-color: rgba(186, 3, 3, 0.84);
        border-radius: 50px;
        height: 17.5px;
        width: 17px;
        z-index: 999;
        font-size: 10px !important;
        text-align: center;
        margin-left: -12px;
        margin-top: -15px;
        color: white !important;
        line-height: 17.5px;
    }

    @media screen and (max-width: 576px) {
        .topCount {
            margin-left: -17.5px !important;
        }
    }

    .info-message
    {
        text-align: center;
        font-size: 13px;
        width: 100%;
    }

    .loader_full {
        position: relative;
        height: 40vh;
        background: url(../public/sports/css/img/Hourglass.gif) center no-repeat;
        background-size: 65px 65px !important;
    }

    .sideloaderChat {
        position: relative;
        height: 40vh;
        background: url(../public/sports/css/img/Hourglass.gif) center no-repeat;
        background-size: 65px 65px !important;
    }

    .sideloaderFriend {
        position: relative;
        height: 40vh;
        background: url(../public/sports/css/img/Hourglass.gif) center no-repeat;
        background-size: 65px 65px !important;
    }

    .chatloader
    {
        position: relative;
        height: 10vh;
        margin-top: -20px;
        background: url(../public/sports/css/img/loader.gif) center no-repeat;
        background-size: 25px 25px !important;

    }


    .sideloaderGroup {
        position: relative;
        height: 40vh;
        background: url(../public/sports/css/img/Hourglass.gif) center no-repeat;
        background-size: 65px 65px !important;
    }

    .sideloaderArchive
    {
        position: relative;
        height: 40vh;
        background: url(../public/sports/css/img/Hourglass.gif) center no-repeat;
        background-size: 65px 65px !important;
    }
    .full_first {
        height: 100vh;
        display: flex;
        justify-content: center;
        align-items: center;
        /*border-left: 5px solid #d4d4d4;*/
        /*border-right: 1px solid #d4d4d4;*/
        text-align: center;
        vertical-align: middle;
        padding-top: 300px;

    }
    .full_loader {
        height: 100vh;
        display: flex;
        justify-content: center;
        align-items: center;
        border-left: 5px solid #d4d4d4;
        border-right: 1px solid #d4d4d4;
        text-align: center;
        vertical-align: middle;
        padding-top: 300px;
        background: url(../public/sports/css/img/Hourglass.gif) center no-repeat;
        background-size: 65px 65px !important;

    }

    .messages-area-left {
        background-color: rgb(237, 237, 237);
        padding: 17px;
        margin-bottom: 10px;
        text-align: center;
        vertical-align: middle;
        color:black;
        font-size: 12px;
    }

    .group_member
    {
        border-radius: 50%;
        width: 20px;
        height: 20px;
        border:2px solid black;
        background-color: white;
        margin-left: -30px;
        margin-right: 20px;
        text-align: center;
        font-size: 10px;
        margin-top: 20px;
        padding:2px;
        color:black;
        font-weight: bold;
        z-index: 9;
    }

    .recieve-message img{
        /*width: 250px !important;*/
        height: auto !important;
        border-radius: 5px !important;
        float: none !important;
        cursor:pointer;
    }
    .cover-photo-zoom {
        cursor: zoom-in;
        transition: 0.3s;
        background: url(img/loader.gif) center no-repeat;
        background-color:black;
        background-size: 45px 45px !important;
    }

    .member-list-custom {
        position: relative;
        display: flex;
        justify-content: space-between;
        align-items: center;
        width: 100%;
    }
    .members-list {
        border: 1px solid #eee;
        padding: 5px 10px;
        overflow-y: scroll;
        height: auto;
    }
    .members-list .media {
        margin-bottom: 10px;
        display: flex;
        justify-content: flex-start;
    }

    .members-list .media img {
        width: 30px;
        border-radius: 100px;
    }

    .member_image
    {
        width: 35px !important;
        height: 35px !important;
        object-fit: cover;
        margin-right: 10px;
    }

    .cover-photo-zoom:hover {opacity: 0.7;}

    .profile-photo-zoom {
        cursor: pointer;
        transition: 0.3s;
    }

    .make_admin
    {
        float: right !important;
        margin-left: 10px;
        color: #7c151f;
    }

    .make_admin_
    {
        float: right !important;
        margin-left: 10px;
        color: #c1c1c1;
    }

    .profile-photo-zomm:hover {opacity: 0.7;}

    #myModal {
        display: none; /* Hidden by default */
        position: fixed; /* Stay in place */
        z-index: 999999; /* Sit on top */
        left: 0;
        top: 0;
        width: 100vw; /* Full width */
        height:100vh; /* Full height */
        overflow: hidden !important;
        background-color: rgb(0,0,0, 0.85); /* Fallback color */
    }

    .img-modal-content {
        width: 100% !important;
        height: 100% !important;
        background-position: center;
        background-repeat: no-repeat;
        background-size: cover;
        object-fit: contain !important;
    }

    .sub-group-field {
        font-size: 15px;
    }

    /* Caption of Modal Image */

    @-webkit-keyframes zoom {
        from {-webkit-transform:scale(0)}
        to {-webkit-transform:scale(1)}
    }

    @keyframes zoom {
        from {transform:scale(0)}
        to {transform:scale(1)}
    }

    /* The Close Button */
    #myModal .close {
        position: absolute;
        top: 10px;
        right: 10px;
        width: 35px;
        height: 35px;
        color: #fff;
        border: none;
        font-weight: lighter;
        font-size: 50px;
        cursor: pointer;
        opacity: 1;
        text-shadow: none;
    }
    #myModal .close:hover,
    #myModal .close:focus {
        color: #fff;
        text-decoration: none;
        cursor: pointer;
    }

    /* 100% Image Width on Smaller Screens */
    @media only screen and (max-width: 700px){
        .img-modal-content {
            width: 100%;
        }
        #myModal .close {
            display: block;
        }
        .user-tab-sec {
            margin-bottom: 5px !important;
        }
    }

    .image-upload input {
        display: none;
    }
    .post-bar-title{
    box-shadow: none !important;
    border-left: 4px solid #e4e4e4;
}

    .via-email {
        font-size: 12px;
        font-weight: 400;
        cursor: pointer;
        color: white !important;
        background-color: #22A852;
        padding:10px 0;
        display: block;
        text-align: center;
        border-radius: 5px;
    }

    .upload-btn-wrappers {
        position: relative;
        overflow: hidden;
        display: inline-block;
    }
    .upload-btn-wrappers{
        margin-top: 5px;
        text-align: center;
        cursor: pointer;
        background-color: #e4e4e4;
        border-radius: 100px;
    }
    .upload-btn-wrappers img{
        width: 15px;
        height: 15px;
    }
    .upload-btn-wrappers input[type=file] {
        font-size: 100px;
        position: absolute;
        left: 0;
        top: 0;
        opacity: 0;
    }

    .switch {
        position: relative;
        display: inline-block;
        width: 45px;
        height: 25px;
    }

    .switch input {
        opacity: 0;
        width: 0;
        height: 0;
    }

    .slider {
        position: absolute;
        cursor: pointer;
        top: 0;
        left: 0;
        right: 0;
        bottom: 0;
        background-color: #ccc;
        -webkit-transition: .4s;
        transition: .4s;
    }

    .slider:before {
        position: absolute;
        content: "";
        height: 18px;
        width: 18px;
        left: 0;
        bottom: 4px;
        background-color: white;
        -webkit-transition: .4s;
        transition: .4s;
    }

    input:checked + .slider {
        background-color: #2196F3;
    }

    input:focus + .slider {
        box-shadow: 0 0 1px #2196F3;
    }

    input:checked + .slider:before {
        -webkit-transform: translateX(25px);
        -ms-transform: translateX(25px);
        transform: translateX(25px);
    }

    /* Rounded sliders */
    .slider.round {
        border-radius: 34px;
    }

    .slider.round:before {
        border-radius: 50%;
    }

    .teamType
    {
        color:red;
        font-weight: bold;
    }

    .swal-footer {
        text-align: center !important;
    }
    @media (max-width:992px){.chat-leftsidebar{min-width:100% !important; max-width:100% !important; left:0 !important;}}
    @media (min-width:1287px){.chat-leftsidebar{min-width:21% !important; max-width:21% !important;}}
    .chat-leftsidebar{
    width: -webkit-fill-available;
    width: -moz-available;
}

    @media screen and (max-width: 1199px) and (min-width: 991.98px) {
        .specialists-post-section {
            min-width: 23.6% !important;
            max-width: 23.6% !important;
        }
    }

    @media screen and (max-width: 991.98px) {
        /*#holder {*/
            /*margin-top: -20px;*/
        /*}*/
    }

</style>

{{--Header--}}

@include('newchat.topheader')
<div id="myModal" class="modal">
    <span class="close">&times;</span>
    <img class="img-modal-content" id="img01">
</div>

<div class="container-fluid p-0 overflow-hidden">
<div class="row layout-wrapper" style="margin-top: 78px; height: calc(100vh - 78px); background-color:#e4e4e4 !important">

    <!-- Start left sidebar-menu -->
    <div class="col-xl-3 col-lg-3 p-0">
    <div class="side-menu me-lg-0 ms-lg-0 position-fixed" style="left:0;" id="tabs_Mobile">
        <!-- Start side-menu nav -->
        <div class="flex-lg-column mb-auto">
            <ul class="nav nav-pills side-menu-nav justify-content-center" role="tablist">

                <li class="nav-item" data-bs-toggle="tooltip" data-bs-placement="top" title="Chats">
                    <a class="nav-link active" id="pills-chat-tab" data-bs-toggle="pill" href="#pills-chat" role="tab" onclick="loadChat()">

                        <i class="fa fa-plus"><span class="topCount chatCount"></span><span style="display: block">Chats</span></i>
                    </a>

                </li>
                <li class="nav-item" data-bs-toggle="tooltip" data-bs-placement="top" title="Teams">
                    <a class="nav-link" id="pills-groups-tab" data-bs-toggle="pill" href="#pills-groups" role="tab" onclick="loadTeams()">
                        {{--<i class="ri-group-line"></i>--}}
                        <i class="fa fa-users"><span class="topCount teamCount"></span><span style="display: block">Teams</span></i>
                    </a>
                </li>

                <li class="nav-item" data-bs-toggle="tooltip" data-bs-placement="top" title="Teams">
                    <a class="nav-link" id="pills-archives-tab" data-bs-toggle="pill" href="#pills-archives" role="tab" onclick="renderChatListArchive1()">
                        {{--<i class="ri-group-line"></i>--}}
                        <i class="fa fa-comment"><span style="display: block">Archives</span></i>
                    </a>
                </li>
                <li class="nav-item" data-bs-toggle="tooltip" data-bs-placement="top" title="Friends">
                    <a class="nav-link" id="pills-contacts-tab" data-bs-toggle="pill" href="#pills-contacts" role="tab" onclick="renderFriendList('null')">
                        {{--<i class="ri-contacts-line"></i>--}}
                        <i class="fa fa-user-plus"><span class="d-block">Friends</span></i>
                    </a>
                </li>
                <li class="nav-item" data-bs-toggle="tooltip" data-bs-placement="top" title="Meeting">
                    <a class="nav-link d-none" id="pills-booking-tab" data-bs-toggle="pill" href="#pills-booking" role="tab" onclick="renderMeetingList('null')">
                        {{--<i class="ri-contacts-line"></i>--}}
                        <i class="fa fa-clock-o"><span class="d-block">Specialists</span></i>
                    </a>
                </li>
            </ul>
        </div>
        <!-- end side-menu nav -->
        <!-- Side menu user -->
    </div>
    <!-- end left sidebar-menu -->

   {{--------------------------------------------------------------------------- Individual Area --------------------------------------------------------------------------------}}

    <!-- start chat-leftsidebar -->
    <div class="chat-leftsidebar me-lg-0 ms-lg-0 position-fixed" style="left:65px; right:0; border-right: 4px solid #e4e4e4; min-width:19.9%; max-width:19.9%;">

        <div class="tab-content">
            <!-- Start chats tab-pane -->
            <div class="tab-pane fade show active" id="pills-chat" role="tabpanel" aria-labelledby="pills-chat-tab">
                <!-- Start chats content -->
                <div>
                    <div class="px-4 pt-4">
                        <div class="user-chat-nav float-end">
                            <div  data-bs-toggle="tooltip" data-bs-placement="bottom" title="Create Chat">
                                <!-- Button trigger modal -->
                                <button type="button" class="btn btn-link text-decoration-none text-muted font-size-14 py-0 new_MessageModal" data-bs-toggle="modal" data-bs-target="#newMessageModal" onClick="clearFriendModel()">
                                    <i class="fa fa-plus"></i>
                                </button>
                            </div>

                        </div>
                        <h4 class="mb-4">Chats</h4>

                        <div class="search-box chat-search-box">
                            <div class="input-group bg-light  input-group-lg rounded-3">
                                <div class="input-group-prepend">
                                    <button class="btn btn-link text-decoration-none text-muted pe-1 ps-3" type="button">
                                        <i class="ri-search-line search-icon font-size-18"></i>
                                    </button>
                                </div>
                                <input type="text" class="form-control bg-light" id="search-chats" placeholder="Search Chats...">
                            </div>
                        </div>
                    </div> <!-- .p-4 -->

                    <!-- Start add group Modal -->
                    <div class="modal fade friend-message-modal" id="newMessageModal" tabindex="-1" role="dialog" aria-labelledby="newMessageModalLabel" aria-hidden="true">
                        <div class="modal-dialog modal-dialog-centered modal-dialog-scrollable">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h5 class="modal-title font-size-16" id="newMessageModalLabel">Create New Message</h5>
                                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close">
                                    </button>
                                </div>
                                <div class="modal-body p-4">
                                    <form>
                                        <div class="mb-4">

                                            <label class="form-label">Select Friend</label>
                                            <div class="mb-3">
                                                {{--<button class="btn btn-light btn-sm" type="button" data-bs-toggle="collapse" data-bs-target="#groupmembercollapse" aria-expanded="false" aria-controls="groupmembercollapse">--}}
                                                    {{--Select Members--}}
                                                {{--</button>--}}
                                                <div class="show-message"></div>
                                                <select class="form-control" style="width: 97%;" id="friends-list">
                                                    <option value="" selected>Select Friend</option>

                                                </select>
                                            </div>
                                        </div>
                                        <div class="mb-3">
                                            <label for="addgroupdescription-input" class="form-label">Message</label>
                                            <textarea class="form-control friend_message" id="new_message_single" rows="3" placeholder="Write message"></textarea>
                                        </div>
                                    </form>
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-danger" data-bs-dismiss="modal">Close</button>
                                    <button type="button" class="btn btn-primary" onclick="sendMessageToFriend()">Send</button>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- End add group Modal -->
                    <!-- Start user status -->
                    {{--<div class="px-4 pb-4" dir="ltr">--}}

                        {{--<div class="owl-carousel owl-theme" id="user-status-carousel">--}}
                        {{--</div>--}}
                        {{--<!-- end user status carousel -->--}}
                    {{--</div>--}}
                    <!-- end user status -->

                    <!-- Start chat-message-list -->
                    <div class="px-2" style="margin-top: 25px;">
                        <h5 class="mb-3 px-3 font-size-16">Recent</h5>

                        <div class="chat-message-list" data-simplebar>
                            <div id="scrollUpChat"> </div>
                            <div class="sideloaderChat" >
                            </div>

                            {{--No List--}}
                            <div class="info-message" id="no-chat-message">
                                No Chats Found.
                            </div>

                            <ul class="list-unstyled chat-list chat-user-list" id="list-tab">
                            </ul>
                        </div>
                    </div>
                    <!-- End chat-message-list -->
                </div>
                <!-- Start chats content -->
            </div>
            <!-- End chats tab-pane -->

        {{--------------------------------------------------------------------------- Groups Area --------------------------------------------------------------------------------}}

            <!-- Start groups tab-pane -->
            <div class="tab-pane" id="pills-groups" role="tabpanel" aria-labelledby="pills-groups-tab">
                <!-- Start Groups content -->
                <div>
                    <div class="p-4">
                        <div class="user-chat-nav float-end">
                            <div  data-bs-toggle="tooltip" data-bs-placement="bottom" title="Create group">
                                <!-- Button trigger modal -->
                                <button type="button" class="btn btn-link text-decoration-none text-muted font-size-14 py-0" data-bs-toggle="modal" data-bs-target="#newGroupModal" onClick="clearGroupModel()">
                                    {{--<i class="ri-group-line me-1 ms-0"></i>--}}
                                    <i class="fa fa-plus"></i>
                                </button>
                            </div>

                        </div>
                        <h4 class="mb-4">Teams</h4>

                        <!-- Start add group Modal -->
                        <div class="modal fade" id="newGroupModal" tabindex="-1" role="dialog" aria-labelledby="newGroupModalLabel" aria-hidden="true">
                            <div class="modal-dialog modal-dialog-centered modal-dialog-scrollable">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <h5 class="modal-title font-size-16" id="newGroupModalLabel">Create New Team</h5>
                                        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close">
                                        </button>
                                    </div>
                                    <div class="modal-body p-4">
                                        <form id="addForm">
                                            <div class="row">
                                                <div class="col-4">
                                                </div>

                                                <div class="col-4" style="text-align:center">

                                                    <img id="create-image" src="{{ asset('public/sports/images') }}/default-group.avatar.png" width="60" height="60">
                                                    <br/>
                                                    <div class="form-group">
                                                        <div class="upload-btn-wrappers sub-group-field p-2">
                                                            <a class="btns">
                                                                Add Avatar
                                                            </a>
                                                            {!! Form::file('picture', [
                                                                'onchange'=>'readCURL(this,)','accept'=>'image/*','id'=>'create_group_img'
                                                            ]) !!}
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="col-4">
                                                </div>
                                            </div>
                                            <div class="show-message"></div>
                                            <div class="mb-4">
                                                <label for="addgroupname-input" class="form-label">Team Name</label>
                                                <input type="text" class="form-control group-name" id="group-name" placeholder="Enter Team Name">
                                            </div>
                                            <div class="mb-4">
                                                <label class="form-label">Team Members</label>
                                                <div class="mb-3">
                                                    <button class="btn btn-light btn-sm" type="button" data-bs-toggle="collapse" data-bs-target="#groupmembercollapse" aria-expanded="false" aria-controls="groupmembercollapse">
                                                        Select Members
                                                    </button>
                                                </div>

                                                <div class="collapse" id="groupmembercollapse">
                                                    <div class="card border">
                                                        <div class="card-header">
                                                            <h5 class="font-size-15 mb-0">Friends</h5>
                                                        </div>
                                                        <div class="card-body p-2">
                                                            <div data-simplebar style="max-height: 150px;" id="friend-select-group-list">
                                                            </div>
                                                        </div>

                                                    </div>
                                                </div>
                                            </div>
                                            <div class="mb-3">
                                                <p style="font-weight: 500;">Select Team Type</p>

                                                <div class="container p-0">
                                                    <div class="row">
                                                        <div class="col-4">
                                                            <input type="radio" id="family" name="team_type" class="team_type" value="Family">
                                                            <label for="html" style="font-weight: normal !important; color: #000 !important ">Family</label>
                                                        </div>
                                                        <div class="col-4">
                                                            <input type="radio" id="sports" name="team_type" class="team_type" value="Sports">
                                                            <label for="css" style="font-weight: normal !important; color: #000 !important; ">Sport</label>
                                                        </div>
                                                        <div class="col-4">
                                                            <input type="radio" id="work" name="team_type" class="team_type" value="Work">
                                                            <label for="javascript" style="font-weight: normal !important; color: #000 !important ">Work</label>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="mb-3">
                                                <label for="addgroupdescription-input" class="form-label">Message</label>
                                                <textarea class="form-control group_message" id="group_message" rows="3" placeholder="Send Message.."></textarea>
                                            </div>
                                        </form>
                                    </div>
                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-danger" data-bs-dismiss="modal">Close</button>
                                        <button type="button" class="btn btn-primary" id="Submit_create" onclick="createGroupOnButton()">Create Team</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- End add group Modal -->


                        <!-- Start update group Modal -->
                        <div class="modal fade" id="editGroupModal" tabindex="-1" role="dialog" aria-labelledby="editGroupModalLabel" aria-hidden="true">
                            <div class="modal-dialog modal-dialog-centered modal-dialog-scrollable">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <h5 class="modal-title font-size-16" id="editGroupModalLabel" style="margin-left: -10px;">Update Team</h5>
                                        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close">
                                        </button>
                                    </div>
                                    <div class="modal-body p-4">
                                        <form id="editForm">
                                            <div class="row">
                                                <div class="col-4">
                                                </div>

                                                <div class="col-4" style="text-align:center">

                                                    <img id="edit-image" src="{{ asset('public/sports/images') }}/default-group.avatar.png" width="60" height="60">
                                                    <br/>
                                                    <div class="form-group">
                                                        <div class="upload-btn-wrappers sub-group-field p-2">
                                                            <a class="btns">
                                                                Add Avatar
                                                            </a>
                                                            {!! Form::file('picture', [
                                                                'onchange'=>'readeditCURL(this,)','accept'=>'image/*','id'=>'edit_group_img'
                                                            ]) !!}
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="col-4">
                                                </div>
                                            </div>
                                            <div class="show-message"></div>
                                            <div class="mb-4">
                                                <label for="addgroupname-input" class="form-label">Team Name</label>
                                                <input type="text" class="form-control group-name" id="group_name_update" placeholder="Enter Team Name">
                                            </div>
                                            <div class="mb-4">
                                                <label for="addgroupname-input" class="form-label">Team Members</label>
                                                <div class="edit-group">
                                                    <div class="members-list"></div>
                                                </div>
                                            </div>
                                            <div class="mb-3">
                                                <p>Select Team Type</p>

                                                <div class="container p-0">
                                                    <div class="row">
                                                        <div class="col-4">
                                                            <input type="radio" id="Family_edit" name="team_type_edit" value="Family">
                                                            <label for="html">Family</label>
                                                        </div>
                                                        <div class="col-4">
                                                            <input type="radio" id="Sports_edit" name="team_type_edit" value="Sports">
                                                            <label for="css">Sport</label>
                                                        </div>
                                                        <div class="col-4">
                                                            <input type="radio" id="Work_edit" name="team_type_edit" value="Work">
                                                            <label for="javascript">Work</label>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                    <div class="modal-footer">
                                        <div class="container">
                                            <div class="row">

                                                <!-- Button trigger New Member modal -->



                                                <div class="col-12 no-pdd pr-1 mb-1">
                                                    <button type="button" style="width: 100%" class="btn btn-primary" onclick="updateGroupName()">Update</button>
                                                </div>

                                                <div class="col-12 no-pdd pr-1 mb-1">
                                                    <button type="button" style="width: 100%" class="btn btn-primary update_group_member" data-bs-toggle="modal" data-bs-target="#addNewMember" data-dismiss="modal" onclick="updateFriends()">Add Friends From My List</button>
                                                </div>

                                                <div class="col-12 no-pdd pr-1 mb-1">
                                                    <button type="button" style="width: 100%" class="btn btn-primary invites_via_group" title="" data-bs-toggle="modal" data-bs-target="#invites_friends" data-dismiss="modal" onclick="via_invites_groups()">Invite Others To The Team</button>
                                                </div>

                                                <div class="col-6 no-pdd pr-1 mb-1 archive_group_update">
                                                    <button type="button" style="width: 100%" class="btn btn-primary" onclick="archiveGroup()">Archive</button>
                                                </div>


                                                <div class="col-6 no-pdd pr-1 mb-1 delete_group_update">
                                                    <button type="button" style="width: 100%" class="btn btn-primary" onclick="deleteGroup()">Delete</button>
                                                </div>

                                                <div class="col-6 no-pdd pr-1 mb-1 unarchive_group_update">
                                                    <button type="button" style="width: 100%" class="btn btn-primary" onclick="unarchiveGroup()">Unarchive</button>
                                                </div>

                                                <div class="col-6 no-pdd pr-1 mb-1 leave_group_update">
                                                    <button type="button" style="width: 100%" class="btn btn-primary" onclick="leaveGroup()">Leave</button>
                                                </div>

                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- End add update Modal -->


                        <!-- Add New Member Modal -->
                        <div class="modal fade" id="addNewMember" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                            <div class="modal-dialog  modal-dialog-centered">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <h5 class="modal-title font-size-16" id="exampleModalLabel">Add New Member</h5>
                                        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close">
                                        </button>
                                    </div>
                                    <div class="modal-body add-member">
                                        <form>
                                            <div class="show-message"></div>
                                            <div class="form-group">
                                                <select class="selectpicker form-control" style="width: 100%" multiple id="add-friend-group" data-live-search="true">
                                                    <option value="" selected>Select Friends</option>
                                                </select>
                                            </div>
                                        </form>
                                    </div>

                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-danger" data-bs-dismiss="modal">Close</button>
                                        <button type="button" class="btn btn-primary" onClick="addMemberToCurrentGroup()">Add Member</button>

                                    </div>
                                </div>
                            </div>
                        </div>

                        <!-- Invites Modal -->
                        <div class="modal fade" id="invites_friends" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                            <div class="modal-dialog  modal-dialog-centered">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <h5 class="modal-title font-size-16" id="exampleModalLabel">Invite Friends</h5>
                                        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close">
                                        </button>
                                    </div>

                                    {!! Form::open(['method' => 'POST', 'id'=>'invites_' ,'route' => ['user.invites'],'data-parsley-validate','files' => 'true','enctype'=>'multipart/form-data']) !!}
                                    <div class="modal-body p-2 text-left">
                                        <div class="post-project-fields pt-0 pb-4 pl-4 pr-4">
                                            <div class="col-12 no-pdd">
                                                <div class="row">
                                                    <div class="col-sm-10 col-9">
                                                        {!! Form::text('urlInvite','', [ 'id'=>'urlInvite' ,'class'=>'form-control','style'=>'width:100% !important; margin-top: 0 !important;', 'readonly'=>'true']) !!}
                                                    </div>
                                                    <div class="col-sm-2 col-3 p-0">
                                                        {{--<span class="tooltiptext" id="myTooltip">Copy to clipboard</span>--}}
                                                        <a class="via-email" style="padding: 10px;width: 70px;" onclick="myFunction()" onmouseout="outFunc()">
                                                            Copy
                                                        </a>
                                                    </div>
                                                </div>
                                            </div>
                                            <br/>
                                            <br/>
                                            <div class="row">
                                                <span id="mobile_error_message_group" style="font-size: 11px;margin-top: 2px;height: 10px">
                                                </span>
                                            </div>

                                            <div class="row no-pdd">
                                                <div class="col-lg-12 no-pdd">
                                                    {!! Form::text('id', '', [ 'id'=>'id_to_invite', 'class'=>'form-control'  , 'required'=>'true','hidden'=>'true']) !!}
                                                    {!! Form::text('level', '', [ 'id'=>'level', 'class'=>'form-control'  , 'required'=>'true','hidden'=>'true']) !!}
                                                    {!! Form::text('c_id', '', [ 'id'=>'c_id', 'class'=>'form-control'  , 'required'=>'true','hidden'=>'true']) !!}
                                                    {!! Form::text('group_id', '', [ 'id'=>'group_id', 'class'=>'form-control'  , 'required'=>'true','hidden'=>'true']) !!}
                                                    {!! Form::text('group_name', '', [ 'id'=>'group_name', 'class'=>'form-control'  , 'required'=>'true','hidden'=>'true']) !!}
                                                    {!! Form::text('group_image', '', [ 'id'=>'group_image', 'class'=>'form-control'  , 'required'=>'true','hidden'=>'true']) !!}
                                                    <div class="sn-field">
                                                        <br/>
                                                        <label class="label-">Phone Number </label>
                                                        <div class="row">
                                                            <div class="col-sm-4">
                                                                {!! Form::select('code', config('status.code'),'', [ 'id'=>'code_group', 'class'=>'form-control' , 'required'=>'']) !!}
                                                            </div>
                                                            <div class="col-sm-8">
                                                                {!! Form::number('mob_', '', [ 'id'=>'mob_group', 'class'=>'form-control' , 'placeholder' => 'Enter phone number' , 'required'=>'']) !!}
                                                                <br/>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <p class="text-center text-dark">OR</p>
                                                    <span id="email_error_message_group" style="font-size: 11px;margin-top: 2px;height: 10px">
                                                    </span>
                                                    <div class="sn-field">
                                                        <br/>
                                                        <label class="label-">Email </label>
                                                        {!! Form::text('email_', '', [ 'id'=>'email_group', 'class'=>'form-control' , 'placeholder' => 'Enter email' , 'required'=>'', ''=>'text-transform:']) !!}
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-link" data-bs-dismiss="modal">Close</button>
                                        <button type="button" class="btn btn-primary" id="invite_button_group" onclick="sendinviteGroup()">SEND INVITE</button>

                                    </div>
                                    {!! Form::close() !!}
                                </div>
                            </div>
                        </div>

                        <div class="search-box chat-search-box">
                            <div class="input-group bg-light  input-group-lg rounded-3">
                                <div class="input-group-prepend">
                                    <button class="btn btn-link text-decoration-none text-muted pe-1 ps-3" type="button">
                                        <i class="ri-search-line search-icon font-size-18"></i>
                                    </button>
                                </div>
                                <input type="text" class="form-control bg-light" id="search-groups" placeholder="Search Teams...">
                            </div>
                        </div>
                    </div>

                    <!-- Start user status -->
                    {{--<div class="px-4 pb-4" dir="ltr">--}}

                        {{--<div class="owl-carousel owl-theme" id="user-status-carousel">--}}
                        {{--</div>--}}
                        {{--<!-- end user status carousel -->--}}
                    {{--</div>--}}
                    <!-- end user status -->

                    <!-- Start chat-message-list -->
                    <div class="px-2">
                        <h5 class="mb-3 px-3 font-size-16">Recent</h5>

                        <div class="chat-message-list" data-simplebar>
                            <div class="sideloaderGroup" >
                            </div>

                            {{--No List--}}
                            <div class="info-message" id="no-group-message">
                                No Teams Found.
                            </div>

                            <ul class="list-unstyled chat-list chat-user-list" id="list-tab-group">
                            </ul>
                        </div>
                    </div>
                    <!-- End chat-message-list -->

                    {{--<!-- Start chat-group-list -->--}}
                    {{--<h5 class="mb-3 px-3 font-size-16">Recent</h5>--}}
                    {{--<div class="p-4 chat-message-list chat-group-list" data-simplebar>--}}
                        {{--<ul class="list-unstyled chat-list" id="list-tab-group">--}}

                        {{--</ul>--}}
                    {{--</div>--}}
                    {{--<!-- End chat-group-list -->--}}
                </div>
                <!-- End Teams content -->
            </div>
            <!-- End groups tab-pane -->

        {{--------------------------------------------------------------------------- archives Area --------------------------------------------------------------------------------}}

        <!-- Start groups tab-pane -->
            <div class="tab-pane" id="pills-archives" role="tabpanel" aria-labelledby="pills-archives-tab">
                <!-- Start Groups content -->
                <div>
                    <div class="p-4">
                        <div class="user-chat-nav float-end">

                        </div>
                        <h4 class="mb-4">Archives</h4>
                        <div class="search-box chat-search-box">
                            <div class="input-group bg-light  input-group-lg rounded-3">
                                <div class="input-group-prepend">
                                    <button class="btn btn-link text-decoration-none text-muted pe-1 ps-3" type="button">
                                        <i class="ri-search-line search-icon font-size-18"></i>
                                    </button>
                                </div>
                                <input type="text" class="form-control bg-light" id="search-archives" placeholder="Search Archives...">
                            </div>
                        </div>

                    </div>

                    <!-- Start user status -->
                    {{--<div class="px-4 pb-4" dir="ltr">--}}

                        {{--<div class="owl-carousel owl-theme" id="user-status-carousel">--}}
                        {{--</div>--}}
                        {{--<!-- end user status carousel -->--}}
                    {{--</div>--}}
                    <!-- end user status -->

                    <!-- Start chat-message-list -->
                    <div class="px-2">
                        <h5 class="mb-3 px-3 font-size-16">Recent</h5>

                        <div class="chat-message-list" data-simplebar>
                            <div class="sideloaderArchive" >
                            </div>

                            {{--No List--}}
                            <div class="info-message" id="no-archive-message">
                                No Archives Found.
                            </div>

                            <ul class="list-unstyled chat-list chat-user-list" id="list-tab-archive">
                            </ul>
                        </div>
                    </div>
                    <!-- End chat-message-list -->
                </div>
                <!-- End Teams content -->
            </div>
            <!-- End groups tab-pane -->

           {{--------------------------------------------------------------------------- Friends Area --------------------------------------------------------------------------------}}

            <!-- Start contacts tab-pane -->
            <div class="tab-pane" id="pills-contacts" role="tabpanel" aria-labelledby="pills-contacts-tab">
                <!-- Start Contact content -->
                <div>
                    <div class="p-4">

                        <h4 class="mb-4">Friends</h4>
                        <div class="search-box chat-search-box">
                            <div class="input-group bg-light  input-group-lg rounded-3">
                                <div class="input-group-prepend">
                                    <button class="btn btn-link text-decoration-none text-muted pe-1 ps-3" type="button">
                                        <i class="ri-search-line search-icon font-size-18"></i>
                                    </button>
                                </div>
                                <input type="text" class="form-control bg-light" id="search-friends" placeholder="Search Friends...">
                            </div>
                        </div>
                        <!-- End search-box -->
                    </div>
                    <!-- end p-4 -->

                    <!-- Start user status -->
                    {{--<div class="px-4 pb-4" dir="ltr">--}}

                        {{--<div class="owl-carousel owl-theme" id="user-status-carousel">--}}
                        {{--</div>--}}
                        {{--<!-- end user status carousel -->--}}
                    {{--</div>--}}
                    <!-- end user status -->

                    <!-- Start chat-message-list -->
                    <div class="px-2" style="margin-top: 10px;">
                        <div class="chat-message-list" data-simplebar>
                            <div class="sideloaderFriend" >
                            </div>

                            {{--No List--}}
                            <div class="info-message" id="no-friend-message" style="margin-top: 20px;">
                                No Friends Found.
                            </div>

                            <ul class="list-unstyled chat-list chat-user-list" id="list-tab-friend">
                            </ul>
                        </div>
                    </div>
                    <!-- end contact lists -->
                </div>
                <!-- Start Contact content -->
            </div>
            <!-- End contacts tab-pane -->

                {{--------------------------------------------------------------------------- Meetings Area --------------------------------------------------------------------------------}}

            <!-- Start meetings tab-pane -->
            <div class="tab-pane" data-simplebar style="overflow-y: scroll; height:850px;" id="pills-booking" role="tabpanel" aria-labelledby="pills-contacts-tab">
                <!-- Start meetings content -->
                <div>
                    <div class="p-4 pt-0 simplebar-wrapper" id="expmob-data">
                        @include('newchat.expmobile')
                    </div>
                </div>
                <!-- End meetings content -->
            </div>
            <div class="ajax-load text-center d-none">
                <div class="process-comm">
                    <div class="spinner">
                    <div class="bounce1"></div>
                    <div class="bounce2"></div>
                    <div class="bounce3"></div>
                    </div>
                    <br/>
                    <br/>
                </div>
            </div>
            <!-- End meetings tab-pane -->
        </div>
        <!-- end tab content -->

    </div>
</div>
    <!-- end chat-leftsidebar -->

  {{--------------------------------------------------------------------------- Chat Area --------------------------------------------------------------------------------}}

    <!-- Start User chat -->
<div class="col-xl-6 col-lg-6 col-md-12 col-sm-12 col-12 p-0">

    <div class="user-chat w-100 overflow-hidden full_first" style="display: block">
            Pop open a chat to start the conversation.
    </div>

    <div class="user-chat w-100 overflow-hidden full_loader" style="display: none">
    </div>

    <div class="user-chat overflow-hidden full-chat" style="display: none;">
        <div class="d-lg-flex" >

            <!-- start chat conversation section -->
            <div class="w-100 overflow-hidden position-relative">
                <div class="p-2 p-lg-2 border-bottom user-chat-topbar">
                    <div class="row align-items-center">
                        <div class="col-6 col-lg-8">
                            <div class="d-flex align-items-center">
                                <div class="d-block d-lg-none me-2 ms-0">
                                    <a href="javascript: void(0);" class="user-chat-remove text-muted font-size-16 p-2" onclick="removeCurrentGroup()"><i class="ri-arrow-left-s-line"></i></a>
                                </div>
                                <div class="me-3 ms-0">
                                    <span id="default-img"><img src="{{ asset('public/uploads/white.jpg') }}" class="rounded-circle avatar-xs" alt=""></span>
                                </div>
                                <div class="flex-grow-1 overflow-hidden">
                                    <h5 class="font-size-16 mb-0 text-truncate" id="chat-title">
                                    </h5>
                                </div>
                            </div>
                        </div>
                        <div class="col-6 col-lg-4">
                            <div id="smsGroupM">
                                <div class="d-flex align-items-center" style="position: absolute; right:0; margin-right: 10px;">
                                    <div class="d-block d-lg-none me-2 ms-0">
                                        <a href="" class="user-chat-remove text-muted font-size-16 p-2"></a>
                                    </div>
                                    <div class="me-3 ms-0">
                                        <label class="switch">
                                            <input type="checkbox" onchange="updateSMSNotification()" id="notifyCheck">
                                            <span class="slider round"></span>
                                        </label>
                                    </div>
                                    <div class="flex-grow-1 overflow-hidden" style="margin-top: -6px;">
                                        <h5 class="font-size-13 mb-0 text-truncate">
                                            SMS Notification
                                        </h5>
                                    </div>
                                </div>

                            </div>
                            <ul class="list-inline user-chat-nav text-end mb-0" id="editGroupM">
                                <li class="list-inline-item">
                                    <div class="dropdown">
                                        <button class="btn nav-btn dropdown-toggle" style="font-size: 13px;margin-right: 31px;font-weight: bold;margin-bottom:10px;margin-top:-10px;" data-bs-toggle="modal" data-bs-target="#editGroupModal" onclick="editGroup()">
                                           <span class="d-block" style="position: absolute; left: 0; font-family: 'Open Sans', sans-serif;">Update <i class="fa fa-users"></i></span>
                                        </button>
                                    </div>
                                </li>
                            </ul>

                            <ul class="list-inline user-chat-nav text-end mb-0" id="unarchiveGroupM">
                                <li class="list-inline-item">
                                    <div class="dropdown">
                                        <button class="btn nav-btn dropdown-toggle" style="font-size: 12px;margin-right: 65px;font-weight: bold" onclick="unarchiveGroup()">
                                            Unarchive Team
                                        </button>
                                    </div>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
                <!-- end chat user head -->

                <!-- start chat conversation -->
               <div id="holder">

                        <div class="chat-conversation p-3 p-lg-3" data-simplebar="init">

                            <div class="chatloader" >
                            </div>
                            <ul class="list-unstyled mb-0" id="chat-msg-area">

                            </ul>
                        </div>
                        <!-- end chat conversation end -->
                </div>
            <!-- end chat conversation section -->

            </div>
        </div>
        <!-- End User chat -->
        <!-- end modal -->
    </div>

    <!-- start chat input section -->
    <div class="chat-input-section p-2 p-lg-2 mb-0" id="new_document_attachment">
        <div class="row g-0">

            <div class="col-7 col-sm-9 col-md-9 col-lg-9 col-xl-9">
                <span class="mt-1 textarea form-control form-control-lg bg-light border-light" role="textbox" contenteditable id="message-field"></span>
            </div>
            <div class="col-5 col-sm-3 col-md-3 col-lg-3 col-xl-3 send-file-icons">
                <div class="chat-input-links ms-md-2 me-md-0">
                    <ul class="list-inline mb-0">
                        <li class="list-inline-item" data-bs-toggle="tooltip" data-bs-placement="top" title="Attached File">
                            <!-- File Upload -->
                            <div class="image-upload">
                                <label for="file-input">
                                    <div class="btn btn-link text-decoration-none font-size-16 btn-lg waves-effect">
                                        <i class="ri-attachment-line"></i>
                                    </div>
                                </label>
                                <input name="file" id="file-input" ref="fileInput" type="file" onchange="sendAttachment()"  accept="image/*,.xls,.xlsx,.doc,.docx,.zip,.pdf"/><span  id="fileName"></span>
                            </div>
                        </li>
                        <li class="list-inline-item">
                            <button type="submit" class="btn btn-primary font-size-16 btn-sm chat-send waves-effect waves-light" onclick="sendMessageToChat()">
                                <i class="ri-send-plane-2-fill" ></i>
                            </button>
                        </li>
                    </ul>
                </div>

            </div>
        </div>
    </div>
    <!-- end chat input section -->
</div>
    <!-- Start contacts tab-pane -->
    <div class="tab-pane col-xl-3 col-lg-3 d-none d-lg-block p-0 specialists-post-section" data-simplebar   style="overflow-y: auto; height: calc(100vh - 73px); min-width: 23.9%; max-width: 23.9%;  " id="pills-booking" role="tabpanel" aria-labelledby="pills-contacts-tab">
        <!-- Start Contact content -->
        <div >
            <div class="p-0" onscroll="checkscroll()" id="exp-data">
                <div class="post-bar mb-3 post-bar-title rounded-0" style="display: inline-flex !important">
                    <h6 class="col-lg-6 col-md-6 col-sm-6 col-6 ml-0" style="font-weight:bolder !important; color:black !important;" title="" >Specialists</h6>
                    <a class="col-lg-6 col-md-6 col-sm-6 col-6 mt-1" href="{{ url('/post/all')}}" style="font-size: 12px !important; text-align: end; font-family: 'Open Sans', sans-serif !important ; color: #385499 !important ; ">View More</a>
                </div>
                @if(count($exp_posts)>0)
                @include('newchat.teamexp')
                @else
                <div class="main-ws-sec">
                    <div class="posts-section">
                        <div class="post-bar rounded-0 post-bar-title">
                            <div class="post_topbar">
                            </div>
                            <div class="epi-sec">
                                <div class="job_descp">
                                    <p>No Post Yet.</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            @endif
            </div>
        </div>
        <!-- Start Contact content -->
    </div>
    <div class="ajax-load text-center d-none">
        <div class="process-comm">
            <div class="spinner">
            <div class="bounce1"></div>
            <div class="bounce2"></div>
            <div class="bounce3"></div>
            </div>
            <br/>
            <br/>
        </div>
    </div>
    <!-- End contacts tab-pane -->

</div>
</div>
<!-- end layout wrapper -->



    <!-- Invite Friends Modal -->
    @include('newchat.modals.invites-friends')
    @include('newchat.js.script')
    @include('sports.modal.editpostteam')
    @include('sports.js.post')
    
    <!-- Script File -->
    @include('newchat.newchatscript')
    @include('newchat.js.invites')
    @include('sports.js.images')

<script>
    const PasteArea= document.getElementById('holder');
    PasteArea.addEventListener('paste', e => {
    var filescopied = e.clipboardData.files;
    document.getElementById('file-input').files =filescopied;
    sendAttachment();
});
    function readfiles(files) {
  for (var i = 0; i < files.length; i++) {
    reader = new FileReader();
    reader.onload = function(event) {
    }
    reader.readAsDataURL(files[i]);
  }
  document.getElementById('file-input').files = files;
}
var holder = document.getElementById('holder');
holder.ondragover = function () { this.className = 'hover'; return false; };
holder.ondragend = function () { this.className = ''; return false; };
holder.ondrop = function (e) {
  this.className = '';
  e.preventDefault();
  readfiles(e.dataTransfer.files);
  sendAttachment();
}




    {{--Copy Invite URL--}}
    function myFunction() {
        var copyText = document.getElementById("urlInvite");
        copyText.select();
        copyText.setSelectionRange(0, 99999);
        navigator.clipboard.writeText(copyText.value);

        var tooltip = document.getElementById("myTooltip");
    }

    function outFunc() {
        var tooltip = document.getElementById("myTooltip");
    }
</script>
<script>
     $('.simplebar-content-wrapper').on('scroll', function() {
    let div = $(this).get(0);
    if(div.scrollTop + div.clientHeight >= div.scrollHeight) {
        if(success==1)
                    page++;
                if(load==0)
                    loadMoreData(page);
    }
});
      
    
        function loadMoreData(page){
            $.ajax(
                {
                    url: '?page=' + page,
                    type: "get",
                    beforeSend: function()
                    {$('.ajax-load').show();}
                })
                .done(function(data)
                {
                    if(data.count == 0)
                    {
                        load=1;
                        if(page!=2)
                            $('.ajax-load').html("No more records found..<br/><br/><br/><br/><br/><br/>");
                        else
                            $('.ajax-load').hide();
    
    
                        return;
                    }
                    $('.ajax-load').hide();
                    $("#exp-data").append(data.html);
                    $("#expmob-data").append(data.html);
                    success=1;
                })
                .fail(function(jqXHR, ajaxOptions, thrownError)
                { success=0;});
        }
    </script>
<script>
    $('#newGroupModal').appendTo("body");
    $('#newMessageModal').appendTo("body");
    $('#editGroupModal').appendTo("body");
    $('#addNewMember').appendTo("body");
    $('#invites_friends').appendTo("body");
</script>
</body>
</html>