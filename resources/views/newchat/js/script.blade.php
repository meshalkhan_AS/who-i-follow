<?php
$helper = new \App\Helpers\GeneralHelper();
$ads = \App\Helpers\GeneralHelper::getRandomAdBanner(Auth::user()->id, 'Messages');
$userprofile = \App\Models\Admin\UserProfile::where('user_id',Auth::user()->id)->first();
?>

<!-- Firebase -->

<script src="https://www.gstatic.com/firebasejs/8.8.0/firebase-app.js"></script>
<script src="https://www.gstatic.com/firebasejs/8.8.0/firebase-firestore.js"></script>
<script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/js/select2.min.js" defer></script>
<script src="https://cdn.jsdelivr.net/npm/moment@2.29.1/moment.min.js"></script>
<script type="text/javascript" src="{{ asset('public/sports/js') }}/jquery.timeago.js"></script>
<script src="https://cdn.jsdelivr.net/npm/lodash@4.17.10/lodash.min.js"></script>

<!-- Begin emoji-picker JavaScript -->
<script src="https://onesignal.github.io/emoji-picker/lib/js/config.js"></script>
<script src="https://onesignal.github.io/emoji-picker/lib/js/util.js"></script>
<script src="https://onesignal.github.io/emoji-picker/lib/js/jquery.emojiarea.js"></script>
<script src="https://onesignal.github.io/emoji-picker/lib/js/emoji-picker.js"></script>
<!-- End emoji-picker JavaScript -->

{{--Token imported scripts--}}

<script src="https://www.gstatic.com/firebasejs/8.8.0/firebase-auth.js"></script>
<script src="https://www.gstatic.com/firebasejs/8.8.0/firebase-database.js"></script>
<script src="https://www.gstatic.com/firebasejs/8.8.0/firebase-messaging.js"></script>
<script src="https://www.gstatic.com/firebasejs/8.8.0/firebase-functions.js"></script>

<script>

    const IMGURL = "{{ asset('public/sports/images') }}";
    const BASE_URL = "{{ url('/') }}";
    const currentURL= window.location.href;
    const currentURLFind= BASE_URL+'/chat/user';
    const currentURLFind2= BASE_URL+'/chat/user#';
    searchList=1;
    countMessages='new';
    imageLoaderSent=0;
    lastTimeStamp=new Date();
    lastMessageTimeStamp=new Date();
    limitChatList=8;
    chatScrollFine=0;
    chatUpNotAllowed=1;
    messagesDateArray=[];
    archive='1';
    chathtml='';
//    messagesChecked();

    if (window.matchMedia("(max-width: 991.98px)").matches) {
        $("#new_document_attachment").css('display','none');
    }


    //---- Make SMS Notificaton Null --------

    function messagesChecked(groupID)
    {
        $.ajax({
            type: 'POST',
            url: '{{ route('user.sms.messagesChecked') }}',
            data: {
                _token: '{{ csrf_token() }}',
                group_id:groupID
            },
            success: function (response) {

            }
        });
    }

    const SPONSERED = JSON.parse('{!! str_replace("'", "\'", json_encode($ads)) !!}');

    // Your web app's Firebase configuration
    var firebaseConfig = {
        apiKey: "{{env('apiKey')}}",
        authDomain: "{{env('authDomain')}}",
        projectId: "{{env('projectId')}}",
        storageBucket: "{{env('storageBucket')}}",
        messagingSenderId: "{{env('messagingSenderId')}}",
        appId: "{{env('appId')}}"
    };
    // Initialize Firebase
    firebase.initializeApp(firebaseConfig);


//    ----------- Token Save ------------------------------------------------------------------

    //firebase.analytics();
    const messaging = firebase.messaging();
    messaging
        .requestPermission()
        .then(function () {
// get the token in the form of promise
            return messaging.getToken()
        })
        .then(function(token) {
            // print the token on the HTML page
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

            $.ajax({
                url: '{{ url("save-token/user") }}',
                type: 'POST',
                data: {
                    _token: '{{ csrf_token() }}',
                    token: token
                },
                dataType: 'JSON',
                success: function (response) {
                },
                error: function (err) {
                },
            });



        })
        .catch(function (err) {
        });

    messaging.onMessage(function(payload) {
        var notification;
        notification = new Notification(payload.notification.title,{
            body: payload.notification.body,
            icon: payload.notification.icon,
            tag: "Dummy"
        });
    });

    //firebase.initializeApp(config);
    var database = firebase.database().ref().child("/users/");

    database.on('value', function(snapshot) {
        renderUI(snapshot.val());
    });

    // On child added to db
    database.on('child_added', function(data) {
        if(Notification.permission!=='default'){
            var notification;

            notification= new Notification('CodeWife - '+data.val().username,{
                'body': data.val().message,
                'icon': 'bell.png',
                'tag': data.getKey()
            });
            notification.onclick = function(){
                alert(this.tag);
            }
        }else{
            alert('Please allow the notification first');
        }
    });

    self.addEventListener('notificationclick', function(event) {
        event.notification.close();
    });

//    ----------- End Token Save --------------------------------------------------------------


    var user = {
        id: parseInt('{{auth()->user()->id}}'),
        name: '{{auth()->user()->name}}',
        admin: 1,
        status:1,
        leave:0,
        notify:'on',
        read:0
    };
    const db = firebase.firestore();
    var groups = [],groupsNotification=[], chatsNotification=[], currentGroup = undefined, messages = [], friends = []; allUsers=[];

    const CURRENT_USER_PIC = "{{ $userprofile['picture']=='yes'? url('/').'/public/storage/users/'.$userprofile['user_id'].'/profile_picture.jpg?v='.time() : asset('public/uploads/white.jpg') }}";

    disableChatArea();
    chatList='Chat';
    load='1';
    archive='1';

    //  fetch friends
    getFriendList();
    getUserList();
    notificationTeam();
    notificationChat();

    function getUserList()
    {
        $.ajax({
            method: 'GET',
            url: BASE_URL+'/users/list',
            data: {
                _token: '{{ csrf_token() }}'
            },
        }).done(function (result){
            allUsers = result;
        });
    }

    function removeCurrentGroup() {
        currentGroup=undefined;
        document.getElementById("new_document_attachment").style.zIndex = "1";
        if (window.matchMedia("(max-width: 991.98px)").matches) {
            $("#new_document_attachment").css('display','none');
        }
    }

    function notificationTeam()
    {

        var groupIDOnSession= sessionStorage.getItem("groupID");
        // If Friend Click
        if(groupIDOnSession)
        {
            messagesChecked(groupIDOnSession);
            fetchGroupById(groupIDOnSession).then(function(groupExists) {
                groupFound = groupExists;
                members = groupFound.members;

                //             update Read Counter
                _.forEach(members, function (member) {
                    if (member.id == user.id) {
                        members['' + member.id + ''].read = 0;
                    }
                });
                groupFound.members = members;
                updateGroup(groupFound);
            });
        }
        var uid=user.id;
        return new Promise((resolve, reject) => {
            const groupRef = db.collection('group')
            groupRef.where('members.'+uid+'.id', '==', parseInt(uid)).where('type', '==', 'group').where('members.'+uid+'.status', '==', 1).where('members.'+uid+'.leave', '==', 0).where('status', '==', 1).onSnapshot((querySnapshot) => {
            const allGroups = []
            querySnapshot.forEach((doc) => {
            const data = doc.data()
            data.id = doc.id;
        allGroups.push(data);
    })
        groupsNotification = allGroups;

        //  Count Notification
        const groupsTemp = _.orderBy(groupsNotification, ['lastMessageAt'], ['desc']);
        var countNotification = 0;

        for (let index in groupsTemp) {
            var groupObj = groupsTemp[index];

            if(currentGroup!=undefined)
            {
                //  Update Counter;
                if(groupObj.id==currentGroup.id)
                {
                    messagesChecked(currentGroup.id);
                    fetchGroupById(currentGroup.id).then(function(groupExists) {
                        groupFound = groupExists;
                        members = groupFound.members;

                        //             update Read Counter
                        _.forEach(members, function (member) {
                            if (member.id == user.id) {
                                members['' + member.id + ''].read = 0;
                            }
                        });
                        groupFound.members = members;
                        currentGroup = groupFound;
                        updateGroup(groupFound);
                    });
                }
                else
                {
                    if (groupObj.members[user
                            .id].read > 0) {
                        countNotification = countNotification + 1;
                    }
                }
            }
            else
            {
                if (groupObj.members[user
                        .id].read > 0) {
                    countNotification = countNotification + 1;
                }
            }
        }

        if(countNotification>0)
        {
            $(".teamCount").css('display','block');
            $(".teamCount").html('');
            $(".teamCount").html(countNotification);
        }
        else
        {
            $(".teamCount").css('display','none');
        }
    })
    })
    }

    function notificationChat()
    {
            var uid=user.id;
            return new Promise((resolve, reject) => {
                const groupRef = db.collection('group')
                groupRef.where('members.'+uid+'.id', '==', parseInt(uid)).where('type', '==', 'individual').where('members.'+uid+'.status', '==', 1).where('members.'+uid+'.leave', '==', 0).where('status', '==', 1).onSnapshot((querySnapshot) => {
                const allGroups = []
                querySnapshot.forEach((doc) => {
                const data = doc.data();
            data.id = doc.id;
            allGroups.push(data);
        })
            chatsNotification = allGroups;
            //  Count Notification
            const groupsTemp = _.orderBy(chatsNotification, ['lastMessageAt'], ['desc']);
            var countNotification = 0;

            for (let index in groupsTemp) {
                var groupObj = groupsTemp[index];

                if (currentGroup != undefined) {
                    //  Update Counter;
                    if (groupObj.id == currentGroup.id) {
                        messagesChecked(currentGroup.id);
                        fetchGroupById(currentGroup.id).then(function (groupExists) {
                            groupFound = groupExists;
                            members = groupFound.members;

                            //             update Read Counter
                            _.forEach(members, function (member) {
                                if (member.id == user.id) {
                                    members['' + member.id + ''].read = 0;
                                }
                            });
                            groupFound.members = members;
                            currentGroup = groupFound;
                            updateGroup(groupFound);
                        });
                    }
                    else
                    {
                        if (groupObj.members[user
                                .id].read > 0) {
                            countNotification = countNotification + 1;
                        }
                    }
                }
                else
                {
                    if (groupObj.members[user
                            .id].read > 0) {
                        countNotification = countNotification + 1;
                    }
                }
            }

            if(countNotification>0)
            {
                $(".chatCount").css('display','block');
                $(".chatCount").html('');
                $(".chatCount").html(countNotification);
            }
            else
            {
                $(".chatCount").css('display','none');
            }
        })
        })
    }


    function fetchGroupByUserID(uid) {
        return new Promise((resolve, reject) => {
            const groupRef = db.collection('group')
            groupRef.where('members.'+uid+'.id', '==', parseInt(uid)).where('type', '==', 'group').where('members.'+uid+'.status', '==', 1).where('members.'+uid+'.leave', '==', 0).where('status', '==', 1).onSnapshot((querySnapshot) => {
            const allGroups = []
            querySnapshot.forEach((doc) => {
            const data = doc.data()
            data.id = doc.id;
            allGroups.push(data);
            })
            groups = allGroups;
            renderGroupListIndividual1();
    })
    })
    }

    function fetchIndividualByUserID(uid) {
        return new Promise((resolve, reject) => {
            const groupRef = db.collection('group')
            groupRef.where('members.'+uid+'.id', '==', parseInt(uid)).where('type', '==', 'individual').where('members.'+uid+'.status', '==', 1).where('members.'+uid+'.leave', '==', 0).where('status', '==', 1).onSnapshot((querySnapshot) => {
            const allGroups = []
            querySnapshot.forEach((doc) => {
            const data = doc.data();
            data.id = doc.id;
            allGroups.push(data);
            })
        groups = allGroups;
        if(currentGroup!=undefined)
        {
            fetchGroupById(currentGroup.id).then(function(groupExists) {
                currentGroup = groupExists;
            });
        }
        renderChatListIndividual1();
    })
    })
    }


    //    ======== tabs ==================================================================

    if(sessionStorage.getItem('loadChat')=='Groups')
    {
        loadTeams();
    }
    else if(sessionStorage.getItem('loadChat')=='Archive')
    {
        loadArchives();
    }
    else
    {
        loadChat();
    }
    if(sessionStorage.getItem('PreTab')=='1')
    {
        loadspecialist();
    }


    function fetchGroupById(groupID)
    {
        return new Promise((resolve, reject) => {
            db.collection('group')
            .doc(groupID)
            .get()
            .then(snapshot => {
                const document = snapshot.data();
                return resolve(document)
            })
        });
        // retrieve a document

    }

    function getOtherUser(members) {
        const otherMember = _.filter(members, function(member) {
            return user.id !== member.id ? true : false;
        });
        return otherMember.length > 0 ? otherMember[0] : [];
    }

    function fetchGroupByMembers(userId, getOtherUserId) {
        return new Promise((resolve, reject) => {
            const groupRef = db.collection('group')
            groupRef.where('members.'+userId+'.id', '==', parseInt(userId)).where('members.'+getOtherUserId+'.id', '==', parseInt(getOtherUserId)).where('type', '==', 'individual').get().then(function(result) {
                if (!result.empty) {
                    var groupObject = result.docs[0].data();
                    groupObject['id'] = result.docs[0].id;
                    return resolve(groupObject);
                } else {
                    return resolve(null);
                }
            },
        );
    });
    }

    function createGroup(userArray, createdBy, name, type, path,team_type)
    {
        let now = new Date();

        var dateString = moment(now).format('YYYY-MM-DD HH:mm:ss');
        const groupObj = {
            createdAt: new Date(),
            lastMessageAt: dateString,
            lastMessage: 'no',
            createdBy,
            members: userArray,
            name,
            type,
            status:1,
            attachment: typeof path !== 'undefined' ? path : false,
            team_type:typeof team_type !== 'undefined' ? team_type : false
        };
        return new Promise((resolve, reject) => {
            db.collection('group')
            .add(groupObj)
            .then(function (docRef) {
                groupObj.id = docRef.id;
                // fetchGroupByUserID(user.id)
                resolve(groupObj)
            })
            .catch(function (error) {
                reject(error)
            })
    })
    }

    //    =================================================== Archieve Chat =====================================================

    function fetchArchivedGroupByUserID(uid)
    {
        groupsA = [];
        return new Promise((resolve, reject) => {
            const groupRef = db.collection('group')
            groupRef.where('members.'+uid+'.id', '==', parseInt(uid)).where('members.'+uid+'.status', '==', 2).where('status', '==', 1).onSnapshot((querySnapshot) => {
            const allGroups = []
            querySnapshot.forEach((doc) => {
            const data = doc.data()
            data.id = doc.id;
            allGroups.push(data);
            })
        groupsA = allGroups;
        disableChatArea();
        renderChatListArchive();
            })
        })
    }
    function renderChatListArchive1() {
        $('#list-tab-archive').html('');
        sessionStorage.setItem("PreTab",'5');
        chatList="Archive";
        fetchArchivedGroupByUserID(user.id);
    }
    function renderChatListArchive()
    {
        chatList="Archive";
        $('#no-archive-message').css('display', 'none');
        enableArchiveLoaderSideArea();
        if (groupsA.length <= 0) {
            $('#no-archive-message').css('display', 'block');
            disableChatArea();
        }

        //sort list by time
        const groupsTemp = _.orderBy(groupsA, ['lastMessageAt'], ['desc']);
        var chatHtml = '';
        var i = 0;
        checkCurrent=0;
        var countNotification = 0;
        var checkFirst = 0;
        for (let index in groupsTemp)
        {
            var groupObj = groupsTemp[index];

            if(currentGroup!=undefined) {
                if (groupObj.id != currentGroup.id) {
                    if (groupObj.members[user
                            .id].read > 0) {
                        countNotification = countNotification + 1;
                    }
                }
            }
            else
            {
                if (groupObj.members[user
                        .id].read > 0) {
                    countNotification = countNotification + 1;
                }
            }
            var convertDate = moment(groupObj.lastMessageAt, 'YYYY-MM-DD[T]HH:mm:ss').format("YYYY-MM-DD[T]HH:mm:ss");
            var chatTime = moment(convertDate).fromNow();
            if (groupObj.type != 'group')
            {
                var otherChatMember = getOtherUser(groupObj.members);
                const friendObj = _.filter(allUsers, friend => friend.user_id == otherChatMember.id);
                if(friendObj.length>0)
                {
                    if(checkFirst==0)
                    {
                        checkFirst=groupObj.id;
                    }
                    var userName=(friendObj.length>0)?friendObj[0].fname+' '+friendObj[0].lname:otherChatMember.name;
                    const userProfilePictureOriginal = BASE_URL+'/public/storage/users/'+otherChatMember.id+'/profile_picture.jpg?v={{time()}}';
                    const userProfilePicture = BASE_URL+'/public/uploads/white.jpg';

                    const groupImg = groupObj.attachment ? BASE_URL+groupObj.attachment : IMGURL+'/default-group.avatar.png';
                    chatHtml += '<li class="list- unread" id="div-'+groupObj.id+'" onClick="renderChatNotification(\''+groupObj.id+'\',0)" >\n' +
                        '                                    <a href="#">\n' +
                        '                                        <div class="d-flex">\n' +
                        '                                            <div id="chat-user-img-'+groupObj.id+'" class="chat-user-img away align-self-center me-3 ms-0">\n' +
                        '                                                <img src="'+userProfilePicture+'" src-original="'+userProfilePictureOriginal+'" img-load="true" class="rounded-circle avatar-xs" alt="">\n' +
                        '                                            </div>\n' +
                        '                                            <div class="flex-1 overflow-hidden">\n' +
                        '                                                <h5 class="text-truncate font-size-15 pb-1">'+userName+'</h5>\n' +
                        '                                                <p class="chat-user-message text-truncate mb-0"></p>\n' +
                        '                                            </div>\n' +
                        '                                            <div class="font-size-11">'+ chatTime +'</div>\n';
                    if(currentGroup!=undefined) {
                        if (groupObj.id != currentGroup.id) {
                            if(groupObj.members[user
                                    .id].read>0)
                            {
                                chatHtml +='                                            <div class="unread-message">\n' +
                                    '                                                <span class="badge rounded-pill">'+groupObj.members[user.id].read+'</span>\n' +
                                    '                                            </div>\n' +
                                    '                                        </div>\n' +
                                    '                                    </a>\n' +
                                    '                                </li>';
                            }
                        }
                    }
                    else if(groupObj.members[user
                            .id].read>0)
                    {
                        chatHtml +='                                            <div class="unread-message">\n' +
                            '                                                <span class="badge rounded-pill">'+groupObj.members[user.id].read+'</span>\n' +
                            '                                            </div>\n' +
                            '                                        </div>\n' +
                            '                                    </a>\n' +
                            '                                </li>';
                    }
                    chatHtml += '</div></a></li>';

                }

            }
            else {
                if(checkFirst==0)
                {
                    checkFirst=groupObj.id;
                }

                var sumMember=0;
                _.forEach(groupObj.members, function (member) {
                    if(member.leave==0)
                    {
                        const friendObj = _.filter(allUsers, friend => friend.user_id == member.id);
                        if(friendObj.length>0) {
                            sumMember=sumMember+ 1;
                        }

                    }

                });

                const groupImg = groupObj.attachment ? BASE_URL+groupObj.attachment : IMGURL+'/default-group.avatar.png';
                chatHtml += '<li class="list- unread" id="div-'+groupObj.id+'" onClick="renderChatNotification(\''+groupObj.id+'\',0)" >\n' +
                    '                                    <a href="#">\n' +
                    '                                        <div class="d-flex">\n' +
                    '                                            <div id="chat-user-img-'+groupObj.id+'" class="chat-user-img away align-self-center me-3 ms-0">\n' +
                    '                                                <img src="'+groupImg+'" class="rounded-circle avatar-xs" alt="">\n' +
                    '                                            </div><div class="group_member">'+sumMember+'</div>\n' +
                    '                                            <div class="flex-1 overflow-hidden">\n' +
                    '                                                <h5 class="text-truncate font-size-15 pb-1">'+groupObj.name+'</h5> ' ;
                if(groupObj.team_type)
                {
                    chatHtml +='<p class="chat-user-message text-truncate mb-0 d-none"><text class="teamType">('+groupObj.team_type+')<text></p>' ;
                }

                if(groupObj.team_type)
                {
                    chatHtml += '</div><div class="font-size-11" style="position: absolute; top: 55%; margin-left: 60px;">'+ chatTime +'</div>\n';
                }
                else {
                    chatHtml += '</div><div class="font-size-11" style="position: absolute; top: 55%; margin-left: 60px;">'+ chatTime +'</div>\n';
                }



                if(currentGroup!=undefined) {
                    if (groupObj.id != currentGroup.id) {
                        if(groupObj.members[user
                                .id].read>0)
                        {
                            chatHtml +='                                            <div class="unread-message">\n' +
                                '                                                <span class="badge rounded-pill">'+groupObj.members[user.id].read+'</span>\n' +
                                '                                            </div>\n' +
                                '                                        </div>\n' +
                                '                                    </a>\n' +
                                '                                </li>';
                        }
                    }
                }
                else if(groupObj.members[user
                        .id].read>0)
                {
                    chatHtml +='                                            <div class="unread-message">\n' +
                        '                                                <span class="badge rounded-pill">'+groupObj.members[user.id].read+'</span>\n' +
                        '                                            </div>\n' +
                        '                                        </div>\n' +
                        '                                    </a>\n' +
                        '                                </li>';
                }
                chatHtml += '</div></a></li>';
            }
            i++;
        }

        $('#list-tab-archive').html(chatHtml);
        sessionGroups();
        renderImages();
        disableArchiveLoaderSideArea();
    }

    //    ------------- Loaders --------------------------------------------------------------------------------------------

    function enableArchiveLoaderSideArea() {
        $('.sideloaderArchive').css('display', 'block');
    }
    function disableArchiveLoaderSideArea() {
        $('.sideloaderArchive').css('display', 'none');
    }
    //    =================================================== Individual Chat =====================================================
    function renderMeetingList() {
        sessionStorage.setItem("PreTab",'1');
    }
    function renderChatListIndividual1()
    {
        $('#list-tab').html('');
        renderChatListIndividual();
    }

    function renderChatListIndividual()
    {
        chatList="Chat";
        $('#no-chat-message').css('display', 'none');
        enableChatLoaderSideArea();

        //sort list by time
        const groupsTemp = _.orderBy(groups, ['lastMessageAt'], ['desc']);
        var chatHtml = '';
        var i = 0;
        var single = 0;
        checkCurrent=0;
        var countNotification = 0;
        var checkFirst = 0;
        for (let index in groupsTemp)
        {
            var groupObj = groupsTemp[index];

            if(currentGroup!=undefined) {
                if (groupObj.id != currentGroup.id) {
                    if (groupObj.members[user
                            .id].read > 0) {
                        countNotification = countNotification + 1;
                    }
                }
            }
            else
            {
                if (groupObj.members[user
                        .id].read > 0) {
                    countNotification = countNotification + 1;
                }
            }

            var convertDate = moment(groupObj.lastMessageAt, 'YYYY-MM-DD[T]HH:mm:ss').format("YYYY-MM-DD[T]HH:mm:ss");
            var chatTime = moment(convertDate).fromNow();
            if (groupObj.type != 'group')
            {
                var otherChatMember = getOtherUser(groupObj.members);
                const friendObj = _.filter(allUsers, friend => friend.user_id == otherChatMember.id);

                if(friendObj.length>0)
                {
                    if(checkFirst==0)
                    {
                        checkFirst=groupObj.id;
                    }

                    var userName=(friendObj.length>0)?friendObj[0].fname+' '+friendObj[0].lname:otherChatMember.name;
                    const userProfilePictureOriginal = BASE_URL+'/public/storage/users/'+otherChatMember.id+'/profile_picture.jpg?v={{time()}}';
                    const userProfilePicture = BASE_URL+'/public/uploads/white.jpg';

                    const groupImg = groupObj.attachment ? BASE_URL+groupObj.attachment : IMGURL+'/default-group.avatar.png';
                    chatHtml += '<li class="list- unread" id="div-'+groupObj.id+'" onClick="renderChatNotification(\''+groupObj.id+'\',0)" >\n' +
                        '                                    <a href="#">\n' +
                        '                                        <div class="d-flex">\n' +
                        '                                            <div id="chat-user-img-'+groupObj.id+'" class="chat-user-img away align-self-center me-3 ms-0">\n' +
                        '                                                <img src="'+userProfilePicture+'" src-original="'+userProfilePictureOriginal+'" img-load="true" class="rounded-circle avatar-xs" alt="">\n' +
                        '                                            </div>\n' +
                        '                                            <div class="flex-1 overflow-hidden">\n' +
                        '                                                <h5 class="text-truncate font-size-15 pb-1">'+userName+'</h5>\n' +
                        '                                                <p class="chat-user-message text-truncate mb-0"></p>\n' +
                        '                                            </div>\n' +
                        '                                            <div class="font-size-11" style="position: absolute; top: 55%; margin-left: 50px">'+ chatTime +'</div>\n';
                    if(currentGroup!=undefined) {
                        if (groupObj.id != currentGroup.id) {
                            if(groupObj.members[user
                                    .id].read>0)
                            {
                                chatHtml +='                                            <div class="unread-message">\n' +
                                    '                                                <span class="badge rounded-pill">'+groupObj.members[user.id].read+'</span>\n' +
                                    '                                            </div>\n' +
                                    '                                        </div>\n' +
                                    '                                    </a>\n' +
                                    '                                </li>';
                            }
                        }
                    }
                    else if(groupObj.members[user
                            .id].read>0)
                    {
                        chatHtml +='                                            <div class="unread-message">\n' +
                            '                                                <span class="badge rounded-pill">'+groupObj.members[user.id].read+'</span>\n' +
                            '                                            </div>\n' +
                            '                                        </div>\n' +
                            '                                    </a>\n' +
                            '                                </li>';
                    }
                    chatHtml += '</div></a></li>';
                    single++;
                }

                lastTimeStamp=groupObj.createdAt;

            }
            i++;
        }
        $('#list-tab').append(chatHtml);
        sessionGroups();
        renderImages();
        disableChatLoaderSideArea();

        if (single == 0) {
            $('#no-chat-message').css('display', 'block');
            disableChatArea();
        }

        if(countNotification>0 && BASE_URL)
        {
            var notificationFind= sessionStorage.getItem("notificationFind");
            if(currentURL==currentURLFind || currentURL==currentURLFind2)
            {
                $(".count-message").css('display','none');
                $(".count-message").html('');
                sessionStorage.setItem("notificationFind", '0');
            }
            else if(notificationFind=='0')
            {
                $(".count-message").css('display','none');
                $(".count-message").html('');
                sessionStorage.setItem("notificationFind", '1');
            }
            else
            {
                $(".count-message").css('display','block');
                $(".count-message").html('');
                $(".count-message").html(countNotification);
                sessionStorage.setItem("notificationFind", '1');
            }

        }
        else
        {
            $(".count-message").css('display','none');
            $(".count-message").html('');
            sessionStorage.setItem("notificationFind", '1');
        }

    }


    //    ------------- Loaders --------------------------------------------------------------------------------------------

    function enableChatLoaderSideArea() {
        $('.sideloaderChat').css('display', 'block');
    }
    function disableChatLoaderSideArea() {
        $('.sideloaderChat').css('display', 'none');
    }

    //    =================================================== Group Chat =====================================================

    function renderGroupListIndividual1() {
        $('#list-tab-group').html('');
        renderGroupListIndividual();
    }

    function renderGroupListIndividual()
    {
        $('#no-group-message').css('display', 'none');
        chatList="Groups";
        $('#list-tab-group').html('');
        chatList="Group";
        $('#no-group-message').css('display', 'none');
        enableGroupLoaderSideArea();

        //sort list by time
        const groupsTemp = _.orderBy(groups, ['lastMessageAt'], ['desc']);
        var chatHtml = '';
        var i = 0;
        checkCurrent=0;
        var countNotification = 0;
        var checkFirst = 0;
        var groupsall = 0;
        var groupSum=0;
        for (let index in groupsTemp)
        {
            var groupObj = groupsTemp[index];
            if(currentGroup!=undefined) {
                if (groupObj.id != currentGroup.id) {
                    if (groupObj.members[user
                            .id].read > 0) {
                        countNotification = countNotification + 1;
                    }
                }
            }
            else
            {
                if (groupObj.members[user
                        .id].read > 0) {
                    countNotification = countNotification + 1;
                }
            }
            var convertDate = moment(groupObj.lastMessageAt, 'YYYY-MM-DD[T]HH:mm:ss').format("YYYY-MM-DD[T]HH:mm:ss");
            var chatTime = moment(convertDate).fromNow();
            if (groupObj.type == 'group') {
                groupSum=groupSum+1;
                if(checkFirst==0)
                {
                    checkFirst=groupObj.id;
                }

                var sumMember=0;
                _.forEach(groupObj.members, function (member) {
                    if(member.leave==0)
                    {
                        const friendObj = _.filter(allUsers, friend => friend.user_id == member.id);
                        if(friendObj.length>0) {
                            sumMember=sumMember+ 1;
                        }
                        else if (member.id==user.id)
                        {
                            sumMember=sumMember+ 1;
                        }
                        else
                        {
                        //  Delete Member from admin

                            if(member.id!=user.id)
                            {
                                $.ajax({
                                    method: 'GET',
                                    url: BASE_URL+'/users/list',
                                    data: {
                                        _token: '{{ csrf_token() }}'
                                    },
                                }).done(function (result){
                                    allUsers = result;
                                    const friendObj = _.filter(allUsers, friend => friend.user_id == member.id);
                                    if(friendObj.length==0) {
                                        groupObj.members['' + member.id + ''].leave = 1;
                                        groupObj.members['' + member.id + ''].admin = 0;
                                        updateGroup(groupObj);
                                        saveMessage('((deleted_group_undefined))', new Date(), groupObj.id, member.id);
                                    }
                                });
                            }
                        }

                    }


                });

                const groupImg = groupObj.attachment ? BASE_URL+groupObj.attachment : IMGURL+'/default-group.avatar.png';
                chatHtml += '<li class="list- unread" id="div-'+groupObj.id+'" onClick="renderChatNotification(\''+groupObj.id+'\',0)" >\n' +
                    '                                    <a href="#">\n' +
                    '                                        <div class="d-flex">\n' +
                    '                                            <div id="chat-user-img-'+groupObj.id+'" class="chat-user-img away align-self-center me-3 ms-0">\n' +
                    '                                                <img src="'+groupImg+'" class="rounded-circle avatar-xs" alt="">\n' +
                    '                                            </div><div class="group_member">'+sumMember+'</div>\n' +
                    '                                            <div class="flex-1 overflow-hidden">\n' +
                    '                                                <h5 class="text-truncate font-size-15 pb-1">'+groupObj.name+'</h5> ' ;
                                    if(groupObj.team_type)
                                    {
                                        chatHtml +='<p class="chat-user-message text-truncate mb-0 d-none"><text class="teamType">('+groupObj.team_type+')<text></p>' ;
                                    }

                if(groupObj.team_type)
                {
                    chatHtml += ' </div><div class="font-size-11" style="position: absolute; top: 57.5%; margin-left: 60px;">'+ chatTime +'</div>\n';
                }
                else{
                    chatHtml += ' </div><div class="font-size-11" style="position: absolute; top: 57.5%; margin-left: 60px;">'+ chatTime +'</div>\n';
                }

                if(currentGroup!=undefined) {
                    if (groupObj.id != currentGroup.id) {
                        if(groupObj.members[user
                                .id].read>0)
                        {
                            chatHtml +='                                            <div class="unread-message">\n' +
                                '                                                <span class="badge rounded-pill">'+groupObj.members[user.id].read+'</span>\n' +
                                '                                            </div>\n' +
                                '                                        </div>\n' +
                                '                                    </a>\n' +
                                '                                </li>';
                        }
                    }
                }
                else if(groupObj.members[user
                        .id].read>0)
                {
                    chatHtml +='                                            <div class="unread-message">\n' +
                        '                                                <span class="badge rounded-pill">'+groupObj.members[user.id].read+'</span>\n' +
                        '                                            </div>\n' +
                        '                                        </div>\n' +
                        '                                    </a>\n' +
                        '                                </li>';
                }
                chatHtml += '</div></a></li>';
                groupsall++;
            }
            i++;
        }
        $('#list-tab-group').html('');
        $('#list-tab-group').html(chatHtml);
        sessionGroups();
        renderImages();
        disableGroupLoaderSideArea();
        if (groupSum == 0) {
            $('#no-group-message').css('display', 'block');
            disableChatArea();
        }


        if(countNotification>0 && BASE_URL)
        {

            var notificationFind= sessionStorage.getItem("notificationFind");
            if(currentURL==currentURLFind || currentURL==currentURLFind2)
            {
                $(".count-message").css('display','none');
                $(".count-message").html('');
                sessionStorage.setItem("notificationFind", '0');
            }
            else if(notificationFind=='0')
            {
                $(".count-message").css('display','none');
                $(".count-message").html('');
                sessionStorage.setItem("notificationFind", '1');
            }
            else
            {
                $(".count-message").css('display','block');
                $(".count-message").html('');
                $(".count-message").html(countNotification);
                sessionStorage.setItem("notificationFind", '1');
            }

        }
        else
        {
            $(".count-message").css('display','none');
            $(".count-message").html('');
            sessionStorage.setItem("notificationFind", '1');
        }
    }


    //    ------------- Loaders --------------------------------------------------------------------------------------------

    function enableGroupLoaderSideArea() {
        $('.sideloaderGroup').css('display', 'block');
    }
    function disableGroupLoaderSideArea() {
        $('.sideloaderGroup').css('display', 'none');
    }


    // ================================================== Friends ===========================================================

    function updateFriends()

    {
        $('#addNewMember .show-message').html('');
        $('#add-friend-group').val(null).trigger('change');
        $('#editGroupModal .btn-close').click();
        getFriendList();
    }
    function getFriendList()
    {
        $.ajax({
            method: 'GET',
            url: BASE_URL+'/friendsdata/list',
            data: {
                _token: '{{ csrf_token() }}',
                name: 'null'
            },
        }).done(function (result){
            friends = result;
            sessionFriends();
            renderFriendsDropDown(result);
        });
    }

    function sessionFriends()
    {
        var friendIDOnSession= sessionStorage.getItem("friendID");
        $(".full-chat").addClass("user-chat-show");
        // If Friend Click
        if(friendIDOnSession)
        {
            var members = {};
            members[user.id] = user;
            // set other user
            const friendObj = _.filter(friends, friend => friend.user_id == friendIDOnSession);
            members[friendObj[0].user_id] = { id: friendObj[0].user_id, name: friendObj[0].fname+' '+friendObj[0].lname,admin:0,status:1,leave:0,read:0,notify:'on'};

            fetchGroupByMembers(user.id, friendIDOnSession).then(function(groupExists){
                if (groupExists)
                {
                    messagesChecked(groupExists.id);
                        groupFound = groupExists;
                        members = groupFound.members;

                        //             update Read Counter
                        _.forEach(members, function (member) {
                            if (member.id == user.id) {
                                members['' + member.id + ''].read = 0;
                            }
                        });
                        groupFound.members = members;
                        updateGroup(groupFound);
                    renderChat(groupExists.id,1);
                    //remove selection
                    $('.list-group-item').removeClass('active');
                }
                else
                {
                    currentGroup=undefined;
                    createGroup(members, user.id, 'individual - '+user.name, 'individual').then(function(groupResponse) {
                        currentGroup=groupResponse;
                        renderChat(groupResponse.id,1);
                    });
                }
            });
            sessionStorage.setItem("friendID", '');
        }
    }

    function newFriendModelClick(id,name)
    {
        sessionStorage.setItem("friendID", id);
        var friendIDOnSession= sessionStorage.getItem("friendID");
        $(".full-chat").addClass("user-chat-show");
        sessionFriends();
    }

    function renderFriendList(name)
    {
        $('#list-tab-friend').html('');
        sessionStorage.setItem("PreTab",'4');
        chatList="Friends";
        $.ajax({
            method: 'GET',
            url: BASE_URL+'/friendsdata/list',
            data: {
                _token: '{{ csrf_token() }}',
                name: name
            },
            beforeSend: function () {
                $('#no-friend-message').css('display','none');
                enableFriendLoaderSideArea();
            }
        }).done(function (result){
            friends = result;
            renderFriendsAllList(result);
        });
    }

    function renderFriendsDropDown(data) {

        var name="{{$userprofile['fname']}}"+" "+"{{$userprofile['lname']}}";
        var options = '<div class="fw-bold text-primary">You</div>' +
            '<div class="form-check">' +
            '<input type="checkbox" class="form-check-input" name="friend-select-group" value="'+user.id+'" id="'+user.id+'" checked> ' +
            '<label class="form-check-label" for="'+user.id+'">'+user.name+'</label>' +
            '</div>';

        if(data.length>0)
        {
            var first_letter=data[0].fname.charAt(0);
            options+='<div class="fw-bold text-primary">'+first_letter+'</div>';
            for(var i=0;i<data.length;i++)
            {

                if(first_letter==data[i].fname.charAt(0))
                {

                }
                else
                {
                    first_letter=data[i].fname.charAt(0);
                    options+='<div class="fw-bold text-primary">'+first_letter+'</div>';
                }

                options+='<ul class="list-unstyled contact-list"><li><div class="form-check">' +
                    '<input type="checkbox" class="form-check-input" name="friend-select-group" value="'+data[i].user_id+'" id="'+data[i].user_id+'">' +
                    '<label class="form-check-label" for="'+data[i].user_id+'">'+data[i].fname+' '+data[i].lname+'</label></div></li></ul>';
            }
            options+="</div>";
        }


        var optionsSingle = '';
        _.forEach(data, function(user){
            optionsSingle += '<option value="'+user.user_id+'" >'+user.fname+' '+user.lname+'</option>';
        });


        const optionFriendDropdown = '<option value="" disabled selected>Select a friend</option>'+optionsSingle;
        $('#friends-list').html(optionFriendDropdown);

        $('#friend-select-group-list').html('');
        $('#friend-select-group-list').html(options);

        var optionsSingles = '';

        _.forEach(data, function(user)
        {
                        optionsSingles += '<option value="'+user.user_id+'" >'+user.fname+' '+user.lname+'</option>';
        });
        $('#add-friend-group').html(optionsSingles);
    }

    function addMemberToCurrentGroup() {
        var friendIds = $('#add-friend-group').val();
        if (friendIds.length <= 0) {
            $('#addNewMember .show-message').html('<div class="alert alert-danger">Please select friend</div>'); return;
        }

        var i=0;
        var s=0;
        var members = currentGroup.members;
        // set other user

        _.forEach(friendIds, function (friendId){
            if (typeof members[friendId] !== 'undefined')
            {
                if(members[friendId].leave==0)
                {
                    s=1;
                    swal({
                        title: '',
//                        text: members[friendId].name+' Already Exists.',
                        text: 'This Action is Invalid.',
                        button: false,
                        icon:'error',
                        timer:1000
                    });
                }
            }
        });

        if(s==0)
        {
            _.forEach(friendIds, function (friendId){
                if (typeof members[friendId] !== 'undefined')
                {
                    if(members[friendId].leave==1)
                    {
                        i=1;
                        members[friendId].leave = 0;
                        saveMessage('((added_group_undefined))', new Date(), currentGroup.id, members[friendId].name);
                    }
                }
                else
                {
                    i=1;
                    const friendObj = _.filter(friends, friend => friend.user_id == friendId);
                    members[friendObj[0].user_id] = { id: friendObj[0].user_id, name: friendObj[0].fname+' '+friendObj[0].lname,status:1,leave:0,admin:0,read:0,notify:'on'};
                    saveMessage('((added_group_undefined))', new Date(), currentGroup.id, friendObj[0].fname+' '+friendObj[0].lname);
                }
            });
            swal({
                title: '',
                text: 'Members Updated Successfully',
                button: false,
                icon:'success',
                timer:1000
            });
            currentGroup.members = members;
            let now = new Date();
            var dateString = moment(now).format('YYYY-MM-DD HH:mm:ss');
            currentGroup.lastMessageAt=  dateString;

            updateGroup(currentGroup);
            $('#addNewMember .btn-close').click();
            $('#add-friend-group').val(null).trigger('change');
        }

    }

    function renderFriendsAllList(result) {
        disableChatArea();
        if(result.length>0)
        {
            var first_letter=result[0].fname.charAt(0);
            var chatHtml='';
            chatHtml+='<div><div class="p-3 fw-bold text-primary">'+first_letter+'</div>';
            for(var i=0;i<result.length;i++)
            {
                const userProfilePictureOriginal = BASE_URL+'/public/storage/users/'+result[i].user_id+'/profile_picture.jpg?v={{time()}}';
                const userProfilePicture = BASE_URL+'/public/uploads/white.jpg';
                if(first_letter==result[i].fname.charAt(0))
                {

                }
                else
                {
                    first_letter=result[i].fname.charAt(0);
                    chatHtml+='</div><div><div class="p-3 fw-bold text-primary">'+first_letter+'</div>';
                }

                chatHtml += '<ul class="list-unstyled contact-list"><li><div class="d-flex align-items-center"><div class="flex-1">' +
                    '<h5 class="font-size-14 m-0">'+result[i].fname+' '+result[i].lname+'</h5></div>' +
                    '<a onClick="newFriendModelClick(\''+result[i].user_id+'\',\''+result[i].fname+' '+result[i].lname+'\')"><i class="ri-message-3-line"></i></a>'+
                    '</div>' +
                    '</li></ul>';

            }

            $('#list-tab-friend').html(chatHtml);
        }
        else
        {
            $('#no-friend-message').css('display', 'block');
        }

        renderImages();
        disableFriendLoaderSideArea();
    }

    //  ==================================  Friends Search ==========================================================

    $('#search-friends').keyup(function (e){
        const search = $(this).val();
        $('#no-friend-message').css('display','none');

        if($.trim(search).length!=0)
        {
            $('#list-tab-friend').html('');
            enableFriendLoaderSideArea();
            renderFriendList($.trim(search));
        }
        else if(search=='')
        {
            renderFriendList('null');
        }

    });



    //    ------------- Loaders --------------------------------------------------------------------------------------------

    function enableFriendLoaderSideArea() {
        $('.sideloaderFriend').css('display', 'block');
    }
    function disableFriendLoaderSideArea() {
        $('.sideloaderFriend').css('display', 'none');
    }


    // ==================================================== Messages Area ==================================================

    function disableChatArea() {

        if(archive=='1')
        {
            $('.full-chat').css('display', 'none');
            $('.full_first').css('display', 'block');
            $('#default-img').html('');
            $('#chat-title').text('');
        }

    }

    function enableChatArea() {
        $('.full_first').css('display', 'none');
        $('.full_loader').css('display', 'block');
        $('.full-chat').css('display', 'none');
    }

    // ==================================================== Render Chat ====================================================

    function renderChatNotification(groupID,read)
    {
        lastMessageTimeStamp=new Date();
        $('#message-field').html('');
        $('#message-field').text('');
        $('.emojionearea-editor').text('');
        $('.emojionearea-editor').html('');
        $('#message-field').val('');
        $('.emoji-wysiwyg-editor').html('');
        var members = {};
         groupFound=[];

        fetchGroupById(groupID).then(function(groupExists){
            groupFound=groupExists;
            groupFound.id=groupID;

            members = groupFound.members;

//             update Read Counter
            _.forEach(members, function (member) {
                if (member.id == user.id)
                {
                    members[''+member.id+''].read=0;
                }
            });
            groupFound.members=members;

            currentGroup=groupFound;
            currentGroup.id=groupID;

            updateGroup(groupFound);
            messagesChecked(currentGroup.id);

            if (groupFound.type == 'group')
            {
                if(chatList=='Archive')
                {
                    $('#unarchiveGroupM').css('display','block');
                    $('#editGroupM').css('display','none');
                    $('#smsGroupM').css('display','none');
                }
                else
                {
                    $('#editGroupM').css('display','block');
                    $('#smsGroupM').css('display','block');
                    $('#unarchiveGroupM').css('display','none');
                }
            }
            else
            {
                $('#editGroupM').css('display','none');
                $('#smsGroupM').css('display','none');
                $('#unarchiveGroupM').css('display','none');
            }
            renderChat(groupID,read);
            if (window.matchMedia("(max-width: 991.98px)").matches) {
                $("#new_document_attachment").css('display','block');
            }
        });
    }

    function renderChat(groupID,read)
    {
        enableChatArea();
        $('#chat-msg-area').html('');
        if(chatList=='Archive')
        {
            $('#unarchiveGroupM').css('display','block');
            $('#editGroupM').css('display','none');
            $('#smsGroupM').css('display','none');
        }
        else
        {
            $('#editGroupM').css('display','block');
            $('#smsGroupM').css('display','block');
            $('#unarchiveGroupM').css('display','none');
        }

        fetchGroupById(groupID).then(function(groupExists) {
            currentGroup=groupExists;
            currentGroup.id=groupID;
            $('.input-message-field').css('display', 'block');
            $('.group-option').css('display', 'none');
            if (currentGroup.type == 'group') {
                $('.group-option').css('display', 'block');

                if(currentGroup.members['' + user.id + ''].notify==undefined)
                {
                    $( "#notifyCheck" ).prop( "checked", true );
                }
                else if(currentGroup.members['' + user.id + ''].notify=='off')
                {
                    $( "#notifyCheck" ).prop( "checked", false );
                }
                else
                {
                    $( "#notifyCheck" ).prop( "checked", true );
                }
            }
            else
            {
                $( "#notifyCheck" ).prop( "checked", true );
            }

            if (currentGroup.members['' + user.id + ''].status == 1) {
                $('.unarchive_group_update').css('display', 'none');
                $('.archive_group_update').css('display', 'block');
            }
            else {
                $('.archive_group_update').css('display', 'none');
                $('.unarchive_group_update').css('display', 'block');
            }
            fetchMessagesByGroupId(groupID);
            $('#sidebar').toggleClass('active');
            chatBottom();

            setTimeout(function() {
                chatUpNotAllowed=0;
            }, 1500);
        });
    };

    // ==================================================== Fetch Chat =====================================================

    function fetchMessagesByGroupId(groupId)
    {
        chatUpNotAllowed=1;
        messages = [];

        db.collection('message')
            .doc(groupId.trim())
            .collection('messages')
            .orderBy('sentAt','desc')
            .limit(8)
            .onSnapshot((querySnapshot) => {
            const allMessages = []
            querySnapshot.forEach((doc) => {
                if (doc) allMessages.push(doc.data())
            })

        messages = allMessages;

        $('#chat-msg-area').html('');

        if (Array.isArray(messages) && messages.length)
        {}
        else
        {
            setChatTitle();
        }

        countMessages='new';
        //  Check if group leave
        var left=currentGroup.members[user.id].leave;
        if(left==0)
        {
            chathtml='';
            messagesDateArray=[];
            renderMessages();
            chatBottom();
        }
    })
    }

    // ============================================== Render Messages ======================================================

    function renderMessages()
    {
        var chatMessages = '';
        var completeChat = '';
        var firstDate = '';
        var firstDateAppend='';
        var i=1;

        messages = _.orderBy(messages, ['sentAt'], ['acs']);
        _.forEach(messages, function (message)
        {

            var chatMessages='';
            if(i==1)
            {
                lastMessageTimeStamp=message.sentAt;
            }
            i++;
            var messageGroupID=message.groupID;
            const chatTime = moment.unix(message.sentAt.seconds).format("LT");
            var date=moment.unix(message.sentAt).format("D-M");
            var dateObj = new Date();
            var month=dateObj.getUTCMonth() + 1;
            var today = dateObj.getUTCDate()+'-'+month;
            var yesterdaydate=dateObj.getUTCDate()-1;
            var yesterday = yesterdaydate+'-'+month;

            if(today.toString()==date.toString())
            {
                date='Today';
            }
            else if(yesterday.toString()==date.toString())
            {
                date='Yesterday';
            }

            if(firstDate!=date)
            {
                if(date=='Today')
                {
                    firstDate='Today';
                    firstDateAppend='Today';
                }
                else if(date=='Yesterday')
                {
                    firstDate='Yesterday';
                    firstDateAppend='Yesterday';
                }
                else
                {
                    firstDate=moment.unix(message.sentAt).format("D-M");
                    firstDateAppend=moment.unix(message.sentAt).subtract(1969, 'years').format("ll");
                }

                     chatMessages +='<li class="date chat-'+date+'-10" > \n' +
                         '                                    <div class="chat-day-title">\n' +
                         '                                        <span class="title ">'+firstDateAppend+'</span>\n' +
                         '                                    </div>\n' +
                         '                                </li>';

                    $('.chat-'+date+'-10').remove();
            }
            var messageall=message.messageText;
            var file_name;
            const newMessage=messageall.replace(/\\n/g, "\n");
//            var messageText =  '<p class="recieve-message">'+newMessage+'</p>';
            // handle and show attachment sent
            if (message.messageText == 'attachment') {
                var extension =message.attachment.split('.').pop();

                var icon='';
                    if(extension=='xls'|| extension=='xlsx' || extension=='doc' || extension=='docx'|| extension=='txt' || extension=='zip' || extension=='pdf' || extension=='csv' || extension=='pptx' || extension=='ppt')
                    {
                        if(extension=='doc' || extension=='docx' )
                        {
                            icon='ri-file-word-fill';
                        }
                        else if(extension=='txt' )
                        {
                            icon='ri-file-text-fill';
                        }
                        else if(extension=='xls' || extension=='xlsx' || extension=='csv')
                        {
                            icon='ri-file-excel-fill';
                        }
                        else if(extension=='zip')
                        {
                            icon='ri-file-zip-fill';
                        }
                        else if(extension=='pdf')
                        {
                            icon='ri-file-pdf-fill';
                        }
                        else if(extension=='ppt' || extension=='pptx')
                        {
                            icon='ri-file-text-fill';
                        }


                        if(message.filename)
                        {
                             file_name=message.filename;
                        }
                        else
                        {
                             file_name='File.'+extension;
                        }
                        var messageText= '<div class="card p-2 mb-2">\n' +
                            '                                                        <div class="d-flex flex-wrap align-items-center attached-file">\n' +
                            '                                                            <div class="avatar-sm me-3 ms-0 attached-file-avatar">\n' +
                            '                                                                <div class="avatar-title bg-soft-primary text-primary rounded font-size-20">\n' +
                            '                                                                    <i class="'+icon+'"></i>\n' +
                            '                                                                </div>\n' +
                            '                                                            </div>                                                            \n' +
                            '                                                            <div class="flex-1 overflow-hidden">\n' +
                            '                                                                <div class="text-start">\n' +
                            '                                                                    <h5 class="font-size-14 text-truncate mb-1">'+file_name+'</h5>\n' +
                            '                                                                    <p class="text-muted text-truncate font-size-13 mb-0"></p>\n' +
                            '                                                                </div>\n' +
                            '                                                            </div>\n' +
                            '                                                            <div class="ms-4 me-0">\n' +
                            '                                                                <div class="d-flex gap-2 font-size-20 d-flex align-items-start">\n' +
                            '                                                                    <div>\n' +
                            '                                                                        <a href="'+BASE_URL+message.attachment+'" class="text-muted" download>\n' +
                            '                                                                            <i class="ri-download-2-line"></i>\n' +
                            '                                                                        </a>\n' +
                            '                                                                    </div>\n' +
                            '                                                                </div>\n' +
                            '                                                            </div>\n' +
                            '                                                        </div>\n' +
                            '                                                    </div><span class="align-middle" style="margin-top:3px;margin-bottom:5px;margin-left:10px;float:right;font-size:12px;font-style: italic;">'+chatTime+'</span>';
                    }
                    else {

                        if (message.sentBy !== user.id)
                        {
                            var messageText = '<p class="recieve-message"><img class="pointer-cursor" onclick="photo_zoom(\''+message.attachment+'\')" src="'+BASE_URL+message.attachment+'" width="250" height="250"/><br/><a style="color:white" href="'+BASE_URL+message.attachment+'" download>Download</a><span class="align-middle" style="margin-bottom:5px;font-style: italic;margin-left:8px;float:right;font-size:12px;">'+chatTime+'</span></p>';
                        }

                        else
                            var messageText = '<p class="recieve-message"><img class="pointer-cursor" onclick="photo_zoom(\''+message.attachment+'\')" src="'+BASE_URL+message.attachment+'" width="250" height="250"/><br/><a href="'+BASE_URL+message.attachment+'" download>Download</a></p><span class="align-middle" style="margin-bottom:5px;font-style: italic;margin-left:8px;float:right;font-size:12px;">'+chatTime+'</span>';

                        }

                 }
            else
            {
                var messageText =  '<p class="recieve-message">'+newMessage+'<span class="align-middle" style="margin-top:3px;margin-left:10px;float:right;font-size:12px;">'+chatTime+'</span></p>';
            }

            if(message.messageText == '((deleted_group_undefined))')
            {
                const friendObjDelete = _.filter(friends, friend => friend.user_id == message.attachment);
                var memberNameLeft=(friendObjDelete.length>0)?friendObjDelete[0].fname+' '+friendObjDelete[0].lname:currentGroup.members[message.attachment]['name'];

                chatMessages += '<li><div class="messages-area-left">'+
                    '<div class="media"> '+memberNameLeft+' has been removed by Super Admin.</div>'+
                    '</div></li>';
            }
            else if(message.messageText == '((left_group_undefined))')
            {
                const friendObj1 = _.filter(friends, friend => friend.user_id == message.attachment);
                var memberNameLeft=(friendObj1.length>0)?friendObj1[0].fname+' '+friendObj1[0].lname:currentGroup.members[message.attachment]['name'];

                chatMessages += '<li><div class="messages-area-left">'+
                    '<div class="media"> '+memberNameLeft+' has left the conversation.</div>'+
                    '</div></li>';
            }
            else if(message.messageText == '((update_group_name))')
            {
                const friendObj2 = _.filter(friends, friend => friend.user_id == message.attachment);
                var memberName=(friendObj2.length>0)?friendObj2[0].fname+' '+friendObj2[0].lname:currentGroup.members[message.attachment]['name'];

                chatMessages += '<li><div class="messages-area-left">'+
                    '<div class="media"> '+memberName+' changed group name to '+currentGroup.name+'.</div>'+
                    '</div></li>';
            }
            else if(message.messageText == '((make_admin_group))')
            {
                const friendObj0 = _.filter(friends, friend => friend.user_id == message.attachment);
                var memberNameAdmin=(friendObj0.length>0)?friendObj0[0].fname+' '+friendObj0[0].lname:currentGroup.members[message.attachment]['name'];

                const friendObj10 = _.filter(friends, friend => friend.user_id == message.sentBy);
                var adminNam=(friendObj10.length>0)?friendObj10[0].fname+' '+friendObj10[0].lname:currentGroup.members[message.sentBy]['name'];

                chatMessages += '<li><div class="messages-area-left">'+
                    '<div class="media"> '+adminNam+' made '+memberNameAdmin+' an admin.</div>'+
                    '</div></li>';
            } else if(message.messageText == '((remove_admin_group))')
            {
                const friendObj0 = _.filter(friends, friend => friend.user_id == message.attachment);
                var memberNameAdmin=(friendObj0.length>0)?friendObj0[0].fname+' '+friendObj0[0].lname:currentGroup.members[message.attachment]['name'];

                const friendObj10 = _.filter(friends, friend => friend.user_id == message.sentBy);
                var adminNam=(friendObj10.length>0)?friendObj10[0].fname+' '+friendObj10[0].lname:currentGroup.members[message.sentBy]['name'];

                chatMessages += '<li><div class="messages-area-left">'+
                    '<div class="media"> '+adminNam+' removed '+memberNameAdmin+' as an admin.</div>'+
                    '</div></li>';
            }
            else if(message.messageText == '((removed_group_undefined))')
            {
                const friendObj3 = _.filter(friends, friend => friend.user_id == message.attachment);
                var memberNameRemoved=(friendObj3.length>0)?friendObj3[0].fname+' '+friendObj3[0].lname:currentGroup.members[message.attachment]['name'];

                const friendObj4 = _.filter(friends, friend => friend.user_id == message.sentBy);
                var adminName_=(friendObj4.length>0)?friendObj4[0].fname+' '+friendObj4[0].lname:currentGroup.members[message.sentBy]['name'];

                chatMessages += '<li><div class="messages-area-left">'+
                    '<div class="media"> '+adminName_+' has removed '+memberNameRemoved+' from the conversation.</div>'+
                    '</div></li>';
            }
            else if(message.messageText == '((joined_group_undefined))')
            {
                var userNameJoined=message.attachment;

                chatMessages += '<li><div class="messages-area-left">'+
                    '<div class="media"> '+userNameJoined+' has joined the conversation.</div>'+
                    '</div></li>';
            }
            else if(message.messageText == '((added_group_undefined))')
            {
                const memberName_ = message.attachment;
                const friendObj6 = _.filter(friends, friend => friend.user_id == message.sentBy);
                var admin=(friendObj6.length>0)?friendObj6[0].fname+' '+friendObj6[0].lname:currentGroup.members[message.sentBy]['name'];
                chatMessages += '<li><div class="messages-area-left">'+
                    '<div class="media"> '+admin+' has added '+memberName_+' to the conversation.</div>'+
                    '</div></li>';
            }
            else if (message.sentBy !== user.id) {

                const friendObj7 = _.filter(friends, friend => friend.user_id == message.sentBy);


                var userName=(friendObj7.length>0)?friendObj7[0].fname+' '+friendObj7[0].lname:currentGroup.members[message.sentBy]['name'];

                const userProfilePictureOriginal = BASE_URL+'/public/storage/users/'+message.sentBy+'/profile_picture.jpg?v={{time()}}';
                const userProfilePicture = BASE_URL+'/public/uploads/white.jpg';

                chatMessages +='<li><div class="conversation-list"><div class="chat-avatar">\n' +
                    '                    <img  src="'+userProfilePicture+'" img-load="true" src-original="'+userProfilePictureOriginal+'"alt=""></div>' +
                    '                    <div class="user-chat-content">\n' +
                    '                    <div class="ctext-wrap">\n' +
                    '                    <div class="ctext-wrap-content">\n' +
                    '                    <p class="mb-0">\n'+ messageText +'</p>' +
                    '                </div></div>\n' +
                    '                <div class="conversation-name">'+userName+'</div></div></div>\n' +
                    '                </li>';
            }
            else {

                chatMessages +='<li class="right"><div class="conversation-list"><div class="chat-avatar">\n' +
                    '                    <img  src="'+CURRENT_USER_PIC+'" alt=""></div>' +
                    '                    <div class="user-chat-content">\n' +
                    '                    <div class="ctext-wrap">\n' +
                    '                    <div class="ctext-wrap-content" >\n' +
                    '                    <p class="mb-0">\n'+ messageText +'</p>' +
                    '                </div></div>\n' +
                    '                <div class="conversation-name">You</div></div></div>\n' +
                    '                </li>';
            }


            if(messageGroupID==currentGroup.id)
            {
                $('#list-tab .list-').removeClass('active').addClass('unread');
                $('#list-tab .chat-user-img').removeClass('online').addClass('away');

                $('#list-tab-group .list-').removeClass('active').addClass('unread');
                $('#list-tab-group .chat-user-img').removeClass('online').addClass('away');

                $('#div-'+messageGroupID).addClass('active').removeClass('unread');
                $('#chat-user-img-'+messageGroupID).addClass('online').removeClass('away');
                 completeChat=completeChat+chatMessages;
            }
        });

        chathtml=$('#chat-msg-area').html();
        $('#chat-msg-area').html('');
        $('#chat-msg-area').append(completeChat);
        $('#chat-msg-area').append(chathtml);
        $(".full-chat").addClass("user-chat-show");
        $('.chatloader').css('display', 'none');


        // loader chat
        var display;
        if(imageLoaderSent==0)
        {
             display='display:none';
        }
        else
        {
             display='display:block';
        }
        var loaderText = '<p class="recieve-message"><img class="pointer-cursor" style=" background: url(../public/sports/css/img/Hourglass.gif) center no-repeat;background-size: 65px 65px !important;background-color: black" width="250" height="250"/></p>';
        var loaderTextMessage ='<li class="right" id="image-send" style="'+display+'"><div class="conversation-list"><div class="chat-avatar">\n' +
            '                    <img  src="'+CURRENT_USER_PIC+'" alt=""></div>' +
            '                    <div class="user-chat-content">\n' +
            '                    <div class="ctext-wrap">\n' +
            '                    <div class="ctext-wrap-content" >\n' +
            '                    <p class="mb-0">\n'+ loaderText +'</p>' +
            '                </div></div>\n' +
            '                <div class="conversation-name">You</div></div></div>\n' +
            '                </li>';
        $('#chat-msg-area').append(loaderTextMessage);
        chatScrollFine=1;
        renderImages();
        setChatTitle();
        $('.full_loader').css('display', 'none');

        if(archive=='2')
        {
            loadTeams();
            $('.full_first').css('display', 'none');
            $('#editGroupM').css('display','block');
            $('#smsGroupM').css('display','block');
            $('#unarchiveGroupM').css('display','none');

            setTimeout(function() {
                archive='1';
            }, 1500);
        }
        return;

    }
    // ==================================================== Images =========================================================

    function photo_zoom (id)
    {
        document.body.classList.add("remove-scrolling");
        // Get modal
        var modalcover = document.getElementById('myModal');
        var modalImg = document.getElementById("img01");

        modalcover.style.display = "block";
        modalImg.src = BASE_URL+id;

        var spancover = document.getElementsByClassName("close")[0];
        spancover.onclick = function() {
            document.body.classList.remove("remove-scrolling");
            modalcover.style.display = "none";
        };
    }

    function renderImages() {
        $('img[img-load="true"]').each( function(i) {
            const originalImg = $(this).attr('src-original');
            const that = this;
            $.ajax({
                url: originalImg,
                type:'HEAD',
                error: function(){
                    $(document).find(that).attr('img-load','');
                },
                success:
                    function(){
                        $(document).find(that).attr('src', originalImg);
                        $(document).find(that).attr('img-load','');
                    }
            });
        });
    }

    // =========================================Chat Title / Chat Bottom ================================================

    function setChatTitle() {
        if (currentGroup.type == 'group') {
            const groupImg = currentGroup.attachment ? BASE_URL+currentGroup.attachment : IMGURL+'/default-group.avatar.png';

            var defaultimage_='<img src="'+groupImg+'" img-load="true" class="rounded-circle avatar-xs">'+'';
            $('#default-img').html('');
            $('#default-img').append(defaultimage_);
            $('#chat-title').text(currentGroup.name);

        } else {
            const otherChatMember = getOtherUser(currentGroup.members);
            const userProPictureOriginal = BASE_URL+'/public/storage/users/'+otherChatMember.id+'/profile_picture.jpg?v={{time()}}';
            const userProPicture = BASE_URL+'/public/uploads/white.jpg';

            var defaultimage='<img src="'+userProPicture+'" img-load="true" src-original="'+userProPictureOriginal+'" class="rounded-circle avatar-xs">'+'';
            $('#default-img').html('');
            $('#default-img').append(defaultimage);
            const friendObj = _.filter(friends, friend => friend.user_id == otherChatMember.id);
            var userName=(friendObj.length>0)?friendObj[0].fname+' '+friendObj[0].lname:otherChatMember.name;
            $('#chat-title').text(userName);
            $('#editGroupM').css('display','none');
            $('#smsGroupM').css('display','none');
            $('#unarchiveGroupM').css('display','none');
        }
        renderImages();
        $('.full_loader').css('display', 'none');
        $('.full-chat').css('display', 'block');
        document.getElementById("new_document_attachment").style.zIndex = "999";
    }
    function chatBottom() {
        $('.chat-conversation .simplebar-content-wrapper').scrollTop($('#chat-msg-area').height());

    }

    function chatBottomMobile()
    {
        if (window.matchMedia("(max-width: 991.98px)").matches) {
            $('.chat-conversation .simplebar-content-wrapper').scrollTop($('#chat-msg-area').height());
        }


    }

    function chatUp()
    {
        $('.chat-message-list .simplebar-content-wrapper').scrollTop($('#scrollUpChat').height());
    }

    //  ==================================  Send Message on Group =================================================

    function clearGroupModel()
    {
        $('#newGroupModal .show-message').html('');
        getFriendList();
        $('#group-name').val('');
        $('#group_message').val('');
        $("#create_group_img").val('');
        $('.team_type').prop('checked', false);
        document.getElementById('create-image').src='{{ asset('public/sports/images') }}/default-group.avatar.png';
        $('#newGroupModal .btn-primary').prop('disabled', false);
        //fetch friends
        getFriendList();
    }
    //  ==================================  Send Message ==========================================================

    function clearFriendModel()
    {
        $('.show-message').html('');
        $('#friends-list').val(null).trigger('change');
//        $('.emoji-wysiwyg-editor').html('');
        $('#new_message_single').val('');
        getFriendList();
    }
    function newFriendModel(id,name)
    {
        $('#newMessageModal').modal('show');
        $('.show- message').html('');
        $('#friends-list').val(null).trigger('change');
        $('.friend-message-modal .friend_message').val('');
        $('#new_message_single').val('');
        $('#friends-list').html('');
        const optionFriendDropdown = '<option value="'+id+'" selected>'+name+'</option>';
        $('#friends-list').html(optionFriendDropdown);
        $('#friends-list').attr("disabled", true);

    }

    $("#message-field").emojioneArea({
        placeholder: "Type your message",
        inline: true,
        hideSource: true,
        autocomplete: false,
        recentEmojis: false,
        events: {
            keyup: function (editor, event) {
                if (event.which == 13 && ($.trim(editor.text()).length > 0 || $.trim(editor.html()).length > 0)) {

                    const messageTex = $('.emojionearea-editor').html();
                    if(messageTex)
                    {
                        chatUpNotAllowed=1;
                        var messageText= messageTex.replace(/\n/g, "\\n");
                        saveMessage(messageText, new Date(), currentGroup.id);
                        chatUp();
                        $('#message-field').html('');
                        $('#message-field').text('');
                        $('#chat-msg-area').html('');
                        $('.emojionearea-editor').text('');
                        $('.emojionearea-editor').html('');
                        chatBottom();

                        setTimeout(function() {
                            chatUpNotAllowed=0;
                        }, 1500);
                    }
                }
            }

        }
    });

    function sendMessageToChat()
    {
        const messageTex = $('.emojionearea-editor').html();
        if(messageTex)
        {
            chatUpNotAllowed=1;
            var messageText= messageTex.replace(/\n/g, "\\n");
            saveMessage(messageText, new Date(), currentGroup.id);
            chatUp();
            $('#message-field').html('');
            $('#chat-msg-area').html('');
            $('#message-field').text('');
            $('.emojionearea-editor').text('');
            $('.emojionearea-editor').html('');
            chatBottom();
            setTimeout(function() {
                chatUpNotAllowed=0;
            }, 1500);
        }
    }

    function sendMessageToFriend()
    {
        currentGroup=undefined;
        var message = $('.friend-message-modal .friend_message').val();
        var friendId = $('#friends-list').val();

        if (message == '' || message=='undefined') {
            $('.show-message').html('<div class="alert alert-danger">Please enter message</div>'); return;
        }
        if (!friendId) {
            $('.show-message').html('<div class="alert alert-danger">Please select friend</div>'); return;
        }
        $('#newMessageModal .btn-primary').prop('disabled', true);

        var members = {};
        members[user.id] = user;
        // set other user
        const friendObj = _.filter(friends, friend => friend.user_id == friendId);
        members[friendObj[0].user_id] = { id: friendObj[0].user_id, name: friendObj[0].fname+' '+friendObj[0].lname,admin:0,status:1,leave:0,read:0,notify:'on'};

        fetchGroupByMembers(user.id, friendId).then(function(groupExists){
            if (groupExists)
            {
                currentGroup=groupExists;
                saveMessage(message, new Date(), groupExists.id);
                renderChat(groupExists.id,1);
                //remove selection
                $('.list-group-item').removeClass('active');
            }
            else
            {
                createGroup(members, user.id, 'individual - '+user.name, 'individual').then(function(groupResponse) {
                    currentGroup=groupResponse;
                    saveMessage(message, new Date(), groupResponse.id);
                    renderChat(groupResponse.id,1);
                });
            }

            $('.show-message').html('');
            $('#newMessageModal .btn-close').click();
            $('#friends-list').val(null).trigger('change');
            $('.friend-message-modal .friend_message').val('');
            $('#newMessageModal .btn-primary').prop('disabled', false);
        });
    }

    function sendAttachment() {
        var fd = new FormData();
        var files = $('#file-input')[0].files;

        // Check file selected or not
        if(files.length > 0 ){
            fd.append('attachment',files[0]);
            fd.append('_token', '{{ csrf_token() }}');

            $.ajax({
                url: BASE_URL+'/chat/upload-attachment',
                type: 'post',
                data: fd,
                contentType: false,
                processData: false,
                beforeSend: function () {
                    imageLoaderSent=1;
                    $("#image-send").css('display','block');
                    chatBottom();
                }
            }).done(function (result) {
                saveMessage('attachment', new Date(), currentGroup.id, result.path, result.file_name);
                $("#image-send").css('display','none');
                console.log('sent')
                imageLoaderSent=0;
                chatUpNotAllowed=1;
                chatUp();
                $('#chat-msg-area').html('');
                chatBottom();
                setTimeout(function() {
                    chatUpNotAllowed=0;
                }, 1500);
            });
            document.getElementById("file-input").value = "";
        }
    }

    checkSMSGroup='';
    function saveMessage(messageText, sentAt, currentGroupId, path, filename)
    {
        lastMessageTimeStamp= new Date();
        if (messageText.trim())
        {
            var messageReceived=messageText.trim();
            archive='1';
            if(chatList=='Archive')
            {
                archive='2';
            }
            const groupObj = [];

            groupObj[0] = currentGroup;
            let now = new Date();
            var members = {};
            var messageReceivedNew='';
            var headingMessage='';
            members = groupObj[0].members;

//             update Read Counter
            _.forEach(members, function (member) {
                if (member.id == user.id)
                {
                    members[''+member.id+''].read=0;
                }
                else
                {
                    if(members[''+member.id+''].leave!=1)
                    {
                        if(messageReceived!='((left_group_undefined))'  && messageReceived!='((update_group_name))'  && messageReceived!='((make_admin_group))'
                            && messageReceived!='((remove_admin_group))' && messageReceived!='((removed_group_undefined))' && messageReceived!='((joined_group_undefined))'
                            && messageReceived!='((added_group_undefined))' )
                        {
                            members[''+member.id+''].read=members[member.id].read+1;
                        }
                        // ===== SMS Sending ========

                        var group_name='';
                        var notify='';

                        if(groupObj[0].type=='group')
                        {
                            messageReceivedNew=user.name+': '+messageReceived.replace('<br>','');
                            headingMessage=groupObj[0].name;
                            group_name=groupObj[0].name;
                        }
                        else
                        {
                            headingMessage=user.name;
                            group_name='no-group';
                            messageReceivedNew=messageReceived.replace('<br>','');
                        }

                        if(members[''+member.id+''].notify==undefined)
                        {
                            notify='on';
                        }
                        else if(members[''+member.id+''].notify=='off')
                        {
                            notify='off';
                        }
                        else
                        {
                            notify='on';
                        }

                        if(messageReceived!='((left_group_undefined))'  && messageReceived!='((update_group_name))'  && messageReceived!='((make_admin_group))'
                            && messageReceived!='((remove_admin_group))' && messageReceived!='((removed_group_undefined))' && messageReceived!='((joined_group_undefined))'
                            && messageReceived!='((added_group_undefined))' ) {

                            if(messageReceived=='attachment')
                            {
                                if(groupObj[0].type=='group')
                                {
                                    messageReceivedNew=user.name+': sent an attachment';
                                }
                                else
                                {
                                    messageReceivedNew='sent an attachment';
                                }

                            }

                            $.ajax({
                                method: 'POST',
                                url: BASE_URL + '/sms/messageCheck',
                                data: {
                                    _token: '{{ csrf_token() }}',
                                    user_id: member.id,
                                    messageReceived: messageReceivedNew,
                                    group_id: currentGroup.id,
                                    group_name: group_name,
                                    heading: headingMessage,
                                    notify: notify
                                }
                            }).done(function (result) {
                            });
                        }
                        // ====== SMS Sending =======
                    }
                }
                members[member.id].status=1;
            });

            groupObj[0].members=members;

            var dateString = moment(now).format('YYYY-MM-DD HH:mm:ss');
            groupObj[0]['lastMessageAt']=  dateString;
            groupObj[0]['lastMessage']=  'yes';

            const message = {
                messageText,
                sentAt,
                sentBy: this.user.id,
                groupID:currentGroupId,
                attachment: typeof path !== 'undefined' ? path : false,
                filename: typeof filename !== 'undefined' ? filename : false
            };

            if(messageReceived!='((left_group_undefined))'  && messageReceived!='((update_group_name))'  && messageReceived!='((make_admin_group))'
                && messageReceived!='((remove_admin_group))' && messageReceived!='((removed_group_undefined))' && messageReceived!='((joined_group_undefined))'
                && messageReceived!='((added_group_undefined))' )

            {
                setTimeout(function() {
                    updateGroup(groupObj[0]);
                }, 1500);
            }

            return new Promise((resolve, reject) => {
                db.collection('message')
                .doc(currentGroupId)
                .collection('messages')
                .add(message)
                .then(function (docRef) {
                    resolve(message)
                })
                .catch(function (error) {
                    reject(error)
                })
        })
        }
    }


    function updateSMSNotification()
    {
        var notify;
        if($('#notifyCheck').prop("checked") == true){
             notify='on';
        }
        else if($('#notifyCheck').prop("checked") == false){
             notify='off';
        }

        $.ajax({
            method: 'POST',
            url: BASE_URL + '/sms/updateSMSNotification',
            data: {
                _token: '{{ csrf_token() }}',
                group_id: currentGroup.id,
                notify: notify
            }
        }).done(function (result) {
        });

        fetchGroupById(currentGroup.id).then(function(groupExists){
            groupFound=groupExists;
            groupFound.members['' + user.id + ''].notify=notify;
            updateGroup(groupFound);
            });

    }

    function updateGroup(group)
    {
        db.collection('group')
            .doc(group.id)
            .set(group)
            .then(function (docRef) {
            })
            .catch(function (error) {
            });
    }

    //  ==================================  Individual Chat Search ==========================================================

    $('#search-chats').keyup(function (e){
        $('#no-chat-message').css('display','none');
        const search = $(this).val();

        if($.trim(search).length!=0)
        {
            $('#list-tab').html('');
            enableChatLoaderSideArea();
            searchList=0;
            filterChatList($.trim(search));
        }
        else
        {
            searchList=1;
            $('#list-tab').html('');
            renderChatListIndividual();
        }
    });

    function filterChatList(searchValue) {
        var friendsList={};
        $.ajax({
            method: 'GET',
            url: BASE_URL+'/friendsdata/list',
            data: {
                _token: '{{ csrf_token() }}',
                name: searchValue
            },
            beforeSend: function () {
            }
        }).done(function (result){

            friendsList = result;

            $('.modal-backdrop').remove();
            var filteredGroups = _.filter(groups, function (item){
                if(friendsList.length > 0)
                {
                    for(var i=0;i<friendsList.length;i++)
                    {
                        var exist =  _.filter(item.members, member => member.id== friendsList[i].user_id);
                        if (exist.length) {
                            return true;
                        }
                    }
                }
                return false;
            });

            filteredGroups = _.orderBy(filteredGroups, ['createdAt.seconds'], ['desc']);

            if (filteredGroups.length <= 0) {
                $('#no-chat-message').css('display', 'block');
            } else {
                $('#no-chat-message').css('display', 'none');
            }

            var chatHtml = '';
            var checkFirst = 0;
            for (let index in filteredGroups) {
                const groupObj = filteredGroups[index];
                var convertDate = moment(groupObj.lastMessageAt, 'YYYY-MM-DD[T]HH:mm:ss').format("YYYY-MM-DD[T]HH:mm:ss");
                var chatTime = moment(convertDate).fromNow();
                if (groupObj.type != 'group')
                {
                    var otherChatMember = getOtherUser(groupObj.members);
                    const friendObj = _.filter(allUsers, friend => friend.user_id == otherChatMember.id);

                    if(friendObj.length>0)
                    {
                        if(checkFirst==0)
                        {
                            checkFirst=groupObj.id;
                        }

                        var userName=(friendObj.length>0)?friendObj[0].fname+' '+friendObj[0].lname:otherChatMember.name;
                        const userProfilePictureOriginal = BASE_URL+'/public/storage/users/'+otherChatMember.id+'/profile_picture.jpg?v={{time()}}';
                        const userProfilePicture = BASE_URL+'/public/uploads/white.jpg';

                        chatHtml += '<li class="list- unread" id="div-'+groupObj.id+'" onClick="renderChatNotification(\''+groupObj.id+'\',0)" >\n' +
                            '                                    <a href="#">\n' +
                            '                                        <div class="d-flex">\n' +
                            '                                            <div id="chat-user-img-'+groupObj.id+'" class="chat-user-img away align-self-center me-3 ms-0">\n' +
                            '                                                <img src="'+userProfilePicture+'" src-original="'+userProfilePictureOriginal+'" img-load="true" class="rounded-circle avatar-xs" alt="">\n' +
                            '                                            </div>\n' +
                            '                                            <div class="flex-1 overflow-hidden">\n' +
                            '                                                <h5 class="text-truncate font-size-15 pb-1">'+userName+'</h5>\n' +
                            '                                                <p class="chat-user-message text-truncate mb-0"></p>\n' +
                            '                                            </div>\n' +
                            '                                            <div class="font-size-11" style="position:absolute; top: 55%; margin-left: 50px;">'+ chatTime +'</div>\n';
                        if(currentGroup!=undefined) {
                            if (groupObj.id != currentGroup.id) {
                                if(groupObj.members[user
                                        .id].read>0)
                                {
                                    chatHtml +='                                            <div class="unread-message">\n' +
                                        '                                                <span class="badge rounded-pill">'+groupObj.members[user.id].read+'</span>\n' +
                                        '                                            </div>\n' +
                                        '                                        </div>\n' +
                                        '                                    </a>\n' +
                                        '                                </li>';
                                }
                            }
                        }
                        else if(groupObj.members[user
                                .id].read>0)
                        {
                            chatHtml +='                                            <div class="unread-message">\n' +
                                '                                                <span class="badge rounded-pill">'+groupObj.members[user.id].read+'</span>\n' +
                                '                                            </div>\n' +
                                '                                        </div>\n' +
                                '                                    </a>\n' +
                                '                                </li>';
                        }
                        chatHtml += '</div></a></li>';
                    }
                }
            }

            if(searchList==0)
            {
                $('#list-tab').html('');
                $('#list-tab').html(chatHtml);
            }

            renderImages();
            disableChatLoaderSideArea();
        });
    }


    $('#search-archives').keyup(function (e){
        $('#no-archive-message').css('display','none');
        const search = $(this).val();
        if($.trim(search).length!=0)
        {
            $('#list-tab-archive').html('');
            enableArchiveLoaderSideArea();
            filterArchiveList($.trim(search));
        }
    });

    function filterArchiveList(searchValue) {
        var friendsList={};
        $.ajax({
            method: 'GET',
            url: BASE_URL+'/friendsdata/list',
            data: {
                _token: '{{ csrf_token() }}',
                name: searchValue
            },
            beforeSend: function () {
            }
        }).done(function (result){

            friendsList = result;
            var filteredGroups = _.filter(groupsA, function (item){
                if(friendsList.length > 0)
                {
                    for(var i=0;i<friendsList.length;i++)
                    {
                        var exist =  _.filter(item.members, member => member.id== friendsList[i].user_id);
                        if (exist.length) {
                            return true;
                        } else if (item.name.toLowerCase().indexOf(searchValue.toLowerCase()) !== -1) {
                            return true;
                        }
                    }
                }
                else if (item.name.toLowerCase().indexOf(searchValue.toLowerCase()) !== -1) {
                    return true;
                }

                return false;
            });

            filteredGroups = _.orderBy(filteredGroups, ['createdAt.seconds'], ['desc']);

            if (filteredGroups.length <= 0) {
                $('#no-archive-message').css('display', 'block');
            } else {
                $('#no-archive-message').css('display', 'none');
            }

            var chatHtml = '';
            var checkFirst = 0;

            for (let index in filteredGroups) {
                const groupObj = filteredGroups[index];
                var convertDate = moment(groupObj.lastMessageAt, 'YYYY-MM-DD[T]HH:mm:ss').format("YYYY-MM-DD[T]HH:mm:ss");
                var chatTime = moment(convertDate).fromNow();
                if (groupObj.type != 'group') {
                    var otherChatMember = getOtherUser(groupObj.members);
                    const friendObj = _.filter(allUsers, friend => friend.user_id == otherChatMember.id);

                    if(friendObj.length>0)
                    {
                        if(checkFirst==0)
                        {
                            checkFirst=groupObj.id;
                        }
                        var userName=(friendObj.length>0)?friendObj[0].fname+' '+friendObj[0].lname:otherChatMember.name;
                        const userProfilePictureOriginal = BASE_URL+'/public/storage/users/'+otherChatMember.id+'/profile_picture.jpg?v={{time()}}';
                        const userProfilePicture = BASE_URL+'/public/uploads/white.jpg';

                        const groupImg = groupObj.attachment ? BASE_URL+groupObj.attachment : IMGURL+'/default-group.avatar.png';
                        chatHtml += '<li class="list- unread" id="div-'+groupObj.id+'" onClick="renderChatNotification(\''+groupObj.id+'\',0)" >\n' +
                            '                                    <a href="#">\n' +
                            '                                        <div class="d-flex">\n' +
                            '                                            <div id="chat-user-img-'+groupObj.id+'" class="chat-user-img away align-self-center me-3 ms-0">\n' +
                            '                                                <img src="'+userProfilePicture+'" src-original="'+userProfilePictureOriginal+'" img-load="true" class="rounded-circle avatar-xs" alt="">\n' +
                            '                                            </div>\n' +
                            '                                            <div class="flex-1 overflow-hidden">\n' +
                            '                                                <h5 class="text-truncate font-size-15 pb-1">'+userName+'</h5>\n' +
                            '                                                <p class="chat-user-message text-truncate mb-0"></p>\n' +
                            '                                            </div>\n' +
                            '                                            <div class="font-size-11">'+ chatTime +'</div>\n';
                        if(currentGroup!=undefined) {
                            if (groupObj.id != currentGroup.id) {
                                if(groupObj.members[user
                                        .id].read>0)
                                {
                                    chatHtml +='                                            <div class="unread-message">\n' +
                                        '                                                <span class="badge rounded-pill">'+groupObj.members[user.id].read+'</span>\n' +
                                        '                                            </div>\n' +
                                        '                                        </div>\n' +
                                        '                                    </a>\n' +
                                        '                                </li>';
                                }
                            }
                        }
                        else if(groupObj.members[user
                                .id].read>0)
                        {
                            chatHtml +='                                            <div class="unread-message">\n' +
                                '                                                <span class="badge rounded-pill">'+groupObj.members[user.id].read+'</span>\n' +
                                '                                            </div>\n' +
                                '                                        </div>\n' +
                                '                                    </a>\n' +
                                '                                </li>';
                        }
                        chatHtml += '</div></a></li>';
                    }

                }
                else {
                    if(checkFirst==0)
                    {
                        checkFirst=groupObj.id;
                    }

                    var sumMember=0;
                    _.forEach(groupObj.members, function (member) {
                        if(member.leave==0)
                        {
                            const friendObj = _.filter(allUsers, friend => friend.user_id == member.id);
                            if(friendObj.length>0) {
                                sumMember=sumMember+ 1;
                            }
                        }

                    });

                    const groupImg = groupObj.attachment ? BASE_URL+groupObj.attachment : IMGURL+'/default-group.avatar.png';
                    chatHtml += '<li class="list- unread" id="div-'+groupObj.id+'" onClick="renderChatNotification(\''+groupObj.id+'\',0)" >\n' +
                        '                                    <a href="#">\n' +
                        '                                        <div class="d-flex">\n' +
                        '                                            <div id="chat-user-img-'+groupObj.id+'" class="chat-user-img away align-self-center me-3 ms-0">\n' +
                        '                                                <img src="'+groupImg+'" class="rounded-circle avatar-xs" alt="">\n' +
                        '                                            </div><div class="group_member">'+sumMember+'</div>\n' +
                        '                                            <div class="flex-1 overflow-hidden">\n' +
                        '                                                <h5 class="text-truncate font-size-15 pb-1">'+groupObj.name+'</h5> ' ;
                    if(groupObj.team_type)
                    {
                        chatHtml +='<p class="chat-user-message text-truncate mb-0 d-none"><text class="teamType">('+groupObj.team_type+')<text></p>' ;
                    }

                    if(groupObj.team_type)
                    {
                        chatHtml +=                                                   '</div>\n' +
                            '                                            <div class="font-size-11" style="position: absolute; top: 55.7%; margin-left: 60px;">'+ chatTime +'</div>\n';
                    }
                    else {
                        chatHtml += '</div>\n' +
                            '                                            <div class="font-size-11" style="position: absolute; top: 55.7%; margin-left: 60px;">' + chatTime + '</div>\n';
                    }

                    if(currentGroup!=undefined) {
                        if (groupObj.id != currentGroup.id) {
                            if(groupObj.members[user
                                    .id].read>0)
                            {
                                chatHtml +='                                            <div class="unread-message">\n' +
                                    '                                                <span class="badge rounded-pill">'+groupObj.members[user.id].read+'</span>\n' +
                                    '                                            </div>\n' +
                                    '                                        </div>\n' +
                                    '                                    </a>\n' +
                                    '                                </li>';
                            }
                        }
                    }
                    else if(groupObj.members[user
                            .id].read>0)
                    {
                        chatHtml +='                                            <div class="unread-message">\n' +
                            '                                                <span class="badge rounded-pill">'+groupObj.members[user.id].read+'</span>\n' +
                            '                                            </div>\n' +
                            '                                        </div>\n' +
                            '                                    </a>\n' +
                            '                                </li>';
                    }
                    chatHtml += '</div></a></li>';
                }
            }

                $('#list-tab-archive').html(chatHtml);

            renderImages();
            disableArchiveLoaderSideArea();
        });
    }


    //  ==================================  Individual Group Search ==========================================================

    $('#search-groups').keyup(function (e){
        $('#no-group-message').css('display','none');
        const search = $(this).val();
        if(search)
        {
            if($.trim(search).length!=0)
            {
                $('#list-tab-group').html('');
                enableGroupLoaderSideArea();
                searchList=0;
                filterGroupList($.trim(search));
            }
            else
            {
                searchList=1;
                $('#list-tab-group').html('');
                renderGroupListIndividual();
            }
        }
        else
        {
            searchList=1;
            $('#list-tab-group').html('');
            fetchGroupByUserID(user.id);
        }

    });

    function filterGroupList(searchValue) {
        var filteredGroups = _.filter(groups, function (item){
            if (item.name.toLowerCase().indexOf(searchValue.toLowerCase()) !== -1) {
                return true;
            }
            return false;

        });

        filteredGroups = _.orderBy(filteredGroups, ['createdAt.seconds'], ['desc']);

        if (filteredGroups.length <= 0) {
            $('#no-group-message').css('display', 'block');
        } else {
            $('#no-group-message').css('display', 'none');
        }

        var chatHtml = '';
        var checkFirst = 0;
        for (let index in filteredGroups) {
            const groupObj = filteredGroups[index];
            var convertDate = moment(groupObj.lastMessageAt, 'YYYY-MM-DD[T]HH:mm:ss').format("YYYY-MM-DD[T]HH:mm:ss");
            var chatTime = moment(convertDate).fromNow();
            if (groupObj.type == 'group') {
                if(checkFirst==0)
                {
                    checkFirst=groupObj.id;
                }

                var sumMember=0;
                _.forEach(groupObj.members, function (member) {
                    if(member.leave==0)
                    {
                        const friendObj = _.filter(allUsers, friend => friend.user_id == member.id);
                        if(friendObj.length>0) {
                            sumMember=sumMember+ 1;
                        }
//                            else
//                            {
//                                groupObj.members[''+member.id+''].leave=1;
//                                groupObj.members[''+member.id+''].admin=0;
//                                updateGroup(groupObj);
//                                saveMessage('((deleted_group_undefined))', new Date(), groupObj.id, member.id);
//                            }
                    }

                });

                const groupImg = groupObj.attachment ? BASE_URL+groupObj.attachment : IMGURL+'/default-group.avatar.png';
                chatHtml += '<li class="list- unread" id="div-'+groupObj.id+'" onClick="renderChatNotification(\''+groupObj.id+'\',0)" >\n' +
                    '                                    <a href="#">\n' +
                    '                                        <div class="d-flex">\n' +
                    '                                            <div id="chat-user-img-'+groupObj.id+'" class="chat-user-img away align-self-center me-3 ms-0">\n' +
                    '                                                <img src="'+groupImg+'" class="rounded-circle avatar-xs" alt=""></div><div class="group_member">'+sumMember+'</div>\n' +
                    '                                            <div class="flex-1 overflow-hidden">\n' +
                    '                                                <h5 class="text-truncate font-size-15 pb-1">'+groupObj.name+'</h5>\n';
                if(groupObj.team_type)
                {
                    chatHtml +='<p class="chat-user-message text-truncate mb-0 d-none"><text class="teamType">('+groupObj.team_type+')<text></p>' ;
                }

                if(groupObj.team_type)
                {
                    chatHtml +=                                                   '</div>\n' +
                        '                                            <div class="font-size-11" style="position: absolute; top: 57.5%; margin-left: 60px;">'+ chatTime +'</div>\n';
                }

                else {
                    chatHtml +=                                                   '</div>\n' +
                        '                                            <div class="font-size-11" style="position: absolute; top: 57.5%; margin-left: 60px;">'+ chatTime +'</div>\n';
                }

                if(currentGroup!=undefined) {
                    if (groupObj.id != currentGroup.id) {
                        if(groupObj.members[user
                                .id].read>0)
                        {
                            chatHtml +='                                            <div class="unread-message">\n' +
                                '                                                <span class="badge rounded-pill">'+groupObj.members[user.id].read+'</span>\n' +
                                '                                            </div>\n' +
                                '                                        </div>\n' +
                                '                                    </a>\n' +
                                '                                </li>';
                        }
                    }
                }
                else if(groupObj.members[user
                        .id].read>0)
                {
                    chatHtml +='                                            <div class="unread-message">\n' +
                        '                                                <span class="badge rounded-pill">'+groupObj.members[user.id].read+'</span>\n' +
                        '                                            </div>\n' +
                        '                                        </div>\n' +
                        '                                    </a>\n' +
                        '                                </li>';
                }
                chatHtml += '</div></a></li>';
            }
        }

        if(searchList==0)
        {
            $('#list-tab-group').html('');
            $('#list-tab-group').html(chatHtml);
        }

        renderImages();
        disableGroupLoaderSideArea();
    }


    //  ================================================ Update Group ======================================================

    function editGroup()
    {
        $("#edit_group_img").val('');
        renderChat(currentGroup.id,1);
        $('#editGroupModal .group-name').val(currentGroup.name);
        $('#' + currentGroup.team_type+'_edit').prop('checked',true);
        const groupImg = currentGroup.attachment ? BASE_URL+currentGroup.attachment : IMGURL+'/default-group.avatar.png';
        $('#edit-image').attr('src', groupImg);
        renderMembers();

        if(currentGroup.members[user.id].admin==1)
        {
            $('.group_name_update').prop('readonly', false);
            $('.group_picture_update').css('display', 'block');
            $('.leave_group_update').css('display', 'none');
            $('.delete_group_update').css('display', 'block');
            $('.update_group_update').css('display', 'block');
            $('.update_group_member').css('display', 'block');
        }
        else
        {
            $('.group_name_update').prop('readonly', true);
            $('.group_picture_update').css('display', 'none');
            $('.leave_group_update').css('display', 'block');
            $('.delete_group_update').css('display', 'none');
            $('.update_group_update').css('display', 'none');
            $('.update_group_member').css('display', 'none');
        }
    }

    //    ===== Members ========

    function renderMembers() {
        var membersList = '';
        _.forEach(currentGroup.members, function (member){
            if (member.id == user.id) return;
            if(member.leave== 1) return;


            if(currentGroup.members[user.id].admin==1)
            {
                if(member.admin== 0)
                {
                    var make_admin='<span id="membersadmin-'+member.id+'" class="make_admin_" title="Make Admin"><i class="fa fa-user pointer-cursor" onClick="makeAdmin('+member.id+')"></i></span>';
                }
                else
                {
                    var make_admin='<span id="membersadmin-'+member.id+'" class="make_admin" title="Remove Admin"><i class="fa fa-user pointer-cursor" onClick="removeAdmin('+member.id+')"></i></span>';
                }

                var remove_='<span title="Remove Member"><i class="fa fa-user-times pointer-cursor" onClick="removeGroupMember('+member.id+')"></i></span>';
            }
            else
            {
                if(member.admin== 0)
                {
                    var make_admin='<span class="make_admin_" title="Member"><i class="fa fa-user"></i></span>';
                }
                else
                {
                    var make_admin='<span class="make_admin" title="Admin"><i class="fa fa-user"></i></span>';
                }
                var remove_='';
            }




            const friendObj = _.filter(allUsers, friend => friend.user_id == member.id);

            if(friendObj.length>0) {
                const userProfilePictureOriginal = BASE_URL + '/public/storage/users/' + member.id + '/profile_picture.jpg?v={{time()}}';
                const userProfilePicture = BASE_URL + '/public/uploads/white.jpg';
                var userName=(friendObj.length>0)?friendObj[0].fname+' '+friendObj[0].lname:member.name;

                membersList += '<div class="media" id="group-member' + member.id + '">' +
                    '<img class="member_image" src="' + userProfilePicture + '" src-original="' + userProfilePictureOriginal + '" img-load="true" class="mr-3" alt="...">' +
                    '<div class="media-body member-list-custom">' +
                    '<h5 class="mt-0" style="font-size: 14px;">' + userName + '</h5><div>' + make_admin + remove_ +
                    '</div>' +
                    '</div>' +
                    '</div>';
            }
        });

        $('#editGroupModal .members-list').html(membersList);
        renderImages();
    }

    // ===== Update

    function updateGroupName() {
        swal({
            title: "",
            text: "Are you sure you want to update?",
            icon: "warning",
            buttons: [
                'No, cancel it!',
                'Yes, I am sure!'
            ],
            dangerMode: true
        }).then(function(isConfirm)
        {
            if (isConfirm) {
                $('#editGroupModal .show-message').html('');


                var files = $('#edit_group_img')[0].files;
                if(files.length > 0 ){
                    var fd = new FormData();
                    fd.append('attachment',files[0]);
                    fd.append('_token', '{{ csrf_token() }}');

                    $.ajax({
                        url: BASE_URL+'/chat/groups/upload',
                        type: 'post',
                        data: fd,
                        contentType: false,
                        processData: false,
                        beforeSend: function () {
                            swal({
                                title: 'Please Wait!',
                                button: false
                            }).then(
                                function () {},
                                // handling the promise rejection
                                function (dismiss) {
                                }
                            )
                        },
                        error: function(response)
                        {
                            alert(response);
                        },
                        success: function(response){
                            //image update
                            currentGroup.attachment = response.path;
                            $('#edit_group_img').attr('src', BASE_URL+currentGroup.attachment);
                            $("#edit_group_img").val('');

                            //name & image update on firebase
                            var newName=$('#group_name_update').val();
                            if($.trim(currentGroup.name)!=$.trim(newName))
                            {
                                saveMessage('((update_group_name))', new Date(), currentGroup.id, user.id);
                            }
                            currentGroup.name = $('#group_name_update').val();
                            currentGroup.team_type=($('input[name=team_type_edit]:checked', '#editForm').val())?$('input[name=team_type_edit]:checked', '#editForm').val():'';
                            $('#chat-title').text(currentGroup.name);
                            const groupImg = currentGroup.attachment ? BASE_URL+currentGroup.attachment : IMGURL+'/default-group.avatar.png';

                            var defaultimage_='<img src="'+groupImg+'" img-load="true" class="rounded-circle avatar-xs">'+'';
                            $('#default-img').html('');
                            $('#default-img').append(defaultimage_);

                            updateGroup(currentGroup);
                            swal({
                                title: '',
                                text: 'Informations Updated Successfully',
                                button: false,
                                icon:'success',
                                timer:1000
                            });
                            setTimeout(function() {
                                $('#editGroupModal .show-message').html('');
                                $('#editGroupModal .btn-close').click();
                            }, 1500);

                        }
                    });
                    return;
                }

                var newName=$('#group_name_update').val();
                if($.trim(currentGroup.name)!=$.trim(newName))
                {
                    saveMessage('((update_group_name))', new Date(), currentGroup.id, user.id);
                }
                currentGroup.name = $('#group_name_update').val();
                currentGroup.team_type=($('input[name=team_type_edit]:checked', '#editForm').val())?$('input[name=team_type_edit]:checked', '#editForm').val():'';
                $('#chat-title').text(currentGroup.name);
                const groupImg = currentGroup.attachment ? BASE_URL+currentGroup.attachment : IMGURL+'/default-group.avatar.png';

                var defaultimage_='<img src="'+groupImg+'" img-load="true" class="rounded-circle avatar-xs">'+'';
                $('#default-img').html('');
                $('#default-img').append(defaultimage_);
                updateGroup(currentGroup);
                swal({
                    title: '',
                    text: 'Informations Updated Successfully',
                    button: false,
                    icon:'success',
                    timer:1000
                });
                setTimeout(function() {
                    $('#editGroupModal .show-message').html('');
                    $('#editGroupModal .btn-close').click();
                }, 1500);

            }
        });
    }


    function makeAdmin(memberID) {
        swal({
            title: "",
            text: "Are you sure you want to make this member Admin?",
            icon: "warning",
            buttons: [
                'No, cancel it!',
                'Yes, I am sure!'
            ],
            dangerMode: true
        }).then(function(isConfirm)
        {
            if (isConfirm) {
                $("#membersadmin-"+memberID).css('color','#7c151f');
                currentGroup.members[memberID].admin=1;
                updateGroup(currentGroup);
                saveMessage('((make_admin_group))', new Date(), currentGroup.id, memberID);
                swal({
                    title: '',
                    text: 'Assigned Admin Successfully',
                    button: false,
                    icon:'success',
                    timer:1000
                });
                setTimeout(function() {
                    renderMembers();
                    $('#editGroupModal .show-message').html('');
                }, 1500);
            }
        });
    }

    function removeAdmin(memberID) {
        swal({
            title: "",
            text: "Are you sure you want to remove this member as Admin?",
            icon: "warning",
            buttons: [
                'No, cancel it!',
                'Yes, I am sure!'
            ],
            dangerMode: true
        }).then(function(isConfirm)
        {
            if (isConfirm) {
                $("#membersadmin-" + memberID).css('color', '#c1c1c1');
                currentGroup.members[memberID].admin = 0;
                updateGroup(currentGroup);
                saveMessage('((remove_admin_group))', new Date(), currentGroup.id, memberID);
                swal({
                    title: '',
                    text: 'Removed Member as Admin Successfully',
                    button: false,
                    icon: 'success',
                    timer: 1000
                });
                setTimeout(function () {
                    renderMembers();
                    $('#editGroupModal .show-message').html('');
                }, 1500);
            }
        });
    }

    function removeGroupMember(memberID) {
        swal({
            title: "",
            text: "Are you sure you want to remove?",
            icon: "warning",
            buttons: [
                'No, cancel it!',
                'Yes, I am sure!'
            ],
            dangerMode: true
        }).then(function(isConfirm)
        {
            if (isConfirm) {
//                delete currentGroup.members[memberID];
                currentGroup.members[''+memberID+''].leave=1;
                currentGroup.members[''+memberID+''].admin=0;
                updateGroup(currentGroup);
                $('#group-member'+memberID).remove();
                swal({
                    title: '',
                    text: 'Member removed Successfully',
                    button: false,
                    icon:'success',
                    timer:1000
                });
                updateGroup(currentGroup);
                saveMessage('((removed_group_undefined))', new Date(), currentGroup.id, memberID);
            }
        });
    }


    function addNewGroupToUser(user, groupId) {
        $('.modal-backdrop').remove();
        const groupsObj = user.groups ? user.groups : [];
        const existed = groupsObj.filter((group) => group === groupId)
        if (existed.length === 0) {
            groups.push(groupId)
            user.groups = groupsObj
            const userRef = db.collection('user')
            userRef.doc(user.uid).set(user)
        }
    }

    // Leave Group
    function leaveGroup()
    {
        swal({
            title: "",
            text: "Are you sure want to leave this group.?",
            icon: "warning",
            buttons: [
                'No, cancel it!',
                'Yes, I am sure!'
            ],
            dangerMode: true
        }).then(function(isConfirm)
        {
            if (isConfirm) {
                currentGroup.members[''+user.id+''].leave=1;
                currentGroup.members[''+user.id+''].admin=0;
                updateGroup(currentGroup);
                saveMessage('((left_group_undefined))', new Date(), currentGroup.id, user.id);
                swal({
                    title: 'Left!',
                    text: 'You\'ve left the conversation successfully!',
                    icon: 'success',
                    timer: 2000,
                    button: false
                }).then(function() {
                });
                $('#editGroupModal .btn-close').click();
                disableChatArea();
            }
        });
    }

    // Archieve Group
    function archiveGroup()
    {
        swal({
            title: "",
            text: "Are you sure want to archive this group.?",
            icon: "warning",
            buttons: [
                'No, cancel it!',
                'Yes, I am sure!'
            ],
            dangerMode: true
        }).then(function(isConfirm) {
            if (isConfirm) {

                currentGroup.members[''+user.id+''].status=2;
                updateGroup(currentGroup);
                swal({
                    title: 'Archived!',
                    text: 'Group Chat is successfully archived!',
                    icon: 'success',
                    timer: 2000,
                    button: false
                }).then(function() {
                });
                $('#editGroupModal .btn-close').click();
                disableChatArea();
            }
        });
    }

   // Unarchieve Group
    function unarchiveGroup()
    {
        swal({
            title: "",
            text: "Are you sure want to unarchive this group.?",
            icon: "warning",
            buttons: [
                'No, cancel it!',
                'Yes, I am sure!'
            ],
            dangerMode: true
        }).then(function(isConfirm) {
            if (isConfirm) {

                currentGroup.members[''+user.id+''].status=1;
                updateGroup(currentGroup);
                swal({
                    title: 'Unarchived!',
                    text: 'Group Chat is successfully unarchived!',
                    icon: 'success',
                    timer: 2000,
                    button: false
                }).then(function() {
                });
                $('#editGroupModal .btn-close').click();
                $('#chat-msg-area').html('');
                renderMessages();
                disableChatArea();
            }
        });
    }

   // Delete Group

    function deleteGroup()
    {
        swal({
            title: "",
            text: "Are you sure want to delete this group !",
            icon: "warning",
            buttons: [
                'No, cancel it!',
                'Yes, I am sure!'
            ],
            dangerMode: true
        }).then(function(isConfirm) {
            if (isConfirm) {

                currentGroup.status=0;
                updateGroup(currentGroup);
                swal({
                    title: 'Deleted!',
                    text: 'Group Chat is successfully deleted!',
                    icon: 'success',
                    timer: 2000,
                    button: false
                }).then(function() {
                });
                $('#editGroupModal .btn-close').click();
                disableChatArea();
            }
        });

    }


    //  ================================================ Create Group ======================================================

    function createGroupOnButton() {

        currentGroup=undefined;
        const message = $('#group_message').val();
        const groupName = $('#group-name').val();
        const team_type=$('input[name=team_type]:checked', '#addForm').val();
        const friendIds = [];
        $("input:checkbox[name=friend-select-group]:checked").each(function(){
            friendIds.push($(this).val());
        });
        if (groupName == '') {
            $('#newGroupModal .show-message').html('<div class="alert alert-danger">Please enter team name</div>'); return;
        }
        document.getElementById('Submit_create').setAttribute("disabled", "disabled");
        var members = {};
        var youExists='no';
        var admin=1;
        if (friendIds.length>0) {

            //  If I don't exist all will be admin.
            _.forEach(friendIds, function (friendId){

                if(friendId==user.id)
                {
                    youExists='yes';
                    admin=0;
                }

            });
            // set other user
            _.forEach(friendIds, function (friendId){

                if(friendId==user.id)
                {
                    members[user.id] = user;
                }
                else
                {
                    const friendObj = _.filter(friends, friend => friend.user_id == friendId);
                    members[friendObj[0].user_id] = { id: friendObj[0].user_id, name: friendObj[0].fname+' '+friendObj[0].lname,admin:admin,status:1,leave:0,read:0,notify:'on'};
                }
            });

            var files = $('#create_group_img')[0].files;
            if(files.length > 0 ){
                var fd = new FormData();
                fd.append('attachment',files[0]);
                fd.append('_token', '{{ csrf_token() }}');

                $.ajax({
                    url: BASE_URL+'/chat/groups/upload',
                    type: 'post',
                    data: fd,
                    contentType: false,
                    processData: false,
                    beforeSend: function () {
                        swal({
                            title: 'Please Wait!',
                            button: false
                        }).then(
                            function () {},
                            // handling the promise rejection
                            function (dismiss) {
                            }
                        )
                    },
                    success: function(response){
                        createGroup(members, user.id, groupName, 'group', response.path,team_type).then(function(groupResponse) {
                            currentGroup=groupResponse;
                            $('#newGroupModal .show-message').html('');
                            $("#create_group_img").val('');
                            $('#group-name').val('');
                            $('#group_message').val('');
                            document.getElementById('create-image').src='{{ asset('public/sports/images') }}/default-group.avatar.png';
                            $('#newGroupModal .btn-close').click();
                            _.forEach(members, function (member) {
                                if (member.id == user.id) return;
                                saveMessage('((added_group_undefined))', new Date(), groupResponse.id, member.name);
                            });

                            setTimeout(function() {
                                if (message != '') saveMessage(message, new Date(), groupResponse.id);
                                if(youExists=='yes'){
                                    $('#editGroupM').css('display','block');
                                    $('#smsGroupM').css('display','block');
                                    renderChat(groupResponse.id,1);
                                }
                                else
                                {
                                    groupResponse.members[''+user.id+''].leave=1;
                                    updateGroup(groupResponse);
                                }
                                $('#newGroupModal .btn-primary').prop('disabled', false);
                            }, 1500);
                        });
                        swal({
                            title: '',
                            text: 'Group Created Successfully',
                            button: false,
                            icon:'success',
                            timer:1000
                        });
                    },
                });
                return;

        }

            createGroup(members, user.id, groupName, 'group','',team_type).then(function(groupResponse) {
                currentGroup=groupResponse;
                $('#newGroupModal .show-message').html('');
                $("#create_group_img").val('');
                document.getElementById('create-image').src='{{ asset('public/sports/images') }}/default-group.avatar.png';
                $('#group-name').val('');
                $('#group_message').val('');
                $('#newGroupModal .btn-close').click();
                _.forEach(members, function (member) {
                    if (member.id == user.id) return;
                    saveMessage('((added_group_undefined))', new Date(), groupResponse.id, member.name);
                });

                setTimeout(function() {
                    if (message != '') saveMessage(message, new Date(), groupResponse.id);
                    if(youExists=='yes'){
                        $('#editGroupM').css('display','block');
                        $('#smsGroupM').css('display','block');
                        renderChat(groupResponse.id,1);
                    }
                    else
                    {
                        groupResponse.members[''+user.id+''].leave=1;
                        updateGroup(groupResponse);
                    }
                    $('#newGroupModal .btn-primary').prop('disabled', false);

                    swal({
                        title: '',
                        text: 'Group Created Successfully',
                        button: false,
                        icon:'success',
                        timer:1000
                    });
                }, 1000);

            });
        }
        else
        {
            $('#newGroupModal .show-message').html('<div class="alert alert-danger">Invalid Action</div>'); return;
        }
    }

    function readCURL(input) {
        if (input.files && input.files[0])
        {
            var reader = new FileReader();
            reader.onload = function (e) {
                $('#create-image').attr('src' , e.target.result);
            };
            reader.readAsDataURL(input.files[0]);
        }

    }

    function readeditCURL(input) {
        if (input.files && input.files[0])
        {
            var reader = new FileReader();
            reader.onload = function (e) {
                $('#edit-image').attr('src' , e.target.result);
            };
            reader.readAsDataURL(input.files[0]);
        }

    }

    function loadTeams() {
        $('#no-group-message').css('display', 'none');
        $('#pills-chat-tab').removeClass('active');
        $('#pills-chat').removeClass('active');
        $('#pills-chat').removeClass('show');


        $('#pills-archives-tab').removeClass('active');
        $('#pills-archives').removeClass('active');
        $('#pills-archives').removeClass('show');
        $('#list-tab-archive').html('');
        $('#list-tab-group').html('');
        $('#list-tab').html('');

        $('#pills-groups-tab').addClass('active');
        $('#pills-groups').addClass('active');
        $('#pills-groups').addClass('show');

        $('.full_loader').css('display', 'none');

        if(archive!='2')
        {
            currentGroup=undefined;
        }

        $('#list-tab-group').html('');
        sessionStorage.setItem("loadChat",'Groups');
        sessionStorage.setItem("PreTab",'3');
        load='2';
        disableChatArea();

        load='2';
        chatList="Groups";
        $.ajax({
            method: 'GET',
            url: BASE_URL+'/users/list',
            data: {
                _token: '{{ csrf_token() }}'
            }
            ,
            beforeSend: function () {
                enableGroupLoaderSideArea();
            }
        }).done(function (result){
            allUsers = result;
            fetchGroupByUserID(user.id);
          
        });
    }


    function loadArchives() {
        $('#no-group-message').css('display', 'none');
        $('#pills-chat-tab').removeClass('active');
        $('#pills-chat').removeClass('active');
        $('#pills-chat').removeClass('show');

        $('#pills-archives-tab').addClass('active');
        $('#pills-archives').addClass('active');
        $('#pills-archives').addClass('show');
        $('#list-tab-archive').html('');
        $('#list-tab-group').html('');
        $('#list-tab').html('');

        $('#pills-groups-tab').removeClass('active');
        $('#pills-groups').removeClass('active');
        $('#pills-groups').removeClass('show');

        load='2';
        chatList="Archive";

        $.ajax({
            method: 'GET',
            url: BASE_URL+'/users/list',
            data: {
                _token: '{{ csrf_token() }}'
            }
            ,
            beforeSend: function () {
                enableArchiveLoaderSideArea();
            }
        }).done(function (result){
            allUsers = result;
            renderChatListArchive1();

        });
    }
    function loadspecialist() {
        $('#no-group-message').css('display', 'none');
        $('#pills-booking-tab').addClass('active');
        $('#pills-booking').addClass('active');
        $('#pills-booking').addClass('show');


        $('#pills-chat-tab').removeClass('active');
        $('#pills-chat').removeClass('active');
        $('#pills-chat').removeClass('show');

        $('#pills-archives-tab').removeClass('active');
        $('#pills-archives').removeClass('active');
        $('#pills-archives').removeClass('show');
        $('#list-tab-archive').html('');
        $('#list-tab-group').html('');
        $('#list-tab').html('');

        $('#pills-groups-tab').removeClass('active');
        $('#pills-groups').removeClass('active');
        $('#pills-groups').removeClass('show');
    }

    function loadChat()
    {
        loadChatMore='0';
        $('#no-group-message').css('display', 'none');
        $('#pills-chat-tab').addClass('active');
        $('#pills-chat').addClass('active');
        $('#pills-chat').addClass('show');

        $('#pills-archives-tab').removeClass('active');
        $('#pills-archives').removeClass('active');
        $('#pills-archives').removeClass('show');
        $('#list-tab-archive').html('');
        $('#list-tab-group').html('');
        $('#list-tab').html('');

        $('#pills-groups-tab').removeClass('active');
        $('#pills-groups').removeClass('active');
        $('#pills-groups').removeClass('show');

        load='2';
        chatList="Chat";

        $('.full_loader').css('display', 'none');
        sessionStorage.setItem("loadChat",'Chat');
        sessionStorage.setItem("PreTab",'2');
        chatList="Chat";
        currentGroup=undefined;
        load='1';
        disableChatArea();

        $.ajax({
            method: 'GET',
            url: BASE_URL+'/users/list',
            data: {
                _token: '{{ csrf_token() }}'
            }
            ,
            beforeSend: function () {
                enableChatLoaderSideArea();
            }
        }).done(function (result){
            allUsers = result;
            fetchIndividualByUserID(user.id);
        });
    }

    function sessionGroups()
    {
        var groupIDOnSession= sessionStorage.getItem("groupID");
        // If Friend Click
        if(groupIDOnSession)
        {
                renderChat(groupIDOnSession,1);
        }
        sessionStorage.setItem("groupID", '');

    }


    var lastScrollTop = 0;
    //    Message Scroll Up

    $('.chat-conversation .simplebar-content-wrapper').scroll(function(event)
    {
//        var st = $(this).scrollTop();
        if ($('.chat-conversation .simplebar-content-wrapper').scrollTop() == 0){
            lastScrollTop = 0;
            if(chatScrollFine==1 && chatUpNotAllowed==0)
            {

                if(countMessages!='new')
                {
                    if(countMessages>0) {
                        chatScrollFine = 0;
                        chatUpNotAllowed = 1;

                        countMessages = countMessages - 2;
                        $('.chatloader').css('display', 'block');
                        db.collection('message')
                            .doc(currentGroup.id.trim())
                            .collection('messages')
                            .orderBy('sentAt', 'desc')
                            .where('sentAt', '<', lastMessageTimeStamp)
                            .limit(2)
                            .onSnapshot((querySnapshot) => {
                            const allMessages = []
                            querySnapshot.forEach((doc) => {
                            if(doc) allMessages.push(doc.data())
                    })
                        messages = allMessages;
                        //  Check if group leave
                        var left = currentGroup.members[user.id].leave;
                        if (left == 0) {
                            renderMessages();
                            chatUpNotAllowed = 0;
                            $('.chat-conversation .simplebar-content-wrapper').scrollTop(30);
                        }
                    })
                    }
                }
                else if(countMessages=='new')
                {
//                ----------------------------------
                    db.collection('message')
                        .doc(currentGroup.id.trim())
                        .collection('messages')
                        .orderBy('sentAt','desc')
                        .onSnapshot((querySnapshot) => {
                        const alllMessages = []
                        querySnapshot.forEach((doc) => {
                        if (doc) alllMessages.push(doc.data())
                })

                  if(chatUpNotAllowed==0)
                  {
                      mmessages = alllMessages;
                      countMessages=mmessages.length;

                      if(countMessages!=8)
                      {
                          countMessages=countMessages-10;
                          chatScrollFine=0;
                          chatUpNotAllowed=1;
                          $('.chatloader').css('display', 'block');
                          db.collection('message')
                              .doc(currentGroup.id.trim())
                              .collection('messages')
                              .orderBy('sentAt','desc')
                              .where('sentAt','<',lastMessageTimeStamp)
                              .limit(2)
                              .onSnapshot((querySnapshot) => {
                              const allMessages = []
                              querySnapshot.forEach((doc) => {
                              if (doc) allMessages.push(doc.data())
                      })
                          messages = allMessages;
                          //  Check if group leave
                          var left=currentGroup.members[user.id].leave;
                          if(left==0)
                          {
                              renderMessages();
                              chatUpNotAllowed=0;
                              $('.chat-conversation .simplebar-content-wrapper').scrollTop(30);
                          }
                      })
                      }
                  }

                })
//                -----------------------------------------

                }

            }

        }
//        lastScrollTop = st;
    });
</script>



{{--Scroll Work--}}




