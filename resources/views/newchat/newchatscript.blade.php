<script>
    var el = document.querySelector(".input-wrap .input");
    var widthMachine = document.querySelector(".input-wrap .width-machine");
    el.addEventListener("keyup", function() {
        widthMachine.innerHTML = el.value;
    });

    // Dealing with Textarea Height
    function calcHeight(value) {
        var numberOfLineBreaks = (value.match(/\n/g) || []).length;
        // min-height + lines x line-height + padding + border
        var newHeight = 20 + numberOfLineBreaks * 20 + 12 + 2;
        return newHeight;
    }

    var textarea = document.querySelector(".resize-ta");
    textarea.addEventListener("keyup", function() {
        textarea.style.height = calcHeight(textarea.value) + "px";
    });
</script>

<script>
    function open_menu_bar () {
        if($("nav").hasClass('activemenu')){
            document.body.classList.remove("remove-scrolling");
            $("nav").removeClass('activemenu');

        }else{
            document.body.classList.add("remove-scrolling");
            $("nav").addClass('activemenu');
        }
    }
    //        $("nav").toggleClass("active");
    //    }
</script>

<script type="text/javascript">
    $(document).on('click', function(e) {
        if ( $(e.target).closest('.user-info').length ) {
            $('.user-account-settingss').toggle();
        }
        else if ( ! $(e.target).closest('.user-info').length ) {
            $('.user-account-settingss').hide();
        }
    });
</script>

<script type="text/javascript">
    $(document).on('click', function(e) {
        if ( $(e.target).closest('.notification-icon').length ) {
            $('.notification-box').toggle();
        }
        else if ( ! $(e.target).closest('.notification-icon').length ) {
            $('.notification-box').hide();
        }
    });
</script>
<script>
    $(document).ready(function() {
        $('.search-bar').on('click', function() {
            $('.inner-search').show();
        });
    });
</script>
<script>
    $(document).on('click', function(e) {
        if( $(e.target).closest('.back-to-normal').length ) {
            $('.inner-search').hide();
        }
    });
</script>

<script type="text/javascript">
    $(document).on('click', function(e) {
        if ( $(e.target).closest('.ed-opts').length ) {
            $('.ed-options').addClass("active");
        }
        else if ( ! $(e.target).closest('.ed-opts').length ) {
            $('.ed-options').removeClass("active");
        }
    });
</script>

<script>
    var enterKey = 13;
    $(document).keydown(function(e) {
        if (e.keyCode == enterKey) {
            $('.emojionearea .emojionearea-picker').css.display = "none";
            $('.emojionearea-button').removeClass('active')
        }
    });
</script>