<style>
    .post-bar {
    float: left;
    width: 100%;
    background-color: #fff;
    margin-bottom: 20px;
    padding: 20px;
    box-shadow: 0 0 2px rgba(0, 0, 0, .28);
    border-radius: 10px;
}
.epi-sec {
    float: left;
    width: 100%;
    padding: 0
}

.ed-opts {
    float: right;
    position: relative;
    left: 4px;
    margin-top: 6px;
    cursor: pointer
}
.job_descp {
    float: left;
    width: 100%
}

.job_descp h3 {
    color: #333 !important;
    font-size: 16px !important;
    font-weight: 600 !important;
    margin-bottom: 15px !important;
}

.post-bar-title{
    box-shadow: none !important;
    border-left: 4px solid #e4e4e4;
}
.post_topbar {
    float: left;
    width: 100%;
    position: relative
}
.usy-dt {
    float: left;
    width: 100%;
}
.usy-name img {
    margin-top: 4px;
    margin-right: 6px
}
.usy-name{
    float:left;
    margin-left:15px;
    margin-top: 10px;
}
.ed-options li {
    float: left;
    width: 100%;
}

.ed-options li:last-child {
    margin-bottom: 0
}
.ed-options{
    position:absolute;
    top:100%;
    right:9px;
    width: 110px !important;
    background-color:#fff;
    -webkit-box-shadow:0 0 2px rgba(0,0,0,.28);
    -moz-box-shadow:0 0 2px rgba(0,0,0,.28);
    -ms-box-shadow:0 0 2px rgba(0,0,0,.28);
    -o-box-shadow:0 0 2px rgba(0,0,0,.28);
    box-shadow:0 0 2px rgba(0,0,0,.28);
    opacity:0;
    visibility:hidden;
    z-index:0;
    margin-top: 10px;
    cursor: pointer;
}
ul {
  list-style-type: none;
  padding: 0;
}
.ed-options.active{
    opacity:1;
    visibility:visible;
    z-index:999;
}
.ed-options li {
    padding: 10px;
}
.ed-options li:hover {
    background-color: #f1f1f1;
}
.ed-options li a{
    color:#555;
    font-weight:400;
    cursor: pointer;
    font-size: 10px;
}
.ed-options li a .fa {
    background-color: #e4e4e4;
    padding: 5px 6px;
    border-radius: 100px;
    margin-right: 6px;
    font-size: 12px;
    top: 0 !important;
}
.usy-name h3, .request-info h3{
    color:#000;
    font-size:14px;
    text-transform:capitalize;
    font-weight:400;
    margin-bottom:2px;
}
.top-profiles {
    float: left;
    width: 100%;
    border: 1px solid #e5e5e5;
    margin-bottom: 20px
}
.post-popup {
    width: 570px;
    margin: 0 auto;
    position: fixed;
    top: 50%;
    left: 50%;
    -webkit-transform: translateX(-50%) translateY(-50%) scale(.65);
    -moz-transform: translateX(-50%) translateY(-50%) scale(.65);
    -ms-transform: translateX(-50%) translateY(-50%) scale(.65);
    -o-transform: translateX(-50%) translateY(-50%) scale(.65);
    transform: translateX(-50%) translateY(-50%) scale(.65);
    opacity: 0;
    visibility: hidden;
    z-index: 0
}
.post-popup.active {
    opacity: 1;
    visibility: visible;
    z-index: 999;
    -webkit-transform: scale(1) translateX(-50%) translateY(-50%)
}
.ed-options li {
    float: left;
    width: 100%;
}

.ed-options li:last-child {
    margin-bottom: 0
}
.usy-name span{
    color:#555;
    font-size:10px;
}
.usy-dt img, .noty-user-img img {
    width: 40px;
    height: 40px;
    background-position: center;
    background-repeat: no-repeat;
    background-size: contain !important;
    object-fit: cover !important;
    -webkit-border-radius:50%;
    -moz-border-radius:50%;
    border-radius:50%;
    margin-top:2px
}
.pointer-cursor
{
    cursor: pointer;
}
.job_descp > p {
    color: #666 !important;
    font-size: 15px !important;
    line-height: 24px !important;
    margin-bottom: 20px !important;
    overflow-wrap: break-word !important;
}

.job_descp > p a {
    color: #dc3545 !important;
    font-weight: 600 !important;

}

</style>

 

  

  @foreach($exp_posts as $key => $post)

  @php
      $postimages=\App\Models\Admin\PostImage::select('image','id')->where('post_id',$post->id)->where('user_id',$post->user_id)->get();
  @endphp

  <div class="post-bar rounded-0 post-bar-title overflow-auto">
      <div class="post_topbar">
          <div class="usy-dt">
              @if($post->propicture=='yes')
                  <a href="{{route('user.individualprofile',$post->user_link)}}"> <img src="{{url('/').'/public/storage/users/'.$post->user_id.'/profile_picture.jpg'}}?v={{time()}}" alt=""></a>
              @else
                  <a href="{{route('user.individualprofile',$post->user_link)}}"><img src="{{ asset('public/uploads/white.jpg') }}" alt=""></a>
              @endif

              <div class="usy-name">
                  <a href="{{route('user.individualprofile',$post->user_link)}}"><h3>{{ $post->fname}} {{$post->lname}}</h3></a>
                  <span>{{date_format($post->created_at,'F d, Y ')}}</span>
              </div>

                  @if($post->user_id==Auth::user()->id)
                      <div class="ed-opts">
                          <div class="popup ed-opts-open" id="popup-{{$post->id}}"><i class="la la-ellipsis-v"></i></div>
                          <ul class="ed-options" id="subpopup-{{$post->id}}">
                              <li onclick="deletedata('{{$post->id}}')"><a class="pointer-cursor"><i class="fa fa-trash" ></i>Delete Post</a></li>
                              <li onclick="fetchdata('{{$post->id}}')" data-bs-toggle="modal" data-bs-target="#editPost"><a><i class="fa fa-pencil"></i>Update Post</a></li>
                          </ul>
                      </div>

                      <script>
                          $("#popup-"+<?php echo json_encode($post->id); ?>).on("click",function(){
                              $(this).next(".ed-options").toggleClass("active");return false;
                          })
                      </script>
                 @endif
          </div>


      </div>
      <div class="epi-sec">
          <div class="job_descp">
          @if ($post->caption != null)
            <h1>{{$post->title}}</h1>
              <a href="{{  route('user.individualprofile',[$post->user_link,'random'=>$post->id]) }}">
                  <p style="font-size: 15px !important; color: #999 !important;">{{ \Illuminate\Support\Str::limit($post->caption, 100, $end= '...') }}
                      <span style="font-weight: bold;">Read More</span>
                  </p>
              </a>
              @else
              <h1>{{$post->title}}</h1>
              <a href="{{ route('user.individualprofile',[$post->user_link,'random'=>$post->id]) }}">
                  <p style="font-size: 15px !important;">{{ \Illuminate\Support\Str::limit($post->caption, 100, $end= '...') }}
                  </p>
              </a>
          @endif
            
              @if(count($postimages)>0)

                  <div class="top-profiles">
                      <div id="imges-{{$post->id}}" ></div>
                      <script type="text/javascript">

                           obj = <?php echo json_encode($postimages); ?>;
                           s= <?php echo json_encode($post->id); ?>;
                           const arrayyy<?php echo json_encode($post->id); ?>=[];
                          for(var i=0;i<obj.length;i++)
                          {
                              arrayyy<?php echo json_encode($post->id); ?>.push(<?php echo json_encode(url('/').'/public/storage/posts/'); ?>+'/'+obj[i].image);
                          }
                          $(function() {
                              $('#imges-'+<?php echo json_encode($post->id); ?>).imagesGrid({
                                  images: arrayyy<?php echo json_encode($post->id); ?>,
                                  align: true,
                                  getViewAllText: function(imgsCount) { return 'View all' }
                              });
                          });
                      </script>
                  </div>
              @endif
          </div>
          {{--<div class="job-status-bar">--}}
              {{--<ul class="like-com">--}}
              {{--</ul>--}}
          {{--</div>--}}
      </div>
  </div>
@endforeach
