<div class="modal fade" id="invites" tabindex="-1" role="dialog" aria-labelledby="invitefriends-exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered modal-dialog-scrollable">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title font-size-16" id="invitefriends-exampleModalLabel">Invite Friends</h5>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close">
                </button>
            </div>
            {!! Form::open(['method' => 'POST', 'id'=>'invites_' ,'route' => ['user.invites'],'data-parsley-validate','files' => 'true','enctype'=>'multipart/form-data']) !!}
                <div class="modal-body p-4">
                    <span id="mobile_error_message" style="font-size: 11px;margin-top: 2px;height: 10px"></span>
                    {!! Form::text('id', '', [ 'id'=>'id_to_invite', 'class'=>'form-control'  , 'required'=>'true','hidden'=>'true']) !!}
                    <div class="mb-4">
                        <label class="form-label">Phone Number </label>
                        <div class="row">
                            <div class="col-sm-4 col-12 pr-sm-1">
                                {!! Form::select('code', config('status.code'),'', [ 'id'=>'code', 'class'=>'form-control' , 'required'=>'']) !!}
                            </div>
                            <div class="col-sm-8 col-12">
                                {!! Form::number('mob_', '', [ 'id'=>'mob_', 'class'=>'form-control' , 'placeholder' => 'Enter phone number' , 'required'=>'']) !!}
                            </div>
                        </div>
                    </div>
                    <p class="text-center">OR</p>
                    <span id="email_error_message" style="font-size: 11px;margin-top: 2px;height: 10px"></span>
                    <div class="mb-4">
                        <label class="form-label">Email </label>
                        {!! Form::text('email_', '', [ 'id'=>'email_', 'class'=>'form-control' , 'placeholder' => 'Enter email' , 'required'=>'']) !!}
                    </div>
                </div>
                <div class="modal-footer">
                    {{--<button type="button" class="btn btn-danger" data-bs-dismiss="modal">Close</button>--}}
                    <button type="button" id="invite_button_site" class="btn btn-primary w-100" onclick="sendinvite()">Send Invite</button>
                </div>
            {!! Form::close() !!}
        </div>
    </div>
</div>