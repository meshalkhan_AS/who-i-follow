@extends('store.front.layout.master') 

@section('content')
<style type="text/css">
    .rating{
        display: none;
    }

    .wishlist{
        display: none;
    }

    .compare{
        display: none;
    }
    
    /*img.img-responsive {
       height: 100%;
       width: 100%;
   }*/
</style>
<!-- =====  LODER  ===== -->
<div class="loder"></div>

<div class="container-fluid mt_80">
      <div class="row ">
        <!-- =====  BANNER STRAT  ===== -->
        <div class="col-sm-12">
          <div class="breadcrumb ptb_20">
            <h1>Shopping Cart</h1>
           <!--  <ul>
              <li><a href="{{route('front.shop')}}">Home</a></li>
              <li class="active">Shopping Cart</li>
            </ul> -->
          </div>
        </div>
        <!-- =====  BREADCRUMB END===== -->
     <div style="display: none;">  @include('store.front.layout.aside') </div>
        <div class="col-sm-8 col-lg-7 mtb_20">
          <div class="panel-group" id="accordion">
            <div class="panel panel-default  border-0">
              <div class="panel-heading">
                <h4 class="panel-title"> <a data-toggle="collapse" data-parent="#accordion" href="#collapseOne">Shipping Address </a></h4>
                @if ($message = Session::get('success'))
                <div class="alert alert-success alert-block">
                    <button type="button" class="close" data-dismiss="alert">x</button> 
                    <strong>{{ $message }}</strong>
                </div>
                @endif

                @if ($errors->any())
                <div class="alert alert-danger">
                  <ul>
                    @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                    @endforeach
                  </ul>
                </div><br />
                @endif 
              </div>
              <div id="collapseOne" class="panel-collapse collapse in">
                <div class="panel-body border-0">
                  <form class="form" action="{{route('front.store.processCheckout')}}" method="post">
                    @csrf
                   
                    <div class="col-sm-6">
   
                      <div class="form-group">
                        <label for="input-name" class="control-label">Full Name *</label>
                      <input type="text" class="form-control" id="input-name" placeholder="Full Name" value="{{$userprofile->fname}}" name="name">
                      </div>
                      
                    </div>

                    <div class="col-sm-6">
   
                      <div class="form-group">
                        <label for="input-email" class="control-label">Email *</label>
                        <input type="text" class="form-control" id="input-email" placeholder="E-Mail" value="{{$userprofile->email}}" name="email">
                      </div>
                      
                    </div>




                    <div class="col-sm-8">
   
                      <div class="form-group">
                        <label for="input-address" class="control-label">Address *</label>
                        <input type="text" class="form-control" id="input-address" placeholder="Address" value="{{$userprofile->hometown}}" name="address">
                      </div>
                      
                    </div>


                    <div class="col-sm-4">
   
                      <div class="form-group">
                        <label for="input-number" class="control-label">Number</label>
                        <input type="text" class="form-control" id="input-number" placeholder="Number" value="{{$userprofile->mob}}" name="number">
                      </div>
                      
                    </div>


                    <div class="col-sm-5">
   
                      <div class="form-group">
                        <label class="form-control-label control-label" for="country" class="fontsize13px">Billing country *</label>
                        <select name="country" id="country" class="w-100" style="height: 38px;">
                          @if($userprofile->country==null)
                          <option value="">Select Country</option>
                          @else
                          <option value="{{$userprofile->country}}">{{$userprofile->country}}</option>
                          @endif
                          @foreach ($countries as $country)
                            <option value="{{$country}}">{{$country}}</option>
                          @endforeach
                        </select>
                      </div>
                      
                    </div>

                    <div class="col-sm-7">
   
                      <div class="form-group">
                        <label for="input-city" class="control-label">City *</label>
                        <input type="text" class="form-control" id="input-city" placeholder="City" value="{{$userprofile->city}}" name="city">
                      </div>
                      
                    </div>


                    <div class="col-sm-6">
   
                      <div class="form-group">
                        <label for="input-State" class="control-label">State *</label>
                        <input type="text" class="form-control" id="input-state" placeholder="State" value="{{$userprofile->state}}" name="state">
                      </div>
                      
                    </div>


                    <div class="col-sm-6">
   
                      <div class="form-group">
                        <label for="input-zip" class="control-label">Zip *</label>
                        <input type="text" class="form-control" id="input-zip" placeholder="zip" value="{{$userprofile->zip_code}}" name="zip">
                      </div>
                      
                    </div>






                    <div class="buttons clearfix">
                      <!-- <div class="col-md-12 pull-left mt_20"> -->
                        <div  class="col-md-12 mt_20 text-center">
                        <input type="submit" class="btn checkout-btn" data-loading-text="Loading..." id="button-payment-address" value="Proceed to Checkout">
                      </div>
                    </div>
                  </form>
                </div>
              </div>
            </div>
            
          </div>
        </div>


        <div class="col-lg-5">
            <div class="col-lg-12" style="padding: 0px;">
                  <h6 class="order-title border-bottom"> Your Order </h6>

                  @if(count($cart_content))

                  @foreach($cart_content as $cartData)
                  <div class="product-summary w-100">
                      <div class="col-sm-2" style="padding: 0px;">

                       
                           <img src="{{ asset($cartData->attributes->image) }}" class="img-responsive">
                  
                           <!-- <img src="{{ asset('public/front/images/placeholder.png') }}" class="img-responsive"> -->
                       
                      </div>  
                      <div class="col-sm-7">
                          <h5>{{$cartData->name}}</h5>
                          <p class="sm-price"> ${{$cartData->price*$cartData->quantity}}

                            <div id="field1" class="qty-minus">
                                  <button type="button" id="sub" class="sub">x</button>
                                  <input type="number" id="1" value="{{$cartData->quantity}}" min="1" max="100" disabled="" />
                                  <!-- <button type="button" id="add" class="add">+</button> -->
                              </div>
                          </p>
                      </div>
                      <div class="col-sm-3 text-right hide">
                            <p>Size <span>M</span></p><p>Color <span>Blue</span></p>
                            <a href="#" class="btn del-btn">Delete</a>
                      </div>
                  </div>

                  @endforeach
                  @endif


                  <!-- <div class="product-summary w-100">
                      <div class="col-sm-2" style="padding: 0px;">
                          <img src="{{ asset('images/sport1.jpg') }}" class="img-responsive">
                      </div>  
                      <div class="col-sm-7">
                          <h5>Rattlers - All Star</h5>
                          <p class="sm-price"> $19.23</p>
                      </div>
                      <div class="col-sm-3 text-right">
                            <p>Size <span>M</span></p><p>Color <span>Blue</span></p>
                            <a href="#" class="btn del-btn">Delete</a>
                      </div>
                  </div>
 -->

                  <div class="w-100 total-checkout hide">
                      <h5><span> Delivery:</span> $20</h5>
                      <h5><span>Discount:</span> -$10 </h5>
                  </div>
                  <div class="total-amount" style="width: 85%;">
                    <h3>Total <span class="float-right">${{$total_price}}</span></h3>
                  </div>
            </div>
        </div>

      </div>
   
</div>



@stop 

@section('scripts')
<script src="{{asset('public/front')}}/js/jquery-ui.js"></script>
<script type="text/javascript">
  $('.close-cart').click(function(){

    var deleteid = $(this).attr('data-delcart');

    $(".row-del-"+deleteid).remove();
    // alert(1);
  });
</script>

  
  <script type="text/javascript">
        $('.add').click(function () {
    if ($(this).prev().val() < 200) {
      $(this).prev().val(+$(this).prev().val() + 1);
    }
    });
    $('.sub').click(function () {
        if ($(this).next().val() > 1) {
          if ($(this).next().val() > 1) $(this).next().val(+$(this).next().val() - 1);
        }
    });
  </script>

@stop