@extends('store.front.layout.master') 

@section('content')
<style type="text/css">
    .rating{
        display: none;
    }

    .wishlist{
        display: none;
    }

    .compare{
        display: none;
    }

    .border {
       border: solid 1px #eee;
       padding: 12px;
       border-bottom: 3px solid;
       margin-bottom: 10px;
      }
    
    /*img.img-responsive {
       height: 100%;
       width: 100%;
   }*/
</style>
<!-- =====  LODER  ===== -->
<div class="loder"></div>

<div class="container">
      <div class="row ">
        <!-- =====  BANNER STRAT  ===== -->
        <div class="col-sm-12">
          <div class="breadcrumb ptb_20">
            <h1>Orders</h1>
          <!--   <ul>
              <li><a href="{{route('front.shop')}}">Home</a></li>
              <li class="active">Shopping Cart</li>
            </ul> -->
          </div>
        </div>
        <!-- =====  BREADCRUMB END===== -->
       
       <div style="display: none;"> @include('store.front.layout.aside')
       </div>
       <div class="col-sm-12 col-lg-12 mtb_20" style="text-align: center;">
       <button class="toggleAll btn btn-default" id="loadact1"  style="font-size: xx-small;">All Orders</button>
       <button class="togglepast btn btn-default" id="loadact" style="font-size: xx-small;">Active Orders</button>
       <button class="toggle btn btn-default" id="loadact2" style="font-size: xx-small;">Past Orders</button>
       </div>
        <div class="col-sm-12 col-lg-12 mtb_20" id="target"style="border-bottom: 3px solid #969696;">
          
          <form enctype="multipart/form-data" method="post" action="#">
            @if(count($orders)>0)
            <div class="table-responsive">
                @foreach($orders as $order)
                @if (ucfirst($order->status)!='Delivered')
                  @if(!is_null($order->ordered_items))
                  <div class="border">
               <div class="col">
                  <table>
                     <thead>
                        <tr>
                           <td class="text-left">Order No:</td>
                           <td class="text-left" style="padding-left: 12px"> {{$order->invoice_id}}</td>
                           
                        </tr>
                        <tr>
                          <td class="text-left">Transaction ID:</td>
                          <td class="text-left" style="padding-left: 12px"> {{$order->pp_invoice_id}}</td>
                          
                       </tr>

                        <tr>
                           <td class="text-left">Status:</td>
                           <td class="text-left" style="padding-left: 12px; color:green;"> {{ucfirst($order->status)}}</td>
                           
                        </tr>
                        <tr>
                          <td class="text-left">Order Total:</td>
                          <td class="text-left" style="padding-left: 12px;"> {{$order->discounted_subtotal}}</td>
                          
                       </tr>
                        <tr>
                          <td class="text-left">Order Date:</td>
                          <td class="text-left" style="padding-left: 12px"> {{$order->created_at->format('d/m/Y')}}</td>
                          
                       </tr>
                     </thead>
                  </table>
               </div>
              <table class="table table-bordered">
                <thead>
                  <tr>
                    <!-- <td class="text-center">Image</td> -->
                    <td class="text-center"  width="14%">Product</td>
                    <!-- <td class="text-left">Model</td> -->
                    <td class="text-center"  width="100px">Quantity</td>
                    <td class="text-center"  width="100px">Unit Price</td>
                    <td class="text-center"  width="100px">Total</td>
                    <!-- <td class="text-center">Status</td> -->
                  </tr>
                </thead>
                <tbody>

                 

                   <?php
                     $ordered_items=unserialize(base64_decode($order->ordered_items));

                       // echo "<pre>";
                       // print_r($ordered_items);
                   ?>

                   @foreach($ordered_items as $ordered_item)
                   <?php
                   // print_r($ordered_item);
                   // print_r($ordered_item->attributes->image);
                   ?>
                 
                   <tr>
                      <td width="100px">{{$ordered_item->name}}<br>
                        <img src="{{$ordered_item->attributes->image}}" width="100%">
                      </td>
                      <td class="text-center">{{$ordered_item->quantity}}</td>
                      <td class="text-center">{{$ordered_item->price}}</td>
                      <td class="text-center">{{$ordered_item->quantity*$ordered_item->price}}</td>
                      <!-- <td></td> -->
                   </tr>

                   @endforeach

                     

              
                </tbody>
               
              </table>

              </div>

              @endif
              @endif
            @endforeach
            </div>
            @else
            <div class="col-sm-12 col-lg-12 mtb_20">
<p>No Pending Orders Found</p>
            </div>
            @endif
          </form>
         <!--  <h3 class="mtb_10">What would you like to do next?</h3>
          <p>Choose if you have a discount code or reward points you want to use or would like to estimate your delivery cost.</p> -->
       
        
        
        </div>
        <div class="col-sm-12 col-lg-12 mtb_20" id="targetpast">
          
          <form enctype="multipart/form-data" method="post" action="#">
            @if(count($orderspast)>0)
            <div class="table-responsive">

                @foreach($orderspast as $order)
              @if (ucfirst($order->status)=='Delivered'||ucfirst($order->status)=='Rejected')
                  @if(!is_null($order->ordered_items))
                  <div class="border">
               <div class="col">
                  <table>
                     <thead>
                        <tr>
                           <td class="text-left">Order No:</td>
                           <td class="text-left" style="padding-left: 12px"> {{$order->invoice_id}}</td>
                           
                        </tr>
                        <tr>
                          <td class="text-left">Transaction ID:</td>
                          <td class="text-left" style="padding-left: 12px"> {{$order->pp_invoice_id}}</td>
                          
                       </tr>

                        <tr>
                           <td class="text-left">Status:</td>
                           <td class="text-left" style="padding-left: 12px; color:green;"> {{ucfirst($order->status)}}</td>
                           
                        </tr>
                        <tr>
                          <td class="text-left">Order Total:</td>
                          <td class="text-left" style="padding-left: 12px;"> {{$order->discounted_subtotal}}</td>
                          
                       </tr>
                        <tr>
                          <td class="text-left">Order Date:</td>
                          <td class="text-left" style="padding-left: 12px"> {{$order->created_at->format('d/m/Y')}}</td>
                          
                       </tr>

                     </thead>
                  </table>
               </div>
              <table class="table table-bordered">
                <thead>
                  <tr>
                    <!-- <td class="text-center">Image</td> -->
                    <td class="text-center"  width="14%">Product</td>
                    <!-- <td class="text-left">Model</td> -->
                    <td class="text-center"  width="100px">Quantity</td>
                    <td class="text-center"  width="100px">Unit Price</td>
                    <td class="text-center"  width="100px">Total</td>
                    <!-- <td class="text-center">Status</td> -->
                  </tr>
                </thead>
                <tbody>

                 

                   <?php
                     $ordered_items=unserialize(base64_decode($order->ordered_items));

                       // echo "<pre>";
                       // print_r($ordered_items);
                   ?>

                   @foreach($ordered_items as $ordered_item)
                   <?php
                   // print_r($ordered_item);
                   // print_r($ordered_item->attributes->image);
                   ?>
                 
                   <tr>
                      <td width="100px">{{$ordered_item->name}}<br>
                        <img src="{{$ordered_item->attributes->image}}" width="100%">
                      </td>
                      <td class="text-center">{{$ordered_item->quantity}}</td>
                      <td class="text-center">{{$ordered_item->price}}</td>
                      <td class="text-center">{{$ordered_item->quantity*$ordered_item->price}}</td>
                      <!-- <td></td> -->
                   </tr>

                   @endforeach

                     

              
                </tbody>
               
              </table>

              </div>

              @endif
              @endif
            @endforeach
            </div>
            @else
            <div class="col-sm-12 col-lg-12 mtb_20">
<p>No Past Orders Found</p>
            </div>
            @endif
          </form>
         <!--  <h3 class="mtb_10">What would you like to do next?</h3>
          <p>Choose if you have a discount code or reward points you want to use or would like to estimate your delivery cost.</p> -->
       
          
        
        </div>
      </div>
   
    </div>



@stop 

@section('scripts')
<script src="{{asset('public/front')}}/js/jquery-ui.js"></script>
<script type="text/javascript">
  $('.close-cart').click(function(){

    var deleteid = $(this).attr('data-delcart');

    $(".row-del-"+deleteid).remove();
    // alert(1);
  });
  jQuery(function(){
   jQuery('#loadact').click();
});


  $('.toggle').click(function() {
    $('#loadact2').css("background-color","#22559a");
    $('#loadact1').css("background-color","#000000");
    $('#loadact').css("background-color","#000000");
    $('#target').hide();
    $('#targetpast').show();
});
$('.togglepast').click(function() {
  $('#loadact').css("background-color","#22559a");
    $('#loadact2').css("background-color","#000000");
    $('#loadact1').css("background-color","#000000");
    $('#targetpast').hide();
    $('#target').css("border-bottom","none");
    $('#target').show();
});
$('.toggleAll').click(function() {
  $('#loadact').css("background-color","#000000");
  $('#loadact1').css("background-color","#22559a");
  $('#loadact2').css("background-color","#000000");
  $('#target').css("border-bottom","3px solid #969696");
    $('#targetpast').show();
    $('#target').show();
});
</script>
@stop