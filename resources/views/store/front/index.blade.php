@extends('store.front.layout.master') 

@section('content')

<style type="text/css">
    .rating{
        display: none;
    }

    .wishlist{
        display: none;
    }

    .compare{
        display: none;
    }
    
</style>

<?php
  $helper = new \App\Helpers\StoreHelper;
?>


<!-- =====  LODER  ===== -->
<div class="loder"></div>

<div class="wrapper">
    <div id="subscribe-me hide" class="modal animated fade in" role="dialog" data-keyboard="true" tabindex="-1">
        <div class="newsletter-popup">
            <img class="offer" src="{{asset('public/front')}}/images/newsbg.jpg" alt="offer">
            <div class="newsletter-popup-static newsletter-popup-top">
                <div class="popup-text">
                    <div class="popup-title">50% <span>off</span>
                    </div>
                    <div class="popup-desc">
                        <div>Sign up and get 50% off your next Order</div>
                    </div>
                </div>
                <form onsubmit="return  validatpopupemail();" method="post">
                    <div class="form-group required">
                        <input type="email" name="email-popup" id="email-popup" placeholder="Enter Your Email" class="form-control input-lg" required />
                        <button type="submit" class="btn btn-default btn-lg" id="email-popup-submit">Subscribe</button>
                        <label class="checkme">
                            <input type="checkbox" value="" id="checkme" />Dont show again</label>
                    </div>
                </form>
            </div>
        </div>
    </div>
    
    <div class="container banner mt_20 hide">
        <div class="main-banner owl-carousel">
            
            <div class="item">
                <a href="#">
                    <img src="{{asset('public/front')}}/images/main_banner2.jpg" alt="Main Banner" class="img-responsive" />
                </a>
            </div>
        </div>
    </div>
    
    <div class="container-fluid mt_80">
        <!-- =====  SUB BANNER  STRAT ===== -->
        <div class="row hide">
            <div class="cms_banner mt_10">
                <div class="col-xs-4 mt_10">
                    <div id="subbanner1" class="sub-hover">
                        <div class="sub-img">
                            <a href="#">
                                <img src="{{asset('public/front')}}/images/sub1.jpg" alt="Sub Banner1" class="img-responsive">
                            </a>
                        </div>
                        <div class="bannertext text-center">
                            <button class="btn mb_10 cms_btn">View product</button>
                            <h2>Hats & collapse</h2>
                            <p class="mt_10">Wide veriety of sizes,colors</p>
                        </div>
                    </div>
                    <div id="subbanner2" class="sub-hover mt_20">
                        <div class="sub-img">
                            <a href="#">
                                <img src="{{asset('public/front')}}/images/sub2.jpg" alt="Sub Banner2" class="img-responsive">
                            </a>
                        </div>
                        <div class="bannertext text-center">
                            <button class="btn mb_10 cms_btn">View product</button>
                            <h2>Buy Scarfs</h2>
                            <p class="mt_10">Shop collection of designer</p>
                        </div>
                    </div>
                </div>
                <div class="col-xs-4 mt_10">
                    <div id="subbanner3" class="sub-hover">
                        <div class="sub-img">
                            <a href="#">
                                <img src="{{asset('public/front')}}/images/sub3.jpg" alt="Sub Banner3" class="img-responsive">
                            </a>
                        </div>
                    </div>
                </div>
                <div class="col-xs-4 mt_10">
                    <div id="subbanner4" class="sub-hover">
                        <div class="sub-img">
                            <a href="#">
                                <img src="{{asset('public/front')}}/images/sub4.jpg" alt="Sub Banner4" class="img-responsive">
                            </a>
                        </div>
                        <div class="bannertext text-center">
                            <button class="btn mb_10 cms_btn">View product</button>
                            <h2>Handbags</h2>
                            <p class="mt_10">Bags for men & women only</p>
                        </div>
                    </div>
                    <div id="subbanner5" class="sub-hover mt_20">
                        <div class="sub-img">
                            <a href="#">
                                <img src="{{asset('public/front')}}/images/sub5.jpg" alt="Sub Banner5" class="img-responsive">
                            </a>
                        </div>
                        <div class="bannertext text-center">
                            <button class="btn mb_10 cms_btn">View product</button>
                            <h2>Footware</h2>
                            <p class="mt_10">Over 400 luxury designers</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <!-- =====  SUB BANNER END  ===== -->
        <div class="row ">
            @if (session('status'))
    <div class="alert alert-success">
        {{ session('status') }}
    </div>
@endif
            <div class="col-sm-12 mtb_10">
                <!-- =====  PRODUCT TAB  ===== -->
                <div id="product-tab" class="">
                    <div class="heading-part mb_10">
                        <h2 class="main_title">Latest Products</h2>
                    </div>
                  <!--   <ul class="nav text-right">
                        <li class="active"> <a href="#nArrivals" data-toggle="tab">New Arrivals</a> 
                        </li>
                      
                    </ul> -->
                    <div class="tab-content clearfix box pb-3">
                        <div class="tab-pane active" id="nArrivals">
                            <div class="nArrivals">

                               <!--  owl-carousel -->
                                @if(count($helper->lastProducts()))
                                @foreach($helper->lastProducts() as $product)
                                <div class="product-grid col-sm-3">
                                    <div class="item">
                                        <div class="product-thumb">
                                            <div class="image product-imageblock">
                                                <a href="{{route('front.viewProduct',$product->id)}}">

                                                    @if (file_exists($product->image))
                                                    <img data-name="product_image" src="{{asset($product->image)}}" alt="{{$product->name}}" title="{{$product->name}}" class="img-responsive">
                                                    <img src="{{asset($product->image)}}" alt="{{$product->name}}" title="{{$product->name}}" class="img-responsive">
                                                    @else

                                                    <img data-name="product_image" src="{{asset('public/front/images/placeholder.png')}}" alt="{{$product->name}}" title="{{$product->name}}" class="img-responsive" style="height: 216px">
                                                    <img src="{{asset('public/front/images/placeholder.png')}}" alt="{{$product->name}}" title="{{$product->name}}" class="img-responsive"  style="height: 216px">

                                                    @endif

                                                    
                                                </a>

                                                <div class="button-group text-center">
                                                    <!-- <div class="wishlist"><a href="#"><span>wishlist</span></a>
                                                    </div> -->
                                                    <!-- <div class="quickview"><a href="#"><span>Quick View</span></a>
                                                    </div> -->
                                                    <!-- <div class="compare"><a href="#"><span>Compare</span></a>
                                                    </div> -->
                                                    <!-- <div class="add-to-cart" data-cart="{{$product->id}}"><a href="#"><span>Add to cart</span></a>
                                                    </div> -->
                                                </div>
                                            </div>
                                            <div class="caption product-detail text-left">
                                               
                                                <h6 data-name="product_name" class="product-name"><a href="{{route('front.viewProduct',$product->id)}}" title="Casual Shirt With Ruffle Hem">{{$product->name}}</a></h6>
                                                <span class="price"><span class="amount"><span class="currencySymbol">$</span>{{$product->price}}</span>
                                                </span>

                                                 <div class="add-to-cart" data-cart="{{$product->id}}"><a href="#"><span>Add to cart</span></a>
                                                    </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                @endforeach
                                @endif
                                
                                
                            </div>
                        </div>
                        <div class="tab-pane" id="Bestsellers">
                            <div class="Bestsellers owl-carousel">
                                <div class="product-grid">
                                    <div class="item">
                                        <div class="product-thumb  mb_30">
                                            <div class="image product-imageblock">
                                                <a href="{{url('/')}}">
                                                    <img data-name="product_image" src="{{asset('public/front')}}/images/product/product2-1.jpg" alt="iPod Classic" title="iPod Classic" class="img-responsive">
                                                    <img src="{{asset('public/front')}}/images/product/product2-1.jpg" alt="iPod Classic" title="iPod Classic" class="img-responsive">
                                                </a>
                                                <div class="button-group text-center">
                                                    <div class="wishlist"><a href="#"><span>wishlist</span></a>
                                                    </div>
                                                    <div class="quickview"><a href="#"><span>Quick View</span></a>
                                                    </div>
                                                    <div class="compare"><a href="#"><span>Compare</span></a>
                                                    </div>
                                                    <div class="add-to-cart"><a href="#"><span>Add to cart</span></a>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="caption product-detail text-left">
                                                <!-- <div class="rating"> 
                              <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-1x"></i><i class="fa fa-star fa-stack-1x"></i></span> <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-1x"></i><i class="fa fa-star fa-stack-1x"></i></span> <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-1x"></i><i class="fa fa-star fa-stack-1x"></i></span> <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-1x"></i><i class="fa fa-star fa-stack-1x"></i></span> <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-1x"></i><i class="fa fa-star fa-stack-x"></i></span> 
                            </div> -->
                                                <h6 data-name="product_name" class="product-name"><a href="#" title="Casual Shirt With Ruffle Hem">New LCDScreen and HD Vide..</a></h6>
                                                <span class="price"><span class="amount"><span class="currencySymbol">$</span>70.00</span>
                                                </span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="product-grid">
                                    <div class="item">
                                        <div class="product-thumb  mb_30">
                                            <div class="image product-imageblock">
                                                <a href="{{url('/')}}">
                                                    <img data-name="product_image" src="{{asset('public/front')}}/images/product/product2-1.jpg" alt="iPod Classic" title="iPod Classic" class="img-responsive">
                                                    <img src="{{asset('public/front')}}/images/product/product2-1.jpg" alt="iPod Classic" title="iPod Classic" class="img-responsive">
                                                </a>
                                                <div class="button-group text-center">
                                                    <div class="wishlist"><a href="#"><span>wishlist</span></a>
                                                    </div>
                                                    <div class="quickview"><a href="#"><span>Quick View</span></a>
                                                    </div>
                                                    <div class="compare"><a href="#"><span>Compare</span></a>
                                                    </div>
                                                    <div class="add-to-cart"><a href="#"><span>Add to cart</span></a>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="caption product-detail text-left">
                                                <!-- <div class="rating"> <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-1x"></i><i class="fa fa-star fa-stack-1x"></i></span> <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-1x"></i><i class="fa fa-star fa-stack-1x"></i></span> <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-1x"></i><i class="fa fa-star fa-stack-1x"></i></span> <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-1x"></i><i class="fa fa-star fa-stack-1x"></i></span> <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-1x"></i><i class="fa fa-star fa-stack-x"></i></span> 
                            </div> -->
                                                <h6 data-name="product_name" class="product-name"><a href="#" title="Casual Shirt With Ruffle Hem">New LCDScreen and HD Vide..</a></h6>
                                                <span class="price"><span class="amount"><span class="currencySymbol">$</span>70.00</span>
                                                </span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="product-grid">
                                    <div class="item">
                                        <div class="product-thumb  mb_30">
                                            <div class="image product-imageblock">
                                                <a href="{{url('/')}}">
                                                    <img data-name="product_image" src="{{asset('public/front')}}/images/product/product2-1.jpg" alt="iPod Classic" title="iPod Classic" class="img-responsive">
                                                    <img src="{{asset('public/front')}}/images/product/product2-1.jpg" alt="iPod Classic" title="iPod Classic" class="img-responsive">
                                                </a>
                                                <div class="button-group text-center">
                                                    <div class="wishlist"><a href="#"><span>wishlist</span></a>
                                                    </div>
                                                    <div class="quickview"><a href="#"><span>Quick View</span></a>
                                                    </div>
                                                    <div class="compare"><a href="#"><span>Compare</span></a>
                                                    </div>
                                                    <div class="add-to-cart"><a href="#"><span>Add to cart</span></a>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="caption product-detail text-left">
                                                <!--  <div class="rating"> <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-1x"></i><i class="fa fa-star fa-stack-1x"></i></span> <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-1x"></i><i class="fa fa-star fa-stack-1x"></i></span> <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-1x"></i><i class="fa fa-star fa-stack-1x"></i></span> <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-1x"></i><i class="fa fa-star fa-stack-1x"></i></span> <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-1x"></i><i class="fa fa-star fa-stack-x"></i></span> 
                            </div> -->
                                                <h6 data-name="product_name" class="product-name"><a href="#" title="Casual Shirt With Ruffle Hem">New LCDScreen and HD Vide..</a></h6>
                                                <span class="price"><span class="amount"><span class="currencySymbol">$</span>70.00</span>
                                                </span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="product-grid">
                                    <div class="item">
                                        <div class="product-thumb  mb_30">
                                            <div class="image product-imageblock">
                                                <a href="{{url('/')}}">
                                                    <img data-name="product_image" src="{{asset('public/front')}}/images/product/product2-1.jpg" alt="iPod Classic" title="iPod Classic" class="img-responsive">
                                                    <img src="{{asset('public/front')}}/images/product/product2-1.jpg" alt="iPod Classic" title="iPod Classic" class="img-responsive">
                                                </a>
                                                <div class="button-group text-center">
                                                    <div class="wishlist"><a href="#"><span>wishlist</span></a>
                                                    </div>
                                                    <div class="quickview"><a href="#"><span>Quick View</span></a>
                                                    </div>
                                                    <div class="compare"><a href="#"><span>Compare</span></a>
                                                    </div>
                                                    <div class="add-to-cart"><a href="#"><span>Add to cart</span></a>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="caption product-detail text-left">
                                                <!-- <div class="rating"> 
                              <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-1x"></i><i class="fa fa-star fa-stack-1x"></i></span> <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-1x"></i><i class="fa fa-star fa-stack-1x"></i></span> <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-1x"></i><i class="fa fa-star fa-stack-1x"></i></span> <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-1x"></i><i class="fa fa-star fa-stack-1x"></i></span> <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-1x"></i><i class="fa fa-star fa-stack-x"></i></span>
                            </div>
                            -->
                                                <h6 data-name="product_name" class="product-name"><a href="#" title="Casual Shirt With Ruffle Hem">New LCDScreen and HD Vide..</a></h6>
                                                <span class="price"><span class="amount"><span class="currencySymbol">$</span>70.00</span>
                                                </span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="product-grid">
                                    <div class="item">
                                        <div class="product-thumb  mb_30">
                                            <div class="image product-imageblock">
                                                <a href="{{url('/')}}">
                                                    <img data-name="product_image" src="{{asset('public/front')}}/images/product/product2-1.jpg" alt="iPod Classic" title="iPod Classic" class="img-responsive">
                                                    <img src="{{asset('public/front')}}/images/product/product2-1.jpg" alt="iPod Classic" title="iPod Classic" class="img-responsive">
                                                </a>
                                                <div class="button-group text-center">
                                                    <div class="wishlist"><a href="#"><span>wishlist</span></a>
                                                    </div>
                                                    <div class="quickview"><a href="#"><span>Quick View</span></a>
                                                    </div>
                                                    <div class="compare"><a href="#"><span>Compare</span></a>
                                                    </div>
                                                    <div class="add-to-cart"><a href="#"><span>Add to cart</span></a>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="caption product-detail text-left">
                                                <!-- <div class="rating">
                             <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-1x"></i><i class="fa fa-star fa-stack-1x"></i></span> <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-1x"></i><i class="fa fa-star fa-stack-1x"></i></span> <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-1x"></i><i class="fa fa-star fa-stack-1x"></i></span> <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-1x"></i><i class="fa fa-star fa-stack-1x"></i></span> <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-1x"></i><i class="fa fa-star fa-stack-x"></i></span>
                            </div> -->
                                                <h6 data-name="product_name" class="product-name"><a href="#" title="Casual Shirt With Ruffle Hem">New LCDScreen and HD Vide..</a></h6>
                                                <span class="price"><span class="amount"><span class="currencySymbol">$</span>70.00</span>
                                                </span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="product-grid">
                                    <div class="item">
                                        <div class="product-thumb  mb_30">
                                            <div class="image product-imageblock">
                                                <a href="{{url('/')}}">
                                                    <img data-name="product_image" src="{{asset('public/front')}}/images/product/product2-1.jpg" alt="iPod Classic" title="iPod Classic" class="img-responsive">
                                                    <img src="{{asset('public/front')}}/images/product/product2-1.jpg" alt="iPod Classic" title="iPod Classic" class="img-responsive">
                                                </a>
                                                <div class="button-group text-center">
                                                    <div class="wishlist"><a href="#"><span>wishlist</span></a>
                                                    </div>
                                                    <div class="quickview"><a href="#"><span>Quick View</span></a>
                                                    </div>
                                                    <div class="compare"><a href="#"><span>Compare</span></a>
                                                    </div>
                                                    <div class="add-to-cart"><a href="#"><span>Add to cart</span></a>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="caption product-detail text-left">
                                                <!-- <div class="rating"> 
                              <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-1x"></i><i class="fa fa-star fa-stack-1x"></i></span> <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-1x"></i><i class="fa fa-star fa-stack-1x"></i></span> <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-1x"></i><i class="fa fa-star fa-stack-1x"></i></span> <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-1x"></i><i class="fa fa-star fa-stack-1x"></i></span> <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-1x"></i><i class="fa fa-star fa-stack-x"></i></span> 
                            </div> -->
                                                <h6 data-name="product_name" class="product-name"><a href="#" title="Casual Shirt With Ruffle Hem">New LCDScreen and HD Vide..</a></h6>
                                                <span class="price"><span class="amount"><span class="currencySymbol">$</span>70.00</span>
                                                </span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="tab-pane" id="Featured">
                            <div class="Featured owl-carousel">
                                <div class="product-grid">
                                    <div class="item">
                                        <div class="product-thumb  mb_30">
                                            <div class="image product-imageblock">
                                                <a href="{{url('/')}}">
                                                    <img data-name="product_image" src="{{asset('public/front')}}/images/product/product2.jpg" alt="iPod Classic" title="iPod Classic" class="img-responsive">
                                                    <img src="{{asset('public/front')}}/images/product/product2-1.jpg" alt="iPod Classic" title="iPod Classic" class="img-responsive">
                                                </a>
                                                <div class="button-group text-center">
                                                    <div class="wishlist"><a href="#"><span>wishlist</span></a>
                                                    </div>
                                                    <div class="quickview"><a href="#"><span>Quick View</span></a>
                                                    </div>
                                                    <div class="compare"><a href="#"><span>Compare</span></a>
                                                    </div>
                                                    <div class="add-to-cart"><a href="#"><span>Add to cart</span></a>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="caption product-detail text-left">
                                                <!-- <div class="rating"> <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-1x"></i><i class="fa fa-star fa-stack-1x"></i></span> <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-1x"></i><i class="fa fa-star fa-stack-1x"></i></span> <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-1x"></i><i class="fa fa-star fa-stack-1x"></i></span> <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-1x"></i><i class="fa fa-star fa-stack-1x"></i></span> <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-1x"></i><i class="fa fa-star fa-stack-x"></i></span> 
                            </div> -->
                                                <h6 data-name="product_name" class="product-name"><a href="#" title="Casual Shirt With Ruffle Hem">New LCDScreen and HD Vide..</a></h6>
                                                <span class="price"><span class="amount"><span class="currencySymbol">$</span>70.00</span>
                                                </span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="product-grid">
                                    <div class="item">
                                        <div class="product-thumb  mb_30">
                                            <div class="image product-imageblock">
                                                <a href="{{url('/')}}">
                                                    <img data-name="product_image" src="{{asset('public/front')}}/images/product/product4.jpg" alt="iPod Classic" title="iPod Classic" class="img-responsive">
                                                    <img src="{{asset('public/front')}}/images/product/product4-1.jpg" alt="iPod Classic" title="iPod Classic" class="img-responsive">
                                                </a>
                                                <div class="button-group text-center">
                                                    <div class="wishlist"><a href="#"><span>wishlist</span></a>
                                                    </div>
                                                    <div class="quickview"><a href="#"><span>Quick View</span></a>
                                                    </div>
                                                    <div class="compare"><a href="#"><span>Compare</span></a>
                                                    </div>
                                                    <div class="add-to-cart"><a href="#"><span>Add to cart</span></a>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="caption product-detail text-left">
                                                <!-- <div class="rating"> <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-1x"></i><i class="fa fa-star fa-stack-1x"></i></span> <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-1x"></i><i class="fa fa-star fa-stack-1x"></i></span> <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-1x"></i><i class="fa fa-star fa-stack-1x"></i></span> <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-1x"></i><i class="fa fa-star fa-stack-1x"></i></span> <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-1x"></i><i class="fa fa-star fa-stack-x"></i></span> 
                            </div> -->
                                                <h6 data-name="product_name" class="product-name"><a href="#" title="Casual Shirt With Ruffle Hem">New LCDScreen and HD Vide..</a></h6>
                                                <span class="price"><span class="amount"><span class="currencySymbol">$</span>70.00</span>
                                                </span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="product-grid">
                                    <div class="item">
                                        <div class="product-thumb  mb_30">
                                            <div class="image product-imageblock">
                                                <a href="{{url('/')}}">
                                                    <img data-name="product_image" src="{{asset('public/front')}}/images/product/product6.jpg" alt="iPod Classic" title="iPod Classic" class="img-responsive">
                                                    <img src="{{asset('public/front')}}/images/product/product6-1.jpg" alt="iPod Classic" title="iPod Classic" class="img-responsive">
                                                </a>
                                                <div class="button-group text-center">
                                                    <div class="wishlist"><a href="#"><span>wishlist</span></a>
                                                    </div>
                                                    <div class="quickview"><a href="#"><span>Quick View</span></a>
                                                    </div>
                                                    <div class="compare"><a href="#"><span>Compare</span></a>
                                                    </div>
                                                    <div class="add-to-cart"><a href="#"><span>Add to cart</span></a>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="caption product-detail text-center">
                                                <!--  <div class="rating"> 
                              <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-1x"></i><i class="fa fa-star fa-stack-1x"></i></span> <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-1x"></i><i class="fa fa-star fa-stack-1x"></i></span> <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-1x"></i><i class="fa fa-star fa-stack-1x"></i></span> <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-1x"></i><i class="fa fa-star fa-stack-1x"></i></span> <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-1x"></i><i class="fa fa-star fa-stack-x"></i></span>
                            </div> -->
                                                <h6 data-name="product_name" class="product-name"><a href="#" title="Casual Shirt With Ruffle Hem">New LCDScreen and HD Vide..</a></h6>
                                                <span class="price"><span class="amount"><span class="currencySymbol">$</span>70.00</span>
                                                </span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="product-grid">
                                    <div class="item">
                                        <div class="product-thumb  mb_30">
                                            <div class="image product-imageblock">
                                                <a href="{{url('/')}}">
                                                    <img data-name="product_image" src="{{asset('public/front')}}/images/product/product8.jpg" alt="iPod Classic" title="iPod Classic" class="img-responsive">
                                                    <img src="{{asset('public/front')}}/images/product/product8-1.jpg" alt="iPod Classic" title="iPod Classic" class="img-responsive">
                                                </a>
                                                <div class="button-group text-center">
                                                    <div class="wishlist"><a href="#"><span>wishlist</span></a>
                                                    </div>
                                                    <div class="quickview"><a href="#"><span>Quick View</span></a>
                                                    </div>
                                                    <div class="compare"><a href="#"><span>Compare</span></a>
                                                    </div>
                                                    <div class="add-to-cart"><a href="#"><span>Add to cart</span></a>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="caption product-detail text-center">
                                                <!-- <div class="rating"> <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-1x"></i><i class="fa fa-star fa-stack-1x"></i></span> <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-1x"></i><i class="fa fa-star fa-stack-1x"></i></span> <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-1x"></i><i class="fa fa-star fa-stack-1x"></i></span> <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-1x"></i><i class="fa fa-star fa-stack-1x"></i></span> <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-1x"></i><i class="fa fa-star fa-stack-x"></i></span> 
                            </div> -->
                                                <h6 data-name="product_name" class="product-name"><a href="#" title="Casual Shirt With Ruffle Hem">New LCDScreen and HD Vide..</a></h6>
                                                <span class="price"><span class="amount"><span class="currencySymbol">$</span>70.00</span>
                                                </span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="product-grid">
                                    <div class="item">
                                        <div class="product-thumb  mb_30">
                                            <div class="image product-imageblock">
                                                <a href="{{url('/')}}">
                                                    <img data-name="product_image" src="{{asset('public/front')}}/images/product/product10.jpg" alt="iPod Classic" title="iPod Classic" class="img-responsive">
                                                    <img src="{{asset('public/front')}}/images/product/product10-1.jpg" alt="iPod Classic" title="iPod Classic" class="img-responsive">
                                                </a>
                                                <div class="button-group text-center">
                                                    <div class="wishlist"><a href="#"><span>wishlist</span></a>
                                                    </div>
                                                    <div class="quickview"><a href="#"><span>Quick View</span></a>
                                                    </div>
                                                    <div class="compare"><a href="#"><span>Compare</span></a>
                                                    </div>
                                                    <div class="add-to-cart"><a href="#"><span>Add to cart</span></a>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="caption product-detail text-center">
                                                <!-- <div class="rating"> <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-1x"></i><i class="fa fa-star fa-stack-1x"></i></span> <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-1x"></i><i class="fa fa-star fa-stack-1x"></i></span> <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-1x"></i><i class="fa fa-star fa-stack-1x"></i></span> <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-1x"></i><i class="fa fa-star fa-stack-1x"></i></span> <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-1x"></i><i class="fa fa-star fa-stack-x"></i></span> 
                            </div> -->
                                                <h6 data-name="product_name" class="product-name"><a href="#" title="Casual Shirt With Ruffle Hem">New LCDScreen and HD Vide..</a></h6>
                                                <span class="price"><span class="amount"><span class="currencySymbol">$</span>70.00</span>
                                                </span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="product-grid">
                                    <div class="item">
                                        <div class="product-thumb  mb_30">
                                            <div class="image product-imageblock">
                                                <a href="{{url('/')}}">
                                                    <img data-name="product_image" src="{{asset('public/front')}}/images/product/product2.jpg" alt="iPod Classic" title="iPod Classic" class="img-responsive">
                                                    <img src="{{asset('public/front')}}/images/product/product2-1.jpg" alt="iPod Classic" title="iPod Classic" class="img-responsive">
                                                </a>
                                                <div class="button-group text-center">
                                                    <div class="wishlist"><a href="#"><span>wishlist</span></a>
                                                    </div>
                                                    <div class="quickview"><a href="#"><span>Quick View</span></a>
                                                    </div>
                                                    <div class="compare"><a href="#"><span>Compare</span></a>
                                                    </div>
                                                    <div class="add-to-cart"><a href="#"><span>Add to cart</span></a>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="caption product-detail text-center">
                                                <!-- <div class="rating"> <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-1x"></i><i class="fa fa-star fa-stack-1x"></i></span> <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-1x"></i><i class="fa fa-star fa-stack-1x"></i></span> <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-1x"></i><i class="fa fa-star fa-stack-1x"></i></span> <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-1x"></i><i class="fa fa-star fa-stack-1x"></i></span> <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-1x"></i><i class="fa fa-star fa-stack-x"></i></span> 
                            </div> -->
                                                <h6 data-name="product_name" class="product-name"><a href="#" title="Casual Shirt With Ruffle Hem">New LCDScreen and HD Vide..</a></h6>
                                                <span class="price"><span class="amount"><span class="currencySymbol">$</span>70.00</span>
                                                </span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- =====  PRODUCT TAB  END ===== -->
                
               
                <!-- =====  Blog ===== -->
              
            </div>
        </div>
        
        <div class="newsletters mb_50 hide">
            <div class="row">
                <div class="col-sm-6">
                    <div class="news-head pull-left">
                        <h2>SIGN UP FOR NEWSLETTER</h2>
                        <div class="new-desc">Be the First to know about our Fresh Arrivals and much more!</div>
                    </div>
                </div>
                <div class="col-sm-6">
                    <div class="news-form pull-right">
                        <form onsubmit="return validatemail();" method="post">
                            <div class="form-group required">
                                <input name="email" id="email" placeholder="Enter Your Email" class="form-control input-lg" required="" type="email">
                                <button type="submit" class="btn btn-default btn-lg">Subscribe</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
    @include('sports.modal.invites')

@stop 

@section('scripts')
<script type="text/javascript">
    // script here
</script>
@stop