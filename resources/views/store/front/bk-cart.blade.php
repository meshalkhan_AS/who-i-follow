@extends('store.front.layout.master') 

@section('content')
<style type="text/css">
    .rating{
        display: none;
    }

    .wishlist{
        display: none;
    }

    .compare{
        display: none;
    }
    
    /*img.img-responsive {
       height: 100%;
       width: 100%;
   }*/
</style>
<!-- =====  LODER  ===== -->
<div class="loder"></div>

<div class="container">
      <div class="row ">
        <!-- =====  BANNER STRAT  ===== -->
        <div class="col-sm-12">
          <div class="breadcrumb ptb_20">
            <h1>Shopping Cart</h1>
          <!--   <ul>
              <li><a href="{{route('front.shop')}}">Home</a></li>
              <li class="active">Shopping Cart</li>
            </ul> -->
          </div>
        </div>
        <!-- =====  BREADCRUMB END===== -->
       
       <div style="display: none;"> @include('store.front.layout.aside')
       </div>
        <div class="col-sm-12 col-lg-12 mtb_20">
          <form enctype="multipart/form-data" method="post" action="#">
            <div class="table-responsive">
              <table class="table table-bordered">
                <thead>
                  <tr>
                    <td class="text-center">Image</td>
                    <td class="text-left">Product Name</td>
                    <!-- <td class="text-left">Model</td> -->
                    <td class="text-left">Quantity</td>
                    <td class="text-right">Unit Price</td>
                    <td class="text-right">Total</td>
                  </tr>
                </thead>
                <tbody>

                  @if(count($cart_content))
                  @foreach($cart_content as $cartData)
                  <tr class="row-del-{{$cartData->id}}">
                    <td class="text-center"><a href="#">
                      <img src="{{$cartData->attributes->image}}" alt="{{$cartData->name}}" title="{{$cartData->name}}"></a>
                    </td>
                    <td class="text-left"><a href="product.html">{{$cartData->name}}</a></td>
                    <!-- <td class="text-left">product 11</td> -->
                    <td class="text-left">
                     
                      <div style="max-width: 200px;" class="input-group btn-block">
                       
                        <input type="text" class="form-control quantity_{{$cartData->id}}" size="1" value="{{$cartData->quantity}}" name="quantity">
                        <span class="input-group-btn">
                      <button class="btn update-cart" title="" data-toggle="tooltip" type="button" data-original-title="Update" data-proid="{{$cartData->id}}" data-price="{{$cartData->price}}"><i class="fa fa-refresh"></i>
                      </button>
                      <button  class="btn btn-danger close-cart" data-delcart="{{$cartData->id}}" title="" data-toggle="tooltip" type="button" data-original-title="Remove"><i class="fa fa-times-circle"></i></button>
                      </span>
                       
                    </div>

                    </td>
                    <td class="text-right">${{$cartData->price}}</td>
                    <td class="text-right subtotal_{{$cartData->id}}">${{$cartData->price*$cartData->quantity}}</td>
                  </tr>
                  @endforeach
                  @endif
                </tbody>
              </table>
            </div>
          </form>
         <!--  <h3 class="mtb_10">What would you like to do next?</h3>
          <p>Choose if you have a discount code or reward points you want to use or would like to estimate your delivery cost.</p> -->
       
          <div class="row">
            <div class="col-sm-4 col-sm-offset-8">
              <table class="table table-bordered">
                <tbody>
               
                  <tr>
                    <td class="text-right"><strong>Total:</strong></td>
                    <td class="text-right p-total">${{$total_price}}</td>
                  </tr>
                </tbody>
              </table>
            </div>
          </div>
          <form action="{{url('shop')}}">
            <input class="btn pull-left mt_30" type="submit" value="Continue Shopping" />
          </form>
          <form action="{{url('shop/checkout')}}">
            <input class="btn pull-right mt_30" type="submit" value="Checkout" />
          </form>
        </div>
      </div>
   
    </div>



@stop 

@section('scripts')
<script src="{{asset('public/front')}}/js/jquery-ui.js"></script>
<script type="text/javascript">
  $('.close-cart').click(function(){

    var deleteid = $(this).attr('data-delcart');

    $(".row-del-"+deleteid).remove();
    // alert(1);
  });
</script>
@stop