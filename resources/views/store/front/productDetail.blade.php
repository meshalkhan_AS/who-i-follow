@extends('store.front.layout.master')

@section('content')
<style type="text/css">
    .wishlist{
        display: none;
    }

    .compare{
        display: none;
    }

    .rating{
        display: none;
    }
</style>
<!-- =====  LODER  ===== -->
<div class="container-fluid">
            <div class="row ">
               <!-- =====  BANNER STRAT  ===== -->
               <div class="col-sm-12">
                  <div class="breadcrumb pt_20 mt_80">
                    <!--  <h1>{{!empty($product) ? $product->name : ''}}</h1> -->
                    <a href="{{url('/')}}/shop" class="pull-left back-btn"><i class="fa fa-arrow-left"></i> Back to Products </a>
                    <!--  <ul>
                        <li><a href="index.html">Home</a></li>
                        <li><a href="{{route('front.products',$product->category_id)}}">Products</a></li>
                        <li class="active">{{!empty($product) ? $product->name : ''}}</li>
                     </ul>
 -->                  </div>
               </div>
               <!-- =====  BREADCRUMB END===== -->
               <div class="box-product-cover w-100">
               @include('store.front.layout.aside')
               <div class="col-sm-8 col-lg-9 border-left">
                  <div class="row">
                     <div class="col-md-7 border-right">
                        <div><a class="thumbnails"> <img data-name="product_image" src="{{asset(!empty($product) ? $product->image : '')}}" alt="" /></a></div>
                        <div id="product-thumbnail" class="owl-carousel">
                           <div class="item">
                              <div class="image-additional"><a class="thumbnail" href="{{asset(!empty($product) ? $product->image : '')}}" data-fancybox="group1"> <img src="{{asset(!empty($product) ? $product->image : '')}}" alt="" /></a></div>
                           </div>

                            @foreach($product->gallery as $gallery)
                
                                 <div class="item">
                                    <div class="image-additional"><a class="thumbnail" href="{{asset(!empty($gallery) ? $gallery->path : '')}}" data-fancybox="group1"> <img src="{{asset(!empty($gallery) ? $gallery->path : '')}}" alt="" /></a></div>
                                 </div>
                               
                           @endforeach
                         
                         
                        </div>


                    

                     </div>
                     <div class="col-md-5 prodetail caption product-thumb">
                        
                        <div class="rating">
                           <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-1x"></i><i class="fa fa-star fa-stack-1x"></i></span>
                           <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-1x"></i><i class="fa fa-star fa-stack-1x"></i></span>
                           <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-1x"></i><i class="fa fa-star fa-stack-1x"></i></span>
                           <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-1x"></i><i class="fa fa-star fa-stack-1x"></i></span>
                           <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-1x"></i><i class="fa fa-star fa-stack-x"></i></span>
                        </div>
                        
                     <!--    <hr> -->
                        <!-- <ul class="list-unstyled product_info mtb_20">
                           <li>
                              <label>Brand:</label>
                              <span> <a href="#">Apple</a></span>
                           </li>
                           <li>
                              <label>Product Code:</label>
                              <span> product 20</span>
                           </li>
                           <li>
                              <label>Availability:</label>
                              <span> In Stock</span>
                           </li>
                        </ul> -->
                        <hr>
                     
                        <div id="product" style="padding: 20px;">
                           <div class="form-group">
                              <div class="row">
                                 <h6 class="pro-title hide">Choose Size</h6>
                                 <ul class="size-filter mb_20 hide">
                                    <li><button>XS</button></li>
                                    <li><button>S</button></li>
                                    <li><button>M</button></li>
                                    <li><button>L</button></li>
                                    <li><button>XL</button></li>
                                 </ul>


                                 <h6 class="pro-title hide"> Choose Color </h6>
                                 <ul class="color-filter hide">
                                    <li><button style="background:#e02b38;">red</button></li>
                                    <li><button style="background: #000;">black</button></li>
                                    <li><button style="background: #f6f91c;">yellow</button></li>
                                   
                                 </ul>


                                    <!-- <div class="Sort-by col-md-6">
                                    <label>Sort by</label>
                                    <select name="product_size" id="select-by-size" class="selectpicker form-control">
                                       <option>Small</option>
                                       <option>Medium</option>
                                       <option>Large</option>
                                    </select>
                                 </div> -->
                                 <!-- <div class="Color col-md-6">
                                    <label>Color</label>
                                    <select name="product_color" id="select-by-color" class="selectpicker form-control">
                                       <option>Blue</option>
                                       <option>Green</option>
                                       <option>Orange</option>
                                       <option>White</option>
                                    </select>
                                 </div> -->
                              </div>
                           </div>
                           <div class="qty mt_30 form-group2">
                              <label>Qty</label>
                              <input name="product_quantity" id="product_quantity" min="1" value="1" type="number">
                           </div>

                              <div class="mt_20">
                                  <h6 class="pro-title" style="font-size: 13px;">  Categories </h6>

                                  <ul class="category-list">
                                       <li><span>Main Category:</span> {{isset($product->category->parent->name) ? $product->category->parent->name : ''}}</li>
                                       <li><span>Sub Category:</span> {{isset($product->category->name) ? $product->category->name : ''}}</li>
                                  </ul>
                           </div>


                           <div class="mt_60">
                              <div class="add-to-cart" data-cart="{{$product->id}}" data-type="input"><a href="#"><span>Add to cart</span></a></div>
                              <div class="wishlist"><a href="#"><span>wishlist</span></a></div>
                              <div class="compare"><a href="#"><span>Compare</span></a></div>
                           </div>


                     

                        </div>
                     </div>
                  </div>
                  <div class="row">
                     <div class="col-md-12">
                        <div id="exTab5" class="mtb_30" style="display: none;">
                           <ul class="nav nav-tabs">
                              <li class="active"> <a href="#1c" data-toggle="tab">Overview</a> </li>
                              <!-- <li><a href="#2c" data-toggle="tab">Reviews (1)</a> </li> -->
                              <!-- <li><a href="#3c" data-toggle="tab">Solution</a> </li> -->
                           </ul>
                           <div class="tab-content ">
                              <div class="tab-pane active" id="1c">
                                 <p>{{!empty($product) ? $product->product_description : ''}}</p>
                              </div>
                              <div class="tab-pane" id="2c">
                                 <form class="form-horizontal">
                                    <div id="review"></div>
                                    <h4 class="mt_20 mb_30">Write a review</h4>
                                    <div class="form-group required">
                                       <div class="col-sm-12">
                                          <label class="control-label" for="input-name">Your Name</label>
                                          <input name="name" value="" id="input-name" class="form-control" type="text">
                                       </div>
                                    </div>
                                    <div class="form-group required">
                                       <div class="col-sm-12">
                                          <label class="control-label" for="input-review">Your Review</label>
                                          <textarea name="text" rows="5" id="input-review" class="form-control"></textarea>
                                          <div class="help-block"><span class="text-danger">Note:</span> HTML is not translated!</div>
                                       </div>
                                    </div>
                                    <div class="form-group required">
                                       <div class="col-md-6">
                                          <label class="control-label">Rating</label>
                                          <div class="rates"><span>Bad</span>
                                             <input name="rating" value="1" type="radio">
                                             <input name="rating" value="2" type="radio">
                                             <input name="rating" value="3" type="radio">
                                             <input name="rating" value="4" type="radio">
                                             <input name="rating" value="5" type="radio">
                                             <span>Good</span>
                                          </div>
                                       </div>
                                       <div class="col-md-6">
                                          <div class="buttons pull-right">
                                             <button type="submit" class="btn btn-md btn-link">Continue</button>
                                          </div>
                                       </div>
                                    </div>
                                 </form>
                              </div>
                              <div class="tab-pane" id="3c">
                                 <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut lobortis malesuada mi id tristique. Sed ipsum nisi, dapibus at faucibus non, dictum a diam. Nunc vitae interdum diam. Sed finibus, justo vel maximus facilisis, sapien turpis euismod tellus, vulputate semper diam ipsum vel tellus.applied clearfix to the tab-content to rid of the gap between the tab and the content</p>
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
               
               </div>
            </div>
            </div>
            
         </div>
@stop 

@section('scripts')
<script type="text/javascript">
    // script here
</script>
@stop