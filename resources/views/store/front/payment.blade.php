@extends('store.front.layout.master') 

@section('content')
<style type="text/css">
    .rating{
        display: none;
    }

    .wishlist{
        display: none;
    }

    .compare{
        display: none;
    }

    div#stripe-form\ center_div {
            width: 50%;
            margin: 0 auto;
        }

        .d-none{
                display: none;
        }
        .minput{
          margin: 15px 0px !important;

        }
        a.disabled {
  pointer-events: none;
  cursor: default;
}
    
    /*img.img-responsive {
       height: 100%;
       width: 100%;
   }*/
</style>
<!-- =====  LODER  ===== -->
<div class="loder"></div>

<div class="container-fluid mt_80">
      <div class="row ">
        <!-- =====  BANNER STRAT  ===== -->
        <div class="col-sm-12">
          <div class="breadcrumb ptb_20">
            <h1>Payment</h1>
           <!--  <ul>
              <li><a href="{{route('front.shop')}}">Home</a></li>
              <li class="active">Shopping Cart</li>
            </ul> -->
          </div>
        </div>
        <!-- =====  BREADCRUMB END===== -->
     <div style="display: none;">  @include('store.front.layout.aside') </div>
        <div class="col-sm-12 col-lg-12 mtb_20">
          
          <div class="panel-group" id="accordion">
            <div class="panel panel-default  border-0">
              <!-- <div class="panel-heading">
                <h4 class="panel-title"> <a data-toggle="collapse" data-parent="#accordion" href="#collapseOne">Shipping Address </a></h4>
              </div> -->
              <div id="collapseOne" class="panel-collapse collapse in">
                <div class="panel-body border-0">
                  
                        <div id="stripe-form center_div">
                          
                                <div class="form-row row  col-md-12 form-group mt-30">
                                   
                                  <p id="errorid" style="color: red;"></p>
                @if ($errors->any())
                <div class="alert alert-danger">
                 
                  <ul>
                    @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                    @endforeach
                  </ul>
                </div><br />
                @endif
                
                                        <div class="">
                                    
                                
                                  </div>
                                </div>
                                
                                <form id="paymentForm" method="post" action="{{ url('charge') }}">
                                  {{ csrf_field() }}
                                  
                                <p><input type="hidden" name="amount" value="{{$totalamount}}"  /></p>
                                  <label class='control-label minput'>Card Number</label>
                                  <p><input type="text" id="cardNumber" placeholder="Card Number" maxlength="16"/></p>
                                  <label class='control-label minput'>Expiration Month</label>
                                  <p><input type="text" id="expMonth" placeholder='MM' size='2'/></p>
                                  <label class='control-label minput'>Expiration Year</label>
                                  <p><input type="text" id="expYear" placeholder='YYYY' size='4'/></p>
                                  <label class='control-label minput'>CVC</label>
                                  <p><input type="text" id="cardCode" placeholder='ex. 311' size='3'/></p>
                                  <input type="hidden" name="opaqueDataValue" id="opaqueDataValue" />
                                  <input type="hidden" name="opaqueDataDescriptor" id="opaqueDataDescriptor" />
                                  <a id="pay-submit"><button type="button" class="btn btn-primary btn-lg btn-block minput"  onclick="sendPaymentDataToAnet()">Pay Now</button></a>
                              </form>
    {{-- <form role="form" action="{{ route('payment.stripe-process') }}" method="post" class="require-validation"
        data-cc-on-file="false"
        data-stripe-publishable-key="{{ env('STRIPE_KEY') }}"
        id="payment-form">
        @csrf


        <div class='form-row row'>
            <div class='col-md-12 col-md-12 form-group  required'>
                <label class='control-label'>Card Number</label> <input
                    autocomplete='off' class='form-control card-number' size='20'
                    type='text'>
            </div>
        </div>
        

        <div class='form-row row'>
            <div class='col-xs-12 col-md-12 form-group cvc required'>
                <label class='control-label'>CVC</label> <input autocomplete='off'
                    class='form-control card-cvc' placeholder='ex. 311' size='4'
                    type='text'>
            </div>
         </div>

         <div class='form-row row'>
            <div class='col-xs-12 col-md-12 form-group expiration required'>
                <label class='control-label'>Expiration Month</label> <input
                    class='form-control card-expiry-month' placeholder='MM' size='2'
                    type='text'>
            </div>
        </div>
        <div class='form-row row'>
            <div class='col-xs-12 col-md-12 form-group expiration required'>
                <label class='control-label'>Expiration Year</label> <input
                    class='form-control card-expiry-year' placeholder='YYYY' size='4'
                    type='text'>
            </div>
       </div>

        <div class='form-row row'>
            <div class='col-md-6 error form-group d-none'>
                <div class='alert-danger alert'></div>
            </div>
        </div>

        <div class="form-row row">
            <div class="col-md-6 form-group">
                <button type="submit" class="btn btn-primary btn-lg btn-block stripe-submit">Pay with Stripe</button>
            </div>
        </div> 
    </form> --}}
</div>

                </div>
              </div>
            </div>
            
          </div>
        </div>


      </div>
   
</div>



@stop 

@section('scripts')
{{-- <script src="{{asset('public/front')}}/js/jquery-ui.js"></script>
<script src="https://js.stripe.com/v2/"></script> --}}
<script type="text/javascript" src="https://js.authorize.net/v1/Accept.js" charset="utf-8"></script>
<script type="text/javascript">
document.addEventListener('contextmenu', function(e) {
  e.preventDefault();
});
function sendPaymentDataToAnet() {
    // Set up authorisation to access the gateway.
    var authData = {};
        authData.clientKey = "{!! env('ANET_PUBLIC_CLIENT_KEY') !!}";
        authData.apiLoginID = "{!! env('ANET_API_LOGIN_ID') !!}";
 
    var cardData = {};
        cardData.cardNumber = document.getElementById("cardNumber").value;
        cardData.month = document.getElementById("expMonth").value;
        cardData.year = document.getElementById("expYear").value;
        cardData.cardCode = document.getElementById("cardCode").value;
  if(cardData.cardCode===''){
    error="Please Enter CVC!";
    document.getElementById("errorid").innerHTML = error;
  }
  else{
    // Now send the card data to the gateway for tokenisation.
    // The responseHandler function will handle the response.
    var secureData = {};
        secureData.authData = authData;
        secureData.cardData = cardData;
        Accept.dispatchData(secureData, responseHandler);
}
}
 
function responseHandler(response) {
    if (response.messages.resultCode === "Error") {
        var i = 0;
        while (i < response.messages.message.length) {
                // response.messages.message[i].code + ": " +
                error=response.messages.message[i].text;
        document.getElementById("errorid").innerHTML = error;
            i = i + 1;
        }
    } else {
      $("#pay-submit").attr("disabled");
        paymentFormUpdate(response.opaqueData);
    }
}
  
function paymentFormUpdate(opaqueData) {
    document.getElementById("opaqueDataDescriptor").value = opaqueData.dataDescriptor;
    document.getElementById("opaqueDataValue").value = opaqueData.dataValue;
    document.getElementById("paymentForm").submit();
}
</script>

<script type="text/javascript">
  $('.close-cart').click(function(){

    var deleteid = $(this).attr('data-delcart');

    $(".row-del-"+deleteid).remove();
    // alert(1);
  });
</script>

  
  <script type="text/javascript">
        $('.add').click(function () {
    if ($(this).prev().val() < 200) {
      $(this).prev().val(+$(this).prev().val() + 1);
    }
    });
    $('.sub').click(function () {
        if ($(this).next().val() > 1) {
          if ($(this).next().val() > 1) $(this).next().val(+$(this).next().val() - 1);
        }
    });

    $(document).ready(function() {
    "use strict";

    var $form = $(".require-validation");
    $('form.require-validation').bind('submit', function(e) {
        var $form         = $(".require-validation"),
        inputSelector = ['input[type=email]', 'input[type=password]',
                         'input[type=text]', 'input[type=file]',
                         'textarea'].join(', '),
        $inputs       = $form.find('.required').find(inputSelector),
        $errorMessage = $form.find('div.error'),
        valid         = true;
        $errorMessage.addClass('d-none');
 
        $('.has-error').removeClass('has-error');
        $inputs.each(function(i, el) {
            var $input = $(el);
            if ($input.val() === '') {
                $input.parent().addClass('has-error');
                $errorMessage.removeClass('d-none');
                e.preventDefault();
            }
        });
  
        if (!$form.data('cc-on-file')) {
                        e.preventDefault();
                        Stripe.setPublishableKey($form.data('stripe-publishable-key'));
                        Stripe.createToken({
                                number: $('.card-number').val(),
                                cvc: $('.card-cvc').val(),
                                exp_month: $('.card-expiry-month').val(),
                                exp_year: $('.card-expiry-year').val()
                        }, stripeResponseHandler);
        }
  
    });
  
    function stripeResponseHandler(status, response) {
      if (response.error) {
        $('.d-none').css("display","block");
        $('.error').removeClass('hide').find('.alert').text(response.error.message);
        $('.stripe-submit').attr('disabled',false);
      } 
      else {
        // token contains id, last4, and card type
        var token = response['id'];
        // insert the token into the form so it gets submitted to the server
        $form.find('input[type=text]').empty();
        $form.append("<input type='hidden' name='stripeToken' value='" + token + "'/>");
        $('.stripe-submit').attr('disabled',true);
        $form.get(0).submit();
      }
    }
});
    
  </script>

@stop