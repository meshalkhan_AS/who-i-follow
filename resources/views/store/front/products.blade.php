@extends('store.front.layout.master') 

@section('content')
<style type="text/css">
    .rating{
        display: none;
    }

    .wishlist{
        display: none;
    }

    .compare{
        display: none;
    }
    
    /*img.img-responsive {
       height: 100%;
       width: 100%;
   }*/
</style>
<!-- =====  LODER  ===== -->
<div class="loder"></div>

<div class="container-fluid">
            <div class="row ">
             
               <div class="col-sm-12 mtb_10">
                  <div class="category-page-wrapper mb_30">
                     <div class="list-grid-wrapper pull-left">
                        <div class="btn-group btn-list-grid">
                           <button type="button" id="grid-view" class="btn btn-default grid-view active"></button>
                           <button type="button" id="list-view" class="btn btn-default list-view"></button>
                        </div>
                     </div>
                    
                  </div>
                  <div class="row">

                     @if(count($products))
                     @foreach($products as $product)
                     <div class="product-layout  product-grid  col-md-4 col-sm-6 col-xs-12 ">
                        <div class="item">
                           <div class="product-thumb clearfix mb_30">
                              <div class="image product-imageblock">
                                 <a href="{{route('front.viewProduct',$product->id)}}">
                                   @if (file_exists($product->image))
                                  <img data-name="product_image" src="{{asset($product->image)}}" alt="iPod Classic" title="iPod Classic" class="img-responsive img-height" />
                                   <img src="{{asset($product->image)}}" alt="iPod Classic" title="iPod Classic" class="img-responsive" /> 
                                   @else

                                   <img data-name="product_image" src="{{asset('public/front/images/placeholder.png')}}" alt="{{$product->name}}" title="{{$product->name}}" class="img-responsive">
                                                    <img src="{{asset('public/front/images/placeholder.png')}}" alt="{{$product->name}}" title="{{$product->name}}" class="img-responsive"  >

                                   @endif
                                 </a>
                                 <div class="button-group text-center">
                                    <div class="wishlist"><a href="#"><span>wishlist</span></a></div>
                                    <!-- <div class="quickview"><a href="#"><span>Quick View</span></a>
                                    </div> -->
                                    <div class="compare"><a href="#"><span>Compare</span></a></div>
        
                                 </div>
                              </div>
                              <div class="caption product-detail text-left">
                                 <h6 data-name="product_name" class="product-name mt_20"><a href="#" title="Casual Shirt With Ruffle Hem">
                                    {{$product->name}}
                                 </a>
                                 </h6>
                                 <div class="rating"> <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-1x"></i><i class="fa fa-star fa-stack-1x"></i></span> <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-1x"></i><i class="fa fa-star fa-stack-1x"></i></span> <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-1x"></i><i class="fa fa-star fa-stack-1x"></i></span> <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-1x"></i><i class="fa fa-star fa-stack-1x"></i></span> <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-1x"></i><i class="fa fa-star fa-stack-x"></i></span> 

                                 </div>
                                 <span class="price">
                                    <span class="amount">
                                       <span class="currencySymbol">$</span>{{$product->price}}
                                    </span>
                                 </span>

                                 <div class="add-to-cart"  data-cart="{{$product->id}}"><a href="#"><span>Add to cart</span></a></div>
                                 <!-- <p class="product-desc mt_20 mb_60"> $product->price</p> -->
                              </div>
                           </div>
                        </div>
                     </div>
                     @endforeach
                     @endif
                     
                    
                  </div>

                   @if(count($products) > 0)
                  {{ $products->links('store.front.pagination') }}
                 <!--  <div class="pagination-nav text-center mt_50">
                     <ul>

                      

                        <li><a href="#"><i class="fa fa-angle-left"></i></a></li>
                        <li class="active"><a href="#">1</a></li>
                        <li><a href="#">2</a></li>
                        <li><a href="#">3</a></li>
                        <li><a href="#"><i class="fa fa-angle-right"></i></a></li>
                     </ul>
                  </div> -->
                  @endif
               </div>
            </div>
  
         </div>


@stop 

@section('scripts')
<script src="{{asset('public/front')}}/js/jquery-ui.js"></script>
<script>
         $(function() {
           $("#slider-range").slider({
             range: true,
             min: 0,
             max: 500,
             values: [75, 300],
             slide: function(event, ui) {
               $("#amount").val("$" + ui.values[0] + " - $" + ui.values[1]);
             }
           });
           $("#amount").val("$" + $("#slider-range").slider("values", 0) +
             " - $" + $("#slider-range").slider("values", 1));
         });
      </script>
@stop