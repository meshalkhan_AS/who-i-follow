<!DOCTYPE html>
<html lang="en">
    <head>
        <title></title>
         @section('header')
            @include('store.front.layout.header')
         @show
    </head>
    <body>

        <?php
          $helper = new \App\Helpers\StoreHelper;
        ?>
      

        @section('mainheader')
            @include('store.front.layout.mainheader')
        @show

        <!-- content -->
        @yield('content')

        @section('footer')
            @include('store.front.layout.footer')
        @show

      


        {{--  load scripts --}}
        @yield('scripts')

    </body>
</html>
