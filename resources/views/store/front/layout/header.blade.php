<!-- =====  BASIC PAGE NEEDS  ===== -->
    <meta charset="utf-8">
    <title>WHO I FOLLOW</title>
    <!-- =====  SEO MATE  ===== -->
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="description" content="">
    <meta name="keywords" content="">
    <meta name="distribution" content="global">
    <meta name="revisit-after" content="2 Days">
    <meta name="robots" content="ALL">
    <meta name="rating" content="8 YEARS">
    <meta name="Language" content="en-us">
    <meta name="GOOGLEBOT" content="NOARCHIVE">
    <meta http-equiv="refresh" content="{{ config('session.lifetime') * 60}}">
    <!-- =====  MOBILE SPECIFICATION  ===== -->
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <meta name="viewport" content="width=device-width">
    <!-- =====  CSS  ===== -->
    <link rel="stylesheet" type="text/css" href="{{ asset('public/sports/css') }}/line-awesome.css">
    <link rel="stylesheet" type="text/css" href="{{ asset('public/sports/css') }}/line-awesome-font-awesome.min.css">
    <link rel="stylesheet" type="text/css" href="{{asset('public/front')}}/css/font-awesome.min.css" />
    <link rel="stylesheet" type="text/css" href="{{asset('public/front')}}/css/bootstrap.css" />
    <link rel="stylesheet" type="text/css" href="{{asset('public/newchat/css/commonstyle.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('public/front')}}/css/style.css">
    <link rel="stylesheet" type="text/css" href="{{ asset('public/sports/css') }}/chat.css">
{{--    <link rel="stylesheet" type="text/css" href="{{ asset('public/front/css') }}/responsive.css">--}}
    <link rel="stylesheet" type="text/css" href="{{asset('public/front')}}/css/magnific-popup.css">
    <link rel="stylesheet" type="text/css" href="{{asset('public/front')}}/css/owl.carousel.css">
    <link rel="shortcut icon" href="{{ asset('public/sports/images') }}/favicon.png" type="image/x-icon">
    <link rel="apple-touch-icon" href="{{asset('public/front')}}/images/apple-touch-icon.png">
    <link rel="apple-touch-icon" sizes="72x72" href="{{asset('public/front')}}/images/apple-touch-icon-72x72.png">
    <link rel="apple-touch-icon" sizes="114x114" href="{{asset('public/front')}}/images/apple-touch-icon-114x114.png">

<link rel="stylesheet" href="{{asset('public/front')}}/plugins/datatables-bs4/css/dataTables.bootstrap4.min.css">
<link rel="stylesheet" href="{{asset('public/front')}}/plugins/datatables-responsive/css/responsive.bootstrap4.min.css">
<script type="text/javascript" src="{{ asset('public/sports/js') }}/jquery.min.js"></script>
<style type="text/css">
    li.dropdown-header {
        padding: 0 20px 0px !important;
    }

    .mega-dropdown-menu>li>ul>li>a {
    padding: 0px 20px;}
</style>
<script>
    var menu = function() {
        $('header nav').toggleClass('active');
    };
</script>
<script>
    $(document).on('click', function(e) {
        if ( $(e.target).closest('.user-info').length ) {
            $('.user-account-settingss').toggle();
        }
        else if ( ! $(e.target).closest('.user-info').length ) {
            $('.user-account-settingss').hide();
        }
    });
</script>
<script>
    $(document).on('click', function(e) {
        if ( $(e.target).closest('.notification-icon').length ) {
            $('.notification-box').toggle();
        }
        else if ( ! $(e.target).closest('.notification-icon').length ) {
            $('.notification-box').hide();
        }
    });
</script>