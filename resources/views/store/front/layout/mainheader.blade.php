
@php
    $userprofile = \App\Models\Admin\UserProfile::where('user_id',Auth::user()->id)->first();
    $userprofileall = \App\Models\Admin\UserProfile::take(10)->get();
    $userNotifications = \App\Models\Admin\UserNotification::orderBy('created_at','desc')->where('user_id',Auth::user()->id)->take(20)->get();
    $count = \App\Models\Admin\UserNotification::where('user_id',Auth::user()->id)->sum('checked');
@endphp

<header>
    <div class="container-fluid">
        <div class="header-data">
            <div class="logo">
                <a href="{{route('user.chatSystem')}}" title=""><img src="{{ asset('public/sports/images/login images') }}/logo.svg" alt=""></a>
            </div>
            <div class="search-bar">
                <div class="search-form" >
                    {!! Form::open(['method' => 'get','route' => ['user.search'], 'data-parsley-validate']) !!}
                    <button type="submit" class="border-0">
                        <i class="fa fa-search"></i>
                    </button>
                    <input type="text" style="color: #888 !important;" name="search" placeholder="Search..." list="browsers">
                    {{--<datalist id="browsers">--}}
                        {{--@foreach($userprofileall as $userprofilealls)--}}
                            {{--<option value="{{$userprofilealls->fname}} {{$userprofilealls->lname}}">--}}
                        {{--@endforeach--}}
                    {{--</datalist>--}}
                    {!! Form::close() !!}
                </div>
            </div>
            <nav>
                <ul class="mb-0" style="padding-left: 0!important; ">
                    <li class="small-menu-profile-pic">
                        <a href="{{route('user.home',Auth::user()->user_link)}}">
                            @if($userprofile['picture']=='yes')
                                <img id="profile_picture_check" src="{{url('/').'/public/storage/users/'.$userprofile['user_id'].'/profile_picture.jpg'}}?v={{time()}}" alt="">
                            @else
                                <img src="{{ asset('public/uploads/white.jpg') }}" alt="">
                            @endif
                            <h6>{{ substr(auth()->user()->name, 0,12)}}</h6>
                        </a>
                    </li>
                    <li>
                        <span class="count-message"></span>
                        <a href="{{route('user.chatSystem')}}">Teams</a>
                    </li>
                    <li class="{{(request()->is('/')) ? 'active' : ''}}">
                        <a href="{{route('user.news')}}" title="">Opinions</a>
                    </li>

                    <li class="{{(request()->segment(1) == 'friends') ? 'active' : ''}}">
                        <a href="{{ route('user.friends',Auth::user()->user_link) }}" title="" class="not-box-openm">Friends</a>
                    </li>
                    <li class="{{(request()->is('post/all')) ? 'active' : ''}}">
                        <a href="{{ url('/post/all')}}">Specialists</a>
                    </li>
                    <li class="{{(request()->segment(1) == 'shop') ? 'active' : ''}}">
                        <a style="color: #385499;border-bottom: 3px solid #385499;padding-bottom: 5px;font-size: 14px;font-weight: 400;" href="{{route('front.shop')}}">Shop</a>
                    </li>
                    <li class="{{(request()->segment(2) == 'feedback') ? 'active' : ''}}">
                        <a href="{{route('user.feedback')}}">Feedback</a>
                    </li>

                    {{--Mobile View--}}
                    <li class="small-menu-notification-icon">
                        <a href="{{ route('user.notification.view') }}" class="not-box-open notification-link" onclick="checkedNotification()">
                            Notifications
                        </a>
                        <div class="chat-mg bx" id="count-box">
                            @if($count>0)
                                <span class="count-notification small-menu-icon">{{$count}}</span>
                            @else
                                <span class="count-notification small-menu-icon" style="display: none"></span>
                            @endif
                        </div>
                    </li>
                    <li class="small-menu-setting">
                        <a href="{{ route('user.bio') }}">Settings</a>
                    </li>
                    <li class="small-menu-logout">
                        <a href="{{ route('userlogout') }}">Logout</a>
                    </li>

                    {{--Mobile View--}}
                </ul>

            </nav>

            {{--Web View--}}
            <div class="menu-btn">
                <i class="fa fa-bars" onclick="open_menu_bar()"></i>
            </div>
            <div class="user-account mt-4 pt-4">
                <a class="user-account-info" href="{{route('user.home',Auth::user()->user_link)}}">
                    @if($userprofile['picture']=='yes')
                        <img id="profile_picture_check" src="{{url('/').'/public/storage/users/'.$userprofile['user_id'].'/profile_picture.jpg'}}?v={{time()}}" alt="">
                    @else
                        <img src="{{ asset('public/uploads/white.jpg') }}" alt="">
                    @endif
                    <h6>{{ substr(auth()->user()->name, 0,12)}}</h6>
                </a>
                <div class="user-info">
                    <i class="fa fa-caret-down"></i>
                </div>

                <div class="notification-div">
                    <div class="chat-mg bx" id="count-box" style="position: absolute; right: 0; top: 45%;">
                        @if($count>0)
                            <span class="count-notification">{{$count}}</span>
                        @else
                            <span class="count-notification" style="display:none;"></span>
                        @endif
                        <input id="count-input" value="{{$count}}" hidden>
                    </div>

                    {{--    Notification start--}}
                    <a class="not-box-open p-2 rounded-circle notification-link notification-icon" style="cursor: pointer; padding: 10px !important;" onclick="checkedNotification()">
                        <img class="notification-icon" src="{{ asset('public/sports/images/login images') }}/notification.svg" alt="">
                    </a>
                    <div class="notification-box noti" id="notification">
                        <div class="nt-title">
                            <h4>Notifications</h4>
                            {{--<a href="#" title="">Mark all as read</a>--}}
                        </div>
                        <div class="nt-title">
                            <a href="{{ route('user.notification.view') }}" title="">View All Notifications</a>
                        </div>
                        <div class="nott-list" id="notification-lists">
                            @if(count($userNotifications)>0)
                                @foreach($userNotifications as $userNotification)
                                    <?php
                                    $sender=\App\Models\Admin\UserProfile::select('user_profiles.*','users.user_link as user_link')
                                        ->join('users', 'users.id', '=', 'user_profiles.user_id')->where('user_profiles.user_id',$userNotification->send_by)->first();
                                    $username = $sender['fname'].' '.$sender['lname'];
                                    $message_type=$userNotification->message_type;
                                    if($message_type=="followed")
                                    {
                                        $message = "followed you.";
                                        $url=route('user.followers',Auth::user()->user_link);
                                    }
                                    else if($message_type=="unfollowed")
                                    {
                                        $message  = "unfollowed you.";
                                        $url=route('user.followers',Auth::user()->user_link);
                                    }
                                    else if($message_type=="sentFriendRequest")
                                    {
                                        $message  = "sent you friend request.";
                                        $url=route('user.friends',['friendrequest'=>'yes','id'=>Auth::user()->user_link]);
                                    }
                                    else if($message_type=="confirmFriendRequest")
                                    {
                                        $message  = "confirmed your friend request.";
                                        $url=route('user.friends',Auth::user()->user_link);
                                    }
                                    else if($message_type=="addPost")
                                    {
                                        $message  = "added a new post.";
                                        $url=route('user.news',['random'=>$userNotification->table_id]);
                                    }
                                    else if($message_type=="joinedByInvite")
                                    {
                                        $message  = "joined Who I follow and has been added to your friend list.";
                                        $url=route('user.friends',Auth::user()->user_link);
                                    }
                                    else if($message_type=="AddedAsFriend")
                                    {
                                        $message  = "has been added to your friend list.";
                                        $url=route('user.friends',Auth::user()->user_link);
                                    }
                                    else
                                    {
                                        $message  = "";
                                        $url="";
                                    }
                                    ?>

                                    <a href="{{$url}}" class="pointer-cursor" style="display: block;" onclick="readNotification({{$userNotification->id}})">
                                        @if($userNotification->read=='0')
                                            <div class="notfication-details">
                                                @else
                                                    <div class="notfication-details" style="background-color: #f3f3f3">
                                                        @endif

                                                        <div class="noty-user-img">

                                                            @if($sender['picture']=='yes')
                                                                <img src="{{url('/').'/public/storage/users/'.$sender['user_id'].'/profile_picture.jpg'}}?v={{time()}}" alt="">
                                                            @else
                                                                <img src="{{ asset('public/uploads/white.jpg') }}" alt="">
                                                            @endif
                                                        </div>
                                                        <div class="notification-info">
                                                            <h3>
                                                                <text style="font-weight: bold">{{$username}}</text>
                                                                <text style="font-weight: normal">{{$message}}</text>
                                                            </h3>
                                                            <p><time class="timeago" datetime="{{date("Y-m-d H:i:s", strtotime('+5 hours', strtotime($userNotification->created_at)))}}"></time></p>
                                                        </div>
                                                    </div>
                                    </a>
                                @endforeach
                            @endif
                        </div>

                    </div>
                </div>
                {{--    Notification end--}}

                <div class="user-account-settingss">
                    <a href="{{ route('user.bio') }}"><i class="fa fa-cog text-dark  p-2 rounded-circle"></i>Settings</a>
                    <a href="{{ route('userlogout') }}"><i class="fa fa-sign-out text-dark p-2 rounded-circle"></i>Logout</a>
                </div>
            </div>
        </div>
        {{--Web View--}}

    </div>
</header>


<div class="shopcart" >
   
    <div id="cart" class="btn-group btn-block mtb_10" style="display: table-row-group !important;">
        <button type="button" class="btn" style=" margin-top:0 !important;">
            <span id="shippingcart">Cart</span><span id="cart-total"> </span>
        </button>
        
    </div>
    <div id="cart-dropdown" class="cart-menu">
        <ul>
            <li>
                <table class="table table-striped cart-table-data">
                    <tbody>
                    <tr>
                        <td class="text-center">
                            <a href="#">
                                <img src="{{asset('public/front')}}/images/product/70x84.jpg" alt="iPod Classic" title="iPod Classic">
                            </a>
                        </td>
                        <td class="text-left product-name"><a href="#">MacBook Pro</a>  <span class="text-left price">$20.00</span>
                            <input class="cart-qty" name="product_quantity" min="1" value="1" type="number">
                        </td>
                        <td class="text-center"><a class="close-cart"><i class="fa fa-times-circle"></i></a>
                        </td>
                    </tr>
                    <tr>
                        <td class="text-center">
                            <a href="#">
                                <img src="{{asset('public/front')}}/images/product/70x84.jpg" alt="iPod Classic" title="iPod Classic">
                            </a>
                        </td>
                        <td class="text-left product-name"><a href="#">MacBook Pro</a>  <span class="text-left price">$20.00</span>
                            <input class="cart-qty" name="product_quantity" min="1" value="1" type="number">
                        </td>
                        <td class="text-center"><a class="close-cart"><i class="fa fa-times-circle"></i></a>
                        </td>
                    </tr>
                    </tbody>
                </table>
            </li>
            <li>
                <table class="table">
                    <tbody>
                    <!--  <tr>
                       <td class="text-right"><strong>Sub-Total</strong>
                       </td>
                       <td class="text-right">$2,100.00</td>
                     </tr> -->
                    <!-- <tr>
                      <td class="text-right"><strong>Eco Tax (-2.00)</strong>
                      </td>
                      <td class="text-right">$2.00</td>
                    </tr> -->
                    <!-- <tr>
                      <td class="text-right"><strong>VAT (20%)</strong>
                      </td>
                      <td class="text-right">$20.00</td>
                    </tr> -->
                    <tr>
                        <td class="text-right"><strong>Total</strong>
                        </td>
                        <td class="text-right p-total">$0.00</td>
                    </tr>
                    </tbody>
                </table>
            </li>
            <li>
                <form action="{{route('front.carFront')}}">
                    <input class="btn pull-left mt_10" value="View cart" type="submit">
                </form>
                <form action="{{url('/')}}/shop/checkout">
                    <input class="btn pull-right mt_10" value="Checkout" type="submit">
                </form>
            </li>
        </ul>
    </div>
    <a href="{{ route('front.store.orderHistory') }}" style="    color: #000;
    font-weight: 600;
    padding: 0 0 0 30px;
    border: none;
    border-radius: 0;
    float:right;">
Orders History</a> 
</div>
@include('sports.js.notification')

<script>
    function open_menu_bar () {
        if($("nav").hasClass('activemenu')){
            document.body.classList.remove("remove-scrolling");
            $("nav").removeClass('activemenu');

        }else{
            document.body.classList.add("remove-scrolling");
            $("nav").addClass('activemenu');
        }
    }
    //        $("nav").toggleClass("active");
    //    }
</script>
<script>
    $('#cart-dropdown').hide();
    $(document).on('click', function(e) {
        if ( $(e.target).closest('#cart').length ) {
            $('#cart-dropdown').toggle();
        }
        else if ( ! $(e.target).closest('#cart').length ) {
            $('#cart-dropdown').hide();
        }
    });
</script>