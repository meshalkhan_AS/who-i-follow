<?php
 $helper = new \App\Helpers\StoreHelper;
?>
<div id="column-left" class="col-sm-4 col-lg-3">
                  <div id="category-menu" class="mb_40" aria-expanded="true" style="" role="button">
                     <div class="nav-responsive">
                       <!--  <div class="heading-part">
                           <h2 class="main_title">Top category</h2>
                        </div> -->
                       <!--  <ul class="nav  main-navigation collapse in">
                           @if(count($helper->topCategories()) > 0)
                           @foreach($helper->topCategories() as $category)
                           <li><a href="{{route('front.products',$category->id)}}">{{$category->name}}</a></li>
                           @endforeach
                           @endif
                       
                        </ul> -->

                        <h4 data-name="product_name" class="product-name"><a href="#" title="Casual Shirt With Ruffle Hem">{{!empty($product) ? $product->name : ''}}</a></h4>
                        <span class="price mb_20"><span class="amount"><span class="currencySymbol">$</span>{{!empty($product) ? $product->price : ''}}</span>
                        </span>
                     </div>


                      <div class="panel-group product-details mt_100 pt_40" id="accordion">
                         <div class="panel panel-default border-0">
                           <div class="panel-heading">
                             <h4 class="panel-title border-0">
                               <a data-toggle="collapse" data-parent="#accordion" href="#collapse1">Product Info </a>
                             </h4>
                           </div>
                           <div id="collapse1" class="panel-collapse collapse in">
                             <div class="panel-body"><p class="product-desc"> {{!empty($product) ? $product->product_description : ''}}</p></div>
                           </div>
                         </div>
                         <div class="panel panel-default border-0">
                           <div class="panel-heading">
                             <h4 class="panel-title border-0">
                               <a data-toggle="collapse" data-parent="#accordion" href="#collapse2">Details </a>
                             </h4>
                           </div>
                           <div id="collapse2" class="panel-collapse collapse">
                             <div class="panel-body"><h6> <span> Vendor Name:</span> Chris Ponting</h6>
                              <h6> <span> Vendor Commission:</span> 3%</h6>
                             </div>
                           </div>
                         </div>
                      
                       </div> 


                  </div>
                  
                 
               </div>