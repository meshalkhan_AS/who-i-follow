<div class="footer pt_60 hide">
  <div class="container">
    <div class="row">
      <div class="footer-top pb_60 mb_30">
        <div class="col-xs-12 col-sm-6">
          <div class="footer-logo">
            <a href="{{url('/')}}">
              <img src="{{asset('public/front')}}/images/logo.png" alt="">
            </a>
          </div>
          <div class="footer-desc">Lorem ipsum doLorem ipsum dolor sit amet, consectetur adipisicagna.</div>
        </div>
        <!-- =====  testimonial  ===== -->
        <div class="col-xs-12 col-sm-6">
          <div class="Testimonial">
            <div class="client owl-carousel">
              <div class="item client-detail">
                <div class="client-avatar">
                  <img alt="" src="{{asset('public/front')}}/images/user1.jpg">
                </div>
                <div class="client-title"><strong>joseph Lui</strong>
                </div>
                <div class="client-designation mb_10">- php Developer</div>
                <p><i class="fa fa-quote-left" aria-hidden="true"></i>Lorem ipsum dolor sit amet, volumus oporteat his at sea in Rem ipsum dolor sit amet, sea in odio ..</p>
              </div>
              <div class="item client-detail">
                <div class="client-avatar">
                  <img alt="" src="{{asset('public/front')}}/images/user2.jpg">
                </div>
                <div class="client-title"><strong>joseph Lui</strong>
                </div>
                <div class="client-designation mb_10">- php Developer</div>
                <p><i class="fa fa-quote-left" aria-hidden="true"></i>Lorem ipsum dolor sit amet, volumus oporteat his at sea in Rem ipsum dolor sit amet, sea in odio ..</p>
              </div>
              <div class="item client-detail">
                <div class="client-avatar">
                  <img alt="" src="{{asset('public/front')}}/images/user3.jpg">
                </div>
                <div class="client-title"><strong>joseph Lui</strong>
                </div>
                <div class="client-designation mb_10">- php Developer</div>
                <p><i class="fa fa-quote-left" aria-hidden="true"></i>Lorem ipsum dolor sit amet, volumus oporteat his at sea in Rem ipsum dolor sit amet, sea in odio ..</p>
              </div>
            </div>
          </div>
        </div>
        <!-- =====  testimonial end ===== -->
      </div>
    </div>
    <div class="row">
      <div class="col-md-3 footer-block">
        <h6 class="footer-title ptb_20">Information</h6>
        <ul>
          <!-- <li><a href="#">About Us</a></li> -->
          <li><a href="#">Delivery Information</a>
          </li>
          <li><a href="#">Privacy Policy</a>
          </li>
          <li><a href="#">Terms & Conditions</a>
          </li>
          <li><a href="{{url('/')}}">Contact Us</a>
          </li>
        </ul>
      </div>
      <div class="col-md-3 footer-block">
        <h6 class="footer-title ptb_20">Services</h6>
        <ul>
          <li><a href="#">Returns</a>
          </li>
          <li><a href="#">Site Map</a>
          </li>
          <li><a href="#">Wish List</a>
          </li>
          <li><a href="#">My Account</a>
          </li>
          <li><a href="#">Order History</a>
          </li>
        </ul>
      </div>
      <div class="col-md-3 footer-block">
        <h6 class="footer-title ptb_20">Extras</h6>
        <ul>
          <li><a href="#">Brands</a>
          </li>
          <li><a href="#">Gift Certificates</a>
          </li>
          <li><a href="#">Affiliates</a>
          </li>
          <li><a href="#">Specials</a>
          </li>
          <li><a href="#">Newsletter</a>
          </li>
        </ul>
      </div>
      <div class="col-md-3 footer-block">
        <h6 class="footer-title ptb_20">Contacts</h6>
        <ul>
          <li>
            <!-- Warehouse & Offices, 12345 Street name, California USA -->
          </li>
          <li>
            <!-- (+123) 456 789
              <br> (+024) 666 888 -->
          </li>
          <li>
            <!-- Contact@yourcompany.com -->
          </li>
        </ul>
      </div>
    </div>
  </div>
  <div class="footer-bottom mt_60 ptb_20">
    <div class="container">
      <div class="row">
        <div class="col-sm-4">
          <div class="social_icon">
            <ul>
              <li><a href="#"><i class="fa fa-facebook"></i></a>
              </li>
              <li><a href="#"><i class="fa fa-google"></i></a>
              </li>
              <li><a href="#"><i class="fa fa-linkedin"></i></a>
              </li>
              <li><a href="#"><i class="fa fa-twitter"></i></a>
              </li>
              <li><a href="#"><i class="fa fa-rss"></i></a>
              </li>
            </ul>
          </div>
        </div>
        <div class="col-sm-4">
          <div class="copyright-part text-center">@ 2017 All Rights Reserved</div>
        </div>
        <div class="col-sm-4">
          <div class="payment-icon text-right">
            <ul>
              <li><i class="fa fa-cc-paypal "></i>
              </li>
              <li><i class="fa fa-cc-visa"></i>
              </li>
              <li><i class="fa fa-cc-discover"></i>
              </li>
              <li><i class="fa fa-cc-mastercard"></i>
              </li>
              <li><i class="fa fa-cc-amex"></i>
              </li>
            </ul>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

<a id="scrollup"></a>
<script src="{{asset('public/front')}}/js/jQuery_v3.1.1.min.js"></script>
<script src="{{asset('public/front')}}/js/owl.carousel.min.js"></script>
<script src="{{asset('public/front')}}/js/bootstrap.min.js"></script>
<script src="{{asset('public/front')}}/js/jquery.magnific-popup.js"></script>
<script src="{{asset('public/front')}}/js/jquery.firstVisitPopup.js"></script>
<script src="{{asset('public/front')}}/js/custom.js"></script>
<script src="{{asset('public/front')}}/js/notify.js"></script>




<script type="text/javascript">

  // $.notify("Access granted", "success");


  $('.add-to-cart').click(function(){

    var attr = $(this).attr('data-type');

    var qty = 1;
    var product_id = $(this).attr('data-cart');

    if (typeof attr !== 'undefined' && attr !== false) {
      qty = $("#product_quantity").val();
      var input_value = 'true';
      var formData = {
            qty:qty,product_id:product_id,input_value:input_value
          };

    }else{
      var formData = {
            qty:qty,product_id:product_id
          };
    }

    
    $.ajax({
          type:'get',
          url:'{{route("front.cart.add")}}',
          data:formData,
          success: function( msg ) {

            totalCount();

             $.notify("Product added to Cart successfully",'success');
          }
      });

  });

  totalCount();

  function totalCount(){
    $.ajax({
          type:'get',
          url:'{{route("front.getCart")}}',
          success: function( msg ) {

            var total = msg.data.total_count;
            var total_price = msg.data.total_price;
            var totalHtml = 'items ( '+total+' )';

            $("#cart-total").html(totalHtml);
            $(".p-total").html('$ '+total_price);

            var rowHtml = '';

            const cartData = msg.data.cat_content;

            for (x in cartData) {
              // console.log(cartData[x].name);
              rowHtml += `<tr>
                      <td class="text-center">
                        <a href="#">
                          <img src="${cartData[x].attributes.image}" alt="iPod Classic" title="iPod Classic">
                        </a>
                      </td>
                      <td class="text-left product-name"><a href="#">${cartData[x].name}</a>  <span class="text-left price">$${cartData[x].price}</span>
                        <input class="cart-qty" name="product_quantity" min="1" value="${cartData[x].quantity}" type="number">
                      </td>
                      <td class="text-center"><a class="close-cart" data-delcart="${cartData[x].id}"><i class="fa fa-times-circle"></i></a>
                      </td>
                    </tr>`;

            };

            $('.cart-table-data tbody').html(rowHtml);
  
          }
      });

  }

   $(document).on("click",".close-cart",function() {
  
    var product_id = $(this).attr('data-delcart');
    $.ajax({
          type:'get',
          url:'{{route("front.removeCart")}}',
          data:{
            product_id:product_id
          },
          success: function( msg ) {

            totalCount();
          }
      });

    });



   $(document).on("click",".update-cart",function() {
  
    var product_id = $(this).attr('data-proid');
    var product_price = $(this).attr('data-price');
    var qty = $('.quantity_'+product_id).val();

   

    var input_value = 'true';
    var formData = {
          qty:qty,product_id:product_id,input_value:input_value
        };
    $.ajax({
          type:'get',
          url:'{{route("front.cart.add")}}',
          data:formData,
          success: function( msg ) {

            console.log(msg);

            totalCount();
          }
      });

     var sub_total = product_price*qty;

    $(".subtotal_"+product_id).html(sub_total);
    

    });


   
</script>
