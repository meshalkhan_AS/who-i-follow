@extends('store.admin.layout.master')

@section('content')
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">

                    </div>
                </div>
            </div><!-- /.container-fluid -->
        </section>

        <!-- Main content -->
        <section class="content">
            <div class="container-fluid">
                <!-- SELECT2 EXAMPLE -->
                <div class="card card-default">
                    <div class="card-header">
                        <!-- <h3 class="card-title">Select2 (Default Theme)</h3> -->

                        <div class="card-tools">
                            <button type="button" class="btn btn-tool" data-card-widget="collapse"><i class="fas fa-minus"></i></button>
                            <button type="button" class="btn btn-tool" data-card-widget="remove"><i class="fas fa-times"></i></button>
                        </div>
                    </div>
                    <!-- /.card-header -->
                    <div class="card-body">

                        @if ($errors->any())
                            <div class="alert alert-danger">
                                <ul>
                                    @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
                        @endif

                        @if(session()->has('message'))
                            <div class="alert alert-success">
                                {{ session()->get('message') }}
                            </div>
                        @endif

                        <div class="">
                            <div class="col-md-6">
                                <div class="">
                                    <div class="card-header">
                                        {{$title}}
                                    </div>
                                    <div class="card-body">
                                        <form action="{{ route('add.banner.post') }}" method="POST" enctype="multipart/form-data">
                                            {!! csrf_field() !!}


                                            <div class="form-group">
                                                <label for="visible">Category</label>
                                                <select name="category" class="form-control" required>
                                                    <option selected disabled value="">-- Select Category --</option>
                                                    @foreach($categories as $category)
                                                        <option value="{{$category->id}}">{{$category->name}}</option>
                                                    @endforeach
                                                </select>
                                                <small class="form-text text-muted">Choose if the category will be visible</small>
                                            </div>



                                            <div class="form-group">
                                                <input type="text" class="form-control" placeholder="Name" name="name" required>
                                            </div>

                                            <div class="form-group">
                                                <input type="text" class="form-control" placeholder="Website link" name="link" required>
                                            </div>

                                            <div class="form-group">
                                                <input type="text" class="form-control" placeholder="Description" name="description" required>
                                            </div>

                                            <div class="form-group">
                                                <input type="file" class="form-control" placeholder="Description" name="image" required>
                                            </div>



                                            <!-- <div class="form-group">
                                                <label for="visible">Visible</label>
                                                <select name="visible" class="form-control">
                                                    <option selected disabled>-- Select Visiblity --</option>
                                                    <option value="0">No</option>
                                                    <option value="1" selected="">Yes</option>
                                                </select>
                                                <small class="form-text text-muted">Choose if the category will be visible</small>
                                            </div> -->
                                            <button type="submit" class="btn btn-success">Submit</button>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>




                        <!-- /.row -->


                        <!-- /.row -->
                    </div>
                    <!-- /.card-body -->

                </div>
                <!-- /.card -->


                <!-- /.row -->
            </div><!-- /.container-fluid -->
        </section>
        <!-- /.content -->
    </div>
@stop

@section('scripts')

@stop



