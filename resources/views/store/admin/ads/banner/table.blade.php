@extends('store.admin.layout.master')

@section('content')

 <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
          	<h1>Advertisement Banners</h1>

          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <div class="row">
          <div class="col-12">



            @if(session()->has('message'))
                <div class="alert alert-success">
                    {{ session()->get('message') }}
                </div>
            @endif


            <div class="card">

              <!-- /.card-header -->
              <div class="card-body">

              	<a href="{{route('add.banner.view')}}" class="btn btn-primary" style="margin-bottom: 12px;">Add new</a>

                <table id="example1" class="table table-bordered table-striped">
                  	<thead>
					<tr class="table-light">
						<th>Title</th>
						<th>Description</th>
						<th>Category name</th>
						<th>Image</th>
						<th>Link</th>
						<th colspan="2">Action</th>
					</tr>
				</thead>
				<tbody>
					@if(count($banners))
						@foreach($banners as $banner)
							<tr>
								<td>{{ $banner->title }}</td>
								<td>{{ $banner->desc }}</td>
								<td>{{ isset($banner->category) ? $banner->category->name : '' }}</td>
								<td><img src="{{ asset($banner->image) }}" width="100px"></td>
								<td>{{ url($banner->link) }}</td>

								<td>
									<a href="{{ route('banner.edit', $banner->id) }}" class="btn btn-primary">Edit</a>
								</td>
								<td>
									<form action="{{ route('banner.delete', ['id' => $banner->id]) }}" method="POST">
										{!! csrf_field() !!}
										{{ method_field('DELETE') }}
										<button type="submit" class="btn btn-danger">Delete</button>
									</form>
								</td>
							</tr>
						@endforeach
					@else
						<tr class="text-center">
							<td colspan="5">No category found</td>
						</tr>
					@endif
				</tbody>

                </table>
              </div>
              <!-- /.card-body -->
            </div>
            <!-- /.card -->
          </div>
          <!-- /.col -->
        </div>
        <!-- /.row -->
      </div>
      <!-- /.container-fluid -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->


@stop

@section('scripts')
<script>
  $(function () {
    $("#example1").DataTable({
      "responsive": true,
      "autoWidth": false,
      "aaSorting": []
    });
    $('#example2').DataTable({
      "paging": true,
      "lengthChange": false,
      "searching": false,
      "ordering": true,
      "info": true,
      "autoWidth": false,
      "responsive": true,
      "aaSorting": []
    });
  });
</script>
@stop


