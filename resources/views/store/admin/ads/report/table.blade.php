@extends('store.admin.layout.master')

<style>
    .header-body{
        margin-bottom: 20px;
    }
    .header-body .card {
        position: relative;
        display: flex;
        flex-direction: column;
        min-width: 0;
        word-wrap: break-word;
        border: 1px solid rgba(0, 0, 0, .05);
        border-radius: .375rem;
        background-color: #fff;
        background-clip: border-box;
    }

    .header-body .card-body {
        padding: 1.5rem;
        flex: 1 1 auto;
    }

    .header-body .card-title {
        margin-bottom: 1.25rem;
    }

    .header-body .card-stats .card-body {
        padding: 1rem 1.5rem;
    }

    .header-body .icon {
        width: 3rem;
        height: 3rem;
    }

    .header-body .icon i {
        font-size: 2.25rem;
    }

    .header-body .icon-shape {
        display: inline-flex;
        padding: 12px;
        text-align: center;
        border-radius: 50%;
        align-items: center;
        justify-content: center;
    }

    .header-body .icon-shape i {
        font-size: 1.25rem;
    }


</style>
@section('content')

    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">
                        <h1>Advertisement Report</h1>

                    </div>
                </div>
            </div><!-- /.container-fluid -->
        </section>

        <!-- Main content -->
        <section class="content">
            <div class="container-fluid">

                <!--Card Stats-->
                <div class="header-body">
                    <div class="row">
                        @if (count($count_stats) > 0)
                            @foreach ($count_stats as $stat)
                                <div class="col-xl-3 col-lg-6">
                                    <div class="card card-stats mb-4 mb-xl-0">
                                        <div class="card-body">
                                            <div class="row">
                                                <div class="col">
                                                    <h5 class="card-title text-uppercase text-muted mb-0">
                                                        <?php
                                                        $banner=\App\AdvertisementBanner::where('id',$stat->advertisement_banner_id)->first();
                                                        ?>
                                                        @if($banner){{$banner['title']}}@endif</h5>
                                                    <span class="h2 font-weight-bold mb-0">{{$stat->click_count}}</span>
                                                </div>
                                                <div class="col-auto">
                                                    <div class="icon icon-shape bg-danger text-white rounded-circle shadow">
                                                        <i class="fas fa-chart-bar"></i>
                                                    </div>
                                                </div>
                                            </div>
                                            <p class="mt-3 mb-0 text-muted text-sm">
{{--                                                <span class="text-success mr-2"><i class="fa fa-arrow-up"></i> 3.48%</span>--}}
                                                <span class="text-nowrap">Total Clicks</span>
                                            </p>
                                        </div>
                                    </div>
                                </div>
                            @endforeach
                        @endif

                    </div>
                </div>
                <!--Card Stats-->

                <div class="row">
                    <div class="col-12">
                        @if(session()->has('message'))
                            <div class="alert alert-success">
                                {{ session()->get('message') }}
                            </div>
                        @endif


                        <div class="card">



                            <!-- /.card-header -->
                            <div class="card-body">


                                <table id="example1" class="table table-bordered table-striped">
                                    <thead>
                                    <tr class="table-light">
                                        <th>S.No</th>
                                        <th>Category name</th>
                                        <th>Sponser name</th>
                                        <th>User name</th>
                                        <th>Action Type</th>
                                        <th>Created</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @if(count($adsStats))
                                        @foreach($adsStats as $key => $stat)
                                            <tr>
                                                <td>{{ $key+1 }}</td>
                                                <?php
                                                $banner=\App\AdvertisementBanner::where('id',$stat->advertisement_banner_id)->first();
                                                ?>
                                                <td>{{ $stat->category->name }}</td>
                                                <td>@if($banner){{$banner['title']}}@endif</td>
                                                <?php
                                                    $userprofile=\App\Models\Admin\UserProfile::where('user_id',$stat->user_id)->first();
                                                ?>
                                                <td>{{ ($userprofile)?$userprofile['fname'].' '.$userprofile['lname']:''}}</td>
                                                <td>{{ $stat->action_type }}</td>
                                                <td>{{ date('m/d/Y h:i a',  strtotime($stat->created_at)) }}</td>
                                            </tr>
                                        @endforeach
                                    @else
                                        <tr class="text-center">
                                            <td colspan="5">No Stats found</td>
                                        </tr>
                                    @endif
                                    </tbody>

                                </table>
                            </div>
                            <!-- /.card-body -->
                        </div>
                        <!-- /.card -->
                    </div>
                    <!-- /.col -->
                </div>
                <!-- /.row -->
            </div>
            <!-- /.container-fluid -->
        </section>
        <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->


@stop

@section('scripts')
    <script>
        $(function () {
            $("#example1").DataTable({
                "responsive": true,
                "autoWidth": false,
                "aaSorting": []
            });
            $('#example2').DataTable({
                "paging": true,
                "lengthChange": false,
                "searching": false,
                "ordering": true,
                "info": true,
                "autoWidth": false,
                "responsive": true,
                "aaSorting": []
            });
        });
    </script>
@stop


