 <!-- Main Sidebar Container -->
  <aside class="main-sidebar sidebar-dark-primary elevation-4">
    <!-- Brand Logo -->
    <a href="{{url('/admin/categories/list?cat=main')}}" class="brand-link">
      <img src="{{asset('public/AdminLTE')}}/dist/img/AdminLTELogo.png" alt="AdminLTE Logo" class="brand-image img-circle elevation-3"
           style="opacity: .8">
      <span class="brand-text font-weight-light">Shop</span>
    </a>

    <!-- Sidebar -->
    <div class="sidebar">
      <!-- Sidebar user panel (optional) -->
      <div class="user-panel mt-3 pb-3 mb-3 d-flex">
        <div class="image">
          <img src="{{asset('public/AdminLTE')}}/dist/img/user2-160x160.jpg" class="img-circle elevation-2" alt="User Image" style="display: none;">
        </div>
        <div class="info">
          <a href="#" class="d-block">Admin </a>
        </div>
      </div>

      <!-- Sidebar Menu -->
      <nav class="mt-2">
        <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
          <!-- Add icons to the links using the .nav-icon class
               with font-awesome or any other icon font library -->

          <li class="nav-item has-treeview">
            <a href="{{route('admin.categories.list')}}?cat=main" class="nav-link">
              <i class="nav-icon fas fa-edit"></i>
              <p>
                Main categories
              </p>
            </a>

          </li>



          <li class="nav-item has-treeview">
            <a href="{{route('admin.categories.list')}}?cat=sub" class="nav-link">
              <i class="nav-icon fas fa-edit"></i>
              <p>
                Sub categories
                <!-- <i class="fas fa-angle-left right"></i> -->
              </p>
            </a>


           <!--  <ul class="nav nav-treeview">
              <li class="nav-item">
                <a href="{{route('admin.categories.list')}}" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>List</p>
                </a>
              </li>

            </ul> -->
          </li>


          <li class="nav-item has-treeview">
            <a href="{{route('admin.product.index')}}" class="nav-link">
              <i class="nav-icon fas fa-edit"></i>
              <p>
                Products
              </p>
            </a>

          </li>

          <li class="nav-item has-treeview">
            <a href="{{route('admin.orders.list')}}" class="nav-link">
              <i class="nav-icon fas fa-edit"></i>
              <p>
                Orders
              </p>
            </a>

          </li>
          <li class="nav-item has-treeview">
            <a href="{{route('admin.users.list')}}?cat=main" class="nav-link">
              <i class="nav-icon fas fa-edit"></i>
              <p>
                All Users
              </p>
            </a>

          </li>
          <li class="nav-item has-treeview">
            <a href="{{route('admin.post.destroy')}}?cat=main" class="nav-link">
              <i class="nav-icon fas fa-edit"></i>
              <p>
                Manage Posts
              </p>
            </a>

          </li>
          <li class="nav-item has-treeview">
            <a href="{{route('admin.showlist')}}" class="nav-link">
              <i class="nav-icon fas fa-edit"></i>
              <p>
                Feedbacks
              </p>
            </a>

          </li>
          <li class="nav-item has-treeview">
            <a href="{{route('admin.showtypes')}}" class="nav-link">
              <i class="nav-icon fas fa-edit"></i>
              <p>
                Set Feedback Type
              </p>
            </a>

          </li>


          <li class="nav-item has-treeview">
            <a href="{{route('ads.index')}}" class="nav-link">
              <i class="nav-icon fas fa-edit"></i>
              <p>
                Advertisement Category
              </p>
            </a>

          </li>
          <li class="nav-item has-treeview">
            <a href="{{route('banner.index')}}" class="nav-link">
              <i class="nav-icon fas fa-edit"></i>
              <p>
                Advertisement Banner
              </p>
            </a>

          </li>
            <li class="nav-item has-treeview">
                <a href="{{route('ad.report')}}" class="nav-link">
                    <i class="nav-icon fas fa-edit"></i>
                    <p>
                        Advertisement Report
                    </p>
                </a>

            </li>

        </ul>
      </nav>
      <!-- /.sidebar-menu -->
    </div>
    <!-- /.sidebar -->
  </aside>
