<!DOCTYPE html>
<html lang="en">
    <head>
        <title>Who I Follow | Admin</title>
        <link rel = "icon" href = 
"{{ asset('public/sports/images') }}/favicon.png" 
        type = "image/x-icon">
         @section('header')
            @include('store.admin.layout.header')
         @show
    </head>
    <body class="hold-transition sidebar-mini layout-fixed">
        <div class="wrapper">
      

        @section('mainheader')
            @include('store.admin.layout.mainheader')
        @show

        @section('aside')
            @include('store.admin.layout.aside')
        @show

        <!-- content -->
        @yield('content')

        @section('footer')
            @include('store.admin.layout.footer')
        @show

      


        {{--  load scripts --}}
        @yield('scripts')
        </div>

    </body>
</html>
