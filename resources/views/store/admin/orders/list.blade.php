@extends('store.admin.layout.master') 

@section('content')

 <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>{{$title}}</h1>
          </div>
          <!-- <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Home</a></li>
              <li class="breadcrumb-item active">DataTables</li>
            </ol>
          </div> -->
        </div>
      </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <div class="row">
          <div class="col-12">

            @if(session()->has('message'))
                <div class="alert alert-success">
                    {{ session()->get('message') }}
                </div>
            @endif
           

            <div class="card">
              
              <!-- /.card-header -->
              <div class="card-body">
                <table id="example1" class="table table-bordered table-striped">
                  <thead>
                  <tr>
                    @foreach($cols as $col)
                    <th>{{$col}}</th>
                    @endforeach

                    <th>Action</th>
                  </tr>
                  </thead>
                  <tbody>

                  @foreach($datas as $data)
                  <tr>
                    
                    <td>{{!empty($data->user) ? $data->user->name : 'N/A'}}</td>
                    @if ($data->invoice_id==''||$data->invoice_id==null)
                    <td>N/A</td>
                    @else
                    <td>{{$data->invoice_id}}</td>
                    @endif
                    @if ($data->billing_name==''||$data->billing_name==null)
                    <td>N/A</td>
                    @else
                    <td>

                      {{$data->billing_name}}
                    
                    </td>
                    @endif
                    @if ($data->billing_email==''||$data->billing_email==null)
                    <td>N/A</td>
                    @else
                    <td>{{$data->billing_email}}</td>
                    @endif
                    @if ($data->billing_phone==''||$data->billing_phone==null)
                    <td>N/A</td>
                    @else
                    <td>{{$data->billing_phone}}</td>
                    @endif
                    @if ($data->billing_address==''||$data->billing_address==null)
                    <td>N/A</td>
                    @else
                    <td>{{$data->billing_address }}</td>
                    @endif

                    @if ($data->billing_zip==''||$data->billing_zip==null)
                    <td>N/A</td>
                    @else
                    <td>{{$data->billing_zip}}</td>
                    @endif
                    <td width="154px"> 
                      <select class="change_order_status" name="order_status" data-orderid="{{$data->id}}">
                        <option value="pending" {{$data->status == "pending" ? "selected" : ""}}>Pending</option>
                        <option value="accept" {{$data->status == "accept" ? "selected" : ""}}>Accept</option>
                        <option value="dispatch" {{$data->status == "dispatch" ? "selected" : ""}}>Dispatch</option>
                        <option value="delivered" {{$data->status == "delivered" ? "selected" : ""}}>Delivered</option>
                        <option value="rejected" {{$data->status == "rejected" ? "selected" : ""}}>Reject</option>
                      </select>
                      <!-- <a href="#" class="btn btn-danger">{{$data->status}}</a> -->
                    </td>
                  <td width="154px">
                      <a href="{{route('admin.orders.view', $data->id)}}" class="btn btn-primary">View</a>
                  </td>
                  </tr>
                  @endforeach
                  </tbody>
              
                </table>
              </div>
              <!-- /.card-body -->
            </div>
            <!-- /.card -->
          </div>
          <!-- /.col -->
        </div>
        <!-- /.row -->
      </div>
      <!-- /.container-fluid -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

  
@stop 

@section('scripts')

<script src="{{asset('public/front')}}/js/notify.js"></script>

<script>
  $(function () {
    $("#example1").DataTable({
      "responsive": true,
      "autoWidth": false,
      "aaSorting": []
    });
    $('#example2').DataTable({
      "paging": true,
      "lengthChange": false,
      "searching": false,
      "ordering": true,
      "info": true,
      "autoWidth": false,
      "responsive": true,
      "aaSorting": []
    });

    // $()
    // change_order_status


    $( ".change_order_status" ).change(function() {

      var order_id = $(this).attr('data-orderid');
      var order_status = $(this).val();

      if (!confirm("Are you sure you wish to change order status?")) {
    
        return;                  //abort!
      }

      $.ajax({
        url: "{{route('admin.orders.status.change')}}",
        data:{order_id:order_id,order_status:order_status},
        // cache: false,
        success: function(html){

          $.notify(html,'success');
          // $("#results").append(html);
        }
      });

      console.log("continire");
    });
  });
</script>
@stop