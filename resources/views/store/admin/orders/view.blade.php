@extends('store.admin.layout.master') 

@section('content')

<style type="text/css">
  
  @import url('https://fonts.googleapis.com/css?family=Open+Sans&display=swap');

body {
    background-color: #eeeeee;
    font-family: 'Open Sans', serif
}

.container {
    margin-top: 50px;
    margin-bottom: 50px
}

.card {
    position: relative;
    display: -webkit-box;
    display: -ms-flexbox;
    display: flex;
    -webkit-box-orient: vertical;
    -webkit-box-direction: normal;
    -ms-flex-direction: column;
    flex-direction: column;
    min-width: 0;
    word-wrap: break-word;
    background-color: #fff;
    background-clip: border-box;
    border: 1px solid rgba(0, 0, 0, 0.1);
    border-radius: 0.10rem
}

.card-header:first-child {
    border-radius: calc(0.37rem - 1px) calc(0.37rem - 1px) 0 0
}

.card-header {
    padding: 0.75rem 1.25rem;
    margin-bottom: 0;
    background-color: #fff;
    border-bottom: 1px solid rgba(0, 0, 0, 0.1)
}

.track {
    position: relative;
    background-color: #ddd;
    height: 7px;
    display: -webkit-box;
    display: -ms-flexbox;
    display: flex;
    margin-bottom: 60px;
    margin-top: 50px
}

.track .step {
    -webkit-box-flex: 1;
    -ms-flex-positive: 1;
    flex-grow: 1;
    width: 25%;
    margin-top: -18px;
    text-align: center;
    position: relative
}

.track .step.active:before {
    background: #FF5722
}

.track .step::before {
    height: 7px;
    position: absolute;
    content: "";
    width: 100%;
    left: 0;
    top: 18px
}

.track .step.active .icon {
    background: #ee5435;
    color: #fff
}

.track .icon {
    display: inline-block;
    width: 40px;
    height: 40px;
    line-height: 40px;
    position: relative;
    border-radius: 100%;
    background: #ddd
}

.track .step.active .text {
    font-weight: 400;
    color: #000
}

.track .text {
    display: block;
    margin-top: 7px
}

.itemside {
    position: relative;
    display: -webkit-box;
    display: -ms-flexbox;
    display: flex;
    width: 100%
}

.itemside .aside {
    position: relative;
    -ms-flex-negative: 0;
    flex-shrink: 0
}

.img-sm {
    width: 80px;
    height: 80px;
    padding: 7px
}

ul.row,
ul.row-sm {
    list-style: none;
    padding: 0
}

.itemside .info {
    padding-left: 15px;
    padding-right: 7px
}

.itemside .title {
    display: block;
    margin-bottom: 5px;
    color: #212529
}

p {
    margin-top: 0;
    margin-bottom: 1rem
}

.btn-warning {
    color: #ffffff;
    background-color: #ee5435;
    border-color: #ee5435;
    border-radius: 1px
}

.btn-warning:hover {
    color: #ffffff;
    background-color: #ff2b00;
    border-color: #ff2b00;
    border-radius: 1px
}

.track {
    display: none;
}
</style>

 <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>{{$title}}</h1>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <div class="row">
          <div class="col-12">

            @if(session()->has('message'))
                <div class="alert alert-success">
                    {{ session()->get('message') }}
                </div>
            @endif
           

            <div>
              

              <div class="container">
                  <article class="card">
                      <header class="card-header"> My Orders  </header>
                      <div class="card-body">
                          <h6>Invoice ID: {{$product_detail->invoice_id}}</h6>
                          <article class="card">
                              <div class="card-body row">
                                  <div class="col"> <strong>Billing name:</strong> <br>{{$product_detail->billing_name}}</div>
                                  <div class="col"> <strong>Billing email:</strong> <br> {{$product_detail->billing_email}} </div>
                                  <div class="col"> <strong>Billing phone:</strong> <br>  {{$product_detail->billing_phone}} </div>
                                  <div class="col"> <strong>Billing address:</strong> <br>  {{$product_detail->billing_address}} </div>
                              </div>
                          </article>

                          <article class="card">
                              <div class="card-body row">
                                  <div class="col"> <strong>Billing city:</strong> <br>{{$product_detail->billing_city}}</div>
                                  <div class="col"> <strong>Billing state:</strong> <br> {{$product_detail->billing_state}} </div>
                                  <div class="col"> <strong>Billing zip:</strong> <br> {{$product_detail->billing_zip}}  </div>
                                  <div class="col"> <strong>Status:</strong> <br> {{$product_detail->status}}  </div>


                              </div>
                          </article>

                          <article class="card">
                              <div class="card-body row">
                                  <div class="col"> <strong>Billing country:</strong> <br> {{$product_detail->billing_country}} </div>
                                  <div class="col"> <strong>Payment method:</strong> <br>{{$product_detail->payment_method}}  </div>
                                  <div class="col"> <strong>Total amount:</strong> <br> {{$product_detail->total_amount}} </div>
                                  <!-- <div class="col"> <strong>Tracking #:</strong> <br> BD045903594059 </div> -->
                              </div>
                          </article>

                          <div class="track">
                              <div class="step active"> <span class="icon"> <i class="fa fa-check"></i> </span> <span class="text">Order confirmed</span> </div>
                              <div class="step active"> <span class="icon"> <i class="fa fa-user"></i> </span> <span class="text"> Picked by courier</span> </div>
                              <div class="step"> <span class="icon"> <i class="fa fa-truck"></i> </span> <span class="text"> On the way </span> </div>
                              <div class="step"> <span class="icon"> <i class="fa fa-box"></i> </span> <span class="text">Ready for pickup</span> </div>
                          </div>
                          <hr>
                          <ul class="row">

                            @if(!is_null($product_detail->ordered_items))

                            <?php
                                $ordered_items=unserialize(base64_decode($product_detail->ordered_items));

                                // echo "<pre>";
                                // print_r($ordered_items);
                            ?>

                            @foreach($ordered_items as $item)
                            <?php
                            // print_r($item->attributes->image);
                            ?>

                              <li class="col-md-4">
                                  <figure class="itemside mb-3">
                                      <div class="aside"><img src="{{$item->attributes->image}}" class="img-sm border"></div>
                                      <figcaption class="info align-self-center">
                                          <p class="title">{{$item->name}} <br> Qty: {{$item->quantity}}</p> <span class="text-muted">Price: ${{$item->price}} </span>
                                      </figcaption>
                                  </figure>
                              </li>
                              @endforeach
                            @endif
                          </ul>
                          <hr> <a href="{{route('admin.orders.list')}}" class="btn btn-warning" data-abc="true"> <i class="fa fa-chevron-left"></i> Back to orders</a>
                      </div>
                  </article>
              </div>


            </div>
            <!-- /.card -->
          </div>
          <!-- /.col -->
        </div>
        <!-- /.row -->
      </div>
      <!-- /.container-fluid -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

  
@stop 

@section('scripts')

<script src="{{asset('public/front')}}/js/notify.js"></script>

<script>
 
</script>
@stop