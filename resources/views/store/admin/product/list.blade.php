@extends('store.admin.layout.master') 

@section('content')

 <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>{{$title}}</h1>
          </div>
          <!-- <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Home</a></li>
              <li class="breadcrumb-item active">DataTables</li>
            </ol>
          </div> -->
        </div>
      </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <div class="row">
          <div class="col-12">

            @if(session()->has('message'))
                <div class="alert alert-success">
                    {{ session()->get('message') }}
                </div>
            @endif
           

            <div class="card">
              <div class="card-header">
                <h3 class="card-title"><a href="{{route('admin.product.create')}}" class="btn btn-primary">Add new</a></h3>
              </div>
              <!-- /.card-header -->
              <div class="card-body">
                <table id="example1" class="table table-bordered table-striped">
                  <thead>
                  <tr>
                    @foreach($cols as $col)
                    <th>{{$col}}</th>
                    @endforeach

                    <th>Action</th>
                  </tr>
                  </thead>
                  <tbody>

                  @foreach($datas as $data)
                  <tr>
                    <td>{{$data->name}}</td>
                    <td>{{$data->price}}</td>
                    <td>
                    
                      @if (file_exists($data->image))
                      <img src="{{asset($data->image)}}" width="100px" height="100px">
                      @endif
                    </td>
                    <td>{{$data->vendor_name}}</td>
                    <td>{{$data->product_description}}</td>
                    <td>{{!empty($data->category) ? $data->category->parent->name : '' }}</td>

                    <td>{{!empty($data->category) ? $data->category->name : '' }}</td>
                    <td>{{$data->vendor_comission}}%</td>
                    <td width="154px"> 
                      <a href="{{route('admin.product.edit',$data->id)}}" class="btn btn-primary">Edit</a>

                      <a href="{{route('admin.product.destroy',$data->id)}}" class="btn btn-danger">Delete</a>
                    </td>
                  </tr>
                  @endforeach
                  </tbody>
              
                </table>
              </div>
              <!-- /.card-body -->
            </div>
            <!-- /.card -->
          </div>
          <!-- /.col -->
        </div>
        <!-- /.row -->
      </div>
      <!-- /.container-fluid -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

  
@stop 

@section('scripts')
<script>
  $(function () {
    $("#example1").DataTable({
      "responsive": true,
      "autoWidth": false,
      "aaSorting": []
    });
    $('#example2').DataTable({
      "paging": true,
      "lengthChange": false,
      "searching": false,
      "ordering": true,
      "info": true,
      "autoWidth": false,
      "responsive": true,
      "aaSorting": []
    });
  });
</script>
@stop