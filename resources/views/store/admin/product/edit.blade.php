@extends('store.admin.layout.master') 

@section('content')
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>{{$title}}</h1>
          </div>
          <!-- <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Home</a></li>
              <li class="breadcrumb-item active">Advanced Form</li>
            </ol>
          </div> -->
        </div>
      </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <!-- SELECT2 EXAMPLE -->
        <div class="card card-default">
          <div class="card-header">
            <!-- <h3 class="card-title">Select2 (Default Theme)</h3> -->

            <div class="card-tools">
              <button type="button" class="btn btn-tool" data-card-widget="collapse"><i class="fas fa-minus"></i></button>
              <button type="button" class="btn btn-tool" data-card-widget="remove"><i class="fas fa-times"></i></button>
            </div>
          </div>
          <!-- /.card-header -->
          <div class="card-body">

            @if ($errors->any())
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif

            @if(session()->has('message'))
                <div class="alert alert-success">
                    {{ session()->get('message') }}
                </div>
            @endif

            <form method="post" action="{{route('admin.product.update',Request::Segment(3))}}" enctype="multipart/form-data">
                @csrf
                <!-- /
            <div class="row">
              .col -->
                <div class="col-md-6">

                  <div class="form-group">
                      <label for="category">Product name</label>
                      <input type="text" name="product_name" class="form-control" id="category" placeholder="Product name" value="{{$data->name}}">
                  </div>

                  <div class="form-group">
                      <label for="price">Price</label>
                      <input type="text" name="price" class="form-control" id="price" placeholder="Price" value="{{$data->price}}">
                  </div>



                  <div class="form-group">
                      <label for="vendor_name">Vendor name</label>
                      <input type="text" name="vendor_name" class="form-control" id="vendor_name" placeholder="Vendor name" value="{{$data->vendor_name}}">
                  </div>


                   <div class="form-group">
                      <label for="product_description">Product description</label>
                      <textarea name="product_description" class="form-control" id="product_description" placeholder="Product description">{{$data->product_description}}</textarea>
                  </div>


                  <div class="form-group">
                    <label>Main category</label>
                    <select class="form-control main_cat">
                      <option value="">Select</option>

                      @foreach($cats as $cat)
                      <option value="{{$cat->id}}" {{$data->category->parent_id == $cat->id ? 'selected' : ''}}>{{$cat->name}}</option>
                      @endforeach
                   
                    </select>
                  </div>


                  <div class="form-group sub_cate_hide">
                    <label>Sub category</label>
                    <select class="form-control sub_cate" name="category">
                      
                   
                    </select>
                  </div>


                  <div class="form-group">
                      <label for="vendor_comission">Vendor comission</label>
                      <input type="number" name="vendor_comission" class="form-control" id="vendor_comission" placeholder="Vendor comission" value="{{$data->vendor_comission}}">
                  </div>


                   <div class="form-group">
                      <label for="product_image">Thumbnail</label>
                      <input type="file" name="product_image" class="form-control" id="product_image">

                      @if (file_exists($data->image))
                      
                      <div class="col-md-12" style="padding-top: 12px">
                        <img src="{{asset($data->image)}}" width="100px" height="100px">

                      </div>

                      @endif



                      
                  </div>

                  <div class="form-group">
                      <label for="product_gallery">Gallery</label>
                      <input type="file" name="product_gallery[]" class="form-control" id="product_gallery" multiple>
                      <div class="row">
                      <div class="col-md-12">
                      @foreach($data->gallery as $gallery)
                      @if (file_exists($gallery->path))
                      
                        <div class="col-md-3 float-left" style="padding-top: 12px">
                          <img src="{{asset($gallery->path)}}" width="100px" height="100px">

                        </div>

                      @endif
                      @endforeach
                      </div>
                      </div>

                  </div>


                  <!-- /.form-group -->
                  
                  <div class="card-footer">
                    <button type="submit" class="btn btn-primary">Submit</button>
                  </div>

                  <!-- /.form-group -->
                </div>
                <!-- /.col -->
             
            </div>
             </form>
            <!-- /.row -->

            
            <!-- /.row -->
          </div>
          <!-- /.card-body -->
         
        </div>
        <!-- /.card -->

  
        <!-- /.row -->
      </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->
  </div>
@stop 

@section('scripts')
<script>
  $(function () {
    //Initialize Select2 Elements
    $('.select2').select2()

    //Initialize Select2 Elements
    $('.select2bs4').select2({
      theme: 'bootstrap4'
    })

    //Datemask dd/mm/yyyy
    $('#datemask').inputmask('dd/mm/yyyy', { 'placeholder': 'dd/mm/yyyy' })
    //Datemask2 mm/dd/yyyy
    $('#datemask2').inputmask('mm/dd/yyyy', { 'placeholder': 'mm/dd/yyyy' })
    //Money Euro
    $('[data-mask]').inputmask()

    //Date range picker
    $('#reservationdate').datetimepicker({
        format: 'L'
    });
    //Date range picker
    $('#reservation').daterangepicker()
    //Date range picker with time picker
    $('#reservationtime').daterangepicker({
      timePicker: true,
      timePickerIncrement: 30,
      locale: {
        format: 'MM/DD/YYYY hh:mm A'
      }
    })
    //Date range as a button
    $('#daterange-btn').daterangepicker(
      {
        ranges   : {
          'Today'       : [moment(), moment()],
          'Yesterday'   : [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
          'Last 7 Days' : [moment().subtract(6, 'days'), moment()],
          'Last 30 Days': [moment().subtract(29, 'days'), moment()],
          'This Month'  : [moment().startOf('month'), moment().endOf('month')],
          'Last Month'  : [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
        },
        startDate: moment().subtract(29, 'days'),
        endDate  : moment()
      },
      function (start, end) {
        $('#reportrange span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'))
      }
    )

    //Timepicker
    $('#timepicker').datetimepicker({
      format: 'LT'
    })
    
    //Bootstrap Duallistbox
    $('.duallistbox').bootstrapDualListbox()

    //Colorpicker
    $('.my-colorpicker1').colorpicker()
    //color picker with addon
    $('.my-colorpicker2').colorpicker()

    $('.my-colorpicker2').on('colorpickerChange', function(event) {
      $('.my-colorpicker2 .fa-square').css('color', event.color.toString());
    });

    $("input[data-bootstrap-switch]").each(function(){
      $(this).bootstrapSwitch('state', $(this).prop('checked'));
    });

  })


  $('.main_cat').change(function(){
      // alert(1);
      getCat($(this).val() , null);
    });

  var main_id = '<?php echo $data->category->parent_id;?>';
  var sub_id = '{{$data->category_id}}';
  getCat(main_id , sub_id)

  // console.log(main_id,'abcs');

    function getCat($id,sub_cat = 0){
      var parent_id = $id;
      // alert(sub_cat);
      $.ajax({
          type: "get",
          url: "{{route('admin.category.by.id')}}",
          data: {parent_id:parent_id},
          success: function(data) {
              var cat = data.data;

              var html = '';

              if (cat.length  == 0) {
                $('.sub_cate_hide').css('display','none');
              }

              html += '<option>Select</option>';

              for (var i = 0; i < cat.length; i++) {

                if (Number(sub_cat) == Number(cat[i].id)) {
                  var selected = 'selected';
                }else{
                  var selected = '';

                }
                html += '<option value="'+cat[i].id+'" '+selected+'>'+cat[i].name+'</option>';
              }
              $('.sub_cate').html(html);
                // console.log(html);
          }
      });

      
    }

</script>
@stop