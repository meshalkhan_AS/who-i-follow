@extends('store.admin.layout.master')

@section('content')
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">
                        <h1>Edit User</h1>
                    </div>
                    <!-- <div class="col-sm-6">
                      <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="#">Home</a></li>
                        <li class="breadcrumb-item active">Advanced Form</li>
                      </ol>
                    </div> -->
                </div>
            </div><!-- /.container-fluid -->
        </section>

        <!-- Main content -->
        <section class="content">
            <div class="container-fluid">
                <!-- SELECT2 EXAMPLE -->
                <div class="card card-default">
                    <div class="card-header">
                        <!-- <h3 class="card-title">Select2 (Default Theme)</h3> -->

                        <div class="card-tools">
                            <button type="button" class="btn btn-tool" data-card-widget="collapse"><i class="fas fa-minus"></i></button>
                        </div>
                    </div>
                    <!-- /.card-header -->
                    <div class="card-body">

                        @if ($errors->any())
                            <div class="alert alert-danger">
                                <ul>
                                    @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
                        @endif

                        @if(session()->has('message'))
                            <div class="alert alert-success">
                                {{ session()->get('message') }}
                            </div>
                        @endif

                        <form method="post" action="{{route('admin.user.update',Request::Segment(3))}}" enctype="multipart/form-data">
                            @csrf
                            <div class="row">
                                <div class="col-md-6">

                                    <div class="form-group">
                                        <label for="first">First Name</label>
                                        <input type="text" name="first_name" class="form-control" id="category" placeholder="First name" value="{{$data->fname}}">
                                    </div>
                                    <div class="form-group">
                                        <label for="Email">Email</label>
                                        <input type="text" name="email" class="form-control" id="email" placeholder="Email" value="{{$data->email}}">
                                    </div>
                                    <div class="form-group">
                                        <label for="Mobile">Mobile</label>
                                        <input type="text" name="mobile" class="form-control" id="email" placeholder="Mobile" value="{{$data->mob}}">
                                    </div>
                                    <div class="form-group">
                                        <label for="age">Age</label>
                                        <input type="text" name="age" class="form-control" id="email" placeholder="Age" value="{{$data->age}}" readonly>
                                    </div>

                                    <div class="form-group">
                                        <label for="bio">Personal Bio</label>
                                        <textarea type="textarea" name="bio" class="form-control" placeholder="Personal Bio" id="bio">{{$data->bio}}</textarea>
                                    </div>
                                    <div class="form-group">

                                        <input {{isset($data['exp_status']) && $data['exp_status'] == '1' ? 'checked' : ''}} type="checkbox" name="exp" id="exp" value="1">
                                        <label for="exp">Please Check/Uncheck if you want to change the expert status.</label>
                                    </div>
                                </div>
                                <div class="col-md-6">

                                    <div class="form-group">
                                        <label for="last">Last Name</label>
                                        <input type="text" name="last_name" class="form-control" id="category" placeholder="Last name" value="{{$data->lname}}">
                                    </div>



                                    <div class="form-group">
                                        <label for="Hometown">Hometown</label>
                                        <input type="text" name="hometown" class="form-control" id="vendor_name" placeholder="Hometown" value="{{$data->hometown}}">
                                    </div>
                                    <div class="form-group">
                                        <label for="city">City</label>
                                        <input type="text" name="city" class="form-control" id="city" placeholder="City" value="{{$data->city}}">
                                    </div>
                                    <div class="form-group">
                                        <label for="dob">Date of Birth</label>
                                        <input id="dob" type="date" class="form-control" name="dob" value="{{ $data->dob }}" required autofocus>
                                        @if ($errors->has('dob'))
                                            <span class="help-block">
 <strong>{{ $errors->first('dob') }}</strong>
 </span>
                                        @endif
                                    </div>
                                    <label for="Gender">Gender</label>
                                    <select class="form-control" name="gender">
                                        <option value="{{$data->gender}}" selected hidden>{{$data->gender}}</option>
                                        <option value="Male" name="male">Male</option>
                                        <option value="Female" name="female">Female</option>
                                        <option value="Others" name="other">Others</option>
                                    </select>
                                    <input type="hidden" name="user_id" class="form-control" id="vendor_name" placeholder="Gender" value="{{$data->user_id}}">
                                </div>
                            </div>
                            <div class="card-footer text-center">
                                <button type="submit" class="btn btn-primary center">Submit</button>
                            </div>
                        </form>
                    </div>
                </div>
                <!-- /.row -->
            </div><!-- /.container-fluid -->
        </section>
        <!-- /.content -->
    </div>
@stop

@section('scripts')
    <script>
    </script>
@stop