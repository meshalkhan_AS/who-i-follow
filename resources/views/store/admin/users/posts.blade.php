@extends('store.admin.layout.master') 

@section('content')

 <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>{{$title}}</h1>
          </div>
          <!-- <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Home</a></li>
              <li class="breadcrumb-item active">DataTables</li>
            </ol>
          </div> -->
        </div>
      </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <div class="row">
          <div class="col-12">

            @if(session()->has('message'))
                <div class="alert alert-success">
                    {{ session()->get('message') }}
                </div>
            @endif
            

            <div class="card">
              <div class="card-body">
                <table id="example1" class="table table-bordered table-striped">
                  <thead>
                  <tr>
                    @foreach($cols as $col)
                    <th>{{$col}}</th>
                    @endforeach
                  </tr>
                  </thead>
                  <tbody>
                  @foreach($datas as $data)
                  @php
            $postimages=\App\Models\Admin\PostImage::select('image','id')->where('post_id',$data->id)->where('user_id',$data->user_id)->get();
        @endphp
                  <tr>
                    <td>{{ \Illuminate\Support\Str::limit($data->fname, 40, $end= '...') }}</td>
                    <td>{{ \Illuminate\Support\Str::limit($data->lname, 40, $end= '...') }}</td>
                    @if($data->caption==Null)
                    <td>Caption Not Found</td>
                    @else
                    <td>{{ \Illuminate\Support\Str::limit($data->caption, 75, $end= '...') }}</td>
                    @endif
                    @if(count($postimages) == 1) 
                    <td>This Post Has Image</td>
                    @elseif(count($postimages) > 1)
                    <td>This Post Has Images</td>
                    @else
                    <td>No Image Found</td>
                    @endif
                    @if($data->exp_status == 1)
                    <td style="color:green; font-weight: bold;">Expert Post</td>
                    @else
                    <td style="font-weight: bold;" >Normal Post</td>
                    @endif
                    <td width="154px"> 
                        <a class="btn btn-secondary dropdown-toggle" href="#" role="button" id="dropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            View
                          </a>
                        
                          <div class="dropdown-menu" aria-labelledby="dropdownMenuLink">
                            <a class="dropdown-item" href="{{route('admin.post.view',$data->id)}}">View</a>
                            <a class="dropdown-item" onclick="return confirm('Are you sure you want to delete this Post?')" href="{{route('admin.post.delete',$data->id)}}">Delete</a>
                          </div>
                    </td>
                  </tr>
                  @endforeach
                  </tbody>
              
                </table>
              </div>
              <!-- /.card-body -->
            </div>
            <!-- /.card -->
          </div>
          <!-- /.col -->
        </div>
        <!-- /.row -->
      </div>
      <!-- /.container-fluid -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

  
@stop 

@section('scripts')
<script>
  $(function () {
    $("#example1").DataTable({
      "responsive": true,
      "autoWidth": false,
      "aaSorting": []
    });
    $('#example2').DataTable({
      "paging": true,
      "lengthChange": false,
      "searching": false,
      "ordering": true,
      "info": true,
      "autoWidth": false,
      "responsive": true,
      "aaSorting": []
    });
  });
</script>
@stop