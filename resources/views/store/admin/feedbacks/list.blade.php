@extends('store.admin.layout.master') 

@section('content')

 <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>{{$title}}</h1>
          </div>
          <!-- <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Home</a></li>
              <li class="breadcrumb-item active">DataTables</li>
            </ol>
          </div> -->
        </div>
      </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <div class="row">
          <div class="col-12">

            @if(session()->has('message'))
                <div class="alert alert-success">
                    {{ session()->get('message') }}
                </div>
            @endif
           

            <div class="card">
              <div class="card-body">
                <table id="example1" class="table table-bordered table-striped">
                  <thead>
                  <tr>
                    @foreach($cols as $col)
                    <th>{{$col}}</th>
                    @endforeach
                  </tr>
                  </thead>
                  <tbody>

                  @foreach($datas as $data)
                  @php
                  $userdetail=\App\Models\Admin\UserProfile::where('user_id',$data->user_id)->get();
                  $feedbacktype=\App\FeedbackType::where('id',$data->feed_type)->get();
                  $feedimages=\App\FeedbackImages::select('image','id')->where('feedback_id',$data->id)->where('user_id',$data->user_id)->get();
              @endphp
                  <tr>
                      @foreach ($userdetail as $item)
                    <td>{{ \Illuminate\Support\Str::limit($item->fname, 40, $end= '...') }}</td>
                    <td>{{ \Illuminate\Support\Str::limit($item->lname, 40, $end= '...') }}</td>
                    @endforeach
                    @if ($data->discription!=null)
                    <td>{{ \Illuminate\Support\Str::limit($data->discription, 40, $end= '...') }}</td>
                    @else
                    <td>No description Found!</td>
                    @endif
                    @if (count($feedbacktype)>0)
                    @foreach ($feedbacktype as $type)
                    <td>{{ \Illuminate\Support\Str::limit($type->type, 40, $end= '...') }}</td>
                    @endforeach
                    @else
                    <td>No Type Found!</td>
                    @endif
                    @if(count($feedimages) == 1) 
                    <td>This Post Has Image</td>
                    @elseif(count($feedimages) > 1)
                    <td>This Post Has Images</td>
                    @else
                    <td>No Image Found</td>
                    @endif
                    <td>{{ \Illuminate\Support\Str::limit($data->created_at, 40, $end= '...') }}</td>
                    
                    <td width="154px"> 
                        <a class="btn btn-secondary dropdown-toggle" href="#" role="button" id="dropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            View
                          </a>
                        
                          <div class="dropdown-menu" aria-labelledby="dropdownMenuLink">
                            <a class="dropdown-item" href="{{route('admin.feed.view',$data->id)}}">View</a>
                            <a class="dropdown-item" onclick="return confirm('Are you sure you want to delete this Feedback?')" href="{{route('admin.feed.delete',$data->id)}}">Delete</a>
                          </div>
                    </td>
                  </tr>
                  @endforeach
                  </tbody>
              
                </table>
              </div>
              <!-- /.card-body -->
            </div>
            <!-- /.card -->
          </div>
          <!-- /.col -->
        </div>
        <!-- /.row -->
      </div>
      <!-- /.container-fluid -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

  
@stop 

@section('scripts')
<script>
  $(function () {
    $("#example1").DataTable({
      "responsive": true,
      "autoWidth": false,
      "aaSorting": []
    });
    $('#example2').DataTable({
      "paging": true,
      "lengthChange": false,
      "searching": false,
      "ordering": true,
      "info": true,
      "autoWidth": false,
      "responsive": true,
      "aaSorting": []
    });
  });
</script>
@stop