@extends('store.admin.layout.master') 

@section('content')
<style>
    #newfeed{
        display: none;
    }
</style>
 <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
          <h1>{{$title}}</h1>
          </div>
          <!-- <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Home</a></li>
              <li class="breadcrumb-item active">DataTables</li>
            </ol>
          </div> -->
        </div>
      </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <div class="row">
          <div class="col-12">

            @if(session()->has('message'))
                <div class="alert alert-success">
                    {{ session()->get('message') }}
                </div>
            @endif
           

            <div class="card">
              <div class="card-body">
                  @if(count($datas)>0)
                <table id="example1" class="table table-bordered table-striped">
                    <thead>
                    <tr>
                      @foreach($cols as $col)
                      <th>{{$col}}</th>
                      @endforeach
                    </tr>
                    </thead>
                    <tbody>
  
                    @foreach($datas as $data)
                    <tr>
                      <td>{{$data->type}}</td>
                      @if($data->status ==1)
                      <td>Active</td>
                      @else
                      <td>In Active</td>
                      @endif
                      <td width="154px"> 
                          <a class="btn btn-secondary dropdown-toggle" href="#" role="button" id="dropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                              View
                            </a>
                          
                            <div class="dropdown-menu" aria-labelledby="dropdownMenuLink">
                              <a class="dropdown-item" href="{{route('admin.type.edit',$data->id)}}">Edit</a>
                              <a class="dropdown-item" onclick="return confirm('Are you sure you want to delete this Feedback Type?')" href="{{route('admin.type.destroy',$data->id)}}">Delete</a>
                            </div>
                      </td>
                    </tr>
                    @endforeach
                    </tbody>
                
                  </table>
            @endif
                  <form method="post" action="{{route('admin.feedtype')}}" enctype="multipart/form-data">
                @csrf
                    <input id="multiplefeedtype" type='button'style="border: none;background-color:rgba(0,0,0,.5); color:white; width:auto; border-radius: 30px;" value='Add Feed Type'>
                <div class="col-lg-12 no-pdd form-group" id="newfeed">
                    <div class="sn-field">
                <label>Add New Feed Types</label>
                    </div>
                    <div class="field_wrapper">
                        <div class="new_wrapper">
                            <input type="text" name="feedtype[]" id="feedtype" style="margin-bottom: 20px; width:97%" placeholder="Enter New Feedback Type">
                        </div>
                        <div>
                          <a href="javascript:void(0);" class="add_button" title="Add field"><i class="fa fa-plus-square" style="font-size:25px; color:rgba(0,0,0,.5); padding:4px"></i></a>
                        </div>
                    </div>
                    <div class="text-center" style="margin-top: 40px;">
                        <button type="submit" class="btn btn-primary center">Submit</button>
                    </div>
                </div>
                
                  </form>
              </div>
              <!-- /.card-body -->
            </div>
            <!-- /.card -->
          </div>
          <!-- /.col -->
        </div>
        <!-- /.row -->
      </div>
      <!-- /.container-fluid -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

  
@stop 

@section('scripts')
<script>
    $(function () {
    $("#example1").DataTable({
      "responsive": true,
      "autoWidth": false,
      "aaSorting": []
    });
    $('#example2').DataTable({
      "paging": true,
      "lengthChange": false,
      "searching": false,
      "ordering": true,
      "info": true,
      "autoWidth": false,
      "responsive": true,
      "aaSorting": []
    });
  });
    $(document).ready(function(){
            var maxField = 10; //Input fields increment limitation
            var addButton = $('.add_button'); //Add button selector
            var wrapper = $('.field_wrapper'); //Input field wrapper
            var fieldHTML = '<div class="remonclick"><input type="text" class="remonclick" name="feedtype[]" id="feedtype" style="margin-bottom: 20px; width:97%" placeholder="Enter New Feedback Type"/><a href="javascript:void(0);" class="remove_button"><i class="fa fa-minus-square float-right" style="font-size:25px;color:red; padding:4px;"></i></a></div>'; //New input field html 
            var x = 1; //Initial field counter is 1
            
            //Once add button is clicked
            $(addButton).click(function(){
                //Check maximum number of input fields
                if(x < maxField){ 
                    x++; //Increment field counter
                    $('.new_wrapper').append(fieldHTML);
                }
            });
            
            //Once remove button is clicked
            $(wrapper).on('click', '.remove_button', function(e){
                e.preventDefault();
                $(this).parent('div').remove(); //Remove field html
                x--;//Decrement field counter
            });
        });

        $("#multiplefeedtype").click(function() { 
    // assumes element with id='button'
    var change = document.getElementById("multiplefeedtype");
    $("#newfeed").toggle();
    $("#feedtype").val("");
    $(".urlminu").val("");
    $(".remonclick").hide();
    //$(this).val('Close');
    if ($(this).val() == "Add Feed Type")
                {
                    $(this).val('Cancel');
                }
                else {
                    $(this).val('Add Feed Type');
                    
                }
});
</script>
@stop