@extends('store.admin.layout.master') 

@section('content')
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
 <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Feedback</h1>
          </div>
          <!-- <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Home</a></li>
              <li class="breadcrumb-item active">DataTables</li>
            </ol>
          </div> -->
        </div>
      </div><!-- /.container-fluid -->
    </section>
    <section class="content">
        <style>
            .post-bar {
            float: left;
            width: 100%;
            background-color: #fff;
            margin-bottom: 20px;
            padding: 20px;
            box-shadow: 0 0 2px rgba(0, 0, 0, .28);
            border-radius: 10px;
        }
        .epi-sec {
            float: left;
            width: 100%;
            padding: 0
        }
        
        .ed-opts {
            float: right;
            position: relative;
            left: 4px;
            margin-top: 6px;
            cursor: pointer
        }
        .job_descp {
            float: left;
            width: 100%
        }
        
        .job_descp h3 {
            color: #333 !important;
            font-size: 16px !important;
            font-weight: 600 !important;
            margin-bottom: 15px !important;
        }
        
        .post-bar-title{
            box-shadow: none !important;
            border-left: 4px solid #e4e4e4;
        }
        .post_topbar {
            float: left;
            width: 100%;
            position: relative
        }
        .usy-dt {
            float: left;
            width: 100%;
        }
        .usy-name img {
            margin-top: 4px;
            margin-right: 6px
        }
        .usy-name{
            float:left;
            margin-left:15px;
            margin-top: 10px;
        }
        .ed-options li {
            float: left;
            width: 100%;
        }
        
        .ed-options li:last-child {
            margin-bottom: 0
        }
        .ed-options{
            position:absolute;
            top:100%;
            right:9px;
            width: 110px !important;
            background-color:#fff;
            -webkit-box-shadow:0 0 2px rgba(0,0,0,.28);
            -moz-box-shadow:0 0 2px rgba(0,0,0,.28);
            -ms-box-shadow:0 0 2px rgba(0,0,0,.28);
            -o-box-shadow:0 0 2px rgba(0,0,0,.28);
            box-shadow:0 0 2px rgba(0,0,0,.28);
            opacity:0;
            visibility:hidden;
            z-index:0;
            margin-top: 10px;
            cursor: pointer;
        }
        ul {
          list-style-type: none;
          padding: 0;
        }
        .ed-options.active{
            opacity:1;
            visibility:visible;
            z-index:999;
        }
        .ed-options li {
            padding: 10px;
        }
        .ed-options li:hover {
            background-color: #f1f1f1;
        }
        .ed-options li a{
            color:#555;
            font-weight:400;
            cursor: pointer;
            font-size: 10px;
        }
        .ed-options li a .fa {
            background-color: #e4e4e4;
            padding: 5px 6px;
            border-radius: 100px;
            margin-right: 6px;
            font-size: 12px;
            top: 0 !important;
        }
        .usy-name h3, .request-info h3{
            color:#000;
            font-size:14px;
            text-transform:capitalize;
            font-weight:400;
            margin-bottom:2px;
        }
        .top-profiles {
            float: left;
            border: 1px solid #e5e5e5;
            margin-bottom: 20px;
            margin-top: 12px;
        }
        .post-popup {
            width: 570px;
            margin: 0 auto;
            position: fixed;
            top: 50%;
            left: 50%;
            -webkit-transform: translateX(-50%) translateY(-50%) scale(.65);
            -moz-transform: translateX(-50%) translateY(-50%) scale(.65);
            -ms-transform: translateX(-50%) translateY(-50%) scale(.65);
            -o-transform: translateX(-50%) translateY(-50%) scale(.65);
            transform: translateX(-50%) translateY(-50%) scale(.65);
            opacity: 0;
            visibility: hidden;
            z-index: 0
        }
        .post-popup.active {
            opacity: 1;
            visibility: visible;
            z-index: 999;
            -webkit-transform: scale(1) translateX(-50%) translateY(-50%)
        }
        .ed-options li {
            float: left;
            width: 100%;
        }
        
        .ed-options li:last-child {
            margin-bottom: 0
        }
        .usy-name span{
            color:#555;
            font-size:10px;
        }
        .usy-dt img, .noty-user-img img {
            width: 40px;
            height: 40px;
            background-position: center;
            background-repeat: no-repeat;
            background-size: contain !important;
            object-fit: cover !important;
            -webkit-border-radius:50%;
            -moz-border-radius:50%;
            border-radius:50%;
            margin-top:2px
        }
        .job_descp > p {
            color: #666 !important;
            font-size: 15px !important;
            line-height: 24px !important;
            margin-bottom: 20px !important;
            overflow-wrap: break-word !important;
            margin-top: 35px;
        }
        
        .job_descp > p a {
            color: #dc3545 !important;
            font-weight: 600 !important;
        
        }
        .job-status-bar {
            border-bottom: 1px solid #e4e4e4;
            padding-bottom: 69px
        }
        </style>
        <div class="container-fluid">
          <div class="row">
            <div class="col-12">
                @php
    $feedimages=\App\FeedbackImages::select('image','id')->where('feedback_id',$feed->id)->where('user_id',$feed->user_id)->get();
@endphp

<div class="post-bar rounded-0">
    <div class="post_topbar">

        <div class="usy-name">
            @foreach($userdetail as $user)
            <a href=""><h3>{{ $user->fname}} {{$user->lname}}</h3></a>
            @endforeach
            <span>{{date_format($feed->created_at,'F d, Y ')}}</span>
        </div>
        <a onclick="return confirm('Are you sure you want to delete this Feedback?')" href="{{route('admin.feed.delete',$feed->id)}}" class="btn btn-danger float-right">Delete</a>
    </div>
    <div class="epi-sec">
        <div class="job_descp">
            <p>{{$feed->discription}}</p>

            @if(count($feedimages)>0)
              

                <div class="top-profiles">
                    <div id="gall-{{$feed->id}}" ></div>
                    <script type="text/javascript">

                        var obj = <?php echo json_encode($feedimages); ?>;
                        var s= <?php echo json_encode($feed->id); ?>;
                        const arrayy<?php echo json_encode($feed->id); ?>=[];

                        for(var i=0;i<obj.length;i++)
                        {
                            arrayy<?php echo json_encode($feed->id); ?>.push(<?php echo json_encode(url('/').'/public/storage/feeds/'); ?>+'/'+obj[i].image);
                        }
                        $(function() {
                            $('#gall-'+<?php echo json_encode($feed->id); ?>).imagesGrid({
                                images: arrayy<?php echo json_encode($feed->id); ?>,
                                align: true,
                                getViewAllText: function(imgsCount) { return 'View all' }
                            });
                        });
                    </script>
                </div>
            @endif
        </div>
    </div>
</div>



            </div>
            <!-- /.col -->
          </div>
          <!-- /.row -->
        </div>
        <!-- /.container-fluid -->
      </section>
      <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->
    @stop

