@extends('store.admin.layout.master') 

@section('content')

 <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>{{$title}}</h1>
          </div>
          <!-- <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Home</a></li>
              <li class="breadcrumb-item active">DataTables</li>
            </ol>
          </div> -->
        </div>
      </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <div class="row">
          <div class="col-12">

            @if(session()->has('message'))
                <div class="alert alert-success">
                    {{ session()->get('message') }}
                </div>
            @endif
           

            <div class="card">
              <div class="card-header">
                <h3 class="card-title"><a href="{{$request->cat == 'main' ?  route('admin.categories.create').'?cat=main' : route('admin.categories.create').'?cat=sub'}}" class="btn btn-primary">Add new</a></h3>
              </div>
              <!-- /.card-header -->
              <div class="card-body">
                <table id="example1" class="table table-bordered table-striped">
                  <thead>
                  <tr>
                    @foreach($cols as $col)
                    <th>{{$col}}</th>
                    @endforeach

                    <th>Action</th>
                  </tr>
                  </thead>
                  <tbody>

                  @foreach($cats as $cat)
                  <tr>
                    <td>{{$cat->name}}</td>
                    @if($request->cat == "sub")
                    <td>{{$cat->parent->name}}</td>
                    @endif
                    <td> 

                      


                      <a href="{{$request->cat == 'main' ?  route('admin.categories.edit',$cat->id).'?cat=main' : route('admin.categories.edit',$cat->id).'?cat=sub'}}" class="btn btn-primary">Edit</a>

                      <!-- <a href="{{route('admin.categories.edit',$cat->id)}}" class="btn btn-primary">Edit</a> -->

                      <a href="{{$request->cat == 'main' ?  route('admin.categories.destroy',$cat->id).'?cat=main' : route('admin.categories.destroy',$cat->id).'?cat=sub'}} " class="btn btn-danger" onclick="return confirm('Are you sure you want to delete this? All the related sub categories and products will also be delete');"> Delete</a>
                   
                    </td>
                  </tr>
                  @endforeach
                  </tbody>
              
                </table>
              </div>
              <!-- /.card-body -->
            </div>
            <!-- /.card -->
          </div>
          <!-- /.col -->
        </div>
        <!-- /.row -->
      </div>
      <!-- /.container-fluid -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

  
@stop 

@section('scripts')
<script>
  $(function () {
    $("#example1").DataTable({
      "responsive": true,
      "autoWidth": false,
      "aaSorting": []
    });
    $('#example2').DataTable({
      "paging": true,
      "lengthChange": false,
      "searching": false,
      "ordering": true,
      "info": true,
      "autoWidth": false,
      "responsive": true,
      "aaSorting": []
    });
  });
</script>
@stop