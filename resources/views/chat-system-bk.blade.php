<!DOCTYPE html>
<html>

@include('sports.head')

<body style="padding: 0px; overflow: hidden;">

<!-- jQuery -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.1.0/jquery.min.js"></script>
<!-- Firebase -->
<script src="https://www.gstatic.com/firebasejs/3.3.0/firebase.js"></script>
<!-- Firechat -->
<link rel="stylesheet" href="https://cdn.firebase.com/libs/firechat/3.0.1/firechat.min.css" />
<script src="https://cdn.firebase.com/libs/firechat/3.0.1/firechat.min.js"></script>

@include('sports.head_main')

<div class="wrapper" style="padding: 0px;">

<div class="wrapper">
        <!-- Sidebar  -->
        <nav id="sidebar" class="active">
            <div class="sidebar-header">
                <strong></strong>
            </div>

            <ul class="list-unstyled components">
                <li>
                    <a href="#">
                        <i class="fas fa-comments"></i>
                    </a>
                </li>
            </ul>
        </nav>

        <!-- Page Content  -->
        <div id="content">

            <div class="container-fluid m-0 p-0">
                <div class="row">
                    <div class="col-lg-3">
                        <div class="chats-area">
                            <div class="top">
                                <div class="row">
                                    <div class="col-lg-8">
                                        <p>Chats</p>
                                    </div>
                                    <div class="col-lg-2">
                                        <a href="#">
                                            <i class="fas fa-glasses"></i>
                                        </a>
                                    </div>
                                    <div class="col-lg-2">
                                        <a href="#">
                                            <i class="fas fa-edit"></i>
                                        </a>
                                    </div>
                                </div>
                            </div>

                            <div class="search-box">
                                <input type="search" name="search" class="form-control" placeholder="search chats">
                            </div>

                            <div class="chats-list">
                                <p>You don't have any chats yet. Create a group or start a direct message to get the conversations going!</p>


                                <div class="list-group" id="list-tab" role="tablist">

                                  <a class="list-group-item list-group-item-action active" id="list-home-list" data-toggle="list" href="#list-home" role="tab" aria-controls="home">
                                      <div class="chats">
                                            <div class="media">
                                              <img src="{{ asset('public/sports/images') }}/default-group.avatar.png" class="mr-3" alt="...">
                                              <div class="media-body">
                                                <div class="chat-name-and-time">
                                                    <h4 class="chat-name">
                                                        <div class="chat-title">
                                                            <span>Group Name</span>
                                                        </div>
                                                        <div>1:09 AM</div>
                                                    </h4>
                                                </div>

                                                <div class="chat-content">
                                                    <p><span>member-name:</span> hello</p>
                                                </div>
                                              </div>
                                            </div>
                                        </div>
                                    </a>

                                    <a class="list-group-item list-group-item-action" id="list-profile-list" data-toggle="list" href="#list-profile" role="tab" aria-controls="profile">
                                        <div class="chats">
                                            <div class="media">
                                              <img src="{{ asset('public/sports/images') }}/default-group.avatar.png" class="mr-3" alt="...">
                                              <div class="media-body">
                                                <div class="chat-name-and-time">
                                                    <h4 class="chat-name">
                                                        <div class="chat-title">
                                                            <span>Single Chat</span>
                                                        </div>
                                                        <div>1:30 AM</div>
                                                    </h4>
                                                </div>

                                                <div class="chat-content">
                                                    <p><span>Name:</span> Hi!</p>
                                                </div>
                                              </div>
                                            </div>
                                        </div>
                                    </a>

                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="col-lg-9 p-0">
                        <div class="right-side">

                            <div class="tab-content" id="nav-tabContent">
                              <div class="tab-pane fade show active" id="list-home" role="tabpanel" aria-labelledby="list-home-list">

                                    <!-- Chat Start -->
                                    <div class="full-chat">

                                        <!-- Chat Header -->
                                        <div class="chat-header">
                                            <div class="media">
                                              <img src="{{ asset('public/sports/images') }}/default-group.avatar.png" class="mr-3">
                                              <div class="media-body custom-class">
                                                <h5 class="mt-0">Group Name <i class="fas fa-caret-down"></i></h5>
                                                <a href="#"><i class="fa fa-times"></i></a>
                                              </div>
                                            </div>
                                        </div>

                                        <!-- Add Member Area -->
                                        <div class="add-member-box">
                                            <a href="#" class="close-x"><i class="fas fa-times"></i></a>
                                            <div class="btn-member">
                                                <a href="#"><i class="fas fa-user-plus"></i> Add Members</a>
                                            </div>
                                        </div>

                                        <!-- Message Middle Area -->
                                        <div class="full-message-box">

                                            <div class="chat-layout">
                                                <div class="chat-middle">
                                                    <div class="meesage-box">
                                                        <div class="message-time">
                                                            <p>1:09 AM</p>
                                                        </div>
                                                    </div>

                                                    <div class="messages-area">
                                                        <div class="media">
                                                         <img src="{{ asset('public/sports/images') }}/default-group.avatar.png" class="mr-3">
                                                            <div class="media-body">
                                                                <h4 class="mt-0">Member Name</h4>
                                                                <div class="content-and-icons">
                                                                    <p class="recieve-message">Message</p>
                                                                    <div class="icons">
                                                                        <a href="#">
                                                                            <i class="fas fa-ellipsis-h"></i>
                                                                        </a>
                                                                        <a href="#">
                                                                            <i class="fas fa-reply"></i>
                                                                        </a>
                                                                        <a href="#">
                                                                            <i class="fa fa-heart"></i>
                                                                        </a>
                                                                    </div>
                                                                </div>
                                                                <p class="message-time">Sent - 1:09 AM</p>
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div class="meesage-box">
                                                        <div class="message-time">
                                                            <p>1:09 AM</p>
                                                        </div>
                                                    </div>

                                                    <div class="messages-area">
                                                        <div class="media">
                                                         <img src="{{ asset('public/sports/images') }}/default-group.avatar.png" class="mr-3">
                                                            <div class="media-body">
                                                                <h4 class="mt-0">Member Name</h4>
                                                                <div class="content-and-icons">
                                                                    <p class="recieve-message">Message</p>
                                                                    <div class="icons">
                                                                        <a href="#">
                                                                            <i class="fas fa-ellipsis-h"></i>
                                                                        </a>
                                                                        <a href="#">
                                                                            <i class="fas fa-reply"></i>
                                                                        </a>
                                                                        <a href="#">
                                                                            <i class="fa fa-heart"></i>
                                                                        </a>
                                                                    </div>
                                                                </div>
                                                                <p class="message-time">Sent - 1:09 AM</p>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    

                                                </div>
                                            </div>

                                            <div class="input-message-field">
                                                <div class="field">
                                                    <a href="#">
                                                        <i class="fa fa-face"></i>
                                                    </a>
                                                    <textarea name="text-message" id="message-field" placeholder="Send Message..."></textarea>
                                                    <a href="#">
                                                        <i class="fa fa-plus"></i>
                                                    </a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>


                              <div class="tab-pane fade" id="list-profile" role="tabpanel" aria-labelledby="list-profile-list">
                                  <!-- Chat Start -->
                                    <div class="full-chat">

                                        <!-- Chat Header -->
                                        <div class="chat-header">
                                            <div class="media">
                                              <img src="{{ asset('public/sports/images') }}/default-group.avatar.png" class="mr-3">
                                              <div class="media-body custom-class">
                                                <h5 class="mt-0">Group Name <i class="fas fa-caret-down"></i></h5>
                                                <a href="#"><i class="fas fa-times"></i></a>
                                              </div>
                                            </div>
                                        </div>

                                        <!-- Add Member Area -->
                                        <div class="add-member-box">
                                            <a href="#" class="close"><i class="fas fa-times"></i></a>
                                            <div class="btn-member">
                                                <a href="#"><i class="fas fa-user-plus"></i> Add Members</a>
                                            </div>
                                        </div>

                                        <!-- Message Middle Area -->
                                        <div class="full-message-box">

                                            <div class="chat-layout">
                                                <div class="chat-middle">
                                                    <div class="meesage-box">
                                                        <div class="message-time">
                                                            <p>1:09 AM</p>
                                                        </div>
                                                    </div>

                                                    <div class="messages-area">
                                                        <div class="media">
                                                         <img src="{{ asset('public/sports/images') }}/default-group.avatar.png" class="mr-3">
                                                            <div class="media-body">
                                                                <h4 class="mt-0">Member Name</h4>
                                                                <div class="content-and-icons">
                                                                    <p class="recieve-message">Message</p>
                                                                    <div class="icons">
                                                                        <a href="#">
                                                                            <i class="fas fa-ellipsis-h"></i>
                                                                        </a>
                                                                        <a href="#">
                                                                            <i class="fas fa-reply"></i>
                                                                        </a>
                                                                        <a href="#">
                                                                            <i class="fa fa-heart"></i>
                                                                        </a>
                                                                    </div>
                                                                </div>
                                                                <p class="message-time">Sent - 1:09 AM</p>
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div class="meesage-box">
                                                        <div class="message-time">
                                                            <p>1:09 AM</p>
                                                        </div>
                                                    </div>

                                                    <div class="messages-area">
                                                        <div class="media">
                                                         <img src="{{ asset('public/sports/images') }}/default-group.avatar.png" class="mr-3">
                                                            <div class="media-body">
                                                                <h4 class="mt-0">Member Name</h4>
                                                                <div class="content-and-icons">
                                                                    <p class="recieve-message">Message</p>
                                                                    <div class="icons">
                                                                        <a href="#">
                                                                            <i class="fas fa-ellipsis-h"></i>
                                                                        </a>
                                                                        <a href="#">
                                                                            <i class="fas fa-reply"></i>
                                                                        </a>
                                                                        <a href="#">
                                                                            <i class="fa fa-heart"></i>
                                                                        </a>
                                                                    </div>
                                                                </div>
                                                                <p class="message-time">Sent - 1:09 AM</p>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    

                                                </div>
                                            </div>

                                            <div class="input-message-field">
                                                <div class="field">
                                                    <a href="#">
                                                        <i class="fa fa-face"></i>
                                                    </a>
                                                    <textarea name="text-message" id="message-field" placeholder="Send Message..."></textarea>
                                                    <a href="#">
                                                        <i class="fa fa-plus"></i>
                                                    </a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                              </div>
                            </div> 
                        </div>
                    </div>
                </div>
            </div>


        </div>
    </div>
</div>
@include('sports.script')
@include('sports.js.message')


</body>
</html>