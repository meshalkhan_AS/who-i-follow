<script>
    document.getElementById("edit_button_modal").style.display = "none";
    $(document).on('click', function(e) {
        document.getElementById("edit_button_modal").style.display = "none";
    });
    $(document).ready(function () {
        $('#sidebarCollapse').on('click', function () {
            $('#sidebar').toggleClass('active');
        });
    });
    $(document).ready(function () {
        $('.toggle').on("click", function() {
            $('.dropdown-menu').toggle();
        })
    });
    $(document).ready(function () {
        $('.friendsSiderbar-list').css('display','none');
        $('#no-friend-message').css('display','none');
        $('.top p').html('');
        $('.top p').append('Chats');

    });

    // In your Javascript (external .js resource or <script> tag)
    $(document).ready(function() {
        $('#friends-list').select2({
            width: 'resolve', // need to override the changed default
            placeholder: "Select friends"
        });
        $('#friends-list-group').select2({
            width: 'resolve', // need to override the changed default
            placeholder: "Select friends"
        });
        $('#add-friend-group').select2({
            width: 'resolve', // need to override the changed default
            placeholder: "Select friends"
        });
    });

    //    Emojis
    $(function() {
        window.emojiPicker = new EmojiPicker({
            emojiable_selector: '[data-emojiable=true]',
            assetsPath: 'http://onesignal.github.io/emoji-picker/lib/img/',
            popupButtonClasses: 'fa fa-smile-o'
        });
        window.emojiPicker.discover();
    });

</script>

