<!DOCTYPE html>
<html>

@include('sports.head')
<body class="main-body">
<div class="wrapper">
    @include('sports.head_main')
    <main>
        <div class="container">
        </div>
        <div class="main-section">
            <div class="container-fluid">
                <div class="main-section-data">
                    <br/>
                    <br/>
                    <div class="row">
                        <div class="col-lg-3 col-md-4 col-sm-12 col-12">
                        </div>
                        <div class="col-lg-6 col-md-7 col-sm-12 col-12">
                            <div class="suggestions full-width">
                                <div class="sd-title text-center">
                                    <h3 style="font-weight: bold">Group Chat Invitation</h3>
                                    <br/>
                                </div>
                                <div class="suggestions-list">
                                    <div class="view-more">
                                        <div class="user_pro_status text-center">
                                            <ul class="flw-hr">
                                                <li>
                                                    <h1 id="group_name"></h1>
                                                    <div id="default-" style="width: 70%; text-align: center;margin-top: 20px; margin-left: auto; margin-right: auto"></div>
                                                </li>
                                            </ul>
                                        </div>

                                        <div class="user_pro_status" style="border-top:1px solid #cbcbcb;padding-top: 20px ">
                                            <ul class="flw-hr">
                                                <li><a title="" id="joinGroupButton" class="add-friends pointer-cursor" style="color: white;display: none" onclick="joinGroup()">Join Group</a></li>
                                            </ul>

                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-3 col-md-1 col-sm-12 col-12">
                            <a id="chatURL" href="{{route('user.chatSystem')}}" style="display: none;"></a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </main>
</div>
<script src="https://cdn.jsdelivr.net/npm/moment@2.29.1/moment.min.js"></script>
<script type="text/javascript" src="{{ asset('public/sports/js') }}/jquery.timeago.js"></script>
@include('sports.modal.invites')
<div id="edit_button_modal"></div>
{{--@include('sports.script')--}}
<script>
    chatList='Chat';
</script>
@include('sports.js.post')
@include('sports.js.images')
@include('sports.js.invites')
@include('newchat.js.script')
<script>

    groups=[];
    fetchGroup();
    function fetchGroup()
    {
        return new Promise((resolve, reject) =>
        {
            const groupRef = db.collection('group')
            groupRef.where('status', '==', 1).onSnapshot((querySnapshot) => {
            const allGroups = []
            querySnapshot.forEach((doc) => {
            const data = doc.data()
            data.id = doc.id;
        allGroups.push(data);
    })


        groups = allGroups;
        var groupID="{{$findInvitation['level']}}";
        const groupObj = [];

        fetchGroupById(groupID).then(function(groupExists) {
            groupObj[0] = groupExists;
            joinGroupp = groupExists;
            currentGroup = groupExists;


        if(!joinGroupp)
        {
            const groupImg = IMGURL+'/default-group.avatar.png';

            var defaultimage_='<img src="'+groupImg+'" img-load="true" style="width: 100px;height: auto;">'+'';
            $('#default-').hide();
            $('#group_name').text('Group Chat Not Found :( ');
            $('#joinGroupButton').hide();
        }
        else
        {
            const groupImg = groupObj[0].attachment ? BASE_URL+groupObj[0].attachment : IMGURL+'/default-group.avatar.png';

            var defaultimage_='<img src="'+groupImg+'" img-load="true" style="width: 100px;height: auto;">'+'';
            $('#default-').html('');
            $('#default-').append(defaultimage_);
            $('#group_name').text(groupObj[0].name);
            $('#joinGroupButton').show();
        }
        });


    })
    })
    }

    function joinGroup()
    {
        var members = joinGroupp.members;

        var urlChat=BASE_URL+'/chat/user';
        // Verify if you are exists
        if (typeof members[user.id] !== 'undefined')
        {
            if(members[user.id].leave==0)
            {
                swal({
                    title: '',
                    text: 'You\'ve Already Joined the Group.',
                    button: false,
                    icon:'error',
                    timer:3000
                });

            }
            else if(members[user.id].leave==1)
            {
                members[user.id].leave = 0;
                updateJoinGroup(joinGroupp);
                chatList='Chat';
                saveMessage('((joined_group_undefined))', new Date(), joinGroupp.id, members[user.id].name);
                swal({
                    title: '',
                    text: 'Joined Group Successfully',
                    button: false,
                    icon:'success',
                    timer:3000
                });
                joinGroupp.members = members;
                updateGroup(joinGroupp);
            }
        }
        else
        {
            members[user.id] = { id: user.id, name: user.name,status:1,leave:0,admin:0,read:0};
            updateJoinGroup(joinGroupp);
            chatList='Chat';

            saveMessage('((joined_group_undefined))', new Date(), joinGroupp.id, members[user.id].name);

            swal({
                title: '',
                text: 'Joined Group Successfully',
                button: false,
                icon:'success',
                timer:3000
            });
            joinGroupp.members = members;
        }

        $.ajax({
            url: BASE_URL+'/updateInvite/user',
            type: 'post',
            data: {
                _token: '{{ csrf_token() }}',
                id: "{{$findInvitation['id']}}"
            },
            beforeSend: function () {
            }
        }).done(function (result) {
            setTimeout(function() {
                window.open(urlChat,'_self');
            }, 3000);
            sessionStorage.setItem("loadChat",'Groups');
            sessionStorage.setItem("groupID", joinGroupp.id);
        });

    }


    function updateJoinGroup(group)
    {
        db.collection('group')
            .doc(group.id)
            .set(group)
            .then(function (docRef) {})
            .catch(function (error) {

            });

    }

    $(document).on('click', function(e) {
        if ( $(e.target).closest('.user-info').length ) {
            $('.user-account-settingss').toggle();
        }
        else if ( ! $(e.target).closest('.user-info').length ) {
            $('.user-account-settingss').hide();
        }
    });

    $(document).on('click', function(e) {
        if ( $(e.target).closest('.notification-icon').length ) {
            $('.notification-box').toggle();
        }
        else if ( ! $(e.target).closest('.notification-icon').length ) {
            $('.notification-box').hide();
        }
    });

</script>
<script>
    function open_menu_bar () {
        if($("nav").hasClass('activemenu')){
            document.body.classList.remove("remove-scrolling");
            $("nav").removeClass('activemenu');

        }else{
            document.body.classList.add("remove-scrolling");
            $("nav").addClass('activemenu');
        }
    }
//        $("nav").toggleClass("active");
//    }
</script>
</body>
</html>