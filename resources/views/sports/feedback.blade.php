<!DOCTYPE html>
<html>

@include('sports.head')
<body class="main-body">
<div class="wrapper">
    @include('sports.head_main')
    <main>
        <style>
            #feedmain{
                margin-top: 14% !important;
            }
            a.disabled {
  pointer-events: none;
  cursor: default;
}
        </style>
        <div class="container">
        </div>
        <div class="main-section">
            <div class="container-fluid no-pdd">
                <div class="main-section-data">
                    <div class="row">
                        <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12 no-pdd center-column" style="position: absolute; left: 26%; right: 26%; min-width: 48%; max-width: 48%; ">
                            <div>
                                @if(Session::get('success'))
                                    <div id="mydiv">
                                        <div  class="alert alert-success alert-dismissible fade show" role="alert">
                                            <p>{{Session::get('success')}}</p>
                                        </div>
                                    </div>
                                @endif
                            </div>
                                <div class="main-ws-sec" id="feedmain">
                                    <div class="bg-white">
                                        <h5 class="text-center p-4" id="exampleModalLabel">Submit Feedback</h5>
                                    </div>
                                        <div class="post-project-fields">
                                            {!! Form::open(['method' => 'POST', 'id'=>'feed' ,'route' => ['user.feed.store'],'data-parsley-validate','files' => 'true','enctype'=>'multipart/form-data']) !!}
                                            
                                            <div class="row">
                                                <div class="form-group">
                                                    <label for="feedtype">Select Feedback Type:</label>
                                                 <select id="feedtype" name="feedtype" class="form-control" style="margin-top: 10px">
                                                      <option value="" selected hidden>Select Feedback Type</option>
                                                       @foreach($types as $key => $type)
                                                       <option value="{{$type->id}}"> {{$type->type}}</option>
                                                       @endforeach
                                                       </select>
                                                  </div>
                                                <div class="col-lg-12 no-pdd">
                                                    <div class="sn-field">
                                                        {!! Form::textarea('caption', '', [ 'id'=> 'caption', 'class' => 'form-control', 'rows' => '3' ,'style'=>'font-size:14px;height:70px;margin-top:0;margin-bottom:0;','placeholder'=>'Please feel free to tell us about your experience','autofocus'=>'true']) !!}
                                                    </div>
                                                </div>
                                                <div class="col-lg-12 no-pdd" id="div-feedimage">
                                                    <div class="sn-field">
                                                        <label class="label-">Upload Pictures</label>
                                                        <div id="image-data-feed" class="row image-data" style="border: none;">
                                                            @include('sports.modal.feedbackimg')
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="px-2 py-2 border d-flex align-items-center" style="border-radius: 5px;">
                                                <p class="text-secondary">Add image to your feedback</p>
                                                <div class="upload-btn-wrappers select-post-image rounded-0 bg-white">
                                                    <a href="" class="btns"><i class="fa fa-camera"></i></a>
                                                    {!! Form::file('picture[]', [
                                                            'onchange'=>'uploadfeedbackimagemore()','accept'=>'image/*','id'=>'image-feed-data-','multiple'=>'true'
                                                            ]) !!}
                                                    @if($errors->has('picture'))
                                                        <span class="help-block">
                                                                    {{ $errors->first('picture') }}
                                                            </span>
                                                    @endif
                                                </div>
                                            </div>
                                            <div class="col-lg-12 no-pdd">
                                                <a onclick="newfeed()" class="btn login" id="new-feed-submit">Submit</a>
                                            </div>
                                        </div>
                        
                                        {!! Form::close() !!}
                                      
                                </div>
                        </div>
                               
                    </div>
                        
                </div>
            </div>
        </div>
        </div>
    </main>

    <!-- Contact List -->
    @include('sports.contact')

</div>
@include('sports.script')
@include('sports.js.feed')
@include('sports.js.invites')
@include('sports.js.images')
</body>
</html>