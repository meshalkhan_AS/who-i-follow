<div class="modal fade" id="profilephoto" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header bg-white mb-0">
                <h5 class="modal-title border-bottom pb-4" id="exampleModalLabel">Update Profile Picture</h5>
                <button type="button" class="close position-absolute d-flex align-items-center justify-content-center" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body p-2 text-left">
                {!! Form::model($userprofile, ['method' => 'POST','id'=>'profileForm', 'route' => ['user.profile.update', $userprofile->id], 'data-parsley-validate' , 'files'=>'true' , 'enctype'=>'multipart/form-data']) !!}
                    <div class="post-project-fields pt-0 pb-4 pl-4 pr-4 mt-4" id="profilepicdivone">
                        <div class="row">
                            <div class="col-lg-12 col-md-12 col-sm-12 col-12">
                                <div class="upload-btn-wrappers profilephotobtn p-2 bg-transparent mt-2">
                                    <a class="btns">
                                        <a class="profilepic">&plus; Upload Photo</a>
                                    </a>
                                    {!! Form::file('picture', [
                                         'onchange'=>'readpicURL(this,"picture")','accept'=>'image/*','id'=>'picture'
                                    ]) !!}
                                    @if($errors->has('picture'))
                                        <span class="help-block">
                                            {{ $errors->first('picture') }}
                                        </span>
                                    @endif
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="post-project-fields pt-0 pb-4 pl-4 pr-4" id="profilepicdivtwo">
                        <div class="row">
                            <div id="picture_div">
                                @if($userprofile['picture']=='yes')
                                    <img id="id-picture" src="{{url('/').'/public/storage/users/'.$userprofile['user_id'].'/profile_picture.jpg'}}?v={{time()}}" alt="">
                                @else
                                    <img id="id-picture" src="{{ asset('public/uploads/white.jpg') }}" alt="">
                                @endif
                            </div>
                        </div>
                        <div class="save-cancel-profile-photo border-top">
                            <a class="btn cancelprofilebtn" onclick="profilecancel()">Cancel</a>
                            <a class="btn saveprofilebtn text-white" onclick="profilephotocheck()">Save</a>
                        </div>
                    </div>
                {!! Form::close() !!}
            </div>
        </div>
    </div>
</div>
