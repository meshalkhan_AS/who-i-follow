<style>
    .answer{
       display:none;
    }
    </style>
<div class="modal fade post-modal" id="addPost" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header bg-white">
                <h5 class="modal-title" id="exampleModalLabel">Create Post</h5>
                <button type="button" class="btn-close close position-absolute d-flex align-items-center justify-content-center" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body p-2">
               

                <div class="post-project-fields">
                    {!! Form::open(['method' => 'POST', 'id'=>'post' ,'route' => ['user.post.store'],'data-parsley-validate','files' => 'true','enctype'=>'multipart/form-data']) !!}
                    @if($userprofile->exp_status==1)
                    <div class="row mx-o px-2 py-2 pb-2">
                        <label class="switch">
                            <input type="checkbox" class="expert" name="exp" value="1" onchange="valueChanged()">
                            <span class="slider round"></span>
                          </label>
                          <span style="float: right;  margin-right: 10px;">&nbsp;&nbsp;Specialist Post</span>
                          
                    </div>
                    
                    @endif
                    <div class="row">
                        <div class="col-lg-12 no-pdd">
                            <div class="sn-field">
                                {!! Form::textarea('caption', '', [ 'id'=> 'caption', 'class' => 'form-control', 'rows' => '3' ,'style'=>'font-size:14px;height:70px;margin-top:0;margin-bottom:0;', 'placeholder' => 'Whats on your mind, '.ucfirst($userprofile['fname']).' ?','autofocus'=>'true']) !!}
                            </div>
                        </div>
                        <div class="col-lg-12 no-pdd" id="div-postimage">
                            <div class="sn-field">
                                <label class="label-">Your Uploaded Picture</label>
                                <div id="image-data" class="row image-data">
                                    @include('sports.modal.images')
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="px-2 py-2 border d-flex align-items-center">
                        <p class="text-secondary">Add image to your post</p>
                        <div class="upload-btn-wrappers select-post-image rounded-0 bg-white">
                            <a href="" class="btns"><i class="fa fa-camera"></i></a>
                            {!! Form::file('picture[]', [
                                    'onchange'=>'uploadimagemore()','accept'=>'image/*','id'=>'image-post-data','multiple'=>'true'
                                    ]) !!}
                            @if($errors->has('picture'))
                                <span class="help-block">
                                            {{ $errors->first('picture') }}
                                    </span>
                            @endif
                        </div>
                    </div>
                    <div class="col-lg-12 no-pdd">
                        <button onclick="newPost()" class="btn login" id="new-post-submit" disabled="true">Post</button>
                    </div>
                </div>

                {!! Form::close() !!}
            </div>
        </div>
    </div>
</div>