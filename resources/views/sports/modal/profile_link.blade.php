<div class="modal fade" id="profilelink" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header bg-white mb-0">
                <h5 class="modal-title border-bottom pb-4" id="exampleModalLabel">Update Profile Link</h5>
                <button type="button" class="close position-absolute d-flex align-items-center justify-content-center" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body p-2 text-left">
                {!! Form::model($userprofile, ['method' => 'POST','id'=>'profilelinkForm', 'route' => ['user.profilelink.update', $userprofile->id], 'data-parsley-validate']) !!}
                    <div class="row" style="padding: 10px">
                        <div class="col-lg-12 no-pdd form-group">
                            <div class="sn-field">
                                <label class="label-" style="padding-left:13px;font-weight: bold">Username</label>
                            </div>
                            <div class="col-lg-12">
                                &#x2022;
                                <span style="font-size: 12px;" >
                                    {{url('/')}}/<span id="updatedlink" style="text-transform: lowercase;">{{$userprofile['user_link']}}</span>
                                </span>
                            </div>
                            <br/>
                            <div class="col-lg-12">
                                {!! Form::text('user_link',old('user_link'), [ 'id'=>'user_link','class' => 'form-control', 'placeholder' => 'Enter Username','required'=>'true','maxlength'=>'20', 'style'=>'border:1px solid #cbcbcb;text-transform: lowercase;']) !!}
                            </div>
                            <span id="error_message" style="padding-left:15px;font-size: 11px;margin-top: 2px;">

                            </span>
                        </div>
                    </div>

                <div class="post-project-fields pt-0 pb-4 pl-4 pr-4">
                    <div class="save-cancel-profile-photo border-top">
                        <button id="submit-linkform" type="button" class="btn saveprofilebtn text-white" onclick="submitformlink()" disabled>Save changes</button>
                    </div>
                </div>
                {!! Form::close() !!}
            </div>
        </div>
    </div>
</div>

@include('sports.js.linkjs')
