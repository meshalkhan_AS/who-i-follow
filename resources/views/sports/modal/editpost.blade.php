<div class="modal fade post-modal p-0" id="editPost" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header bg-white p-2">
                <h5 class="modal-title" id="exampleModalLabel">Update Post</h5>
                <button type="button" class="btn-close close position-absolute d-flex align-items-center justify-content-center" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body p-2">
                <div class="post-project-fields">
                    {!! Form::open(['method' => 'POST', 'id'=>'updatePost' , 'route' => ['user.post.edit'],'data-parsley-validate','files' => 'true','enctype'=>'multipart/form-data']) !!}
                    <div class="row">
                        <div class="col-lg-12 no-pdd">
                            <div class="sn-field">
                                {!! Form::textarea('caption', old('caption'), [ 'id'=> 'captionedit', 'style'=>'font-size:14px;height:70px', 'class' => 'form-control', 'rows' => '3','placeholder' => 'Whats on your Mind, '.ucfirst($userprofile['fname']).' ?','autofocus'=>'true']) !!}
                                {!! Form::text('id', old('id'), [ 'id'=> 'idedit','hidden'=>'true']) !!}
                                {!! Form::text('title', old('title'), [ 'id'=> '','hidden'=>'true']) !!}
                            </div>
                        </div>
                        <div class="col-lg-12 no-pdd" id="div-postimage-edit">
                            <div class="sn-field">
                                <label class="label-">Your Uploaded Picture</label>
                                <div id="image-data-" class="row image-data">
                                    @include('sports.modal.images')
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-12 px-2 py-2 border d-flex align-items-center">
                        <p class="text-secondary">Add image to your post</p>
                        <div class="upload-btn-wrappers select-post-image rounded-0 bg-white">
                            <a href="#" class="btns"><i class="fa fa-camera"></i></a>
                            {!! Form::file('picture[]', [
                                      'onchange'=>'uploadimagemore("true")','accept'=>'image/*','id'=>'image-post-data-edit','multiple'=>'true'
                                     ]) !!}
                            @if($errors->has('picture'))
                                <span class="help-block">
                                    {{ $errors->first('picture') }}
                                </span>
                            @endif
                        </div>
                    </div>
                    <div class="col-lg-12 no-pdd">
                        <button onclick="postCheck()" class="btn login" id="edit-post-submit" disabled="true">Update</button>
                    </div>
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
</div>