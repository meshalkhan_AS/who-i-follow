<style>
.login {
    background-color: #385499;
    width: 100%;
    color: #fff !important;
    text-align: center;
    margin: 10px 0 0 0;
    border-radius: 5px;
    border: none !important;
    outline: none !important;
    font-weight: 400;
    cursor: pointer;
    font-size: 14px;
}
.modal-body .form-control {
    margin-bottom: 20px; 
    margin-top: 20px;
    text-transform: capitalize;
    padding-left: 17px;
}


.btns a:hover {
    background-color: #dc3545;
    color: #fff
}
.image-post-{
    height:100px;
    width: 125px;
    object-fit: cover;
    margin: 5px;
    background: url(../public/sports/css/img/loader.gif) center no-repeat;
    background-color:black;
    background-size: 15px 15px;
    position: relative; z-index: 1;

}
.post-project-fields {
    float: left;
    width: 100%;
    padding: 0 10px 10px 10px;
    background-color: #fff;
}
.image-data {
    max-height: 160px;
    overflow: auto;
    border: 1px solid #e7e7e7;
    border-radius: 5px;
    padding: 2px;

}
.image-post-delete{
    float: right !important;
    height:25px !important;
    width: auto !important;
    /* z-index: -1 !important; */
   
    /* margin-left: -29px !important;
    margin-top: 10px !important;
    margin-right: 10px !important; */
    padding: 5px !important;
    border: 1px solid grey !important;
    color: #fff;
    background-color: #d63927;
    
    padding: 10px 0;
    margin-bottom: 10px;
    border: none !important;
    border-radius: 0 !important;
    position: absolute;left:80%; top: 6%;z-index: 10;

}
.sn-field {
    float: left;
    width: 100%;
    margin-bottom: 10px;
    position: relative;
}
.label- {
    margin-bottom: 5px;
}

.post-image-pop {
    width: 33%;
    position: relative; width: 145px; height: 115px;
}

</style>
<div class="modal fade post-modal p-0" id="editPost" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header bg-white p-2">
                <h5 class="modal-title" id="exampleModalLabel">Update Post</h5>
                <button type="button" class="btn-close close d-flex align-items-center justify-content-center" data-bs-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body p-2">
                <div class="post-project-fields">
                    {!! Form::text('image-post-data', old('image-post-data'), [ 'id'=> 'image-post-data','hidden'=>'true']) !!}
                    {!! Form::open(['method' => 'POST', 'id'=>'updatePost' , 'route' => ['user.post.edit'],'data-parsley-validate','files' => 'true','enctype'=>'multipart/form-data']) !!}
                    <div class="row">
                        <div class="col-lg-12 no-pdd">
                            <div class="sn-field">
                                {!! Form::textarea('caption', old('caption'), [ 'id'=> 'captionedit', 'style'=>'font-size:14px;height:70px', 'class' => 'form-control', 'rows' => '3','placeholder' => 'Whats on your Mind, '.ucfirst($userprofile['fname']).' ?','autofocus'=>'true']) !!}
                                {!! Form::text('id', old('id'), [ 'id'=> 'idedit','hidden'=>'true']) !!}
                                {!! Form::text('title', old('title'), [ 'id'=> '','hidden'=>'true']) !!}
                            </div>
                        </div>
                        <div class="col-lg-12 no-pdd" id="div-postimage-edit">
                            <div class="sn-field">
                                <label class="label-">Your Uploaded Picture</label>
                                <div id="image-data-" class="row image-data">
                                    @include('sports.modal.images')
                                  
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-12 px-2 py-2 border d-flex align-items-center">
                        <p class="text-secondary">Add image to your post</p>
                        <div class="upload-btn-wrappers select-post-image rounded-0 bg-white" style="margin-top: -12px !important;">
                            <a href="#" class="btns"><i class="fa fa-camera" style="color: black;margin-left: 16px;margin-bottom: 5px;"></i></a>
                            {!! Form::file('picture[]', [
                                      'onchange'=>'uploadimagemore("True")','accept'=>'image/*','id'=>'image-post-data-edit','multiple'=>'true'
                                     ]) !!}
                            @if($errors->has('picture'))
                                <span class="help-block">
                                    {{ $errors->first('picture') }}
                                </span>
                            @endif
                        </div>
                    </div>
                    <div class="col-lg-12 no-pdd">
                        <button onclick="postCheck()" class="btn login" id="edit-post-submit" disabled="true">Update</button>
                    </div>
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
</div>