<div class="modal fade" id="invites" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header bg-white mb-0">
                <h5 class="modal-title" id="exampleModalLabel">Invite Friends</h5>
                <button type="button" class="close position-absolute d-flex align-items-center justify-content-center" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body p-2 text-left">
                <div class="post-project-fields pt-0 pb-4 pl-4 pr-4">
                    {{--<div class="col-12 no-pdd">--}}
                        {{--<div class="">--}}
                            {{--<div class="company-up-info mb-2 pb-4 border-bottom">--}}
                                {{--<div class="row">--}}
                                    {{--<div class="col-lg-6 col-md-6 col-sm-6 col-12 no-pdd">--}}
                                        {{--<a class="via-email" onclick="via_Email()">Invite Via Email</a>--}}
                                    {{--</div>--}}
                                    {{--<div class="col-lg-6 col-md-6 col-sm-6 col-12 no-pdd">--}}
                                        {{--<a class="via-sms" onclick="via_SMS()">Invite Via SMS</a>--}}
                                    {{--</div>--}}
                                {{--</div>--}}
                            {{--</div>--}}
                        {{--</div>--}}
                    {{--</div>--}}

                    {!! Form::open(['method' => 'POST', 'id'=>'invites_' ,'route' => ['user.invites'],'data-parsley-validate','files' => 'true','enctype'=>'multipart/form-data']) !!}
                    <div class="row">
                        <div class="col-lg-12 no-pdd">
                            <div class="row">
                                <span id="mobile_error_message" style="font-size: 11px;margin-top: 2px;height: 10px">
                                </span>
                            </div>
                            {!! Form::text('id', '', [ 'id'=>'id_to_invite', 'class'=>'form-control'  , 'required'=>'true','hidden'=>'true']) !!}
                            <div class="sn-field">
                                <br/>
                                <label class="label-">Phone Number </label>
                                <div class="row">
                                    <div class="col-sm-4 col-12 pr-sm-1 p-0">
                                        {!! Form::select('code', config('status.code'),'', [ 'id'=>'code', 'class'=>'form-control' , 'required'=>'']) !!}
                                    </div>
                                    <div class="col-sm-8 col-12 p-0">
                                        {!! Form::number('mob_', '', [ 'id'=>'mob_', 'class'=>'form-control' , 'placeholder' => 'Enter phone number' , 'required'=>'']) !!}
                                        <br/>
                                    </div>
                                </div>
                            </div>
                            <p class="text-center text-dark">OR</p>
                            <span id="email_error_message" style="font-size: 11px;margin-top: 2px;height: 10px">
                            </span>
                            <div class="sn-field">
                                <br/>
                                <label class="label-">Email </label>
                                {!! Form::text('email_', '', [ 'id'=>'email_', 'class'=>'form-control' , 'placeholder' => 'Enter email' , 'required'=>'']) !!}
                            </div>
                        </div>
                    </div>
                    <div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12 no-pdd border-top">
                        {{--<button type="button" class="btn btn-danger w-25 float-right" data-bs-dismiss="modal">Close</button>--}}
                        <button type="button" id="invite_button_site" class="btn login" onclick="sendinvite()">Send Invite</button>
                    </div>
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
</div>