<div class="modal fade" id="weblink" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header bg-white mb-0">
                <h5 class="modal-title border-bottom pb-4" id="exampleModalLabel">Update Website URL</h5>
                <button type="button" class="close position-absolute d-flex align-items-center justify-content-center" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body p-2 text-left">
                {!! Form::model($userprofile, ['method' => 'POST','id'=>'profilelinkFormurl', 'route' => ['user.weburl'], 'data-parsley-validate']) !!}
                    <div class="row" style="padding: 10px">
                        <div class="col-lg-12 no-pdd form-group">
                            <div class="col-lg-12">
                                &#x2022;
                                <span style="font-size: 12px;" >
                                   Edit Your URL
                                </span>
                            </div>
                            <br/>
                            <div class="col-lg-12">
                                {!! Form::text('weburl',old('weburl'), [ 'id'=>'Url','class' => 'form-control', 'placeholder' => 'Enter URL','required'=>'true', 'style'=>'border:1px solid #cbcbcb;text-transform: lowercase;']) !!}
                                {!! Form::hidden('editUrlID',old('editUrlID'), [ 'id'=>'editUrlID','class' => 'form-control', 'placeholder' => 'Enter URL','required'=>'true']) !!}
                            </div>
                            <span id="error_message" style="padding-left:15px;font-size: 11px;margin-top: 2px;">

                            </span>
                        </div>
                    </div>

                <div class="post-project-fields pt-0 pb-4 pl-4 pr-4">
                    <div class="save-cancel-profile-photo border-top">
                        <a class="btn bio-button" type="submit" onclick="validationcheckURL()">Update
                            URL</a>
                    </div>
                </div>
                {!! Form::close() !!}
            </div>
        </div>
    </div>
</div>

@include('sports.js.linkjs')
<script>
    var element = $("#btnremovee");



    $(document).ready(function() {
        $('#btnattrem').removeAttr("disabled")
    });

</script>
