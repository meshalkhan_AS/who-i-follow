<div class="modal fade" id="invites_friends" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header bg-white mb-0">
                <h5 class="modal-title border-bottom pb-4" id="exampleModalLabel">Invite Friends</h5>
                <button type="button" onclick="removeBackdrop()" class="close position-absolute d-flex align-items-center justify-content-center" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body p-2 text-left">
                {!! Form::open(['method' => 'POST', 'id'=>'invites_' ,'route' => ['user.invites'],'data-parsley-validate','files' => 'true','enctype'=>'multipart/form-data']) !!}

                <div class="post-project-fields pt-0 pb-4 pl-4 pr-4">
                    <div class="col-12 no-pdd">
                        <div class="row no-pdd">
                            <div class="col-sm-10 p-0 pr-sm-1">
                        {!! Form::text('urlInvite','', [ 'id'=>'urlInvite' ,'class'=>'form-control','style'=>'width:100% !important', 'readonly'=>'true']) !!}
                            </div>
                            <div class="col-sm-2 p-0 mt-1">
                                {{--<span class="tooltiptext" id="myTooltip">Copy to clipboard</span>--}}
                                <a class="via-email" style="padding: 10px;width: 70px;" onclick="myFunction()" onmouseout="outFunc()">
                                    Copy
                                </a>
                            </div>
                         </div>
                    </div>
                    <br/>
                    <br/>
                    <div class="row">
                                <span id="mobile_error_message_group" style="font-size: 11px;margin-top: 2px;height: 10px">
                                </span>
                    </div>

                    <div class="row no-pdd">
                        <div class="col-lg-12 no-pdd">
                            {!! Form::text('id', '', [ 'id'=>'id_to_invite', 'class'=>'form-control'  , 'required'=>'true','hidden'=>'true']) !!}
                            {!! Form::text('level', '', [ 'id'=>'level', 'class'=>'form-control'  , 'required'=>'true','hidden'=>'true']) !!}
                            {!! Form::text('group_name', '', [ 'id'=>'group_name', 'class'=>'form-control'  , 'required'=>'true','hidden'=>'true']) !!}
                            <div class="sn-field">
                                <br/>
                                <label class="label-">Phone Number </label>
                                <div class="row">
                                    <div class="col-sm-4 col-12 pr-sm-1 p-0">
                                        {!! Form::select('code', config('status.code'),'', [ 'id'=>'code_group', 'class'=>'form-control' , 'required'=>'']) !!}
                                    </div>
                                    <div class="col-sm-8 col-12 p-0">
                                        {!! Form::number('mob_', '', [ 'id'=>'mob_group', 'class'=>'form-control' , 'placeholder' => 'Enter phone number' , 'required'=>'']) !!}
                                        <br/>
                                    </div>
                                </div>
                            </div>
                            <p class="text-center text-dark">OR</p>
                            <span id="email_error_message_group" style="font-size: 11px;margin-top: 2px;height: 10px">
                            </span>
                            <div class="sn-field">
                                <br/>
                                <label class="label-">Email </label>
                                {!! Form::text('email_', '', [ 'id'=>'email_group', 'class'=>'form-control' , 'placeholder' => 'Enter email' , 'required'=>'', ''=>'text-transform:']) !!}
                            </div>
                        </div>
                    </div>
                    <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12  no-pdd">
                        <button type="button" id="invite_button_group" class="btn login" onclick="sendinviteGroup()">SEND INVITE</button>
                    </div>
                </div>
                {!! Form::close() !!}
            </div>
        </div>
    </div>
</div>

<script>
    function myFunction() {
        var copyText = document.getElementById("urlInvite");
        copyText.select();
        copyText.setSelectionRange(0, 99999);
        navigator.clipboard.writeText(copyText.value);

        var tooltip = document.getElementById("myTooltip");
        tooltip.innerHTML = "Copied: " + copyText.value;
    }

    function outFunc() {
        var tooltip = document.getElementById("myTooltip");
        tooltip.innerHTML = "Copy to clipboard";
    }
</script>