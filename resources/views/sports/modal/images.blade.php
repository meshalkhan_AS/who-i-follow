@if(count($data)>0)
    @foreach($data as $images)
    <div class="post-image-pop" id="{{$images}}">
        <img src="{{ url('/').'/public/storage/posts/' }}/{{$images}}" class="image-post-"/>
        <i class="btn btn-danger fa fa-trash image-post-delete"  onclick="deleteimage('{{$images}}')"></i>
    </div>
    @endforeach
@endif
