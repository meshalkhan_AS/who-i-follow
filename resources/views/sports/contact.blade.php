<style>
     .Friendtabloader {
        position: relative;
        height: 40vh;
        background: url(../public/sports/css/img/Hourglass.gif) center no-repeat;
        background-size: 65px 65px !important;
    }
</style>
<div class="suggestions full-width contact-main">
    <div class="sd-title text-center">
        <h3 class="mr-2 mt-2">Friends</h3>
    </div>
    <div class="Friendtabloader" >
    </div>

    <div class="suggestions-list p-0">
        <ul class="list-unstyled components chat-friends-list">
            <li>
                <div class="list-group" id="list-tab-contact" role="tablist">

                </div>
            </li>
        </ul>

    </div>
</div>
<a class="contact-btn" onclick="show_contact_list()"><i class="fa fa-comments mr-1" aria-hidden="true"></i>Friends</a>