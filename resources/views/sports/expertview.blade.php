<!DOCTYPE html>
<html>

@include('sports.head')

<body class="main-body">
    <div class="wrapper">
        @include('sports.head_main')
        <main>
            <div class="container">
            </div>
            <div class="main-section">
                <div class="container-fluid no-pdd">
                    <div class="main-section-data">
                        <div class="row">
                            <div class="col-lg-3 col-md-2 col-sm-1 col-12 no-pdd">
                            </div>
                            <div class="col-lg-6 col-md-8 col-sm-10 col-12 no-pdd">
                                <div>
                                    @if (Session::get('success'))
                                        <div id="mydiv">
                                            <div class="alert alert-success alert-dismissible fade show" role="alert">
                                                <p>{{ Session::get('success') }}</p>
                                            </div>
                                        </div>
                                    @endif
                                </div>
                              
                                <div class="main-ws-sec">
                                    <div class="posts-section" id="post-data">

                                        @php
                                            $postimages = \App\Models\Admin\PostImage::select('image', 'id')
                                                   ->where('post_id', $post->id)
                                                   ->where('user_id', $post->user_id)
                                                   ->get();
                                        @endphp

                                        <div class="post-bar">
                                            <div class="post_topbar">

                                            </div>
                                            <div class="epi-sec">
                                                <div class="job_descp">
                                                    <h1>{!! nl2br($post->title) !!}</h1>
                                                    <p>{!! nl2br($post->caption) !!}</p>

                                                    <div class="top-profiles">
                                                        <div id="gall-{{$post->id}}" ></div>
                                                        <script type="text/javascript">
                                                
                                                            var obj = <?php echo json_encode($postimages); ?>;
                                                            var s= <?php echo json_encode($post->id); ?>;
                                                            const arrayy<?php echo json_encode($post->id); ?>=[];
                                                
                                                            for(var i=0;i<obj.length;i++)
                                                            {
                                                                arrayy<?php echo json_encode($post->id); ?>.push(<?php echo json_encode(url('/').'/public/storage/posts/'); ?>+'/'+obj[i].image);
                                                            }
                                                            $(function() {
                                                                $('#gall-'+<?php echo json_encode($post->id); ?>).imagesGrid({
                                                                    images: arrayy<?php echo json_encode($post->id); ?>,
                                                                    align: true,
                                                                    getViewAllText: function(imgsCount) { return 'View all' }
                                                                });
                                                            });
                                                        </script>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <br />
                            <div class="col-lg-3 col-md-2 col-sm-1 col-12 no-pdd">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </main>

    <!-- Invites Modal -->
        @include('sports.modal.invites')
    </div>
    @include('sports.script')
    @include('sports.js.post')
    @include('sports.js.invites')
    @include('sports.js.images')
</body>

</html>
