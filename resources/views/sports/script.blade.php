<script type="text/javascript" src="{{ asset('public/sports/js') }}/jquery.min.js"></script>
<script type="text/javascript" src="{{ asset('public/sports/js') }}/popper.js"></script>
<script type="text/javascript" src="{{ asset('public/sports/js') }}/bootstrap.min.js"></script>
<script type="text/javascript" src="{{ asset('public/sports/lib') }}/slick/slick.min.js"></script>
<script type="text/javascript" src="{{ asset('public/sports/js') }}/script.js"></script>
<script src="{{ asset('public/sports/js') }}/images-grid.js"></script>
<script src="https://cdn.jsdelivr.net/npm/moment@2.29.1/moment.min.js"></script>
<!-- jQuery -->
{{--<script src="https://code.jquery.com/jquery-3.4.1.min.js"></script>--}}

<!-- timeago JS -->
<script type="text/javascript" src="{{ asset('public/sports/js') }}/jquery.timeago.js"></script>

<script>

    {{--Find Page setup--}}
    if (window.performance)
    {
        var navEntries = window.performance.getEntriesByType('navigation');
    }
    if (navEntries.length > 0 && navEntries[0].type === 'back_forward')
    {
        console.log('As per API lv2, this page is load from back/forward');
        {{ Session::forget('sweet_alert.alert') }}

    }
    else if (window.performance.navigation && window.performance.navigation.type == window.performance.navigation.TYPE_BACK_FORWARD)
    {
        console.log('As per API lv1, this page is load from back/forward');
        {{ Session::forget('sweet_alert.alert') }}

    }
    else {
        console.log('This is normal page load');
    }

</script>


<script>

    {{------ Upload Image ----------}}
    var readpicURL =function(input)
        {
//    //        Alert Messages
//            $("#message-popup-div").show();
//            $("#alert-text").html('Uploading Profile Picture..');
//            setTimeout(function()
//            {
//                $("#message-popup-div").hide();
//                $("#alert-text").html('');
//            }, 1000);
//    //        Alert Messages

            swal({
                title: 'Uploading!',
                text: 'Image Uploaded Successfully!',
                icon: 'success',
                timer: 1000,
                button: false
            }).then(function() {
            });

        if (input.files && input.files[0]) {
            var reader = new FileReader();

            reader.onload = function (e) {
                $('#id-picture').attr('src' , e.target.result);
            };

            reader.readAsDataURL(input.files[0]);
        }
        document.getElementById("profilepicdivone").style.display = "none";
        document.getElementById("profilepicdivtwo").style.display = "block";
    };
</script>

<script type="text/javascript">

    {{------ Alert Time Out ----------}}
    window.setTimeout(function() {
        $(".alert").fadeTo(500, 0).slideUp(500, function(){
            $(this).remove();
        });
    }, 5000);

    $(function(){
        var current = location.pathname;
        $('nav li a').each(function(){
            var $this = $(this);

            if($this.attr('href').indexOf(current) !== -1){
                $this.addClass('active');
            }
        })
    });

</script>
<script type="text/javascript">
    $(document).on('click', function(e) {
        $('.ed-options').removeClass("active");
        if ( ! $(e.target).closest('.notification-icon').length ) {
            $('.notification-box').hide();
        }
    });
</script>

<script type="text/javascript">
    $(document).on('click', function(e) {
        if ( $(e.target).closest('.user-info').length ) {
            $('.user-account-settingss').show();
        }
        else if ( $(e.target).closest('.user-info').length ) {
            $('.user-account-settingss').hide();
        }
        else if ( ! $(e.target).closest('.user-info').length ) {
            $('.user-account-settingss').hide();
        }
    });
</script>
<script type="text/javascript">
    $(document).on('click', function(e) {
        if ( $(e.target).closest('.dropdown-open').length ) {
            $('.add-remove-cover-photo').toggle();
        }
        else if ( ! $(e.target).closest('.dropdown-open').length ) {
            $('.add-remove-cover-photo').hide();
        }
    });
</script>
<script type="text/javascript">
    $(document).ready(function() {
        $(':input[type="submit"]').prop('disabled', true);
        $('input[type="text"]').keyup(function() {
            if($('input[type="text"]').val() == ""){
                $(':input[type="submit"]').attr('disabled','disabled');
            }
            else{
                $(':input[type="submit"]').removeAttr('disabled');
            }
        });
    });
</script>
<script>
    function open_menu_bar () {
        if($("nav").hasClass('activemenu')){
            document.body.classList.remove("remove-scrolling");
            $("nav").removeClass('activemenu');

        }else{
            document.body.classList.add("remove-scrolling");
            $("nav").addClass('activemenu');
        }
    }
//        $("nav").toggleClass("active");
//    }
</script>
<script>
    $(document).on('click', function(e) {
        if ( $(e.target).closest('#profile-photo').length ) {
            $('#view-update-profile-pic').toggle();
        }
        else if ( ! $(e.target).closest('#profile-photo').length ) {
            $('#view-update-profile-pic').hide();
        }
    });
</script>
<script>
    $(document).on('click', function(e) {
        if ( $(e.target).closest('.contact-btn').length ) {
            $('.contact-main').toggle();
        }
        else if ( ! $(e.target).closest('.contact-btn').length ) {
            $('.contact-main').hide();
        }
    });
</script>
<script>
    $(document).on('click', function(e) {
        if ( $(e.target).closest('.modal-close').length ) {
            document.body.classList.remove('remove-scrolling');
        }

    });
</script>
<script>
    document.addEventListener('keydown', function(event){
        if(event.key === "Escape"){
            document.body.classList.remove('remove-scrolling');
        }
    });
</script>
<script>
//    $(document).ready(function() {
//        $('input[type="text"]').mousedown(function() {
//            $('.inner-search').show();
//        });
//    });
    $(document).ready(function() {
        $('.search-bar').on('click', function() {
            $('.inner-search').show();
        });
    });
</script>
<script>
    $(document).on('click', function(e) {
        if( $(e.target).closest('.back-to-normal').length ) {
            $('.inner-search').hide();
        }
    });
</script>
<script>
    $(document).on('click', function(e) {
        if( $(e.target).closest('#nav-home-tab').length) {
            $('#nav-home-tab').addClass('active');
            $('#nav-profile-tab').removeClass('active');
            $('#nav-contact-tab').removeClass('active');
            $('#nav-home').show();
            $('#nav-profile').hide();
            $('#nav-contact').hide();
        }
    })
</script>
<script>
    $(document).on('click', function(e) {
        if( $(e.target).closest('#nav-profile-tab').length) {
            $('#nav-home-tab').removeClass('active');
            $('#nav-profile-tab').addClass('active');
            $('#nav-contact-tab').removeClass('active');
            $('#nav-home').hide();
            $('#nav-profile').show();
            $('#nav-contact').hide();
        }
    })
</script>
<script>
    $(document).on('click', function(e) {
        if( $(e.target).closest('#nav-contact-tab').length) {
            $('#nav-home-tab').removeClass('active');
            $('#nav-profile-tab').removeClass('active');
            $('#nav-contact-tab').addClass('active');
            $('#nav-home').hide();
            $('#nav-profile').hide();
            $('#nav-contact').show();
        }
    })
</script>

@include('chat.script')
@include('sports.js.firebase')
{{--@include('sports.js.load')--}}
