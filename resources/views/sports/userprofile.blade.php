<!DOCTYPE html>
<html>

@include('sports.head')

@php
    $follower=\App\Models\Admin\Follower::where('follower_id',$id)->where('user_id',Auth::user()->id)->get();
    $friend_request=\App\Models\Admin\FriendList::where('friend_id',$id)->where('user_id',Auth::user()->id)->where('status','0')->get();
    $confirm_request=\App\Models\Admin\FriendList::where('friend_id',Auth::user()->id)->where('user_id',$id)->where('status','0')->first();
    $friend_=\App\Models\Admin\FriendList::where('friend_id',$id)->where('user_id',Auth::user()->id)->where('status','1')->get();
    $friendyou_=\App\Models\Admin\FriendList::where('user_id',$id)->where('friend_id',Auth::user()->id)->where('status','1')->get();
@endphp

<body class="main-body">
<div class="wrapper">
    @include('sports.head_main')
    <section class="cover-sec">
        @if($userprofile['coverphoto']=='yes')
            <img id="id-cover_photo" onclick="cover_photo_zoom()" src="{{url('/').'/public/storage/users/'.$userprofile['user_id'].'/cover_photo.jpg'}}?v={{time()}}" alt="">
        @else
            <img id="id-cover_photo" src="{{ asset('public/sports/images/resources') }}/baseball.jpg" alt="">
        @endif
        <div id="myModal" class="modal">
            <span class="close">&times;</span>
            <img class="img-modal-content" id="img01">
        </div>
    </section>
    <div class="container">
    </div>
    <div class="user-tab-sec">
        <div class="tab-feed">
            <ul>
                <li data-tab="feed-dd" class="active">
                    <a><span>News Feed</span></a>
                </li>
                <li data-tab="info-dd">
                    <a><span>About</span></a>
                </li>
            </ul>
        </div>
    </div>
    <main>
        <div class="main-section">
            <div class="container-fluid no-pdd">
                <div class="row">
                    <div class="col-lg-3">
                        <div class="main-left-sidebar">
                            <div class="user_profile">
                                <div class="user-pro-img">
                                    @if($userprofile['picture']=='yes')
                                        <img id="profile-photo" onclick="profile_photo_zoom()" src="{{url('/').'/public/storage/users/'.$userprofile['user_id'].'/profile_picture.jpg'}}?v={{time()}}" alt="">
                                    @else
                                        <img id="profile-photo" src="{{ asset('public/uploads/white.jpg') }}" alt="">
                                    @endif
                                    <div id="myModal" class="modal">
                                        <span class="close">&times;</span>
                                        <img class="modal-content" id="img01">
                                    </div>
                                </div>
                                <h3>{{ $userprofile['fname']}} {{$userprofile['lname']}}</h3>
                                <p>{{ $userprofile['email']}}</p>
                                <div class="company-up-info text-center">
                                    <ul>
                                        <li>
                                            @if(count($follower)>0)
                                                <a class="btn unfollow-button" href="{{route('user.unfollow',$id)}}">Unfollow</a>
                                            @else
                                                <a class="btn follow-button" href="{{route('user.follow',$id)}}">Follow</a>
                                            @endif
                                        </li>
                                        <li>
                                            @if($confirm_request)
                                                <a href="{{route('user.confirmrequest',$id)}}" class="btn confirm-button" style="background-color: #1dbb6f">Confirm Request</a>
                                                <a href="{{route('user.ignorerequest',$id)}}" class="btn confirm-button">Ignore Request</a>
                                            @elseif(count($friend_request)>0)
                                                <a href="{{route('user.cancelrequest',$id)}}" class="btn cancel-button" >Cancel Request </a>
                                            @elseif(count($friend_)>0)
                                                <a href="{{route('user.unfriend',$id)}}" class="btn unfriend-button">Unfriend</a>
                                            @elseif(count($friendyou_)>0)
                                                <a href="{{route('user.unfriend',$id)}}" class="btn unfriend-button">Unfriend</a>
                                            @else
                                                <a href="{{route('user.friend',$id)}}" class="btn friend-button">Friend Request</a>
                                            @endif
                                        </li>
                                    </ul>
                                </div>
                                <hr>
                                <div class=" user_pro_status row">
                                    <div class="btn col-lg-4 col-md-4 col-sm-4 col-4"><span><a class="follower" href="{{ route('user.followers',$user_link) }}">Followers </a></span></div>
                                    <div class="btn col-lg-2 col-md-2 col-sm-2 col-2"> <span class="follower-numbers"> @if(count($followers)>0){{count($followers)}}@else 0 @endif</span></div>
                                    <div class="btn col-lg-4 col-md-4 col-sm-4 col-4"><span><a class="following" href="{{ route('user.following',$user_link) }}">Following </a></span></div>
                                    <div class="btn col-lg-2 col-md-2 col-sm-2 col-2"> <span class="following-numbers"> @if(count($following)>0){{count($following)}}@else 0 @endif</span></div>
                                </div>
                            </div>
                        </div>

                        <div class="suggestions full-width">
                            <div class="sd-title">
                                <h3>Friends &nbsp;&nbsp; @if($friendsCount)({{$friendsCount}}) @endif </h3>
                                @if(count($friends)>0)
                                    <a href="{{ route('user.friends',$user_link) }}">See All Friends</a>
                                @endif
                            </div>
                            <div class="suggestions-list">
                                @if(count($friends)>0)
                                    @foreach($friends as $friend)
                                        <div class="suggestion-usd">
                                            @if($friend['picture']=='yes')
                                                <img style="width: 40px;height: 40px;" class="img-circle" src="{{url('/').'/public/storage/users/'.$friend['user_profile'].'/profile_picture.jpg'}}?v={{time()}}" alt="">
                                            @else
                                                <img style="width: 40px;height: 40px;"  class="img-circle" src="{{ asset('public/uploads/white.jpg') }}" alt="">
                                            @endif
                                            <div class="sgt-text">
                                                <h4>{{ucfirst($friend['fname'])}} {{ucfirst($friend['lname'])}}</h4>
                                                <span>
                                                          <ul class="flw-hr friend-list">
                                                              <li><a href="{{route('user.individualprofile',$friend['user_link'])}}" title="" class="hre view-profile">View Profile</a></li>
                                                          </ul>
                                                    </span>
                                            </div>
                                        </div>
                                    @endforeach
                                @endif
                                <div class="view-more">
                                    @if(count($friends)==0)
                                        <br/>
                                        <a href="#" title="">There is no friend added in friends list.</a>
                                        <br/>
                                        <br/>
                                        <br/>
                                    @endif
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-6">
                        <div class="main-ws-sec">
                            <div>
                                @if($message = Session::get('success'))
                                    <div id="mydiv">
                                        <div  class="alert alert-success alert-dismissible fade show" role="alert">
                                            <strong>{{$message}}</strong>
                                        </div>
                                    </div>
                                @endif
                            </div>

                            {{-- User Feed--}}
                            <div class="product-feed-tab current" id="feed-dd">

                                @if(count($posts)>0)
                                    <div class="main-ws-sec">
                                        <div class="posts-section" id="post-data"> 
                                        @if($random_post!=0)
                                        @include('randompost')
                                        @endif
                                            @include('sports.lists.profilepost')
                                        </div>
                                    </div>
                                @else
                                    <div class="main-ws-sec">
                                        <div class="posts-section">
                                            @if($random_post!=0)
                                        @include('randompost')
                                        @endif
                                            <div class="post-bar">
                                                <div class="post_topbar">
                                                </div>
                                                <div class="epi-sec">
                                                    <div class="job_descp">
                                                        <p>No Post Yet.</p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                @endif
                                <div class="ajax-load text-center d-none">
                                    <div class="process-comm">
                                        <div class="spinner">
                                            <div class="bounce1"></div>
                                            <div class="bounce2"></div>
                                            <div class="bounce3"></div>
                                        </div>
                                        <br/>
                                        <br/>
                                    </div>
                                </div>
                                <br/>
                            </div>

                            {{--Info--}}
                            <div class="product-feed-tab" id="info-dd">
                                <div class="user-profile-ov">
                                    <h3>About Myself</h3>
                                    <p>{{$userprofile['bio']}}</p>
                                </div>
                                <div class="user-profile-ov st2">
                                    <h3>Basic Information</h3>
                                    <div class="row">
                                        <div class="col-md-6 w-50">
                                            <h4>Date of Birth</h4>
                                        </div>
                                        <div class="col-md-6 w-50 dob">
                                            {{$userprofile['dob']}}
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-md-6 w-50">
                                            <h4>Age</h4>
                                        </div>
                                        <div class="col-md-6 w-50 age">
                                            @if($userprofile['age']){{$userprofile['age']}}@else N/A @endif
                                        </div>
                                    </div>
                                </div>
                                <div class="user-profile-ov">
                                    <h3>Location</h3>
                                    <div class="row">
                                        <div class="col-md-6 w-50">
                                            <h4>City</h4>
                                        </div>
                                        <div class="col-md-6 w-50 city">
                                            @if($userprofile['city']){{$userprofile['city']}}@else N/A @endif
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-6 w-50">
                                            <h4>Hometown</h4>
                                        </div>
                                        <div class="col-md-6 w-50 hometown">
                                            @if($userprofile['hometown']){{$userprofile['hometown']}}@else N/A @endif
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </main>
</div>

{{-- Invite Modal --}}
@include("sports.modal.invites")

{{-- Friends List--}}
@include("sports.contact")

@include('sports.script')
@include('sports.js.post')
@include('sports.js.js')
@include('sports.js.invites')
</body>
</html>