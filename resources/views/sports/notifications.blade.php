<!DOCTYPE html>
<html>
@include('sports.head')
<body class="main-body">
<div class="wrapper">
    @include('sports.head_main')
    <main>
        <div class="main-section">
            <div class="container">
                <div class="main-section-data">
                    <div class="company-title">
                        <h3>All Notifications</h3>
                    </div>
                    <div class="row">
                        <div class="col-lg-3 col-md-12 col-sm-12 col-12">
                        </div>
                        <div class="col-lg-6 col-md-12 col-sm-12 col-12">
                            <div>
                                @if(Session::get('success'))
                                    <div id="mydiv">
                                        <div  class="alert alert-success alert-dismissible fade show" role="alert">
                                            <strong>{{Session::get('success')}}</strong>
                                        </div>
                                    </div>
                                @endif
                            </div>
                                <div class="main-ws-sec">
                                    <div class="posts-section">
                                        <div class="post-bar p-2 text-center" id="notification-data">
                                            @if(count($userNotifications)>0)
                                                @include('sports.lists.notification')
                                            @else
                                                No Notification Yet.
                                            @endif
                                        </div>
                                    </div>
                                </div>
                            <div class="ajax-load text-center d-none">
                                <div class="process-comm">
                                    <div class="spinner">
                                        <div class="bounce1"></div>
                                        <div class="bounce2"></div>
                                        <div class="bounce3"></div>
                                    </div>
                                    <br/>
                                    <br/>
                                </div>
                            </div>
                            <br/>
                        </div>
                        <div class="col-lg-3 col-md-12 col-sm-12 col-12">
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </main>

</div>

{{-- Invite Modal --}}
@include("sports.modal.invites")

{{-- Friends List--}}
@include("sports.contact")

@include('sports.script')
</body>
</html>