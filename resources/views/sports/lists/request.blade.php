@foreach($friend_request as $friend_requests)
    <div class="request-details">
        <div class="noty-user-img">
            @if($friend_requests->user->picture=='yes')
                <a href="{{route('user.individualprofile',$friend_requests->user_link)}}"><img id="profile_picture_check" style="width: 40px; height: 40px; background-size: cover; background-position: center; background-repeat: no-repeat; object-fit: contain; margin-top: -5px;" src="{{url('/').'/public/storage/users/'.$friend_requests->user_id.'/profile_picture.jpg'}}?v={{time()}}" alt=""></a>
            @else
                <a href="{{route('user.individualprofile',$friend_requests->user_link)}}"><img style="width: 40px; height: 40px; background-size: cover; background-position: center; background-repeat: no-repeat; object-fit: contain; margin-top: -5px;" src="{{ asset('public/uploads/white.jpg') }}" alt=""></a>
            @endif
        </div>
        <div class="request-info">
            <h3 href="{{route('user.individualprofile',$friend_requests->user_link)}}"> {{$friend_requests->user->fname}} {{$friend_requests->user->lname}}</h3>
            <span>
                        Joined since {{date_format($friend_requests->user->created_at,'F Y')}}
                    </span>
        </div>
        <div class="accept-feat">
            <ul>
                <li><button type="submit" class="accept-req" onclick="acceptrequest('{{$friend_requests->id}}','{{$friend_requests->user_id}}')">Accept</button></li>
                <li><button type="submit" class="close-req" onclick="rejectrequest('{{$friend_requests->id}}','{{$friend_requests->user_id}}')"><i class="la la-close"></i></button></li>
            </ul>
        </div>
    </div>
@endforeach