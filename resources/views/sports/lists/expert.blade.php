<div class="row post-bar mb-3 post-bar-title rounded-0 ">
  <h1 class="col-lg-6 col-md-6 col-sm-6 col-6 ml-0" style="font-weight:bolder !important; color:black !important;" title="" >Specialists</h1>
    <a class="col-lg-6 col-md-6 col-sm-6 col-6 text-right mt-1" href="{{ url('/post/all')}}" style="font-size: 12px !important; font-family: 'Open Sans', sans-serif !important ; color: #385499 !important ;">View More</a>
</div>
  

  @foreach($exp_posts as $key => $post)

  @php
      $postimages=\App\Models\Admin\PostImage::select('image','id')->where('post_id',$post->id)->where('user_id',$post->user_id)->get();
  @endphp

  <div class="post-bar rounded-0 post-bar-title">
      <div class="post_topbar">
          <div class="usy-dt">
              @if($post->propicture=='yes')
                  <a href="{{route('user.individualprofile',$post->user_link)}}"> <img src="{{url('/').'/public/storage/users/'.$post->user_id.'/profile_picture.jpg'}}?v={{time()}}" alt=""></a>
              @else
                  <a href="{{route('user.individualprofile',$post->user_link)}}"><img src="{{ asset('public/uploads/white.jpg') }}" alt=""></a>
              @endif

              <div class="usy-name">
                  <a href="{{route('user.individualprofile',$post->user_link)}}"><h3>{{ $post->fname}} {{$post->lname}}</h3></a>
                  <span>{{date_format($post->created_at,'F d, Y ')}}</span>
              </div>

                  @if($post->user_id==Auth::user()->id)
                      <div class="ed-opts">
                          <div class="popup ed-opts-open" id="popup-{{$post->id}}"><i class="la la-ellipsis-v"></i></div>
                          <ul class="ed-options" id="subpopup-{{$post->id}}">
                              <li onclick="deletedata('{{$post->id}}')"><a class="pointer-cursor"><i class="fa fa-trash" ></i>Delete Post</a></li>
                              <li onclick="fetchdata('{{$post->id}}')" data-toggle="modal" data-target="#editPost"><a><i class="fa fa-pencil"></i>Update Post</a></li>
                          </ul>
                      </div>

                      <script>
                          $("#popup-"+<?php echo json_encode($post->id); ?>).on("click",function(){
                              $(this).next(".ed-options").toggleClass("active");return false;
                          })
                      </script>
                  @endif
          </div>


      </div>
      <div class="epi-sec">
          <div class="job_descp">
          @if ($post->caption != null)
            <h1>{{$post->title}}</h1>
              <a href="{{ route('user.individualprofile',[$post->user_link,'random'=>$post->id])}}">
                  <p style="font-size: 14px !important;">{{ \Illuminate\Support\Str::limit($post->caption, 100, $end= '...') }}
                      <span style="font-weight: bold;">Read More</span>
                  </p>
              </a>
              @else
              <h1>{{$post->title}}</h1>
              <a href="{{ route('user.individualprofile',[$post->user_link,'random'=>$post->id]) }}">
                  <p style="font-size: 15px !important;">{{ \Illuminate\Support\Str::limit($post->caption, 100, $end= '...') }}
                  </p>
              </a>
          @endif
            
              @if(count($postimages)>0)

                  <div class="top-profiles">
                      <div id="gall-{{$post->id}}" ></div>
                      <script type="text/javascript">

                          var obj = <?php echo json_encode($postimages); ?>;
                          var s= <?php echo json_encode($post->id); ?>;
                          const arrayy<?php echo json_encode($post->id); ?>=[];

                          for(var i=0;i<obj.length;i++)
                          {
                              arrayy<?php echo json_encode($post->id); ?>.push(<?php echo json_encode(url('/').'/public/storage/posts/'); ?>+'/'+obj[i].image);
                          }
                          $(function() {
                              $('#gall-'+<?php echo json_encode($post->id); ?>).imagesGrid({
                                  images: arrayy<?php echo json_encode($post->id); ?>,
                                  align: true,
                                  getViewAllText: function(imgsCount) { return 'View all' }
                              });
                          });
                      </script>
                  </div>
              @endif
          </div>
          {{--<div class="job-status-bar">--}}
              {{--<ul class="like-com">--}}
              {{--</ul>--}}
          {{--</div>--}}
      </div>
  </div>
@endforeach
