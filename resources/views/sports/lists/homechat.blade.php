

<link href="{{ asset('public/newchat/css') }}/app.min.css" id="app-style" rel="stylesheet" type="text/css">
<link href="{{ asset('public/newchat/css') }}/customstyle.css" rel="stylesheet" type="text/css">

<!-- Icons Css -->
<link href="{{ asset('public/newchat/css') }}/icons.min.css" rel="stylesheet" type="text/css">

<!-- App Css-->
<style>
    /* Chrome, Safari, Edge, Opera */
    input::-webkit-outer-spin-button,
    input::-webkit-inner-spin-button {
        -webkit-appearance: none;
        margin: 0;
    }

    /* Firefox */
    input[type=number] {
        -moz-appearance: textfield;
    }
    #no-chat-message{
        display: none;
    }

    .info-message
    {
        text-align: center;
        font-size: 13px;
        width: 100%;
    }

    .topCount
    {
        display: none;
        background-color: rgba(186, 3, 3, 0.84);
        border-radius: 50px;
        height: 16px;
        width: 16px;
        z-index: 999;
        font-size: 10px;
        text-align: center;
        margin-left: -8px;
        margin-top: -15px;
        color: white !important;

    }

    .loader_full {
        position: relative;
        height: 40vh;
        background: url(../public/sports/css/img/Hourglass.gif) center no-repeat;
        background-size: 65px 65px !important;
    }

    .sideloaderChat {
        position: relative;
        height: 40vh;
        background: url(../public/sports/css/img/Hourglass.gif) center no-repeat;
        background-size: 65px 65px !important;
    }

    .sideloaderFriend {
        position: relative;
        height: 40vh;
        background: url(../public/sports/css/img/Hourglass.gif) center no-repeat;
        background-size: 65px 65px !important;
    }

    .sideloaderGroup {
        position: relative;
        height: 40vh;
        background: url(../public/sports/css/img/Hourglass.gif) center no-repeat;
        background-size: 65px 65px !important;
    }

    .sideloaderArchive
    {
        position: relative;
        height: 40vh;
        background: url(../public/sports/css/img/Hourglass.gif) center no-repeat;
        background-size: 65px 65px !important;
    }
    .full_first {
        height: 100vh;
        display: flex;
        justify-content: center;
        align-items: center;
        /*border-left: 5px solid #d4d4d4;*/
        /*border-right: 1px solid #d4d4d4;*/
        text-align: center;
        vertical-align: middle;
        padding-top: 300px;

    }
    .full_loader {
        height: 100vh;
        display: flex;
        justify-content: center;
        align-items: center;
        border-left: 5px solid #d4d4d4;
        border-right: 1px solid #d4d4d4;
        text-align: center;
        vertical-align: middle;
        padding-top: 300px;
        background: url(../public/sports/css/img/Hourglass.gif) center no-repeat;
        background-size: 65px 65px !important;

    }

    .messages-area-left {
        background-color: rgb(237, 237, 237);
        padding: 17px;
        margin-bottom: 10px;
        text-align: center;
        vertical-align: middle;
        color:black;
        font-size: 12px;
    }

    .group_member
    {
        border-radius: 50%;
        width: 20px;
        height: 20px;
        border:2px solid black;
        background-color: white;
        margin-left: -15px;
        margin-right: 20px;
        text-align: center;
        font-size: 10px;
        margin-top: 18px;
        padding:2px;
        color:black;
        font-weight: bold;
        z-index: 9;
    }

    .recieve-message img{
        /*width: 250px !important;*/
        height: auto !important;
        border-radius: 5px !important;
        float: none !important;
        cursor:pointer;
    }
    .cover-photo-zoom {
        cursor: zoom-in;
        transition: 0.3s;
        background: url(img/loader.gif) center no-repeat;
        background-color:black;
        background-size: 45px 45px !important;
    }

    .member-list-custom {
        position: relative;
        display: flex;
        justify-content: space-between;
        align-items: center;
        width: 100%;
    }
    .members-list {
        border: 1px solid #eee;
        padding: 5px 10px;
        overflow-y: scroll;
        height: 120px;
    }
    .members-list .media {
        margin-bottom: 10px;
        display: flex;
        justify-content: flex-start;
    }

    .members-list .media img {
        width: 30px;
        border-radius: 100px;
    }

    .member_image
    {
        width: 35px !important;
        height: 35px !important;
        object-fit: cover;
        margin-right: 10px;
    }

    .cover-photo-zoom:hover {opacity: 0.7;}

    .profile-photo-zoom {
        cursor: pointer;
        transition: 0.3s;
    }

    .make_admin
    {
        float: right !important;
        margin-left: 10px;
        color: #7c151f;
    }

    .make_admin_
    {
        float: right !important;
        margin-left: 10px;
        color: #c1c1c1;
    }

    .profile-photo-zomm:hover {opacity: 0.7;}

    #myModal {
        display: none; /* Hidden by default */
        position: fixed; /* Stay in place */
        z-index: 999999; /* Sit on top */
        left: 0;
        top: 0;
        width: 100vw; /* Full width */
        height:100vh; /* Full height */
        overflow: hidden !important;
        background-color: rgb(0,0,0, 0.85); /* Fallback color */
    }

    .img-modal-content {
        width: 100% !important;
        height: 100% !important;
        background-position: center;
        background-repeat: no-repeat;
        background-size: cover;
        object-fit: contain !important;
    }

    .sub-group-field {
        font-size: 15px;
    }

    /* Caption of Modal Image */

    @-webkit-keyframes zoom {
        from {-webkit-transform:scale(0)}
        to {-webkit-transform:scale(1)}
    }

    @keyframes zoom {
        from {transform:scale(0)}
        to {transform:scale(1)}
    }

    /* The Close Button */
    #myModal .close {
        position: absolute;
        top: 10px;
        right: 10px;
        width: 35px;
        height: 35px;
        color: #fff;
        border: none;
        font-weight: lighter;
        font-size: 50px;
        cursor: pointer;
        opacity: 1;
        text-shadow: none;
    }
    #myModal .close:hover,
    #myModal .close:focus {
        color: #fff;
        text-decoration: none;
        cursor: pointer;
    }
    

    /* 100% Image Width on Smaller Screens */
    @media only screen and (max-width: 700px){
        .img-modal-content {
            width: 100%;
        }
        #myModal .close {
            display: block;
        }
        .user-tab-sec {
            margin-bottom: 5px !important;
        }
    }

    .image-upload input {
        display: none;
    }

    .via-email {
        font-size: 12px;
        font-weight: 400;
        cursor: pointer;
        color: white !important;
        background-color: #22A852;
        padding:10px 0;
        display: block;
        text-align: center;
        border-radius: 5px;
    }

    .upload-btn-wrappers {
        position: relative;
        overflow: hidden;
        display: inline-block;
    }
    .upload-btn-wrappers{
        margin-top: 5px;
        text-align: center;
        cursor: pointer;
        background-color: #e4e4e4;
        border-radius: 100px;
    }
    .upload-btn-wrappers img{
        width: 15px;
        height: 15px;
    }
    .upload-btn-wrappers input[type=file] {
        font-size: 100px;
        position: absolute;
        left: 0;
        top: 0;
        opacity: 0;
    }

    .teamType
    {
        color:red;
        font-weight: bold;
    }

    .swal-footer {
        text-align: center !important;
    }

    .modal-footer buton {
        border: none !important;
    }
    .modal-footer button:hover {
        border: none !important;
        text-decoration: none !important;
    }

    /*@media (max-width:992px){.chat-leftsidebar{min-width:100% !important; max-width:100% !important; left:0 !important;}}*/
    /*@media (min-width:1287px){.chat-leftsidebar{min-width:120% !important; max-width:120% !important;}}*/
    .chat-leftsidebar {
        width: -webkit-fill-available;
        width: -moz-available;
    }

</style>
    <div class="layout-wrapper d-lg-flex overflow-hidden d-none d-lg-block" style="border-right: 3px solid #e4e4e4; margin-top: 0 !important;">
            <div class="side-menu flex-lg-column me-lg-0 ms-lg-0 overflow-hidden">
    <!-- Start side-menu nav -->
    <div class="flex-lg-column mb-auto">
        <ul class="nav nav-pills side-menu-nav justify-content-center" role="tablist">

            <li class="nav-item" data-bs-toggle="tooltip" data-bs-placement="top" title="Chats">
                <a class="nav-link active" id="pills-chat-tab" data-bs-toggle="pill" href="#pills-chat" role="tab" onclick="renderChatListIndividual1()">
                    {{--<i class="ri-message-3-line"></i>--}}
                    <i class="fa fa-plus"><span class="topCount chatCount"></span><span style="display: block">Chats</span></i>
                </a>

            </li>
            <li class="nav-item" data-bs-toggle="tooltip" data-bs-placement="top" title="Teams">
                <a class="nav-link" id="pills-groups-tab" data-bs-toggle="pill" href="#pills-groups" role="tab" onclick="renderGroupListIndividual1()">
                    {{--<i class="ri-group-line"></i>--}}
                    <i class="fa fa-users"><span class="topCount teamCount"></span><span style="display: block">Teams</span></i>
                </a>
            </li>

            <li class="nav-item" data-bs-toggle="tooltip" data-bs-placement="top" title="Teams">
                <a class="nav-link" id="pills-archives-tab" data-bs-toggle="pill" href="#pills-archives" role="tab" onclick="renderChatListArchive1()">
                    {{--<i class="ri-group-line"></i>--}}
                    <i class="fa fa-comment"><span style="display: block">Archives</span></i>
                </a>
            </li>
            <li class="nav-item" data-bs-toggle="tooltip" data-bs-placement="top" title="Friends">
                <a class="nav-link" id="pills-contacts-tab" data-bs-toggle="pill" href="#pills-contacts" role="tab">
                    {{--<i class="ri-contacts-line"></i>--}}
                    <i class="fa fa-user-plus"><span class="d-block">Friends</span></i>
                </a>
            </li>
        </ul>
    </div>
    <!-- end side-menu nav -->
    <!-- Side menu user -->
</div>
<div class="chat-leftsidebar  chatopen">

    <div class="tab-content">
        <!-- Start chats tab-pane -->
        <div class="tab-pane fade show active" id="pills-chat" role="tabpanel" aria-labelledby="pills-chat-tab">
            <!-- Start chats content -->
            <div>
            <div class="px-4 pt-4">
                    <div class="user-chat-nav float-right">
                        <div  data-bs-toggle="tooltip" data-bs-placement="bottom" title="Create Chat">
                            <!-- Button trigger modal -->
                            <button type="button" class="btn btn-link text-decoration-none text-muted font-size-14 py-0 new_MessageModal" data-bs-toggle="modal" data-bs-target="#newMessageModal" onClick="clearFriendModel()">
                                <i class="fa fa-plus"></i>
                            </button>
                        </div>

                    </div>
                    <h4 class="mb-4" style="font-size: 21px;">Chats</h4>

                    <div class="search-box chat-search-box p-0 rounded-lg" style="right:10px;">
                        <div class="input-group  input-group-lg">
                            <div class="input-group-prepend">
                                <button class="btn btn-link text-decoration-none text-muted pe-1 ps-3 rounded-lg" type="button">
                                    <i class="ri-search-line search-icon font-size-18"></i>
                                </button>
                            </div>
                            <input type="text" class="form-control pl-0" id="search-chats" onkeyup="searchChat()" placeholder="Search Chats...">
                        </div>
                    </div>
                </div> <!-- .p-4 -->

                <!-- Start add group Modal -->
                <div class="modal fade friend-message-modal" id="newMessageModal" tabindex="-1" role="dialog" aria-labelledby="newMessageModalLabel" aria-hidden="true">
                    <div class="modal-dialog modal-dialog-centered modal-dialog-scrollable">
                        <div class="modal-content">
                            <div class="modal-header" style="border-bottom: 0.5px solid lightgrey !important ;">
                                <h5 class="modal-title font-size-16" id="newMessageModalLabel">Create New Message</h5>
                                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close" style="background-color: #fff !important; width: 25px !important; height: 25px !important; top: 20px; !important; ">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            <div class="modal-body p-4">
                                <form>
                                    <div class="mb-4">
                                        <label class="form-label">Select Friend</label>
                                        <div class="py-3">
                                            {{--<button class="btn btn-light btn-sm" type="button" data-bs-toggle="collapse" data-bs-target="#groupmembercollapse" aria-expanded="false" aria-controls="groupmembercollapse">--}}
                                                {{--Select Members--}}
                                            {{--</button>--}}
                                            <div class="show-message"></div>
                                            <select class="form-control" style="width: 97%;" id="friends-list">
                                                <option value="" selected>Select Friend</option>

                                            </select>
                                        </div>
                                    </div>
                                    <div class="mb-3">
                                        <label for="addgroupdescription-input" class="form-label py-3">Message</label>
                                        <textarea class="form-control friend_message" id="new_message_single" rows="3" placeholder="Write message" style="height: auto !important;"></textarea>
                                    </div>
                                </form>
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn" data-bs-dismiss="modal" style="background-color: #ef476f; color: aliceblue;">Close</button>
                                <button type="button"  class="btn btn-primary" onclick="sendMessageToFriend()" style="background-color: #385499;">Send</button>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- End add group Modal -->
                <!-- Start user status -->
                {{--<div class="px-4 pb-4" dir="ltr">--}}

                    {{--<div class="owl-carousel owl-theme" id="user-status-carousel">--}}
                    {{--</div>--}}
                    {{--<!-- end user status carousel -->--}}
                {{--</div>--}}
                <!-- end user status -->

                <!-- Start chat-message-list -->
                <div class="px-2" style="margin-top: 90px !important;">
                    <h5 class="mb-3 px-3 font-size-16">Recent</h5>

                    <div class="chat-message-list" data-simplebar>
                        <div class="sideloaderChat" >
                        </div>

                        {{--No List--}}
                        <div class="info-message" id="no-chat-message">
                            No Chats Found.
                        </div>

                        <ul class="list-unstyled chat-list chat-user-list" id="list-tab">
                        </ul>
                    </div>
                </div>
                <!-- End chat-message-list -->
            </div>
            <!-- Start chats content -->
        </div>
        <!-- End chats tab-pane -->

       {{--------------------------------------------------------------------------- Groups Area --------------------------------------------------------------------------------}}

        <!-- Start groups tab-pane -->
        <div class="tab-pane" id="pills-groups" role="tabpanel" aria-labelledby="pills-groups-tab">
            <!-- Start Groups content -->
            <div>
                <div class="p-4">
                    <div class="user-chat-nav float-right">
                        <div  data-bs-toggle="tooltip" data-bs-placement="bottom" title="Create group">
                            <!-- Button trigger modal -->
                            <button type="button" class="btn btn-link text-decoration-none text-muted font-size-14 py-0" data-bs-toggle="modal" data-bs-target="#newGroupModal" onClick="clearGroupModel()">
                                {{--<i class="ri-group-line me-1 ms-0"></i>--}}
                                <i class="fa fa-plus"></i>
                            </button>
                        </div>

                    </div>
                    <h4 class="mb-4" style="font-size: 21px;">Teams</h4>

                    <!-- Start add group Modal -->
                    <div class="modal fade" id="newGroupModal" tabindex="-1" role="dialog" aria-labelledby="newGroupModalLabel" aria-hidden="true">
                        <div class="modal-dialog modal-dialog-centered modal-dialog-scrollable">
                            <div class="modal-content">
                                <div class="modal-header" style="padding-right: 2.5px !important;">
                                    <h5 class="modal-title font-size-16" id="newGroupModalLabel">Create New Team</h5>
                                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>
                                <div class="modal-body p-4" style="letter-spacing: 0.5px !important;">
                                    <form id="addForm">
                                        <div class="row">
                                            <div class="col-4">
                                            </div>

                                            <div class="col-4" style="text-align:center">

                                                <img id="create-image" src="{{ asset('public/sports/images') }}/default-group.avatar.png" width="60" height="60" style="margin-left:27px;">
                                                <br/>
                                                <div class="form-group">
                                                    <div class="upload-btn-wrappers sub-group-field" style="padding: 11px 9px;">
                                                        <a class="btns" style="color: #7269ef;">
                                                            Add Avatar
                                                        </a>
                                                        {!! Form::file('picture', [
                                                            'onchange'=>'readCURL(this,)','accept'=>'image/*','id'=>'create_group_img'
                                                        ]) !!}
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="col-4">
                                            </div>
                                        </div>
                                        <div class="show-message"></div>
                                        <div class="mb-3" style="margin-top: -7px !important ">
                                            <label for="addgroupname-input" class="form-label" style="font-size: 15px; padding-bottom:30px;">Team Name</label>
                                            <input type="text" class="form-control group-name" style="font-size: 14px; color: #bcbecc;" id="group-name" placeholder="Enter Team Name">
                                        </div>
                                        <div>
                                            <label class="form-label pt-2 pb-2" style="font-size: 15px;">Team Members</label>
                                            <div class="mb-2">
                                                <button class="btn btn-light btn-sm" type="button" data-bs-toggle="collapse" data-bs-target="#groupmembercollapse" aria-expanded="false" aria-controls="groupmembercollapse" style="background-color: #e6ebf5;!important; font-size: .81031rem !important;">
                                                    Select Members
                                                </button>
                                            </div>

                                            <div class="collapse" id="groupmembercollapse">
                                                <div class="card border">
                                                    <div class="card-header">
                                                        <h5 class="font-size-15 mb-0">Friends</h5>
                                                    </div>
                                                    <div class="card-body p-2">
                                                        <div data-simplebar style="" id="friend-select-group-list">
                                                        </div>
                                                    </div>

                                                </div>
                                            </div>
                                        </div>
                                        <div class="mb-3">
                                            <p class="py-3" style="color: #495057 !important; font-size: 14.20px !important; font-weight: 600;">Select Team Type</p>

                                            <div class="container p-0" style="margin-left: -15px !important;">
                                                <div class="row">
                                                    <div class="col-4">
                                                        <input type="radio" id="family" name="team_type" class="team_type" value="Family">
                                                        <label for="html">Family</label>
                                                    </div>
                                                    <div class="col-4">
                                                        <input type="radio" id="sports" name="team_type" class="team_type" value="Sports">
                                                        <label for="css">Sport</label>
                                                    </div>
                                                    <div class="col-4">
                                                        <input type="radio" id="work" name="team_type" class="team_type" value="Work">
                                                        <label for="javascript">Work</label>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="mb-3">
                                            <label for="addgroupdescription-input" class="form-label py-3" style="font-size: 15px;">Message</label>
                                            <textarea class="form-control group_message" style="border: 1px solid #e6ebf5;padding-top: 10px !important; padding-bottom: 10px !important; color:#7a7f9a; font-size:14.2px;" id="group_message" rows="3" placeholder="Send Message.."></textarea>
                                        </div>
                                    </form>
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-link px-4 rounded" data-bs-dismiss="modal" style="background-color: #ef476f; color: aliceblue; font-size: 14px !important ">Close</button>
                                    <button type="button" class="btn btn-primary px-4 rounded" id="Submit_create" onclick="createGroupOnButton()" style="background-color: #385499; font-size: 14px !important;">Create Team</button>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- End add group Modal -->


                    <!-- Start update group Modal -->
                    <div class="modal fade" id="editGroupModal" tabindex="-1" role="dialog" aria-labelledby="editGroupModalLabel" aria-hidden="true">
                        <div class="modal-dialog modal-dialog-centered modal-dialog-scrollable">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h5 class="modal-title font-size-16" id="editGroupModalLabel" style="margin-left: -10px;">Update Team</h5>
                                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close">
                                    </button>
                                </div>
                                <div class="modal-body p-4">
                                    <form id="editForm">
                                        <div class="row">
                                            <div class="col-4">
                                            </div>

                                            <div class="col-4" style="text-align:center">

                                                <img id="edit-image" src="{{ asset('public/sports/images') }}/default-group.avatar.png" width="60" height="60">
                                                <br/>
                                                <div class="form-group">
                                                    <div class="upload-btn-wrappers sub-group-field p-2">
                                                        <a class="btns">
                                                            Add Avatar
                                                        </a>
                                                        {!! Form::file('picture', [
                                                            'onchange'=>'readeditCURL(this,)','accept'=>'image/*','id'=>'edit_group_img'
                                                        ]) !!}
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="col-4">
                                            </div>
                                        </div>
                                        <div class="show-message"></div>
                                        <div class="mb-4">
                                            <label for="addgroupname-input" class="form-label">Team Name</label>
                                            <input type="text" class="form-control group-name" id="group_name_update" placeholder="Enter Team Name">
                                        </div>
                                        <div class="mb-4">
                                            <label for="addgroupname-input" class="form-label">Team Members</label>
                                            <div class="edit-group">
                                                <div class="members-list"></div>
                                            </div>
                                        </div>
                                        <div class="mb-3">
                                            <p>Select Team Type</p>

                                            <div class="container p-0">
                                                <div class="row">
                                                    <div class="col-4">
                                                        <input type="radio" id="Family_edit" name="team_type_edit" value="Family">
                                                        <label for="html">Family</label>
                                                    </div>
                                                    <div class="col-4">
                                                        <input type="radio" id="Sports_edit" name="team_type_edit" value="Sports">
                                                        <label for="css">Sport</label>
                                                    </div>
                                                    <div class="col-4">
                                                        <input type="radio" id="Work_edit" name="team_type_edit" value="Work">
                                                        <label for="javascript">Work</label>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                                <div class="modal-footer">
                                    <div class="container">
                                        <div class="row">

                                            <!-- Button trigger New Member modal -->



                                            <div class="col-12 no-pdd pr-1 mb-1">
                                                <button type="button" style="width: 100%" class="btn btn-primary" onclick="updateGroupName()">UPDATE</button>
                                            </div>

                                            <div class="col-12 no-pdd pr-1 mb-1">
                                                <button type="button" style="width: 100%" class="btn btn-primary update_group_member" data-bs-toggle="modal" data-bs-target="#addNewMember" data-dismiss="modal" onclick="updateFriends()">Add member</button>
                                            </div>

                                            <div class="col-12 no-pdd pr-1 mb-1">
                                                <button type="button" style="width: 100%" class="btn btn-primary invites_via_group" title="" data-bs-toggle="modal" data-bs-target="#invites_friends" data-dismiss="modal" onclick="via_invites_groups()">Invite Friends</button>
                                            </div>

                                            <div class="col-6 no-pdd pr-1 mb-1 archive_group_update">
                                                <button type="button" style="width: 100%" class="btn btn-primary" onclick="archiveGroup()">Archive</button>
                                            </div>


                                            <div class="col-6 no-pdd pr-1 mb-1 delete_group_update">
                                                <button type="button" style="width: 100%" class="btn btn-primary" onclick="deleteGroup()">Delete</button>
                                            </div>

                                            <div class="col-6 no-pdd pr-1 mb-1 unarchive_group_update">
                                                <button type="button" style="width: 100%" class="btn btn-primary" onclick="unarchiveGroup()">Unarchive</button>
                                            </div>

                                            <div class="col-6 no-pdd pr-1 mb-1 leave_group_update">
                                                <button type="button" style="width: 100%" class="btn btn-primary" onclick="leaveGroup()">Leave</button>
                                            </div>

                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- End add update Modal -->


                    <!-- Add New Member Modal -->
                    <div class="modal fade" id="addNewMember" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                        <div class="modal-dialog  modal-dialog-centered">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h5 class="modal-title font-size-16" id="exampleModalLabel">Add New Member</h5>
                                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close">
                                    </button>
                                </div>
                                <div class="modal-body add-member">
                                    <form>
                                        <div class="show-message"></div>
                                        <div class="form-group">
                                            <select class="selectpicker form-control" style="width: 100%" multiple id="add-friend-group" data-live-search="true">
                                                <option value="" selected>Select Friends</option>
                                            </select>
                                        </div>
                                    </form>
                                </div>

                                <div class="modal-footer">
                                    <button type="button" class="btn btn-link" data-bs-dismiss="modal">Close</button>
                                    <button type="button" class="btn btn-primary" onClick="addMemberToCurrentGroup()">Add Member</button>

                                </div>
                            </div>
                        </div>
                    </div>

                    <!-- Invites Modal -->
                    <div class="modal fade" id="invites_friends" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                        <div class="modal-dialog  modal-dialog-centered">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h5 class="modal-title font-size-16" id="exampleModalLabel">Invite Friends</h5>
                                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close">
                                    </button>
                                </div>

                                {!! Form::open(['method' => 'POST', 'id'=>'invites_' ,'route' => ['user.invites'],'data-parsley-validate','files' => 'true','enctype'=>'multipart/form-data']) !!}
                                <div class="modal-body p-2 text-left">
                                    <div class="post-project-fields pt-0 pb-4 pl-4 pr-4">
                                        <div class="col-12 no-pdd">
                                            <div class="row">
                                                <div class="col-sm-10 col-9">
                                                    {!! Form::text('urlInvite','', [ 'id'=>'urlInvite' ,'class'=>'form-control','style'=>'width:100% !important', 'readonly'=>'true']) !!}
                                                </div>
                                                <div class="col-sm-2 col-3 p-0">
                                                    {{--<span class="tooltiptext" id="myTooltip">Copy to clipboard</span>--}}
                                                    <a class="via-email" style="padding: 10px;width: 70px;" onclick="myFunction()" onmouseout="outFunc()">
                                                        Copy
                                                    </a>
                                                </div>
                                            </div>
                                        </div>
                                        <br/>
                                        <br/>
                                        <div class="row">
                                            <span id="mobile_error_message_group" style="font-size: 11px;margin-top: 2px;height: 10px">
                                            </span>
                                        </div>

                                        <div class="row no-pdd">
                                            <div class="col-lg-12 no-pdd">
                                                {!! Form::text('id', '', [ 'id'=>'id_to_invite', 'class'=>'form-control'  , 'required'=>'true','hidden'=>'true']) !!}
                                                {!! Form::text('url_', 'chat', [ 'id'=>'url_', 'class'=>'form-control'  , 'required'=>'true','hidden'=>'true']) !!}
                                                {!! Form::text('level', '', [ 'id'=>'level', 'class'=>'form-control'  , 'required'=>'true','hidden'=>'true']) !!}
                                                {!! Form::text('group_name', '', [ 'id'=>'group_name', 'class'=>'form-control'  , 'required'=>'true','hidden'=>'true']) !!}
                                                <div class="sn-field">
                                                    <br/>
                                                    <label class="label-">Phone Number </label>
                                                    <div class="row">
                                                        <div class="col-sm-4">
                                                            {!! Form::select('code', config('status.code'),'', [ 'id'=>'code_group', 'class'=>'form-control' , 'required'=>'']) !!}
                                                        </div>
                                                        <div class="col-sm-8">
                                                            {!! Form::number('mob_', '', [ 'id'=>'mob_group', 'class'=>'form-control' , 'placeholder' => 'Enter phone number' , 'required'=>'']) !!}
                                                            <br/>
                                                        </div>
                                                    </div>
                                                </div>
                                                <p class="text-center text-dark">OR</p>
                                                <span id="email_error_message_group" style="font-size: 11px;margin-top: 2px;height: 10px">
                                                </span>
                                                <div class="sn-field">
                                                    <br/>
                                                    <label class="label-">Email </label>
                                                    {!! Form::text('email_', '', [ 'id'=>'email_group', 'class'=>'form-control' , 'placeholder' => 'Enter email' , 'required'=>'', ''=>'text-transform:']) !!}
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-link" data-bs-dismiss="modal">Close</button>
                                    <button type="button" class="btn btn-primary" id="invite_button_group" onclick="sendinviteGroup()">SEND INVITE</button>

                                </div>
                                {!! Form::close() !!}
                            </div>
                        </div>
                    </div>

                    <div class="search-box chat-search-box rounded-lg" style="right: 10px;">
                        <div class="input-group input-group-lg">
                            <div class="input-group-prepend">
                                <button class="btn btn-link text-decoration-none text-muted pe-1 ps-3 rounded-lg" type="button">
                                    <i class="ri-search-line search-icon font-size-18"></i>
                                </button>
                            </div>
                            <input type="text" class="form-control pl-0" id="search-groups" onkeyup="searchgroup()" placeholder="Search Teams...">
                        </div>
                    </div>
                </div>

                <!-- Start user status -->
                {{--<div class="px-4 pb-4" dir="ltr">--}}

                    {{--<div class="owl-carousel owl-theme" id="user-status-carousel">--}}
                    {{--</div>--}}
                    {{--<!-- end user status carousel -->--}}
                {{--</div>--}}
                <!-- end user status -->

                <!-- Start chat-message-list -->
                <div class="px-2" style="margin-top: 45px !important;">
                    <h5 class="mb-3 px-3 font-size-16">Recent</h5>

                    <div class="chat-message-list" data-simplebar>
                        <div class="sideloaderGroup" >
                        </div>

                        {{--No List--}}
                        <div class="info-message" id="no-group-message">
                            No Teams Found.
                        </div>

                        <ul class="list-unstyled chat-list chat-user-list" id="list-tab-group">
                        </ul>
                    </div>
                </div>
                <!-- End chat-message-list -->

                {{--<!-- Start chat-group-list -->--}}
                {{--<h5 class="mb-3 px-3 font-size-16">Recent</h5>--}}
                {{--<div class="p-4 chat-message-list chat-group-list" data-simplebar>--}}
                    {{--<ul class="list-unstyled chat-list" id="list-tab-group">--}}

                    {{--</ul>--}}
                {{--</div>--}}
                {{--<!-- End chat-group-list -->--}}
            </div>
            <!-- End Teams content -->
        </div>
        <!-- End groups tab-pane -->

     {{--------------------------------------------------------------------------- archives Area --------------------------------------------------------------------------------}}

    <!-- Start groups tab-pane -->
        <div class="tab-pane" id="pills-archives" role="tabpanel" aria-labelledby="pills-archives-tab">
            <!-- Start Groups content -->
            <div>
                <div class="p-4">
                    <div class="user-chat-nav float-right">

                    </div>
                    <h4 class="mb-4" style="font-size: 21px;">Archives</h4>
                    <div class="search-box chat-search-box  rounded-lg" style="right: 10px;">
                        <div class="input-group input-group-lg">
                            <div class="input-group-prepend">
                                <button class="btn btn-link text-decoration-none text-muted pe-1 ps-3 rounded-lg" type="button">
                                    <i class="ri-search-line search-icon font-size-18"></i>
                                </button>
                            </div>
                            <input type="text" class="form-control pl-0" id="search-archives" onkeyup="searchArchives()" placeholder="Search Archives...">
                        </div>
                    </div>

                </div>

                <!-- Start user status -->
                {{--<div class="px-4 pb-4" dir="ltr">--}}

                    {{--<div class="owl-carousel owl-theme" id="user-status-carousel">--}}
                    {{--</div>--}}
                    {{--<!-- end user status carousel -->--}}
                {{--</div>--}}
                <!-- end user status -->

                <!-- Start chat-message-list -->
                <div class="px-2" style="margin-top: 45px !important;">
                    <h5 class="mb-3 px-3 font-size-16">Recent</h5>

                    <div class="chat-message-list" data-simplebar>
                        <div class="sideloaderArchive" >
                        </div>

                        {{--No List--}}
                        <div class="info-message" id="no-archive-message">
                            No Archives Found.
                        </div>

                        <ul class="list-unstyled chat-list chat-user-list" id="list-tab-archive">
                        </ul>
                    </div>
                </div>
                <!-- End chat-message-list -->
            </div>
            <!-- End Teams content -->
        </div>
        <!-- End groups tab-pane -->

    {{--------------------------------------------------------------------------- Friends Area --------------------------------------------------------------------------------}}

        <!-- Start contacts tab-pane -->
        <div class="tab-pane" id="pills-contacts" role="tabpanel" aria-labelledby="pills-contacts-tab">
            <!-- Start Contact content -->
            <div>
                <div class="p-4">

                    <h4 class="mb-4" style="font-size: 21px;">Friends</h4>
                    <div class="search-box chat-search-box rounded-lg" style="right: 10px;">
                        <div class="input-group input-group-lg">
                            <div class="input-group-prepend">
                                <button class="btn btn-link text-decoration-none text-muted pe-1 ps-3 rounded-lg" type="button">
                                    <i class="ri-search-line search-icon font-size-18"></i>
                                </button>
                            </div>
                            <input type="text" class="form-control bg-light pl-0" id="search-friends" placeholder="Search Friends...">
                        </div>
                    </div>
                    <!-- End search-box -->
                </div>
                <!-- end p-4 -->

                <!-- Start user status -->
                {{--<div class="px-4 pb-4" dir="ltr">--}}

                    {{--<div class="owl-carousel owl-theme" id="user-status-carousel">--}}
                    {{--</div>--}}
                    {{--<!-- end user status carousel -->--}}
                {{--</div>--}}
                <!-- end user status -->

                <!-- Start chat-message-list -->
                <div class="px-2" style="margin-top: 45px;">
                    <div class="chat-message-list" data-simplebar>
                        <div class="sideloaderFriend" >
                        </div>

                        {{--No List--}}
                        <div class="info-message" id="no-friend-message" style="margin-top: 20px;">
                            No Friends Found.
                        </div>

                        <ul class="list-unstyled chat-list chat-user-list" id="list-tab-friend">
                        </ul>
                    </div>
                </div>
                <!-- end contact lists -->
            </div>
            <!-- Start Contact content -->
        </div>
        <!-- End contacts tab-pane -->

    </div>
    <!-- end tab content -->

</div>
    </div>

<script src="{{ asset('public/newchat/libs/jquery') }}/jquery.min.js"></script>

<!-- Emoji JS -->
{{--<script src="https://code.jquery.com/jquery-3.6.0.min.js" integrity="sha256-/xUj+3OJU5yExlq6GSYGSHk7tPXikynS7ogEvDej/m4=" crossorigin="anonymous"></script>--}}
<script src="{{ asset('public/newchat/js') }}/emojionearea.min.js"></script>

<script src="{{ asset('public/newchat/libs/bootstrap/js') }}/bootstrap.bundle.min.js"></script>
<script src="{{ asset('public/newchat/libs/simplebar') }}/simplebar.min.js"></script>
<script src="{{ asset('public/newchat/libs/nodes-waves') }}/waves.min.js"></script>


<!-- Magnific Popup-->
<script src="{{ asset('public/newchat/libs/magnific-popup') }}/jquery.magnific-popup.min.js"></script>

<!-- owl.carousel js -->
<script src="{{ asset('public/newchat/libs/owl.carousel') }}/owl.carousel.min.js"></script>

<!-- page init -->
<script src="{{ asset('public/newchat/js/pages') }}/index.init.js"></script>

{{-- Custom Script --}}

{{-- Sweet Alert --}}
<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>

<script>
    $(document).ready(function(){
        $(window).scroll(function(){
            $(".layout-wrapper").css.position = "fixed";
        })
    })
</script>
<script>
    $('#newGroupModal').appendTo("body");
    $('#newMessageModal').appendTo("body");
</script>
