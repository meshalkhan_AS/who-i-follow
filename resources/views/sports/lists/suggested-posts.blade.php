
@foreach($posts as $key => $post)

@php
    $postimages=\App\Models\Admin\PostImage::select('image','id')->where('post_id',$post->id)->where('user_id',$post->user_id)->get();
@endphp

<div class="post-bar">
    <div class="post_topbar">
        <div class="usy-dt">
            @if($post->propicture=='yes')
                <a href="{{route('user.individualprofile',$post->user_link)}}"> <img src="{{url('/').'/public/storage/users/'.$post->user_id.'/profile_picture.jpg'}}?v={{time()}}" alt=""></a>
            @else
                <a href="{{route('user.individualprofile',$post->user_link)}}"><img src="{{ asset('public/uploads/white.jpg') }}" alt=""></a>
            @endif

            <div class="usy-name">
                <a href="{{route('user.individualprofile',$post->user_link)}}"><h3>{{ $post->fname}} {{$post->lname}}</h3></a>
                <span>{{date_format($post->created_at,'F d, Y ')}}</span>
               
            </div>
        </div>


    </div>
    <div class="epi-sec">
        <div class="job_descp">
            <h1>{{$post->title}}</h1>
            <p>{{$post->caption}}</p>

            @if(count($postimages)>0)

                <div class="top-profiles">
                    <div id="gall-{{$post->id}}" ></div>
                    <script>
                        var obj = <?php echo json_encode($postimages); ?>;
                        var s= <?php echo json_encode($post->id); ?>;
                        const arrayy<?php echo json_encode($post->id); ?>=[];

                        for(var i=0;i<obj.length;i++)
                        {
                            arrayy<?php echo json_encode($post->id); ?>.push(<?php echo json_encode(url('/').'/public/storage/posts/'); ?>+'/'+obj[i].image);
                        }
                        $(function() {
                            $('#gall-'+<?php echo json_encode($post->id); ?>).imagesGrid({
                                images: arrayy<?php echo json_encode($post->id); ?>,
                                align: true,
                                getViewAllText: function(imgsCount) { return 'View all' }
                            });
                        });
                    </script>
                </div>
            @endif
        </div>
        {{--<div class="job-status-bar">--}}
            {{--<ul class="like-com">--}}
            {{--</ul>--}}
        {{--</div>--}}
    </div>
</div>
@endforeach


