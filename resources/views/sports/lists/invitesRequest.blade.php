{{--<div class="row">--}}
    {{--<div class="col-lg-3 col-md-4 col-sm-4 col-12 mt-2 mb-2 mr-2 bg-white text-center rounded chk-1 no-pdd">--}}
        {{--<a class="chk-2 nav-link pointer-cursor border-0" style="color: white !important;">--}}
            {{--Invitations--}}
        {{--</a>--}}
    {{--</div>--}}

{{--</div>--}}

<style>
    .social-group-name {
        overflow: hidden;
        text-overflow: ellipsis;
        display: -webkit-box;
        -webkit-line-clamp: 1;
        -webkit-box-orient: vertical;
    }
</style>

<div class="row">
@foreach($invites as $invite)
    <div class="col-lg-2 col-md-3 col-sm-6 col-6 p-1">
        <div class="company_profile_info bg-white">
            <div class="company-up-info friends-requests">
                <br/>
                        @if($invite['group_name']!='')
                            @if($invite['group_image'])
                                <img src="{{ url('/').$invite['group_image'] }}" alt="">
                            @else
                                <img src="{{  asset('public/sports/images').'/default-group.avatar.png' }}" alt="">
                            @endif
                        @else
                            <img style="border-radius:0px;-webkit-border-radius:0;height: 60px;width: auto;margin-top:4px;" src="{{ asset('public/sports/images/login images') }}/logo.svg" alt="">

                        @endif
                <a title="" style="text-transform: none !important;">
                    @if($invite['group_name']!='')
                    @else
                        <br/>
                         <br/>
                    @endif
                    <h3>{{$invite['target']}}</h3>

                        <b>@if($invite['group_name']!='')Invited to {{ \Illuminate\Support\Str::limit($invite['group_name'], 20, $end= '...')}}@else Social Invitation @endif</b>
                </a>
                <ul>
                    <li>
                        <br>
                        <a class="follow unfollow-button" onclick="resendInvitation('{{$invite['id']}}')">Resend</a>
                    </li>
                </ul>
            </div>
        </div>
    </div>
@endforeach
</div>