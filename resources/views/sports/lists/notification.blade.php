
@foreach($userNotifications as $userNotification)

    <?php
    $sender=\App\Models\Admin\UserProfile::select('user_profiles.*','users.user_link as user_link')
        ->join('users', 'users.id', '=', 'user_profiles.user_id')->where('user_profiles.user_id',$userNotification->send_by)->first();
    $username = $sender['fname'].' '.$sender['lname'];
    $message_type=$userNotification->message_type;
    if($message_type=="followed")
    {
        $message = "followed you.";
        $url=route('user.followers',Auth::user()->user_link);
    }
    else if($message_type=="unfollowed")
    {
        $message  = "unfollowed you.";
        $url=route('user.followers',Auth::user()->user_link);
    }
    else if($message_type=="sentFriendRequest")
    {
        $message  = "sent you friend request.";
        $url=route('user.friends',['friendrequest'=>'yes','id'=>Auth::user()->user_link]);
    }
    else if($message_type=="confirmFriendRequest")
    {
        $message  = "confirmed your friend request.";
        $url=route('user.friends',Auth::user()->user_link);
    }
    else if($message_type=="addPost")
    {
        $message  = "added a new post.";
        $url=route('user.news',['random'=>$userNotification->table_id]);
    }
    else if($message_type=="joinedByInvite")
    {
        $message  = "joined Who I follow and has been added to your friend list.";
        $url=route('user.friends',Auth::user()->user_link);
    }
    else if($message_type=="AddedAsFriend")
    {
        $message  = "has been added to your friend list.";
        $url=route('user.friends',Auth::user()->user_link);
    }
    else
    {
        $message  = "";
        $url="";
    }
    ?>

    <a href="{{$url}}" onclick="readNotification({{$userNotification->id}})">
        @if($userNotification->read=='0')
            <div class="notfication-details" style="border-bottom: 1px solid lightgrey;">
                @else
                    <div class="notfication-details" style="border-bottom: 1px solid lightgrey;background-color: #f3f3f3">
                        @endif
                        <div class="noty-user-img">
                            @if($sender['picture']=='yes')
                                <img src="{{url('/').'/public/storage/users/'.$sender['user_id'].'/profile_picture.jpg'}}?v={{time()}}" alt="">
                            @else
                                <img src="{{ asset('public/uploads/white.jpg') }}" alt="">
                            @endif
                        </div>
                        <div class="notification-info">
                            <h3>
                                <text style="font-weight: bold">{{$username}}</text>
                                <text style="font-weight: normal">{{$message}}</text>
                            </h3>
                            <p><time class="timeago" datetime="{{date("Y-m-d H:i:s", strtotime('+5 hours', strtotime($userNotification->created_at)))}}"></time></p>
                        </div>
                    </div>
    </a>
@endforeach

<script>
    $(".timeago").timeago();
</script>
