@foreach($profiles as $profile)
    @if($profile->user_id!=Auth::user()->id)
    @php
        $follower=\App\Models\Admin\Follower::where('follower_id',$profile->user_id)->where('user_id',Auth::user()->id)->get();
        $friend_request=\App\Models\Admin\FriendList::where('friend_id',$profile->user_id)->where('user_id',Auth::user()->id)->where('status','0')->get();
        $confirm_request=\App\Models\Admin\FriendList::where('friend_id',Auth::user()->id)->where('user_id',$profile->user_id)->where('status','0')->first();
        $friend_=\App\Models\Admin\FriendList::where('friend_id',$profile->user_id)->where('user_id',Auth::user()->id)->where('status','1')->get();
        $friendyou_=\App\Models\Admin\FriendList::where('user_id',$profile->user_id)->where('friend_id',Auth::user()->id)->where('status','1')->get();
    @endphp
    <div class="col-lg-2 col-md-3 col-sm-6 col-6 p-1">
        <div class="company_profile_info">
            <div class="company-up-info friends-requests">
                @if($profile->picture=='yes')
                    <a href="{{route('user.individualprofile',$profile->user_link)}}" title="">
                        <img id="profile_picture_check" src="{{url('/').'/public/storage/users/'.$profile->user_id.'/profile_picture.jpg'}}?v={{time()}}" alt="">
                    </a>
                @else
                    <a href="{{route('user.individualprofile',$profile->user_link)}}" title="">
                        <img src="{{ asset('public/uploads/dumy-pic.png') }}" alt="">
                    </a>
                @endif
                <a href="{{route('user.individualprofile',$profile->user_link)}}" title="">
                    <h3>{{$profile->fname}} {{$profile->lname}}</h3>
                </a>
                @if($profile->user_id!=Auth::user()->id)
                    <ul>
                        <li>
                            @if(!$confirm_request)
                                @if(count($follower)>0)
                                    <a href="{{route('user.unfollow',$profile->user_id)}}" class="follow unfollow-button">Unfollow</a>
                                @else
                                    <a href="{{route('user.follow',$profile->user_id)}}" class="unfollow follow-button">Follow</a>
                                @endif
                            @endif
                        </li>
                        <li>
                            @if($confirm_request)
                                <a href="{{route('user.confirmrequest',$profile->user_id)}}" class="hire-us confirm-request" style="background-color: #1dbb6f">Confirm Request</a>
                                <a href="{{route('user.ignorerequest',$profile->user_id)}}" class="hire-us confirm-request delete-request">Ignore Request</a>
                            @elseif(count($friend_request)>0)
                                <a href="{{route('user.cancelrequest',$profile->user_id)}}" title="" class="hire-us request-sent">Cancel Request</a>
                            @elseif(count($friend_)>0)
                                <a href="{{route('user.unfriend',$profile->user_id)}}" title="" class="hire-us unfriend">Unfriend</a>
                            @elseif(count($friendyou_)>0)
                                <a href="{{route('user.unfriend',$profile->user_id)}}" title="" class="hire-us unfriend">Unfriend</a>
                            @else
                                <a href="{{route('user.friend',$profile->user_id)}}" title="" class="hire-us">Friend Request</a>
                            @endif
                        </li>
                    </ul>
                @else
                    <ul>
                        <li>
                            <a href="{{route('user.home',Auth::user()->user_link)}}" title="" class="hire-us you">You</a>
                        </li>
                    </ul>
                @endif
            </div>
        </div>
    </div>
    @endif
@endforeach