
@foreach($allposts as $key => $post)

@php
    $postimages=\App\Models\Admin\PostImage::select('image','id')->where('post_id',$post->id)->where('user_id',$post->user_id)->get();
@endphp

<div class="post-bar">
    <div class="post_topbar">
        <div class="usy-dt">
            @if($post->propicture=='yes')
                <a href="{{route('user.individualprofile',$post->user_link)}}"> <img src="{{url('/').'/public/storage/users/'.$post->user_id.'/profile_picture.jpg'}}?v={{time()}}" alt=""></a>
            @else
                <a href="{{route('user.individualprofile',$post->user_link)}}"><img src="{{ asset('public/uploads/white.jpg') }}" alt=""></a>
            @endif

            <div class="usy-name">
                <a href="{{route('user.individualprofile',$post->user_link)}}"><h3>{{ $post->fname}} {{$post->lname}}</h3></a>
                <span>{{date_format($post->created_at,'F d, Y ')}}</span>
            </div>
        </div>
    </div>
    <div class="epi-sec">
        <div class="job_descp">
            <h1>{{$post->title}}</h1>
            <p>{{$post->caption}}</p>

            @if(count($postimages)>0)
                @if(count($postimages)==1)
                    <style>
                        .imgs-grid .imgs-grid-image .image-wrap img
                        {
                            width: auto;
                            float:inherit;
                        }

                        .imgs-grid .imgs-grid-image:before {
                            background-color: black;
                        }
                    </style>
                @else
                    <style>
                        .imgs-grid .imgs-grid-image .image-wrap img
                        {
                            width: 100%;
                            float:left;
                        }
                    </style>
                @endif

                <div class="top-profiles">
                    <div id="gal-{{$post->id}}" ></div>
                    <script type="text/javascript">

                        var objs = <?php echo json_encode($postimages); ?>;
                        var s= <?php echo json_encode($post->id); ?>;
                        const array<?php echo json_encode($post->id); ?>=[];

                        for(var i=0;i<objs.length;i++)
                        {
                            array<?php echo json_encode($post->id); ?>.push(<?php echo json_encode(url('/').'/public/storage/posts/'); ?>+'/'+objs[i].image);
                        }
                        $(function() {
                            $('#gal-'+<?php echo json_encode($post->id); ?>).imagesGrid({
                                images: array<?php echo json_encode($post->id); ?>,
                                align: true,
                                getViewAllText: function(imgsCount) { return 'View all' }
                            });
                        });
                    </script>
                </div>
            @endif
        </div>
        {{--<div class="job-status-bar">--}}
            {{--<ul class="like-com">--}}
            {{--</ul>--}}
        {{--</div>--}}
    </div>
</div>
@endforeach


