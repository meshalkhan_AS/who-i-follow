<!DOCTYPE html>
<html>

@include('sports.head')
<body class="main-body">
<div class="wrapper">
    @include('sports.head_main')
    <main>
        <div class="container">
        </div>
        <div class="main-section">
            <div class="container-fluid no-pdd">
                <div class="main-section-data">
                    <div class="row">
                        <div class="col-lg-3 col-md-2 col-sm-12 col-12 no-pdd">
                            {{--<div class="suggestions full-width">--}}
                                {{--<div class="sd-title text-center">--}}
                                    {{--<h3>Add or invite friends</h3>--}}
                                {{--</div>--}}
                                {{--<div class="suggestions-list">--}}
                                    {{--<div class="view-more">--}}
                                        {{--<div class="user_pro_status">--}}
                                            {{--<ul class="flw-hr">--}}
                                                {{--<li><a href="{{ route('user.profiles') }}" title="" class="add-friends">Add Friends</a></li>--}}
                                            {{--</ul>--}}
                                            {{--<ul class="flw-hr">--}}
                                                {{--<li><a class="invites-friends" title="" data-toggle="modal" data-target="#invites"  onclick="via_invites()">Invite Friends</a></li>--}}
                                            {{--</ul>--}}
                                        {{--</div>--}}
                                    {{--</div>--}}
                                {{--</div>--}}
                            {{--</div>--}}
                        </div>
                        <div class="col-lg-6 col-md-8 col-sm-12 col-12 no-pdd">
                            <div>
                                @if(Session::get('success'))
                                    <div id="mydiv">
                                        <div  class="alert alert-success alert-dismissible fade show" role="alert">
                                            <p>{{Session::get('success')}}</p>
                                        </div>
                                    </div>
                                @endif
                            </div>
                            <div class="post-topbar">
                         <h1 class="open-post-popup col-lg-12 col-md-12 col-sm-12 text-center" style="font-weight:bolder !important; color:black !important; left:0 !important; top: 0!important;" title="" >Specialists</h1>
                                  
                            </div>


                            @if(count($posts)>0)
                                <div class="main-ws-sec">
                                    <div class="posts-section" id="post-data">
                                        @include('sports.lists.allexpert')
                                    </div>
                                </div>
                            @else
                                <div class="main-ws-sec">
                                    <div class="posts-section">
                                        <div class="post-bar">
                                            <div class="post_topbar">
                                            </div>
                                            <div class="epi-sec">
                                                <div class="job_descp">
                                                    <p>No Post Yet.</p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            @endif
                            <div class="ajax-load text-center d-none">
                                <div class="process-comm">
                                    <div class="spinner">
                                    <div class="bounce1"></div>
                                    <div class="bounce2"></div>
                                    <div class="bounce3"></div>
                                    </div>
                                    <br/>
                                    <br/>
                                </div>
                            </div>
                            <br/>
                        </div>
                        <div class="col-lg-3 col-md-2 col-sm-12 col-12 no-pdd">
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </main>

    <!-- Contact List -->
    @include('sports.contact')

    <!-- Post Modal -->
    @include('sports.modal.addpost')

    <!-- Update Modal -->
    @include('sports.modal.editpost')

    <!-- Invites Modal -->
    @include('sports.modal.invites')
</div>
@include('sports.script')
@include('sports.js.post')
@include('sports.js.invites')
@include('sports.js.images')
</body>
</html>