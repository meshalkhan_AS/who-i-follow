<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js"></script>
{{--<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.0/js/select2.min.js"></script>--}}

<script type="text/javascript">

    {{--bio section--}}
    {{--$(document).ready(function () {--}}
        {{--$(".age-select").select2({--}}
            {{--placeholder: "Select Your Age",--}}
            {{--tags: true,--}}
            {{--tokenSeparators: ["/", ",", ";", " "]--}}
        {{--});--}}
    {{--});--}}
    {{--$(document).ready(function () {--}}
        {{--$(".country-select").select2({--}}
            {{--placeholder: "Select Your Country",--}}
            {{--tags: true,--}}
            {{--tokenSeparators: ["/", ",", ";", " "]--}}
        {{--});--}}
    {{--});--}}

    {{--$(document).ready(function () {--}}
        {{--$(".city-select").select2({--}}
            {{--placeholder: "Select Your city",--}}
            {{--tags: true,--}}
            {{--tokenSeparators: ["/", ",", ";", " "]--}}
        {{--});--}}
    {{--});--}}

    {{--$(document).ready(function () {--}}
        {{--$(".hometown-select").select2({--}}
            {{--placeholder: "Select Your Hometown",--}}
            {{--tags: true,--}}
            {{--tokenSeparators: ["/", ",", ";", " "]--}}
        {{--});--}}
    {{--});--}}

    {{--$(document).ready(function () {--}}
        {{--$(".gender-select").select2({--}}
            {{--placeholder: "Select Your Gender",--}}
            {{--tags: true,--}}
            {{--tokenSeparators: ["/", ",", ";", " "]--}}
        {{--});--}}
    {{--});--}}

    {{--$(document).ready(function () {--}}
        {{--$(".religion-select").select2({--}}
            {{--placeholder: "Select Your Religion",--}}
            {{--tags: true,--}}
            {{--tokenSeparators: ["/", ",", ";", " "]--}}
        {{--});--}}
    {{--});--}}

    {{--sports section--}}
    {{--$(document).ready(function () {--}}
        {{--$(".sport-select").select2({--}}
            {{--placeholder: "Pick Your Sports",--}}
            {{--tags: true,--}}
            {{--tokenSeparators: ["/", ",", ";", " "],--}}
            {{--maximumSelectionLength: 5--}}
        {{--});--}}
    {{--});--}}


    //    Add Row
    var validationcheck = function ()
    {
        var age = $('#age').val();
        var bio = $('#bio').val();
        var city = $('#city').val();
        var url = $('#url').val();
        var hometown = $('#hometown').val();
        
        if (age == '' && bio == '' && city == '' && hometown == '' && url=='') {
            swal("Type Again", "Please fill the required fields", "error");
        }
        else if (url!='') {
            if(isValidURL(url)==true){
            console.log(isValidURL(url));
            $("#bioForm").submit();
        }
        else{
            swal("Type Again", "Please Enter Correct URL", "error");
        }      
        }
        else{
            $("#bioForm").submit();
        }

    };
    function isValidURL(string) {
  var res = string.match(/(http(s)?:\/\/.)?(www\.)?[-a-zA-Z0-9@:%._\+~#=]{2,256}\.[a-z]{2,6}\b([-a-zA-Z0-9@:%_\+.~#?&//=]*)/g);
  return (res !== null)
};


    function checkabout()
    {
        var email = $('#email').val();
        var mob = $('#mob').val();

        if(email!='')
        {
            $.ajax({
                type: 'POST',
                url: '{{ route('login.checkemail') }}',
                data: {
                    _token: '{{ csrf_token() }}',
                    email: email,
                    id: $('#user_id').val()
                },

                success: function (response) {
                    if(response=='repeat')
                    {
                        var title='Email Already Exists.!';
                        swal("Type Again", title, "error");
                    }
                    else if(response=='success')
                    {
                        if (mob == '' )
                        {
                            swal("", "Please Enter Mobile Number", "error");
                        }
                        else
                        {
                            $.ajax({
                                type: 'POST',
                                url: '{{ route('login.checkmobile') }}',
                                data: {
                                    _token: '{{ csrf_token() }}',
                                    mob: mob,
                                    id: $('#user_id').val()
                                },

                                success: function (response) {
                                    if(response=='repeat')
                                    {
                                        var title='Phone Number Already Exists.!';
                                        swal("Type Again", title, "error");
                                    }
                                    else if(response=='success')
                                    {
                                        var date = document.getElementById("dob").value;
                                        var varDate = new Date(date);
                                        var today = new Date('12/31/2005');
                                        if(varDate >= today) {
                                            swal("Type Again", "You are not allowed to enter DOB Date above 2005", "error");
                                        }
                                        else
                                        {
                                            $("#myForm").submit();
                                        }
                                    }

                                }
                            });
                        }

                    }

                }
            });

        }

    };

    //    Add Row
    var aboutcheck = function ()
    {
        var fname = $('#fname').val();
        var lname = $('#lname').val();
        var email = $('#email').val();
        var dob = $('#dob').val();

        if (fname == '' || lname == ''|| email == ''|| dob == '' ) {
            swal("Type Again", "Please fill the required fields", "error");
        }
        else {
            var check = checkabout();
        }


    };

    var coverphoto = function ()
    {
        var coverphoto = $('#coverphoto').val();

        if (coverphoto == '') {
            swal("Choose Image", "Please Pick Your Cover Photo", "error");
        }
        else {
            $("#coverForm").submit();
        }
    };

    var sportcheck = function ()
    {
        var sport = $('#choices-multiple-remove-button').val();

        if (sport == '') {
            swal("Select sports", "Please Pick Your Sports", "error");
        }
        else {
            $("#sportForm").submit();
        }
    };

    $(document).ready(function () {
        $('#nav-tab a[href="#{{ old('tab') }}"]').tab('show')
    });

</script>