<script type="text/javascript">

    $('#invite_button_site').prop('disabled', true);
    $('#invite_button_group').prop('disabled', true);
     mobileValidate = 'false';
     mobileValidateGroup = 'false';
     emailValidate = 'false';
     emailValidateGroup = 'false';

//    Mobile Validation ------------------------------------------------------------------------

    $('#mob_').on('input', function()
    {
        var mob_= $('#mob_').val();
        var email_= $('#email_').val();

        if(mob_!='')
        {
            mobileValidation(mob_,email_);
        }
        else if(email_=='' && mob_=='')
        {
            $('#mobile_error_message').html('');
            $('#invite_button_site').prop('disabled', true);
        }
        else if(email_!='')
        {
            emailValidation(email_,mob_);
        }
    });

    function mobileValidation(mob_,email_) {
        $.ajax({
            type: 'POST',
            url: '{{ route('login.checkmobile') }}',
            data: {
                _token: '{{ csrf_token() }}',
                mob: mob_,
                id: ''
            },
            success: function (response) {
                if(response=='repeat')
                {
                    $('#mobile_error_message').html('');
                    $('#mobile_error_message').css('color','darkred');
                    $('#mobile_error_message').append('Phone Number Already Registered.!');
                    $('#invite_button_site').prop('disabled', true);
                    mobileValidate = 'false';
                }
                else if(response=='success')
                {
                    $('#mobile_error_message').html('');
                    $('#invite_button_site').prop('disabled', false);
                    mobileValidate = 'true';

                    if(email_!='' && emailValidate =='false')
                    {
                        emailValidation(email_,mob_);
                    }
                }

            }
        });
    }

//    Email Validation --------------------------------------------------------------------------

    $('#email_').on('input', function()
    {
        var email_= $('#email_').val();
        var mob_= $('#mob_').val();

        if(email_!='')
        {
           emailValidation(email_,mob_);
           console.log('email validation');
        }
        else if(email_=='' && mob_=='')
        {
            $('#email_error_message').html('');
            $('#invite_button_site').prop('disabled', true);
            console.log('both null');
        }
        else if(mob_!='')
        {
            mobileValidation(mob_,email_);
            console.log('mobile validation');
        }
    });

    function emailValidation(email_,mob_)
    {
        if (!validateEmail(email_)) {
            $('#email_error_message').html('');
            $('#email_error_message').css('color', 'darkred');
            $('#email_error_message').append('Please Enter Valid Email.!');
            $('#invite_button_site').prop('disabled', true);
        }
        else {
            $.ajax({
                type: 'POST',
                url: '{{ route('login.checkemail') }}',
                data: {
                    _token: '{{ csrf_token() }}',
                    email: email_,
                    id: ''
                },
                success: function (response) {
                    if (response == 'repeat') {
                        $('#email_error_message').html('');
                        $('#email_error_message').css('color', 'darkred');
                        $('#email_error_message').append('Email Already Registered.!');
                        $('#invite_button_site').prop('disabled', true);
                        emailValidate = 'false';
                    }
                    else if (response == 'success') {
                        $('#email_error_message').html('');
                        $('#invite_button_site').prop('disabled', false);
                        emailValidate = 'true';

                        if(mob_!='' && mobileValidate =='false')
                        {
                            mobileValidation(mob_,email_);
                        }

                    }

                }
            });
        }
    }

    var via_invites = function ()
    {
        $('#mobile_error_message').html('');
        $('#email_error_message').html('');
        console.log('invites');
        $('#invite_button_site').prop('disabled', true);
        mobileValidate = 'false';
        emailValidate = 'false';
        $('#id_to_invite').val('');
        $('#code').val('+1');
        $('#email_').val('');
        $('#mob_').val('');
    };

    var sendinvite = function ()
    {
        var id= $('#id_to_invite').val();
        var mob_= $('#mob_').val();
        var code= $('#code').val();
        var email_= $('#email_').val();
            $.ajax({
                type: 'POST',
                url: '{{ route('user.invites') }}',
                data: {
                    _token: '{{ csrf_token() }}',
                    email_: email_,
                    mob_: mob_,
                    code: code,
                    id: id,
                    url: 'site'
                },
                beforeSend: function() {
                    swal({
                        title: 'Please Wait!',
                        text: 'Sending..',
                        button: false
                    }).then(
                        function () {},
                        // handling the promise rejection
                        function (dismiss) {
                        }
                    )
                },
                success: function (response) {
                    swal("", "Invitation Sent Successfully", "success");
                    via_invites();

                }
            });

    };

    //    ------------------------------------------------------------------------------------------------------------------------------------

    //    Mobile Validation ------------------------------------------------------------------------

    $('#mob_group').on('input', function()
    {
        var mob_group= $('#mob_group').val();
        var email_group= $('#email_group').val();

        if(mob_group!='')
        {
            mobileValidationGroup(mob_group,email_group);
        }
        else if(email_group=='' && mob_group=='')
        {
            $('#mobile_error_message_group').html('');
            $('#invite_button_group').prop('disabled', true);
        }
        else if(email_group!='')
        {
            emailValidationGroup(email_group,mob_group);
        }
    });

    function mobileValidationGroup(mob_group,email_group)
    {
        $('#mobile_error_message_group').html('');
        $('#invite_button_group').prop('disabled', false);
        mobileValidate = 'true';

        if(email_group!='' && emailValidate =='false')
        {
            emailValidationGroup(email_group,mob_group);
        }
    }

    //    Email Validation --------------------------------------------------------------------------

    $('#email_group').on('input', function()
    {
        var email_group= $('#email_group').val();
        var mob_group= $('#mob_group').val();

        if(email_group!='')
        {
            emailValidationGroup(email_group,mob_group);
            console.log('email validation');
        }
        else if(email_group=='' && mob_group=='')
        {
            $('#email_error_message_group').html('');
            $('#invite_button_group').prop('disabled', true);
            console.log('both null');
        }
        else if(mob_group!='')
        {
            mobileValidationGroup(mob_group,email_group);
            console.log('mobile validation');
        }
    });

    function emailValidationGroup(email_group,mob_group)
    {
        if (!validateEmail(email_group)) {
            $('#email_error_message_group').html('');
            $('#email_error_message_group').css('color', 'darkred');
            $('#email_error_message_group').append('Please Enter Valid Email.!');
            $('#invite_button_group').prop('disabled', true);
        }
        else {

            $('#email_error_message_group').html('');
            $('#invite_button_group').prop('disabled', false);
            emailValidate = 'true';

            if(mob_group!='' && mobileValidate =='false')
            {
                mobileValidationGroup(mob_group,email_group);
            }

        }
    }

    var via_invites_groups = function ()
    {
        $('#editGroupModal .btn-close').click();
        $('#invite_button_group').prop('disabled', true);
        $('#mobile_error_message_group').html('');
        $('#email_error_message_group').html('');
        mobileValidateGroup = 'false';
        emailValidateGroup = 'false';
        $('#url_group').val('');
        $('#mob_group').val('');
        $('#email_group').val('');
        $('#code_group').val('+1');
        create_link();
        $('#editGroupModal .show-message').html('');


    };

    function makeid(length) {
        var result           = '';
        var characters       = 'ABCDE345FGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0126789';
        var charactersLength = characters.length;
        for ( var i = 0; i < length; i++ ) {
            result += characters.charAt(Math.floor(Math.random() *
                charactersLength));
        }
        return result;
    }


    var create_link = function ()
    {
        var c_id=makeid(8);
        var urlFinal='{{url('/')}}/'+currentGroup.id+'/{{Auth::user()->user_link}}/register/'+c_id;
        $('#urlInvite').val(urlFinal);
        var level=currentGroup.id;
        $('#level').val(level);
        $('#c_id').val(c_id);
        $('#group_id').val(currentGroup.id);
        $('#group_name').val(currentGroup.name);
        console.log(currentGroup);
        $('#group_image').val(currentGroup.attachment);

    };

    function validateEmail(email) {
        const re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        return re.test(email);
    }


    var sendinviteGroup = function ()
    {
        var id= $('#id_to_invite').val();
        var mob_group= $('#mob_group').val();
        var code_group= $('#code_group').val();
        var email_group= $('#email_group').val();

        var urlInvite= $('#urlInvite').val();
        var level= $('#level').val();
        var c_id= $('#c_id').val();
        var group_id= $('#group_id').val();
        var group_name= $('#group_name').val();
        var group_image= $('#group_image').val();
        $.ajax({
            type: 'POST',
            url: '{{ route('user.invites') }}',
            data: {
                _token: '{{ csrf_token() }}',
                email_: email_group,
                mob_: mob_group,
                id: id,
                c_id: c_id,
                code: code_group,
                urlInvite: urlInvite,
                level: level,
                group_id: group_id,
                group_name: group_name,
                group_image: group_image,
                url:'group'
            },
            beforeSend: function() {
                swal({
                    title: 'Please Wait!',
                    text: 'Sending..',
                    button: false
                }).then(
                    function () {},
                    // handling the promise rejection
                    function (dismiss) {
                    }
                )
            },
            success: function (response) {
                swal("", "Invitation Sent Successfully", "success");
                $('#invite_button_group').prop('disabled', true);
                $('#error_message_group').html('');
                mobileValidateGroup = 'false';
                emailValidateGroup = 'false';
                $('#url_group').val('');
                $('#mob_group').val('');
                $('#email_group').val('');

            }
        });

    }
</script>
