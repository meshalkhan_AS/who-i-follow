<script type="text/javascript">
    {{-- ---------- Upload Post Images ---------- --}}

    function uploadimagemore(isTeamPost = false) {
        // Read selected files
        var formData = new FormData();
        formData.append("_token", '{{ csrf_token() }}');
        if (!isTeamPost) {
            var imageele = document.getElementById('image-post-data');
            var totalfiles = document.getElementById('image-post-data').files.length;
            formData.append("totalfiles", totalfiles);
            jQuery.each(jQuery('#image-post-data')[0].files, function(i, file) {
                formData.append('file-' + i, file);
            });

            if (totalfiles != 0) {
                if (totalfiles > 1) {
                    var title = "Images Uploaded Successfully.!";
                } else {
                    var title = "Image Uploaded Successfully.!";
                }
            }
        } else {
            var totalfiles_ = document.getElementById('image-post-data-edit').files.length;
            formData.append("totalfiles_", totalfiles_);
            jQuery.each(jQuery('#image-post-data-edit')[0].files, function(i, file) {
                formData.append('fileedit-' + i, file);
            });

            if (totalfiles_ != 0) {
                if (totalfiles_ > 1) {
                    var title = "Images Uploaded Successfully.!";
                } else {
                    var title = "Image Uploaded Successfully.!";
                }
            }

        }



        $.ajax({
                url: '{{ route('user.post.uploadimages') }}',
                type: 'POST',
                data: formData,
                dataType: 'json',
                processData: false,
                contentType: false,
                beforeSend: function() {
                    swal({
                        title: 'Please Wait!',
                        text: 'Uploading..',
                        button: false
                    }).then(
                        function() {},
                        // handling the promise rejection
                        function(dismiss) {}
                    )
                }
            })
            .done(function(data) {
                count = data.count;
                if (data.edit == 'yes') {
                    if (data.count > 0) {
                        document.getElementById("div-postimage-edit").style.display = "block";
                        $("#image-data-").append(data.html);
                        $('#edit-post-submit').prop('disabled', false);
                    } else {
                        document.getElementById("div-postimage-edit").style.display = "none";
                        if ($('#captionedit').val() == '') {
                            $('#edit-post-submit').prop('disabled', true);
                        } else {
                            $('#edit-post-submit').prop('disabled', false);
                        }
                    }
                } else {
                    if (data.count > 0) {
                        document.getElementById("div-postimage").style.display = "block";
                        $("#image-data").append(data.html);
                        $('#new-post-submit').prop('disabled', false);
                    } else {
                        document.getElementById("div-postimage").style.display = "none";
                        if ($('#caption').val() == '') {
                            $('#new-post-submit').prop('disabled', true);
                        } else {
                            $('#new-post-submit').prop('disabled', false);
                        }
                    }

                }

                swal({
                    title: 'Uploaded!',
                    text: title,
                    icon: 'success',
                    timer: 1000,
                    button: false
                }).then(function() {});
            })
            .fail(function() {
                swal("", "Try Again", "error");
            });
        document.getElementById("image-post-data").value = null;
    }

    function uploadfeedbackimagemore(isTeamPost = false) {
        // Read selected files
        var formData = new FormData();
        formData.append("_token", '{{ csrf_token() }}');
        if (!isTeamPost) {
            var imageele = document.getElementById('image-feed-data-');
            var totalfiles = document.getElementById('image-feed-data-').files.length;
            formData.append("totalfiles", totalfiles);
            jQuery.each(jQuery('#image-feed-data-')[0].files, function(i, file) {
                formData.append('file-' + i, file);
            });

            if (totalfiles != 0) {
                if (totalfiles > 1) {
                    var title = "Images Uploaded Successfully.!";
                } else {
                    var title = "Image Uploaded Successfully.!";
                }
            }
        } else {
            var totalfiles_ = document.getElementById('image-post-data-edit').files.length;
            formData.append("totalfiles_", totalfiles_);
            jQuery.each(jQuery('#image-post-data-edit')[0].files, function(i, file) {
                formData.append('fileedit-' + i, file);
            });

            if (totalfiles_ != 0) {
                if (totalfiles_ > 1) {
                    var title = "Images Uploaded Successfully.!";
                } else {
                    var title = "Image Uploaded Successfully.!";
                }
            }

        }



        $.ajax({
                url: '{{ route('user.feedback.uploadimages') }}',
                type: 'POST',
                data: formData,
                dataType: 'json',
                processData: false,
                contentType: false,
                beforeSend: function() {
                    swal({
                        title: 'Please Wait!',
                        text: 'Uploading..',
                        button: false
                    }).then(
                        function() {},
                        // handling the promise rejection
                        function(dismiss) {}
                    )
                }
            })
            .done(function(data) {
                count = data.count;
                    if (data.count > 0) {
                        document.getElementById("div-feedimage").style.display = "block";
                        $("#image-data-feed").append(data.html);
                        $('#new-feed-submit').prop('disabled', false);
                    } else {
                        document.getElementById("div-feedimage").style.display = "none";
                        if ($('#caption').val() == '') {
                            $('#new-feed-submit').prop('disabled', true);
                        } else {
                            $('#new-feed-submit').prop('disabled', false);
                        }
                    }

                swal({
                    title: 'Uploaded!',
                    text: title,
                    icon: 'success',
                    timer: 1000,
                    button: false
                }).then(function() {});
            })
            .fail(function() {
                swal("", "Try Again", "error");
            });
        document.getElementById("image-feed-data-").value = null;
    }
 


    {{-- ---------- Delete Post Images ---------- --}}
    var deleteimage = function(id) {
        swal({
            title: "Are you sure?",
            text: "You will not be able to revert this !",
            icon: "warning",
            buttons: [
                'No, cancel it!',
                'Yes, I am sure!'
            ],
            dangerMode: true
        }).then(function(isConfirm) {
            if (isConfirm) {

                $.ajax({
                    type: 'POST',
                    url: '{{ route('user.post.deleteimages') }}',
                    data: {
                        _token: '{{ csrf_token() }}',
                        id: id
                    },

                    success: function(data) {

                        count = data.count;
                        document.getElementById(id).style.display = "none";
                        if (data.edit == 'yes') {
                            if (data.count > 0) {
                                document.getElementById("div-postimage-edit").style.display =
                                    "block";
                                $('#edit-post-submit').prop('disabled', false);
                            } else {
                                document.getElementById("div-postimage-edit").style.display =
                                    "none";
                                if ($('#captionedit').val() == '') {
                                    $('#edit-post-submit').prop('disabled', true);
                                } else {
                                    $('#edit-post-submit').prop('disabled', false);
                                }
                            }
                        } else {
                            if (data.count > 0) {
                                document.getElementById("div-postimage").style.display =
                                "block";
                                $('#new-post-submit').prop('disabled', false);
                            } else {
                                document.getElementById("div-postimage").style.display = "none";
                                if ($('#caption').val() == '') {
                                    $('#new-post-submit').prop('disabled', true);
                                } else {
                                    $('#new-post-submit').prop('disabled', false);
                                }
                            }

                        }
                        swal({
                            title: 'Deleted!',
                            text: 'Image deleted successfully!',
                            icon: 'success',
                            timer: 1000,
                            button: false
                        }).then(function() {});
                    }
                });
            }
        });
    }


    var deleteimagefeed = function(id) {
        swal({
            title: "Are you sure?",
            text: "You will not be able to revert this !",
            icon: "warning",
            buttons: [
                'No, cancel it!',
                'Yes, I am sure!'
            ],
            dangerMode: true
        }).then(function(isConfirm) {
            if (isConfirm) {

                $.ajax({
                    type: 'POST',
                    url: '{{ route('user.feed.deleteimages') }}',
                    data: {
                        _token: '{{ csrf_token() }}',
                        id: id
                    },

                    success: function(data) {

                        count = data.count;
                        document.getElementById(id).style.display = "none";
                        if (data.edit == 'yes') {
                            if (data.count > 0) {
                                document.getElementById("div-postimage-edit").style.display =
                                    "block";
                                $('#edit-post-submit').prop('disabled', false);
                            } else {
                                document.getElementById("div-postimage-edit").style.display =
                                    "none";
                                if ($('#captionedit').val() == '') {
                                    $('#edit-post-submit').prop('disabled', true);
                                } else {
                                    $('#edit-post-submit').prop('disabled', false);
                                }
                            }
                        } else {
                            if (data.count > 0) {
                                document.getElementById("div-postimage").style.display =
                                "block";
                                $('#new-post-submit').prop('disabled', false);
                            } else {
                                document.getElementById("div-postimage").style.display = "none";
                                if ($('#caption').val() == '') {
                                    $('#new-post-submit').prop('disabled', true);
                                } else {
                                    $('#new-post-submit').prop('disabled', false);
                                }
                            }

                        }
                        swal({
                            title: 'Deleted!',
                            text: 'Image deleted successfully!',
                            icon: 'success',
                            timer: 1000,
                            button: false
                        }).then(function() {});
                    }
                });
            }
        });
    }

</script>
