<script type="text/javascript">
    $('#user_link').on('input', function() {
        var newlink = $(this).val();
        $('#updatedlink').html('');
        $('#updatedlink').append(newlink);

        var regExp = /[a-zA-Z]/g;
        var letterNumber = /^[0-9a-zA-Z]+$/;

        if (!regExp.test(newlink)) {
            $('#error_message').html('');
            $('#error_message').css('color', 'darkred');
            $('#error_message').append('Username must contain alphabet.');
            $("#submit-linkform").prop('disabled', true);
        } else if (newlink.indexOf(' ') >= 0) {
            $('#error_message').html('');
            $('#error_message').css('color', 'darkred');
            $('#error_message').append('Username must not contain whitespace.');
            $("#submit-linkform").prop('disabled', true);
        } else if (!newlink.match(letterNumber)) {
            $('#error_message').html('');
            $('#error_message').css('color', 'darkred');
            $('#error_message').append('Username must not contain special characters.');
            $("#submit-linkform").prop('disabled', true);
        } else if (newlink.length < 5) {
            $('#error_message').html('');
            $('#error_message').css('color', 'darkred');
            $('#error_message').append('Username must be atleast 5 characters.');
            $("#submit-linkform").prop('disabled', true);

        } else if (newlink.match(/\d/g).length < 4) {
            $('#error_message').html('');
            $('#error_message').css('color', 'darkred');
            $('#error_message').append('Username must contain atleast 4 numbers.');
            $("#submit-linkform").prop('disabled', true);
        } else {
            checkvalidation(newlink);
        }
    });

    function checkvalidation(newlink) {
        $.ajax({
            type: 'POST',
            url: '{{ route('login.checkuserlink') }}',
            data: {
                _token: '{{ csrf_token() }}',
                newlink: newlink,
                id: ''
            },
            success: function(response) {
                if (response == 'repeat') {
                    $('#error_message').html('');
                    $('#error_message').css('color', 'darkred');
                    $('#error_message').append('Username is not available.');
                    $("#submit-linkform").prop('disabled', true);

                } else if (response == 'success') {
                    $('#error_message').html('');
                    $('#error_message').css('color', 'green');
                    $('#error_message').append('Username is available.');
                    $("#submit-linkform").prop('disabled', true);
                    $("#submit-linkform").prop('disabled', false);

                }
            }
        });

    }
    var validationcheckURL = function ()
    {
        var url = $('#Url').val();
        
        if (url=='') {
            swal("Type Again", "Please fill the required fields", "error");
        }
        else if (url!='') {
            if(isValidURL(url)==true){
            console.log(isValidURL(url));
            $("#profilelinkFormurl").submit();
        }
        else{
            swal("Type Again", "Please Enter Correct URL", "error");
        }      
        }
        else{
            $("#profilelinkFormurl").submit();
        }

    };
    function isValidURL(string) {
  var res = string.match(/(http(s)?:\/\/.)?(www\.)?[-a-zA-Z0-9@:%._\+~#=]{2,256}\.[a-z]{2,6}\b([-a-zA-Z0-9@:%_\+.~#?&//=]*)/g);
  return (res !== null)
};
    function viewuserlink() {

        $('#error_message').html('');
        $("#submit-linkform").prop('disabled', true);
        var userlink = "{{ Auth::user()->user_link }}";
        $('#user_link').val(userlink);
        $('#updatedlink').html('');
        $('#updatedlink').append(userlink);
    }
    async function viewuserweburl(id) {
        $('#editUrlID').val(id);
        $.ajax({
            type: 'GET',
            url: '/who-i-follow/weburl/edit/'+id,
            success: function(response) {
         $('#Url').val(response.data.urls);
         
            }
        });
    }

    function submitformlink() {
        swal({
            title: "Are you sure to continue.?",
            text: "You will only be allowed to edit once.",
            icon: "warning",
            buttons: [
                'No, cancel it!',
                'Yes, I am sure!'
            ],
            dangerMode: true
        }).then(function(isConfirm) {
            if (isConfirm) {
                $("#profilelinkForm").submit();
            } else {
                swal("Cancelled", "", "error");
            }
        });

    }

</script>
