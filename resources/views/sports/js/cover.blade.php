<script>
    var coverpicURL =function(input) {
        swal({
            title: 'Uploading!',
            text: 'Cover photo uploaded successfully!',
            icon: 'success',
            timer: 2000,
            button: false
        }).then(function() {
        });
        if (input.files && input.files[0]) {
            var reader = new FileReader();

            reader.onload = function (e) {
                $('#id-cover_photo').attr('src' , e.target.result);
            };

            reader.readAsDataURL(input.files[0]);
        }
        document.getElementById("update-cancel-cover-photo").style.display = "block";
        document.getElementById("cover-update").style.display = "block";
        document.getElementById("cover-cancel").style.display = "block";
    };
</script>

<script type="text/javascript">

    var coverphoto = function ()
    {
        var coverphoto = $('#coverphoto').val();

        if (coverphoto == '') {
            swal("Choose Image", "Please Pick Your Cover Photo", "error");
        }
        else {
            $("#coverForm").submit();
        }
    };

    var removecoverphoto = function ()
    {
        swal({
            title: "Are you sure",
            text: "You want to remove cover photo",
            icon: "warning",
            buttons: [
                'No, cancel it!',
                'Yes, I am sure!'
            ],
            dangerMode: true,
        }).then(function(isConfirm) {
            if (isConfirm) {

                $.ajax({
                    type: 'POST',
                    url: '{{ route('user.cover.remove') }}',
                    data: {
                        _token: '{{ csrf_token() }}'

                    },

                    success: function (response)
                    {
                        swal({
                            title: 'Removed!',
                            text: 'Cover photo removed successfully!',
                            icon: 'success',
                            timer: 2000,
                            button: false,
                        }).then(function() {
                        });
                        location.reload();
                    }
                });
            } else {
                swal("Cancelled","Cover photo is saved", "error");
            }
        });


    };

    var covercancel = function ()
    {
        location.reload();
    };
</script>