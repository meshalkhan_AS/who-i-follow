<script type="text/javascript">
    var page = 1;success=1;
    $(window).scroll(function() {
        if($(window).scrollTop() >= $(document).height() - $(window).height() - 10) {
            if(success==1) {
                page++;
            }
        loadMoreData(page);
        }
    });
    function loadMoreData(page){
        $.ajax(
            {
                url: '?page=' + page,
                type: "get",
                data: {
                    "_token": "{{ csrf_token() }}",
                    "search": $("#search").val()
                },
                beforeSend: function()
                {
                    $('.ajax-load').show();
                }
            })
            .done(function(data)
            {
                if(data.html == ""){
                    success=0;
                    $('.ajax-load').html("No more records found..<br/><br/><br/><br/><br/><br/>");
                    return;
                }
                    $('.ajax-load').hide();
                    $("#profile-data").append(data.html);
                    success=1;
            })
            .fail(function(jqXHR, ajaxOptions, thrownError)
            {
                success=0;
            });
    }
</script>