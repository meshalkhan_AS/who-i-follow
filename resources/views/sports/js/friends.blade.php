<script type="text/javascript">

    var openedTab= sessionStorage.getItem("openedTab");
    console.log(openedTab);
    first=1;
    @if($request_=='no')
    $(document).ready(function () {

        if(openedTab==1 || openedTab==null)
        {
            myfriends();
        }
        else if (openedTab==2)
        {
            @if($reqSentCount>0 || count($invites)>0)
                sentrequest();
            @else
                myfriends();
            @endif
        }
        else if (openedTab==3)
        {

            @if($reqReveivedCount>0)
                receivedrequest();
            @else
                myfriends();
            @endif
        }

    });
    @else
        @if($reqReveivedCount>0)
        receivedrequest();
        @else
        $(document).ready(function () {
            myfriends();
        });
        @endif
    @endif

    function myfriends()
    {
        openedTab=1;
        filter('1');
        $(".chk-1").addClass("active-list");
        $(".chk-2").removeClass("active-list");
        $(".chk-3").removeClass("active-list");
        $(".invitesDiv").css('display','none');
        sessionStorage.setItem("openedTab",1);
    }

    function sentrequest()
    {
        filter('2');
        $(".chk-2").addClass("active-list");
        $(".chk-1").removeClass("active-list");
        $(".chk-3").removeClass("active-list");
        $(".invitesDiv").css('display','block');
        sessionStorage.setItem("openedTab",2);
        openedTab=2;
    }
    function receivedrequest()
    {
        filter('3');
        $(".chk-3").addClass("active-list");
        $(".chk-1").removeClass("active-list");
        $(".chk-2").removeClass("active-list");
        $(".invitesDiv").css('display','none');
        sessionStorage.setItem("openedTab",3);
        openedTab=3;
    }
    
    function resendInvitation(id) {
        $.ajax({
            type: 'GET',
            url: '{{ route('user.re_invites') }}',
            data: {
                _token: '{{ csrf_token() }}',
                id: id

            },
            beforeSend: function() {
                swal({
                    title: 'Please Wait!',
                    text: 'Sending..',
                    button: false
                }).then(
                    function () {},
                    // handling the promise rejection
                    function (dismiss) {
                    }
                )
            },
            success: function (response) {
                swal("", "Invitation Sent Successfully", "success");
            }
        });
        
    }

    function filter(filterID){

        if(filterID!='1' && first==1)
        {
            first=0;
        }

        $.ajax(
            {
                url: '?',
                type: "get",
                data: {
                    "_token": "{{ csrf_token() }}",
                    "filterID": filterID
                },
                beforeSend: function () {

                    if(first==0)
                    {
                        swal({
                            title: 'Please Wait!',
                            button: false
                        }).then(
                            function () {},
                            // handling the promise rejection
                            function (dismiss) {
                            }
                        );
                        $("#post-data").html('');
                    }

                }
            })
            .done(function(data)
            {
                if(first==0) {
                    swal({
                        title: 'Please Wait!',
                        button: false,
                        timer: 100
                    });
                }
                $("#post-data").html('');
                if(data.html == "")
                {
                    $("#no-record").show();
                }
                else
                {
                    $("#post-data").append(data.html);
                    $("#no-record").hide();
                }
                $("#no-record1").hide();
                if(openedTab==2)
                {
                    var count='{{count($invites)}}';
                    if(count>0)
                    {
                        $("#no-record").hide();
                    }
                }
            })
    }
</script>