<script src="//js.pusher.com/3.1/pusher.min.js"></script>
<script src="//maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>

<script type="text/javascript">

    $(document).ready(function ()
    {
        setInterval(function(){ sendSMS();  },20000);
        $(".timeago").timeago();
        $("#prog_date").text($.timeago(new Date()));

        const BASE_URL = "{{ url('/') }}";
        var platform="{{env('NOTIFICATION_PLATFORM')}}";
        var pusher = new Pusher('aecd40330890cfb9b8fe', {
            cluster: 'ap4'
        });

        var channel = pusher.subscribe('notification');
            channel.bind('App\\Events\\sentNotification', function (data)
            {
                    var id = <?php echo json_encode(\Auth::user()->id); ?>;
                    if (data['user_id'] == id && platform==data['platform'] )
                    {
                        var message = data['message'];
                        var username = data['username'];
                        var $url = data['url'];
                        var    notification_id= data['notification_id'];
                        const counter = parseInt(data['counter']);
                        var send_by = data['send_by'];
                        var send_by_id = data['send_by_id'];
                        const previous=parseInt(document.getElementById("count-input").value);
                        const count = counter + previous;
                        const route_followed = BASE_URL+'/profile/'+send_by;

                        if(data['profile_exists']=='yes')
                        {
                            var profile_url = BASE_URL+'/public/storage/users/'+send_by_id+'/profile_picture.jpg';
                        }
                        else
                        {
                            var profile_url = BASE_URL+'/public/uploads/white.jpg';
                        }

                        var notificationDate = moment(data['time'], 'YYYY-MM-DD[T]HH:mm:ss').format("YYYY-MM-DD[T]HH:mm:ss");
//                        console.log(notificationDate);
                        var notificationTime = moment(notificationDate).fromNow();

                        var list = '<a href="'+$url+'" class="pointer-cursor" style="display: block;" onclick="readNotification('+notification_id+')"><div class="notfication-details" style="background-color: #f3f3f3">\n' +
                            '                                    <div class="noty-user-img">\n' +
                            '                                        <img src="'+profile_url+'" alt="">\n' +
                            '                                    </div>\n' +
                            '                                    <div class="notification-info">\n' +
                            '                                        <h3><text style="font-weight: bold">'+username+'</text>\n' +
                            '                                            <text style="font-weight: normal" onclick="readNotification('+notification_id+')">' + message + '</text></h3>\n' +
                            '                                        <p> about '+notificationTime+'</p>\n' +
                            '                                    </div>\n' +
                            '                                </div></a>';

                        var existing_lists= $("#notification-lists").html();
                        $("#notification-lists").html('');
                        $("#notification-lists").append(list+existing_lists);

                         var existing_list= $("#notification-list").html();
                        $("#notification-list").html('');
                        $("#notification-list").append(list+existing_list);

                        var existing= $("#notification-data").html();
                        $("#notification-data").html('');
                        $("#notification-data").append(list+existing);

                        $(".count-notification").html('');
                        $(".count-notification").show();
                        $(".count-notification").append(count);
                        $("#count-input").val(count);
                        var snd = new  Audio("data:audio/wav;base64,//uQRAAAAWMSLwUIYAAsYkXgoQwAEaYLWfkWgAI0wWs/ItAAAGDgYtAgAyN+QWaAAihwMWm4G8QQRDiMcCBcH3Cc+CDv/7xA4Tvh9Rz/y8QADBwMWgQAZG/ILNAARQ4GLTcDeIIIhxGOBAuD7hOfBB3/94gcJ3w+o5/5eIAIAAAVwWgQAVQ2ORaIQwEMAJiDg95G4nQL7mQVWI6GwRcfsZAcsKkJvxgxEjzFUgfHoSQ9Qq7KNwqHwuB13MA4a1q/DmBrHgPcmjiGoh//EwC5nGPEmS4RcfkVKOhJf+WOgoxJclFz3kgn//dBA+ya1GhurNn8zb//9NNutNuhz31f////9vt///z+IdAEAAAK4LQIAKobHItEIYCGAExBwe8jcToF9zIKrEdDYIuP2MgOWFSE34wYiR5iqQPj0JIeoVdlG4VD4XA67mAcNa1fhzA1jwHuTRxDUQ//iYBczjHiTJcIuPyKlHQkv/LHQUYkuSi57yQT//uggfZNajQ3Vmz+Zt//+mm3Wm3Q576v////+32///5/EOgAAADVghQAAAAA//uQZAUAB1WI0PZugAAAAAoQwAAAEk3nRd2qAAAAACiDgAAAAAAABCqEEQRLCgwpBGMlJkIz8jKhGvj4k6jzRnqasNKIeoh5gI7BJaC1A1AoNBjJgbyApVS4IDlZgDU5WUAxEKDNmmALHzZp0Fkz1FMTmGFl1FMEyodIavcCAUHDWrKAIA4aa2oCgILEBupZgHvAhEBcZ6joQBxS76AgccrFlczBvKLC0QI2cBoCFvfTDAo7eoOQInqDPBtvrDEZBNYN5xwNwxQRfw8ZQ5wQVLvO8OYU+mHvFLlDh05Mdg7BT6YrRPpCBznMB2r//xKJjyyOh+cImr2/4doscwD6neZjuZR4AgAABYAAAABy1xcdQtxYBYYZdifkUDgzzXaXn98Z0oi9ILU5mBjFANmRwlVJ3/6jYDAmxaiDG3/6xjQQCCKkRb/6kg/wW+kSJ5//rLobkLSiKmqP/0ikJuDaSaSf/6JiLYLEYnW/+kXg1WRVJL/9EmQ1YZIsv/6Qzwy5qk7/+tEU0nkls3/zIUMPKNX/6yZLf+kFgAfgGyLFAUwY//uQZAUABcd5UiNPVXAAAApAAAAAE0VZQKw9ISAAACgAAAAAVQIygIElVrFkBS+Jhi+EAuu+lKAkYUEIsmEAEoMeDmCETMvfSHTGkF5RWH7kz/ESHWPAq/kcCRhqBtMdokPdM7vil7RG98A2sc7zO6ZvTdM7pmOUAZTnJW+NXxqmd41dqJ6mLTXxrPpnV8avaIf5SvL7pndPvPpndJR9Kuu8fePvuiuhorgWjp7Mf/PRjxcFCPDkW31srioCExivv9lcwKEaHsf/7ow2Fl1T/9RkXgEhYElAoCLFtMArxwivDJJ+bR1HTKJdlEoTELCIqgEwVGSQ+hIm0NbK8WXcTEI0UPoa2NbG4y2K00JEWbZavJXkYaqo9CRHS55FcZTjKEk3NKoCYUnSQ0rWxrZbFKbKIhOKPZe1cJKzZSaQrIyULHDZmV5K4xySsDRKWOruanGtjLJXFEmwaIbDLX0hIPBUQPVFVkQkDoUNfSoDgQGKPekoxeGzA4DUvnn4bxzcZrtJyipKfPNy5w+9lnXwgqsiyHNeSVpemw4bWb9psYeq//uQZBoABQt4yMVxYAIAAAkQoAAAHvYpL5m6AAgAACXDAAAAD59jblTirQe9upFsmZbpMudy7Lz1X1DYsxOOSWpfPqNX2WqktK0DMvuGwlbNj44TleLPQ+Gsfb+GOWOKJoIrWb3cIMeeON6lz2umTqMXV8Mj30yWPpjoSa9ujK8SyeJP5y5mOW1D6hvLepeveEAEDo0mgCRClOEgANv3B9a6fikgUSu/DmAMATrGx7nng5p5iimPNZsfQLYB2sDLIkzRKZOHGAaUyDcpFBSLG9MCQALgAIgQs2YunOszLSAyQYPVC2YdGGeHD2dTdJk1pAHGAWDjnkcLKFymS3RQZTInzySoBwMG0QueC3gMsCEYxUqlrcxK6k1LQQcsmyYeQPdC2YfuGPASCBkcVMQQqpVJshui1tkXQJQV0OXGAZMXSOEEBRirXbVRQW7ugq7IM7rPWSZyDlM3IuNEkxzCOJ0ny2ThNkyRai1b6ev//3dzNGzNb//4uAvHT5sURcZCFcuKLhOFs8mLAAEAt4UWAAIABAAAAAB4qbHo0tIjVkUU//uQZAwABfSFz3ZqQAAAAAngwAAAE1HjMp2qAAAAACZDgAAAD5UkTE1UgZEUExqYynN1qZvqIOREEFmBcJQkwdxiFtw0qEOkGYfRDifBui9MQg4QAHAqWtAWHoCxu1Yf4VfWLPIM2mHDFsbQEVGwyqQoQcwnfHeIkNt9YnkiaS1oizycqJrx4KOQjahZxWbcZgztj2c49nKmkId44S71j0c8eV9yDK6uPRzx5X18eDvjvQ6yKo9ZSS6l//8elePK/Lf//IInrOF/FvDoADYAGBMGb7FtErm5MXMlmPAJQVgWta7Zx2go+8xJ0UiCb8LHHdftWyLJE0QIAIsI+UbXu67dZMjmgDGCGl1H+vpF4NSDckSIkk7Vd+sxEhBQMRU8j/12UIRhzSaUdQ+rQU5kGeFxm+hb1oh6pWWmv3uvmReDl0UnvtapVaIzo1jZbf/pD6ElLqSX+rUmOQNpJFa/r+sa4e/pBlAABoAAAAA3CUgShLdGIxsY7AUABPRrgCABdDuQ5GC7DqPQCgbbJUAoRSUj+NIEig0YfyWUho1VBBBA//uQZB4ABZx5zfMakeAAAAmwAAAAF5F3P0w9GtAAACfAAAAAwLhMDmAYWMgVEG1U0FIGCBgXBXAtfMH10000EEEEEECUBYln03TTTdNBDZopopYvrTTdNa325mImNg3TTPV9q3pmY0xoO6bv3r00y+IDGid/9aaaZTGMuj9mpu9Mpio1dXrr5HERTZSmqU36A3CumzN/9Robv/Xx4v9ijkSRSNLQhAWumap82WRSBUqXStV/YcS+XVLnSS+WLDroqArFkMEsAS+eWmrUzrO0oEmE40RlMZ5+ODIkAyKAGUwZ3mVKmcamcJnMW26MRPgUw6j+LkhyHGVGYjSUUKNpuJUQoOIAyDvEyG8S5yfK6dhZc0Tx1KI/gviKL6qvvFs1+bWtaz58uUNnryq6kt5RzOCkPWlVqVX2a/EEBUdU1KrXLf40GoiiFXK///qpoiDXrOgqDR38JB0bw7SoL+ZB9o1RCkQjQ2CBYZKd/+VJxZRRZlqSkKiws0WFxUyCwsKiMy7hUVFhIaCrNQsKkTIsLivwKKigsj8XYlwt/WKi2N4d//uQRCSAAjURNIHpMZBGYiaQPSYyAAABLAAAAAAAACWAAAAApUF/Mg+0aohSIRobBAsMlO//Kk4soosy1JSFRYWaLC4qZBYWFRGZdwqKiwkNBVmoWFSJkWFxX4FFRQWR+LsS4W/rFRb/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////VEFHAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAU291bmRib3kuZGUAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAMjAwNGh0dHA6Ly93d3cuc291bmRib3kuZGUAAAAAAAAAACU=");
                        snd.play();
                    }
            });
    });


    var readNotification= function(id)
    {
        $.ajax({
            type: 'POST',
            url: '{{ route('user.notification.read') }}',
            data: {
                _token: '{{ csrf_token() }}',
                id: id
            },
            success: function (response) {
            }
        });
    };

    var checkedNotification= function()
    {
        $(".count-notification").html('');
        $(".count-notification").hide();

        $.ajax({
            type: 'POST',
            url: '{{ route('user.notification.checked') }}',
            data: {
                _token: '{{ csrf_token() }}'
            },
            success: function (response) {
            }
        });
    };

    var sendSMS= function()
    {
        $.ajax({
            type: 'POST',
            url: '{{ route('user.sms.sendSMS') }}',
            data: {
                _token: '{{ csrf_token() }}'
            },
            success: function (response) {
            }
        });
    }
</script>

<script type="text/javascript">

    var page = 1;success=1; loaded=0;
    $(window).scroll(function() {
        if($(window).scrollTop() >= $(document).height() - $(window).height() - 100) {
            if(success==1)
                page++;
            if(loaded==0)
                loadMoreData(page);
        }
    });

    function loadMoreData(page)
    {
        $.ajax(
            {
                url: '?page=' + page,
                type: "get",
                beforeSend: function()
                {$('.ajax-load').show();}
            })
            .done(function(data)
            {
                if(data.count == 0)
                {
                    loaded=1;
                    if(page!=2)
                        $('.ajax-load').html("No more records found..<br/><br/><br/><br/><br/><br/>");
                    else
                        $('.ajax-load').hide();
                    return;
                }
                $('.ajax-load').hide();
                $("#notification-data").append(data.html);
                success=1;
            })
            .fail(function(jqXHR, ajaxOptions, thrownError)
            { success=0;});
    }
</script>