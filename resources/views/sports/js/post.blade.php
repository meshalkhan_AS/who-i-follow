<script type="text/javascript">

    var count=0;
    {{-------Add Post Validation -----------}}
    $(document).ready(function () {

        $('#caption').on('input change', function () {
            if ($(this).val() != '') {
                   $('#new-post-submit').prop('disabled', false);
            }
            else {
                if(count==0)
                {
                    $('#new-post-submit').prop('disabled', true);
                }
            }
        });
    });
        $(document).ready(function () {
        $('#captionedit').on('input change', function () {
            if ($(this).val() != '') {
                $('#edit-post-submit').prop('disabled', false);
            }
            else {
                if(count==0)
                {
                    $('#edit-post-submit').prop('disabled', true);
                }
            }
        });
    });

    {{---------Submit Post ----------}}
    var newPost = function ()
    {
        var title = $('#title').val();
        var caption = $('#caption').val();
        $('#new-post-submit').prop('disabled', true);
        $("#post").submit();
    };

    {{---------- Fetch Post data ----------}}
    var fetchdata = function (id)
    {
        refreshdata();
        $.ajax({
            type: 'POST',
            url: '{{ route('user.post.fetchdata') }}',
            data: {
                _token: '{{ csrf_token() }}',
                id: id
            },

            success: function (response) {
                $('#titleedit').val(response.post[0].title);
                $('#captionedit').val(response.post[0].caption);
                $('#idedit').val(response.post[0].id);
                $('#editImg').html('');
                count=response.count;

                if(response.edit=='yes')
                {
                    if(response.count>0)
                    {
                        document.getElementById("div-postimage-edit").style.display="block";
                        $('#edit-post-submit').prop('disabled', false);
                    }
                    else
                    {
                        document.getElementById("div-postimage-edit").style.display="none";
                        if($('#captionedit').val()=='')
                        {
                            $('#edit-post-submit').prop('disabled', true);
                        }
                        else
                        {
                            $('#edit-post-submit').prop('disabled', false);
                        }
                    }
                }
                else
                {
                    if(response.count>0)
                    {
                        document.getElementById("div-postimage").style.display="block";
                        $('#new-post-submit').prop('disabled', false);
                    }
                    else
                    {
                        document.getElementById("div-postimage").style.display="none";
                        if($('#caption').val()=='')
                        {
                            $('#new-post-submit').prop('disabled', true);
                        }
                        else
                        {
                            $('#new-post-submit').prop('disabled', false);
                        }
                    }

                }

                var html='';
                var base_url = window.location.origin;
                for(i = 0; i < response.post_image.length; i++)
                {
                    html+=' <div class="post-image-pop"  id="' + response.post_image[i].image + '">\n' +
                        '        <img src="'+base_url+'/public/storage/posts/' + response.post_image[i].image + '" class="image-post-"/>\n' +
                        '        <i class="btn btn-danger fa fa-trash image-post-delete"  onclick="deleteimage(\'' + response.post_image[i].image + '\')"></i>\n' +
                        '    </div>';
                }
                $("#image-data-").append(html);
            }
        });
    };

    {{---------- Refresh Post data ----------}}
    var refreshdata = function ()
    {
        $("#title").val('');
        $("#caption").val('');
        $("#myImg").html('');
        $('#div-postimage').hide();
        $('#div-postimage-edit').hide();
        $("#image-data").html('');
        $("#image-data-").html('');
        document.getElementById("image-post-data").value=null;
        $.ajax({
            type: 'POST',
            url: '{{ route('user.post.clearimages') }}',
            data: {
                _token: '{{ csrf_token() }}'
            },

            success: function (response) {
            }
        });

    };

    {{------------ Update Post ------------}}
    var postCheck = function ()
    {

        $("#updatePost").submit();
    };

    {{------------ Delete Post ------------}}
    var deletedata = function (id)
    {
        swal({
            title: "Are you sure?",
            text: "You will not be able to revert this !",
            icon: "warning",
            buttons: [
                'No, cancel it!',
                'Yes, I am sure!'
            ],
            dangerMode: true
        }).then(function(isConfirm) {
            if (isConfirm) {

                $.ajax({
                    type: 'POST',
                    url: '{{ route('user.post.delete') }}',
                    data: {
                        _token: '{{ csrf_token() }}',
                        id: id
                    },

                    success: function (response) {
                        swal({
                            title: 'Deleted!',
                            text: 'Post is successfully deleted!',
                            icon: 'success',
                            timer: 3000,
                            button: false
                        }).then(function() {
                        });
                        location.reload();
                    }
                });
            } else {
                swal({
                    title: 'Cancelled',
                    text: 'Your post is safe',
                    icon: 'error',
                    timer: 3000,
                    button: false
                }).then(function() {
                });
            }
        });
    };
</script>

    {{------------ Ajax Pagination Post ------------}}
<script type="text/javascript">
    var page = 1;success=1; load=0;
    $(window).scroll(function() {
        if($(window).scrollTop() >= $(document).height() - $(window).height() - 100) {
            if(success==1)
                page++;
            if(load==0)
                loadMoreData(page);
        }
    });

    function loadMoreData(page){
        $.ajax(
            {
                url: '?page=' + page,
                type: "get",
                beforeSend: function()
                {$('.ajax-load').show();}
            })
            .done(function(data)
            {
                if(data.count == 0)
                {
                    load=1;
                    if(page!=2)
                        $('.ajax-load').html("No more records found..<br/><br/><br/><br/><br/><br/>");
                    else
                        $('.ajax-load').hide();


                    return;
                }
                $('.ajax-load').hide();
                $("#post-data").append(data.html);
                success=1;
            })
            .fail(function(jqXHR, ajaxOptions, thrownError)
            { success=0;});
    }
</script>
