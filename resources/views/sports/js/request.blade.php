<script type="text/javascript">

    {{------------ Accept Request ------------}}
    function acceptrequest(id,friend_id)
    {
        swal({
            title: "Are you sure to Accept the Request?",
            icon: "warning",
            buttons: [
                'No, cancel it!',
                'Yes, I am sure!'
            ],
            dangerMode: true
        }).then(function(isConfirm) {
            if (isConfirm) {

                $.ajax({
                    type: 'POST',
                    url: '{{ route('user.acceptrequest') }}',
                    data: {
                        _token: '{{ csrf_token() }}',
                        id: id,
                        friend_id: friend_id
                    },

                    success: function (response)
                    {
                        swal({
                            title: 'Accept Request!',
                            text: 'Request Accepted Successfully!',
                            icon: 'success',
                            timer: 2000,
                            button: false
                        }).then(function() {
                        });
                        location.reload();
                    }
                });
            } else {
                swal("Cancelled","", "error");
            }
        });
    }

    {{------------ Reject Request ------------}}
    function rejectrequest(id,friend_id)
    {
        swal({
            title: "Are you sure to Reject the Request?",
            icon: "warning",
            buttons: [
                'No, cancel it!',
                'Yes, I am sure!'
            ],
            dangerMode: true
        }).then(function(isConfirm) {
            if (isConfirm) {

                $.ajax({
                    type: 'POST',
                    url: '{{ route('user.rejectrequest') }}',
                    data: {
                        _token: '{{ csrf_token() }}',
                        id: id,
                        friend_id: friend_id
                    },

                    success: function (response)
                    {
                        swal({
                            title: 'Reject Request!',
                            text: 'Request Rejected Successfully!',
                            icon: 'success',
                            timer: 2000,
                            button: false
                        }).then(function() {
                        });
                        location.reload();
                    }
                });
            } else {
                swal("Cancelled","", "error");
            }
        });
    }

</script>
