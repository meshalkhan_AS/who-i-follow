<script>

    {{--------- Cover Photo Zoom --------}}

    function cover_photo_zoom ()
    {
        document.body.classList.add("remove-scrolling");

        // Get modal
        var modalcover = document.getElementById('myModal');
        var modalImg = document.getElementById("img01");

        // Get Cover ID
        var imgSrc = document.getElementById("id-cover_photo");
        modalcover.style.display = "block";
        modalImg.src = imgSrc.src;

        var spancover = document.getElementsByClassName("close")[0];
        spancover.onclick = function() {
            document.body.classList.remove("remove-scrolling");
            modalcover.style.display = "none";
        };
    }

    {{--------- Profile Photo Zoom --------}}

    function profile_photo_zoom ()
    {
        document.body.classList.add("remove-scrolling");

        // Get modal
        var modalprofile = document.getElementById('myModal');
        var modalImg = document.getElementById("img01");

        // Get profile ID
        var imgSrc = document.getElementById("profile-photo");
        modalprofile.style.display = "block";
        modalImg.src = imgSrc.src;

        var spanprofile = document.getElementsByClassName("close")[0];
        spanprofile.onclick = function() {
            document.body.classList.remove("remove-scrolling");
            modalprofile.style.display = "none";
        };
    }

    {{-------Cover Photo Alert -----------}}

    function addRemoveCoverPhoto() {
        document.getElementById("dropdown-content").classList.toggle("show");
    }

    {{-------Profile Photo Alert -----------}}

    function addRemoveProfilePhoto() {
        document.getElementById("profilephoto-dropdown-content").classList.toggle("show");
    }

    {{-------Cover Photo Dropdown -----------}}

        window.onclick = function(event) {
        if (!event.target.matches('.dropdown-open')) {
            var dropdowns = document.getElementsByClassName("add-remove-cover-photo");
            var i;
            for (i = 0; i < dropdowns.length; i++) {
                var openDropdown = dropdowns[i];
            }
        }
    };
</script>