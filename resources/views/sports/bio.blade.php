<!DOCTYPE html>
<html>

@include('sports.head')

<body class="main-body">
    <style>
       select.round {
  background-image:
    linear-gradient(45deg, transparent 50%, gray 50%),
    linear-gradient(135deg, gray 50%, transparent 50%),
    radial-gradient(#ddd 70%, transparent 72%);
  background-position:
    calc(100% - 20px) calc(1em + 2px),
    calc(100% - 15px) calc(1em + 2px),
    calc(100% - .5em) .5em;
  background-size:
    5px 5px,
    5px 5px,
    1.5em 1.5em;
  background-repeat: no-repeat;
}

select.round:focus {
  background-image:
    linear-gradient(45deg, white 50%, transparent 50%),
    linear-gradient(135deg, transparent 50%, white 50%),
    radial-gradient(gray 70%, transparent 72%);
  background-position:
    calc(100% - 15px) 1em,
    calc(100% - 20px) 1em,
    calc(100% - .5em) .5em;
  background-size:
    5px 5px,
    5px 5px,
    1.5em 1.5em;
  background-repeat: no-repeat;
  border-color: green;
  outline: 0;
}
#newurl{
  display:none;
  margin-top: 15px;
}

    </style>
    <div class="wrapper">
        <div class="container">
        </div>
        @include('sports.head_main')
        <div class="wrapper">
            <section class="profile-account-setting">
                <div class="container-fluid">
                    <div class="account-tabs-setting">
                        <div class="row">
                            <div class="col-lg-1"></div>
                            <div class="col-lg-10 col-md-12 col-sm-12 col-12 mb-4 no-pdd">
                                <div class="acc-leftbar">
                                    <div class="nav nav-tabs" id="nav-tab" role="tablist">
                                        <a class="col-lg-3 col-md-4 col-sm-12 col-12 no-pdd nav-item nav-link {{ $active == 'bio' ? 'active ' : '' }}"
                                            id="nav-acc-tab" data-toggle="tab" href="#nav-acc" role="tab"
                                            aria-controls="nav-acc" aria-selected="true"><i
                                                class="fa fa-info-circle text-dark"></i>Contact & Basic Info</a>
                                        <a class="col-lg-3 col-md-3 col-sm-12 col-12 no-pdd nav-item nav-link {{ $active == 'about' ? 'active' : '' }}"
                                            id="nav-status-tab" data-toggle="tab" href="#nav-status" role="tab"
                                            aria-controls="nav-status" aria-selected="false"><i
                                                class="fa fa-address-book-o text-dark"></i>About You</a>
                                        <a class="col-lg-3 col-md-3 col-sm-12 col-12 no-pdd nav-item nav-link {{ $active == 'request' ? 'active' : '' }}"
                                            id="nav-privcy-tab" data-toggle="tab" href="#privcy" role="tab"
                                            aria-controls="privacy" aria-selected="false"><i
                                                class="fa fa-user-circle-o text-dark"></i>Friend Requests</a>
                                        <a class="col-lg-3 col-md-4 col-sm-12 col-12 no-pdd nav-item nav-link {{ $active == 'defaulttab' ? 'active' : '' }}"
                                            id="nav-page-tab" data-toggle="tab" href="#page" role="tab"
                                            aria-controls="page" aria-selected="false"><i
                                                class="fa fa-list-alt text-dark"></i>Page Configuration</a>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-1"></div>
                            <div class="col-lg-1"></div>
                            <div class="col-lg-10 col-md-12 col-sm-12 col-12">
                                <div class="notification-measage">
                                    @if ($message = Session::get('defaulttab'))
                                    <div id="mydiv">
                                        <div class="alert alert-success alert-dismissible fade show" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </div>
                                    </div>
                                @endif
                                    @if ($message = Session::get('about'))
                                        <div id="mydiv">
                                            <div class="alert alert-success alert-dismissible fade show" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </div>
                                        </div>
                                    @endif
                                    @if ($message = Session::get('bio'))
                                        <div id="mydiv">
                                            <div class="alert alert-success alert-dismissible fade show" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </div>
                                        </div>
                                    @endif
                                </div>
                            </div>
                            <div class="col-lg-1"></div>
                            <div class="col-lg-1"></div>

                            <div class="col-lg-10 col-md-12 col-sm-12 col-12 no-pdd">
                                <div class="tab-content" id="nav-tabContent">
                                    <div class="tab-pane fade {{ $active == 'bio' ? 'active show' : '' }}"
                                        id="nav-acc" role="tabpanel" aria-labelledby="nav-acc-tab">
                                        <div class="acc-setting">
                                            <h3>Contact & Basic Info</h3>
                                            <div class="col-lg-12">
                                                <div class="login-sec p-0">
                                                    {!! Form::model($userprofile, ['method' => 'POST', 'id' => 'bioForm', 'route' => ['user.bio.store', $userprofile->id], 'data-parsley-validate', 'files' => 'true', 'enctype' => 'multipart/form-data']) !!}
                                                    <div class="col-lg-12 no-pdd form-group">
                                                        <div class="sn-field">
                                                            <label class="label-">Your Age</label>
                                                        </div>
                                                        <div>
                                                            {!! Form::text('age', old('age'), ['id' => 'age', 'class' => 'form-control age-select', 'placeholder' => 'Enter your age', 'disabled' => 'true']) !!}
                                                            {!! Form::text('id', old('id'), ['id' => 'id', 'hidden' => 'true', 'required' => 'true']) !!}
                                                            {!! Form::text('user_id', old('user_id'), ['id' => 'user_id', 'hidden' => 'true', 'required' => 'true']) !!}
                                                        </div>
                                                    </div>
                                                    <div class="col-lg-12 no-pdd form-group">
                                                        <div class="sn-field">
                                                            <label class="label-">Who You Are</label>
                                                        </div>
                                                        <div>
                                                            {!! Form::textarea('bio', old('bio'), ['id' => 'bio', 'class' => 'form-control', 'rows' => '3', 'placeholder' => 'Introduce yourself', 'required' => 'true']) !!}
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                    <div class="col-lg-6 no-pdd form-group">
                                                        <div class="sn-field">
                                                            <label class="label-">Your City</label>
                                                        </div>
                                                        <div style="width: 95% !important;">
                                                            {!! Form::text('city', old('city'), ['id' => 'city', 'class' => 'form-control city-select', 'placeholder' => 'Enter your city', 'required' => 'true']) !!}
                                                        </div>
                                                    </div>
                                                    <div class="col-lg-6 no-pdd form-group">
                                                        <div class="sn-field">
                                                            <label class="label-">Your State</label>
                                                        </div>
                                                        <div>
                                                            {!! Form::text('state', old('state'), ['id' => 'state', 'class' => 'form-control state-select', 'placeholder' => 'Enter your state', 'required' => 'true']) !!}
                                                        </div>
                                                    </div>
                                                    <div class="col-lg-6 no-pdd form-group">
                                                        <div class="sn-field">
                                                            <label class="label-">Your Hometown</label>
                                                        </div>
                                                        <div style="width: 95% !important;">
                                                            {!! Form::text('hometown', old('hometown'), ['id' => 'hometown', 'class' => 'form-control hometown-select', 'placeholder' => 'Enter your hometown', 'required' => 'true']) !!}
                                                        </div>
                                                    </div>
                                                    <div class="col-lg-6 no-pdd form-group">
                                                        <div class="sn-field">
                                                            <label class="label-">Zip Code</label>
                                                        </div>
                                                        <div>
                                                            {!! Form::text('zip_code', old('zip_code'), ['id' => 'zip_code', 'class' => 'form-control zip_code-select', 'placeholder' => 'Enter Zipcode', 'required' => 'true']) !!}
                                                        </div>
                                                    </div>
                                                    </div>
                                                    <div class="col-lg-12 no-pdd form-group">
                                                        <div class="sn-field">
                                                            <label class="label-">Your Country</label>
                                                        </div>
                                                        <div>
                                                            <select name="country" id="country" class="w-100" style="height: 38px; padding-left: 10px; border-radius: 4px;border: 1px solid #999;">
                                                                @if($userprofile->country==null)
                                                                <option value="">Select Country</option>
                                                                @else
                                                                <option value="{{$userprofile->country}}">{{$userprofile->country}}</option>
                                                                @endif
                                                                @foreach ($allcountries as $country)
                                                                  <option value="{{$country}}">{{$country}}</option>
                                                                @endforeach
                                                              </select>
                                                        </div>
                                                    </div>
                                                    <div class="col-lg-12 no-pdd mt-4 pt-4 mb-4">
                                                        <h3 class="text-primary profile-link">Profile Link:
                                                            <span>
                                                                <a href="{{ url('/') }}/{{ $userprofile['user_link'] }}"
                                                                    class="ml-2 text-dark">{{ url('/') }}/{{ $userprofile['user_link'] }}</a>
                                                            </span>

                                                            @if ($userprofile['level'] != '1')
                                                                <span class="float-right"><i
                                                                        class="fa fa-pencil text-primary pointer-cursor"
                                                                        data-toggle="modal" data-target="#profilelink"
                                                                        onclick="viewuserlink()"></i></span>
                                                            @endif
                                                        </h3>
                                                    </div>
                                                    <div class="col-lg-12 no-pdd mt-4 pt-4">
                                                        @if (count($user_urls)>0)
                                                        <h3 class="text-primary profile-link" style="overflow-wrap: break-word;">Website URLs:
                                                        
                                                        @endif
                                                            <br><br>
                                                            @foreach ($user_urls as $urlweb)
                                                            <span>
                                                                <a href="{{$urlweb->urls}}"
                                                                    class="ml-2 text-dark" target="_blank">{{ $urlweb->urls }}</a><br>
                                                            </span>
                                                          <a href="{{route('user.weblink',$urlweb->id)}}" onclick="return confirm('Are you sure you want to delete this URL?')"> <span class="float-right" style="margin-top: -22px;"><i
                                                                    class="fa fa-trash text-primary pointer-cursor"
                                                                    ></i></span></a> <span id="btnEditUrl" class="float-right" ><i
                                                                        class="fa fa-pencil text-primary pointer-cursor"
                                                                        data-toggle="modal" data-target="#weblink" 
                                                                        onclick="viewuserweburl({{$urlweb->id}})"></i></span>
                                                                    <br><br>
                                                            @endforeach
                                                        </h3> 
                                                    </div>
                                                    {{$user_urls}}
                                                    <br>
                                                    <input id="multipleweb" type='button'style="background-color:#385499; color:white; width:auto; line-height:0" value='Add URL'>
                                                    <div class="col-lg-12 no-pdd form-group" id="newurl">
                                                        <div class="sn-field">
                                                    <label>Add Your Website URLs</label>
                                                        </div>
                                                        <div class="field_wrapper ">
                                                            <div class="new_wrapper">
                                                                <input type="url" name="Url[]" id="url" placeholder="http(s)://example.com" style="margin-bottom: 20px; width:90%">
                                                            </div>
                                                            <div>
                                                                <a href="javascript:void(0);" class="add_button" title="Add field"><i class="fa fa-plus-square" style="font-size:25px; color:#007bff; padding:4px"></i></a>
                                                            </div>
                                                        </div>
                                                    
                                                    </div>
                                                    <div class="col-lg-12 no-pdd">
                                                        <a class="btn bio-button" type="submit" onclick="validationcheck()">Update
                                                            Bio</a>
                                                    </div>
                                                    {!! Form::close() !!}
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="tab-pane fade {{ $active == 'about' ? 'active show' : '' }}"
                                        id="nav-status" role="tabpanel" aria-labelledby="nav-status-tab">
                                        <div class="acc-setting">
                                            <h3>About You</h3>
                                            <div class="col-lg-12">
                                                <div class="login-sec p-0">
                                                    {!! Form::model($userprofile, ['method' => 'PUT', 'id' => 'myForm', 'route' => ['user.about.update', $userprofile->id], 'data-parsley-validate', 'files' => 'true', 'enctype' => 'multipart/form-data']) !!}
                                                    {{-- <div class="col-lg-12 no-pdd"> --}}
                                                    {{-- <div class=""> --}}
                                                    {{-- <label class="label-">Profile Picture</label> --}}
                                                    {{-- <div class="box-profile"> --}}

                                                    {{-- <div id="picture_div"> --}}
                                                    {{-- @if ($userprofile['picture'] == 'yes') --}}
                                                    {{-- <img id="id-picture" src="{{url('/').'/public/storage/users/'.$userprofile['user_id'].'/profile_picture.jpg'}}?v={{time()}}" alt=""> --}}
                                                    {{-- @else --}}
                                                    {{-- <img id="id-picture" src="{{ asset('public/uploads/white.jpg') }}" alt=""> --}}
                                                    {{-- @endif --}}
                                                    {{-- </div> --}}

                                                    {{-- <div class="upload-btn-wrappers p-2"> --}}
                                                    {{-- <a class="btns"> --}}
                                                    {{-- <i class="fas fa-camera"></i> --}}
                                                    {{-- </a> --}}
                                                    {{-- {!! Form::file('picture', [ --}}
                                                    {{-- 'onchange'=>'readpicURL(this,"picture")','accept'=>'image/*','id'=>'picture' --}}
                                                    {{-- ]) !!} --}}
                                                    {{-- @if ($errors->has('picture')) --}}
                                                    {{-- <span class="help-block"> --}}
                                                    {{-- {{ $errors->first('picture') }} --}}
                                                    {{-- </span> --}}
                                                    {{-- @endif --}}
                                                    {{-- </div> --}}
                                                    {{-- </div> --}}
                                                    {{-- </div> --}}
                                                    {{-- </div> --}}

                                                    <div class="col-lg-12 no-pdd form-group">
                                                        <div class="sn-field">
                                                            <label class="label-">First Name</label>
                                                        </div>
                                                        <div>
                                                            {!! Form::text('fname', old('fname'), ['id' => 'fname', 'class' => 'form-control', 'required' => 'true', 'maxlength' => '25']) !!}
                                                            {!! Form::text('id', old('id'), ['id' => 'id', 'hidden' => 'true', 'required' => 'true']) !!}
                                                            {!! Form::text('user_id', old('user_id'), ['id' => 'user_id', 'hidden' => 'true', 'required' => 'true']) !!}
                                                        </div>
                                                    </div>
                                                    <div class="col-lg-12 no-pdd form-group">
                                                        <div class="sn-field">
                                                            <label class="label-">Last Name</label>
                                                        </div>
                                                        <div>
                                                            {!! Form::text('lname', old('lname'), ['id' => 'lname', 'class' => 'form-control', 'required' => 'true', 'maxlength' => '15']) !!}
                                                        </div>
                                                    </div>
                                                    <div class="col-lg-12 no-pdd form-group">
                                                        <div class="sn-field">
                                                            <label class="label-">Email</label>
                                                        </div>
                                                        <div>
                                                            {!! Form::input('email', 'email', old('email'), ['id' => 'email', 'class' => 'form-control', 'required' => 'true', 'maxlength' => '35']) !!}
                                                        </div>
                                                    </div>
                                                    <div class="col-lg-12 no-pdd form-group">
                                                        <div class="sn-field">
                                                            <label class="label-">Date of Birth</label>
                                                        </div>
                                                        <div>
                                                            {!! Form::date('dob', old('dob'), ['id' => 'dob', 'class' => 'form-control', 'required' => 'true']) !!}
                                                        </div>
                                                    </div>
                                                    <div class="col-lg-12 no-pdd">
                                                        <div class="sn-field">
                                                            <label class="label-">Mobile Number</label>
                                                        </div>
                                                        <div>
                                                            {!! Form::number('mob', old('mob'), ['id' => 'mob', 'class' => 'form-control', 'placeholder' => 'Enter Your Mobile Number', 'required' => 'true']) !!}
                                                        </div>
                                                    </div>
                                                    <div class="col-lg-12 no-pdd my-4">
                                                        <a class="btn about-button" onclick="aboutcheck()">Update About
                                                            Info</a>
                                                    </div>
                                                    {!! Form::close() !!}
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="tab-pane fade {{ $active == 'request' ? 'active show' : '' }}"
                                        id="privcy" role="tabpanel" aria-labelledby="nav-privcy-tab">
                                        @include('sports.request')
                                    </div>
                                    <div class="tab-pane fade {{ $active == 'defaulttab' ? 'active show' : '' }}"
                                        id="page" role="tabpanel" aria-labelledby="nav-page-tab">
                                        <div class="acc-setting">
                                            <h3>Default Page Configuration</h3>
                                            <div class="col-lg-12">
                                                <div class="login-se p-0">
                                                    <form method="post"
                                                        action="{{ route('user.tab.update', $userprofile->user_id) }}"
                                                        enctype="multipart/form-data">
                                                        @csrf
                                                        <div class="col-lg-12 no-pdd form-group">
                                                            <div>
                                                                <label style="margin-bottom: 20px;" for="page">Please
                                                                    Select Your Default Page</label>
                                                                <select class="form-control round" name="page">
                                                                  
                                                                    @php
                                                                        if ($userprofile->default_tab == 1) {
                                                                            $select = 'Opinions as Default Page';
                                                                            $valu=1;
                                                                        } else {
                                                                            $select = 'Teams as Default Page';
                                                                            $valu=0;
                                                                        }
                                                                    @endphp
                                                                    <option class="fa" value="{{ $valu }}" selected hidden>{{ $select }}</option>
                                                                    <option value="0" name="teams">Teams as Default Page
                                                                    </option>
                                                                    <option value="1" name="opinion">Opinions as Default
                                                                        Page</option>
                                                                </select>
                                                            </div>
                                                        </div>
                                                        <div class="col-lg-12 no-pdd">
                                                            <button type="submit" id="btnremovee"
                                                                class="btn bio-button center">Update Configurations</button>
                                                        </div>
                                                    </form>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </div>
    </div>

    <!-- Invites Modal -->
    @include('sports.modal.invites')

    {{-- Friends List --}}
    @include("sports.contact")

    @include('sports.script')
    @include('sports.js.bio')
    @include('sports.js.post')
    @include('sports.js.invites')
    @include('sports.js.images')
    @include('sports.js.js')
    @include('sports.modal.profile_link')
    @include('sports.modal.weblink')

    <script>
        var element = $("#btnremovee");



        $(document).ready(function() {
            $('#btnremovee').removeAttr("disabled")
        });

    </script>
    <script type="text/javascript">
        $(document).ready(function(){
            var maxField = 10; //Input fields increment limitation
            var wrapper = $('.field_wrapper'); //Input field wrapper
            var fieldHTML = '<div class="remonclick"><input type="url" name="Url[]" id="urlminu" class="urlminu" placeholder="http(s)://example.com" style="width:90%;margin-bottom: 20px;"/><a href="javascript:void(0);" class="remove_button"><i class="fa fa-minus-square" style="font-size:25px;color:red; padding:4px;"></i></a></div>'; //New input field html 
            var btnForadd ='<a href="javascript:void(0);" class="add_button" title="Add field"><i class="fa fa-plus-square" style="font-size:25px; color:#007bff; padding:4px"></i></a>'
            var x = 1; //Initial field counter is 1
            
            //Once remove button is clicked
            $(wrapper).on('click', '.remove_button', function(e){
                e.preventDefault();
                $(this).parent('div').remove(); //Remove field html
                x--;
                if(x = 1){
                    $("#multipleweb").show();
                } //Decrement field counter
            });
            var addButton = $('.add_button'); //Add button selector
            
            //Once add button is clicked
            $(wrapper).on('click', '.add_button', function(e){
                //Check maximum number of input fields
                if(x < maxField){ 
                    x++; //Increment field counter
                    $('.new_wrapper').append(fieldHTML); //Add field html
                }
            });
            
        });
        </script>
    <script>
        $("#multipleweb").click(function() { 
    // assumes element with id='button'
    var change = document.getElementById("multipleweb");
    $("#newurl").toggle();
    $("#url").val("");
    $(".urlminu").val("");
    $(".remonclick").hide();
    //$(this).val('Close');
    if ($(this).val() == "Add URL")
                {
                    $(this).val('Cancel');
                }
                else {
                    $(this).val('Add URL');
                    
                }
});


    </script>
</body>

</html>
