<head>
    <meta charset="UTF-8">
    <title>WHO I FOLLOW</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="viewport" content="width=device-width, height=device-height, initial-scale=1.0, minimum-scale=1.0">

    <meta name="description" content="" />
    <meta name="keywords" content="" />
    <meta http-equiv="refresh" content="{{ config('session.lifetime') * 61 }}">
    <link rel="shortcut icon" href="{{ asset('public/sports/images') }}/favicon.png" type="image/x-icon">
    <link rel="stylesheet" type="text/css" href="{{ asset('public/sports/css') }}/animate.css">
    <link rel="stylesheet" type="text/css" href="{{ asset('public/sports/css') }}/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="{{ asset('public/sports/css') }}/line-awesome.css">
    <link rel="stylesheet" type="text/css" href="{{ asset('public/sports/css') }}/line-awesome-font-awesome.min.css">
    <link href="{{ asset('public/sports/vendor/fontawesome-free/css') }}/all.min.css" rel="stylesheet" type="text/css">
    <link rel="stylesheet" type="text/css" href="{{ asset('public/sports/css') }}/font-awesome.min.css">
    <link rel="stylesheet" type="text/css" href="{{ asset('public/sports/lib/slick') }}/slick.css">
    <link rel="stylesheet" type="text/css" href="{{ asset('public/sports/lib/slick') }}/slick-theme.css">
    <link rel="stylesheet" type="text/css" href="{{ asset('public/sports/css') }}/style.css">
    <link rel="stylesheet" type="text/css" href="{{ asset('public/sports/css') }}/mystyle.css">
    <link href="{{ asset('public/newchat/css') }}/commonstyle.css" rel="stylesheet" type="text/css">
    <link rel="stylesheet" type="text/css" href="{{ asset('public/sports/css') }}/chat.css">
    <link rel="stylesheet" type="text/css" href="{{ asset('public/sports/css') }}/responsive.css">
    <link rel="stylesheet" href="{{ asset('public/sports/css') }}/images-grid.css">

    {{-- Image Loader--}}
    {{--<link rel="stylesheet" href="{{ asset('public/sports/css') }}/modal-loading.css">--}}
    {{--<link rel="stylesheet" href="{{ asset('public/sports/css') }}/modal-loading-animate.css">--}}

    <!-- Begin emoji-picker Stylesheets -->
    <link href="https://onesignal.github.io/emoji-picker/lib/css/emoji.css" rel="stylesheet">
    <!-- End emoji-picker Stylesheets -->

    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.13.1/css/bootstrap-select.css" />
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.0/css/select2.min.css"/>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <!-- Popper.JS -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.0/umd/popper.min.js" integrity="sha384-cs/chFZiN24E4KMATLdqdvsezGxaGsi4hLGOzlXwp5UZB1LY//20VyM2taTB4QvJ" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.bundle.min.js	"></script>
    <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
    {{--<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.js"></script>--}}
</head>