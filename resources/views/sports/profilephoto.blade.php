<div class="add-pic-box">
    <div class="container">
        <div class="row no-gutters">
            <div class="cover-photo">
                <button onclick="addRemoveProfilePhoto()" class="profilephoto-dropdown-open"><i class="fa fa-camera"></i></button>
                <div id="profilephoto-dropdown-content" class="add-remove-profile-photo">
                    {!! Form::model($userprofile, ['method' => 'PUT','id'=>'profilephoto', 'route' => ['user.about.update', $userprofile->id], 'data-parsley-validate' , 'files'=>'true' , 'enctype'=>'multipart/form-data']) !!}
                    <div class="upload-btn-wrappers upload-cover-photo">
                        <a class="upload-photo"><i class="fa fa-upload mr-2"></i>Upload Profile Photo</a>
                        {!! Form::file('picture', [
                            'onchange'=>'readpicURL(this,"picture")','accept'=>'image/*','id'=>'picture'
                        ]) !!}
                        @if($errors->has('picture'))
                            <span class="help-block">
                                {{ $errors->first('picture') }}
                            </span>
                        @endif
                    </div>
                    {!! Form::close() !!}
                    @if($userprofile['picture']=='yes')
                        <a class="remove-photo" onclick="removeprofilephoto()"><i class="fa fa-trash mr-2"></i>Remove Profile Photo</a>
                    @endif
                </div>
            </div>
        </div>
    </div>
</div>
@include('sports.js.cover')