<!DOCTYPE html>
<html>

@include('sports.head')

<body class="main-body">
    <div class="wrapper">
        @include('sports.head_main')
        <section class="companies-info">
            <div class="container">
                <div class="company-title">
                    <h3>{{ $user['fname']}}  {{ $user['lname']}} Followers List</h3>
                </div>
                <div class="companies-list">
                    @if(count($profiles)>0)
                        <div class="row" id="post-data">
                            @include('sports.lists.profiles')
                        </div>
                    @else
                        <div class="row">
                            <div class="col-lg-12">
                                <div class="company_profile_info no-profile">
                                    <div class="company-up-info">
                                        <h3 class="mb-0">No User Profile Found.</h3>
                                    </div>
                                </div>
                            </div>
                        </div>
                    @endif
                    <div class="ajax-load text-center" style="display:none">
                        <div class="process-comm">
                            <div class="spinner">
                                <div class="bounce1"></div>
                                <div class="bounce2"></div>
                                <div class="bounce3"></div>
                            </div>
                            <br/>
                            <br/>
                        </div>
                    </div>
                    <br/>
                </div>
            </div>
        </section>
    </div>

    {{-- invite Modal --}}
    @include("sports.modal.invites")

    {{-- Friends List--}}
    @include("sports.contact")

    @include('sports.script')
    @include('sports.js.post')
</body>
</html>