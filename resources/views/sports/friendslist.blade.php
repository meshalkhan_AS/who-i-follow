<!DOCTYPE html>
<html>

@include('sports.head')

<body class="main-body">
<div class="wrapper">
    @include('sports.head_main')
    <div class="container">
    </div>
    <section class="companies-info">

        <div class="row">
            {{--<div class="col-lg-3">--}}
            {{--<div class="suggestions full-width">--}}
            {{--<div class="sd-title text-center">--}}
            {{--<h3>Add or invite friends</h3>--}}
            {{--</div>--}}
            {{--<div class="suggestions-list">--}}
            {{--<div class="view-more">--}}
            {{--<div class="user_pro_status">--}}
            {{--<ul class="flw-hr">--}}
            {{--<li><a href="{{ route('user.profiles') }}" title="" class="add-friends">Add Friends</a></li>--}}
            {{--</ul>--}}
            {{--<ul class="flw-hr">--}}
            {{--<li><a class="invites-friends" title="" data-toggle="modal" data-target="#invites"  onclick="via_invites()">Invite Friends</a></li>--}}
            {{--</ul>--}}
            {{--</div>--}}
            {{--</div>--}}
            {{--</div>--}}
            {{--</div>--}}
            {{--</div>--}}
            <div class="col-lg-12 mt-4">
                <div class="companies-list">
                    @if($user['user_id']==Auth::user()->id)
                        <div class="row">
                            <div class="col-lg-3"></div>
                            <div class="col-lg-2 col-md-4 col-sm-4 col-12 mt-2 mb-2 mr-2 bg-white text-center rounded chk-1 no-pdd">
                                <a class="chk-1 nav-link pointer-cursor border-0" onclick="myfriends()">
                                    All Friends
                                </a>
                            </div>
                            @if($reqReveivedCount>0)
                                <div class="col-lg-2 col-md-4 col-sm-4 col-12 mt-2 mb-2 mr-2 bg-white text-center rounded chk-3 no-pdd">
                                    <a class="chk-3 nav-link pointer-cursor border-0" onclick="receivedrequest()">
                                        Request Received
                                        <div class="chat-mg bx" style="margin-left:-35px;">
                                            <span>{{$reqReveivedCount}}</span>
                                        </div>
                                    </a>
                                </div>
                            @endif
                            @if($reqSentCount>0 || count($invites)>0)
                                <div class="col-lg-2 col-md-4 col-sm-4 col-12 mt-2 mb-2 bg-white text-center rounded chk-2 no-pdd">
                                    <a class="chk-2 nav-link pointer-cursor border-0" onclick="sentrequest()">
                                        Request Sent
                                        @if($reqSentCount>0 || count($invites)>0)
                                        <div class="chat-mg bx">
                                            <span>{{$reqSentCount+count($invites)}}</span>
                                        </div>
                                        @endif
                                    </a>
                                </div>
                            @endif
                            <div class="col-lg-3"></div>
                        </div>
                    @else
                        <div class="company-title">
                            <h3>{{ $user['fname']}}  {{ $user['lname']}} Friends List</h3>
                        </div>
                    @endif
                    <br/>
                    <br/>

                    <div class="row" id="post-data">
                        @include('sports.lists.friends')
                    </div>

                    @if(count($invites)>0)
                        <div class="invitesDiv">
                            @include('sports.lists.invitesRequest')
                        </div>
                    @endif

                    <div class="row" id="no-record" style="display: none;">
                        <div class="main-ws-sec col-lg-12 no-pdd">
                            <div class="posts-section">
                                <div class="post-bar">
                                    <div class="post_topbar">
                                    </div>
                                    <div class="epi-sec text-center">
                                        <div class="job_descp">
                                            <p class="mb-0">No User Found.</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>


    {{-- Friends List--}}
    @include("sports.contact")

<!-- Invites Modal -->
    @include('sports.modal.invites')
</div>
@include('sports.script')
@include('sports.js.friends')
@include('sports.js.invites')
</body>
</html>