<div class="acc-setting">
    <h3>Friend Requests</h3>
    <div class="requests-list">
    @if(count($friend_request)>0)
        <div id="post-data">
            @include('sports.lists.request')
        </div>
        @else
            <div class="request-details">
                <div class="request-info">
                    <h3>No Requests.</h3>
                </div>
            </div>
        @endif
        <div class="ajax-load text-center" style="display:none">
            <div class="process-comm">
                <div class="spinner">
                    <div class="bounce1"></div>
                    <div class="bounce2"></div>
                    <div class="bounce3"></div>
                </div>
                <br/>
                <br/>
            </div>
        </div>
        <br/>
    </div>
</div>
@include('sports.js.request')
@include('sports.js.post')