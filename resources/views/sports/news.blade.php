<!DOCTYPE html>
<html>

@include('sports.head')
<body class="main-body">
<div class="wrapper">
    @include('sports.head_main')
    <main>
        <div class="container">
        </div>
        <div class="main-section">
            <div class="container-fluid no-pdd">
                <div class="main-section-data">
                    <div class="row">
                        <div class="col-xl-3 col-lg-3 d-none d-lg-block no-pdd position-fixed" style="height: calc(100vh - 73px); min-width: 26%; max-width: 26%;">
                            @include('sports.lists.homechat')
                        </div>
                        <div class="col-xl-6 col-lg-6 col-md-12 col-sm-12 col-12 no-pdd center-column" style="position: absolute; left: 26%; right: 26%; min-width: 48%; max-width: 48%; ">
                            <div>
                                @if(Session::get('success'))
                                    <div id="mydiv">
                                        <div  class="alert alert-success alert-dismissible fade show" role="alert">
                                            <p>{{Session::get('success')}}</p>
                                        </div>
                                    </div>
                                @endif
                            </div>
                            <div class="post-topbar rounded-0">
                                <div class="post-st">
                                    <div class="usy-dt">
                                        @if($userprofile['picture']=='yes')
                                            <a href="{{route('user.home',Auth::user()->user_link)}}">   <img id="profile_picture_check" src="{{url('/').'/public/storage/users/'.$userprofile['user_id'].'/profile_picture.jpg'}}?v={{time()}}" alt=""></a>
                                        @else
                                            <a href="{{route('user.home',Auth::user()->user_link)}}">       <img src="{{ asset('public/uploads/white.jpg') }}" alt=""></a>
                                        @endif
                                        <div class="row">
                                            <a class="open-post-popup col-lg-11 col-md-11 col-sm-11 col-11" title="" data-toggle="modal" data-target="#addPost" onclick="refreshdata()" >Whats on your mind, {{ ucfirst($userprofile['fname'])}}?</a>
                                        </div>
                                    </div>
                                </div>
                            </div>


                            @if(count($posts)>0)
                                <div class="main-ws-sec">
                                    <div class="posts-section" id="post-data">

                                        @if($random_post!=0)
                                        @include('randompost')
                                        @endif
                                        @include('sports.lists.opinion')
                                    </div>
                                </div>
                            @else
                                <div class="main-ws-sec">
                                    <div class="posts-section">
                                        <div class="post-bar rounded-0">
                                            <div class="post_topbar">
                                            </div>
                                            <div class="epi-sec">
                                                <div class="job_descp">
                                                    <p>No Post Yet.</p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            @endif
                            <div class="ajax-load text-center d-none">
                                <div class="process-comm">
                                    <div class="spinner">
                                    <div class="bounce1"></div>
                                    <div class="bounce2"></div>
                                    <div class="bounce3"></div>
                                    </div>
                                    <br/>
                                    <br/>
                                </div>
                            </div>
                            <br/>
                        </div>
                        
                        <div class="col-xl-3 col-lg-3  d-none d-lg-block no-pdd position-fixed" style="overflow-y: auto; height: calc(100vh - 73px); right: 0; min-width: 26%; max-width: 26%;">
                            @if(count($exp_posts)>0)
                            @include('sports.lists.expert')
                            @else
                            <div class="main-ws-sec">
                                <div class="posts-section">
                                    <div class="row post-bar mb-3 post-bar-title rounded-0 ">
                                        <h1 class="col-lg-6 col-md-6 col-sm-6 col-6 ml-0" style="font-weight:bolder !important; color:black !important;" title="" >Specialists</h1>
                                          <a class="col-lg-6 col-md-6 col-sm-6 col-6 text-right mt-1" href="{{ url('/post/all')}}" style="font-size: 12px !important; font-family: 'Open Sans', sans-serif !important ; color: #385499 !important ;">View More</a>
                                      </div>
                                    <div class="post-bar rounded-0 post-bar-title">
                                        <div class="post_topbar">
                                        </div>
                                        <div class="epi-sec">
                                            <div class="job_descp">
                                                <p>No Post Yet.</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        @endif
                        </div>
                        
                    </div>
                </div>
            </div>
        </div>
    </main>

    <!-- Contact List -->
    @include('sports.contact')

    <!-- Post Modal -->
    @include('sports.modal.addpost')

    <!-- Update Modal -->
    @include('sports.modal.editpost')

    <!-- Invites Modal -->
    @include('sports.modal.invites')
</div>
@include('sports.script')
@include('sports.js.post')
@include('sports.js.invites')
@include('sports.js.images')
</body>
</html>