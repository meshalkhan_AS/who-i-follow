
<!DOCTYPE html>
<html>

@include('sports.head')

<body class="main-body">
<div class="wrapper">
    @include('sports.head_main')
    <section class="cover-sec">
        @if($userprofile['coverphoto']=='yes')
            <img id="id-cover_photo" class="cover-photo-zoom" onclick="cover_photo_zoom()" src="{{url('/').'/public/storage/users/'.$userprofile['user_id'].'/cover_photo.jpg'}}?v={{time()}}" alt="">
        @else
            <img id="id-cover_photo" src="{{ asset('public/sports/images/resources') }}/baseball.jpg" alt="" >
        @endif
        @include('sports.coverphoto')
        <div id="myModal" class="modal">
            <span class="close">&times;</span>
            <img class="img-modal-content" id="img01">
        </div>
        <div id="update-cancel-cover-photo">
            <a id="cover-update" onclick="coverphoto()">Save</a>
            <a id="cover-cancel" onclick="covercancel()">Cancel</a>
        </div>
    </section>
    <div class="user-tab-sec">
        <div class="tab-feed">
            <ul>
                <li data-tab="feed-dd" class="active">
                    <a>
                        <span>My Opinions</span>
                    </a>
                </li>
                <li data-tab="info-dd">
                    <a>
                        <span>About</span>
                    </a>
                </li>
            </ul>
        </div>
    </div>
    <main>
        <div class="main-section">
            <div class="container-fluid no-pdd">
                <div class="row">
                    <div class="col-lg-3">
                        <div class="main-left-sidebar">
                            <div class="user_profile">
                                <div class="user-pro-img">
                                    @if($userprofile['picture']=='yes')
                                        <img id="profile-photo" class="profile-photo-zoom" src="{{url('/').'/public/storage/users/'.$userprofile['user_id'].'/profile_picture.jpg'}}?v={{time()}}" alt="">
                                    @else
                                        <img id="profile-photo" src="{{ asset('public/uploads/white.jpg') }}" alt="">
                                    @endif
                                    <i class="fa fa-camera" title="" data-toggle="modal" data-target="#profilephoto"></i>
                                    <div id="view-update-profile-pic">
                                        <div class="zoom-profile-pic">
                                            <a onclick="profile_photo_zoom()">View Profile Picture</a>
                                        </div>
                                        <div class="upload-profile-pic">
                                            <a title="" data-toggle="modal" data-target="#profilephoto">Update Profile Picture</a>
                                        </div>
                                    </div>
                                    <div id="myModal" class="modal">
                                        <span class="close">&times;</span>
                                        <img class="img-modal-content" id="img01">
                                    </div>
                                </div>
                                <h3>{{ ucfirst($userprofile['fname'])}} {{ucfirst($userprofile['lname'])}}</h3>
                                <p>{{ $userprofile['email']}}</p>
                                <a class="edit-profile" href="{{ route('user.bio') }}" title="">Edit Profile <span><i class="fa fa-pencil text-primary"></i></span></a>
                                <hr>
                                <div class=" user_pro_status follower_following row">
                                    <div class="btn col-lg-4 col-md-4 col-sm-4 col-4"><span><a href="{{ route('user.followers',Auth::user()->user_link) }}">Followers </a></span></div>
                                    <div class="btn col-lg-2 col-md-2 col-sm-2 col-2"><span> @if($followers>0){{$followers}}@else 0 @endif</span></div>
                                    <div class="btn col-lg-4 col-md-4 col-sm-4 col-4"><span><a href="{{ route('user.following',Auth::user()->user_link) }}">Following </a></span></div>
                                    <div class="btn col-lg-2 col-md-2 col-sm-2 col-2"> <span> @if($following>0){{$following}}@else 0 @endif</span></div>
                                </div>
                            </div>
                        </div>

                        <div class="suggestions full-width">
                            <div class="sd-title">
                                <h3>Friends &nbsp;&nbsp; @if($friendsCount)({{$friendsCount}}) @endif </h3>
                                @if(count($friends)>0)
                                    <a href="{{ route('user.friends',Auth::user()->user_link) }}">See All Friends</a>
                                @endif
                            </div>
                            <div class="suggestions-list">
                                @if(count($friends)>0)
                                    @foreach($friends as $friend)
                                        <div class="suggestion-usd friends">
                                            @if($friend['picture']=='yes')
                                                <img id="profile_picture_check" class="img-circle" src="{{url('/').'/public/storage/users/'.$friend['user_id'].'/profile_picture.jpg'}}?v={{time()}}" alt="">
                                            @else
                                                <img class="img-circle" src="{{ asset('public/uploads/white.jpg') }}" alt="">
                                            @endif
                                            <div class="sgt-text">
                                                <h4>{{ucfirst($friend['fname'])}} {{ucfirst($friend['lname'])}}</h4>
                                                <span>
                                                    <ul class="flw-hr">
                                                        <li><a href="{{route('user.individualprofile',$friend['user_link'])}}" title="" class="hre show-profile">View Profile</a></li>
                                                    </ul>
                                                </span>
                                            </div>
                                        </div>
                                    @endforeach
                                @endif
                                <div class="view-more">
                                    @if(count($friends)==0)
                                        <a href="#" title="">There is no friend added in your friends list.</a>
                                        {{--<div class="user_pro_status mt-3">--}}
                                        {{--<ul class="flw-hr">--}}
                                        {{--<li><a href="{{ route('user.profiles') }}" title="" class="add-friends">Add Friends</a></li>--}}
                                        {{--</ul>--}}
                                        {{--<ul class="flw-hr">--}}
                                        {{--<li><a title="" class="invites-friends" data-toggle="modal" data-target="#invites" onclick="via_invites()">Invite Friends</a></li>--}}
                                        {{--</ul>--}}
                                        {{--</div>--}}
                                        {{--@else--}}
                                        {{--<div class="user_pro_status mt-3">--}}
                                        {{--<ul class="flw-hr">--}}
                                        {{--<li><a href="{{ route('user.profiles') }}" title="" class="add-friends">Add Friends</a></li>--}}
                                        {{--</ul>--}}
                                        {{--<ul class="flw-hr">--}}
                                        {{--<li><a title="" class="invites-friends" data-toggle="modal" data-target="#invites"  onclick="via_invites()">Invite Friends</a></li>--}}
                                        {{--</ul>--}}
                                        {{--</div>--}}
                                    @endif
                                </div>

                            </div>

                        </div>
                    </div>
                    <div class="col-lg-6">
                        <div class="main-ws-sec">
                            <div>
                                @if($message = Session::get('success'))
                                    <div id="mydiv">
                                        <div  class="alert alert-success alert-dismissible fade show mb-4" role="alert">
                                            <strong>{{$message}}</strong>
                                        </div>
                                    </div>
                                @endif
                            </div>

                            {{-- Feed--}}
                            <div class="product-feed-tab current" id="feed-dd">
                                <div class="main-ws-sec">
                                    <div class="post-topbar">
                                        <div class="post-st">
                                            <div class="usy-dt">
                                                @if($userprofile['picture']=='yes')
                                                    <a href="{{route('user.home',Auth::user()->user_link)}}">   <img id="profile_picture_check" src="{{url('/').'/public/storage/users/'.$userprofile['user_id'].'/profile_picture.jpg'}}?v={{time()}}" alt=""></a>
                                                @else
                                                    <a href="{{route('user.home',Auth::user()->user_link)}}">       <img src="{{ asset('public/uploads/white.jpg') }}" alt=""></a>
                                                @endif
                                                <div class="row">
                                                    <a class="open-post-popup col-lg-11 col-md-11 col-sm-11 col-11" title="" data-toggle="modal" data-target="#addPost" onclick="refreshdata()" >Whats on your mind, {{ ucfirst($userprofile['fname'])}} ?</a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    @if(count($posts)>0)
                                        <div class="posts-section" id="post-data">   
                                            @if($random_post!=0)
                                            
                                            @include('randompost')
                                            @endif
                                            @include('sports.lists.profilepost')
                                        </div>
                                    @else
                                        <div class="posts-section">
                                            <div class="post-bar">
                                                <div class="post_topbar">
                                                </div>
                                                <div class="epi-sec">
                                                    <div class="job_descp">
                                                        <p>No Post Yet.</p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                    @endif
                                    <div class="ajax-load text-center d-none mb-2">
                                        <div class="process-comm">
                                            <div class="spinner mb-2">
                                                <div class="bounce1"></div>
                                                <div class="bounce2"></div>
                                                <div class="bounce3"></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            {{--Info--}}
                            <div class="product-feed-tab" id="info-dd">
                                <div class="user-profile-ov">
                                    <div class="ed-opts">
                                        <a href="{{ route('user.bio') }}"><i class="fa fa-pencil text-primary"></i></a>
                                    </div>
                                    <h3>About Myself</h3>
                                    <p>{{$userprofile['bio']}}</p>
                                </div>
                                <div class="user-profile-ov st2">
                                    <div class="ed-opts">
                                        <a href="{{ route('user.bio') }}"><i class="fa fa-pencil text-primary"></i></a>
                                    </div>
                                    <h3>Basic Information</h3>
                                    <div class="container">
                                        <div class="row">
                                            <div class="col-md-6 w-50">
                                                <h4>Date of Birth <span> : </span></h4>
                                            </div>
                                            <div class="col-md-6 w-50 dob">
                                                {{$userprofile['dob']}}
                                            </div>
                                            <div class="col-md-6 w-50">
                                                <h4>Age <span> : </span> </h4>
                                            </div>
                                            <div class="col-md-6 w-50 age">
                                                @if($userprofile['age']){{$userprofile['age']}}@else N/A @endif
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="user-profile-ov">
                                    <div class="ed-opts">
                                        <a href="{{ route('user.bio') }}"><i class="fa fa-pencil text-primary"></i></a>
                                    </div>
                                    <h3>Location</h3>

                                    <div class="container">
                                        <div class="row">
                                            <div class="col-md-6 w-50">
                                                <h4>City <span> : </span> </h4>
                                            </div>
                                            <div class="col-md-6 w-50 city">
                                                @if($userprofile['city']){{$userprofile['city']}}@else N/A @endif
                                            </div>
                                            <div class="col-md-6 w-50">
                                                <h4>Hometown <span> : </span> </h4>
                                            </div>
                                            <div class="col-md-6 w-50 hometown">
                                                @if($userprofile['hometown']){{$userprofile['hometown']}}@else N/A @endif
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3">

                        {{--@if($ads)--}}
                        {{--<div class="col-lg-12">--}}
                        {{--<h5 style="color:#65676b;margin-bottom: 10px;">Sponsored</h5>--}}
                        {{--<a href="{{url('/').'/ads/click/'.$ads->id}}"><img src="{{url('/').'/'.$ads->image}}" style="border-radius: 10px;"/></a><br/>--}}
                        {{--</div>--}}
                        {{--<div class="col-lg-12">--}}
                        {{--<a href="{{url('/').'/ads/click/'.$ads->id}}">{{$ads->title}}</a><br/>--}}
                        {{--<a href="{{url('/').'/ads/click/'.$ads->id}}"><small style="font-size: 10px;color: #65676b;">{{str_replace('https://', '', $ads->link)}}</small></a>--}}
                        {{--</div>--}}
                        {{--@endif--}}
                    </div>
                </div>
            </div>
        </div>
    </main>

    {{-- Friends List--}}
    @include("sports.contact")

<!-- Invites Modal -->
    @include('sports.modal.invites')

<!-- Post Modal -->
    @include('sports.modal.addpost')

<!-- Update Modal -->
    @include('sports.modal.editpost')

<!-- Invites Modal -->
    @include('sports.modal.profilephoto')
</div>
@include('sports.script')
@include('sports.js.post')
@include('sports.js.invites')
@include('sports.js.images')
@include('sports.js.js')
@include('sports.js.profile')
</body>
</html>
