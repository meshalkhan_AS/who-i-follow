<div class="add-pic-box">
    <div class="container">
        <div class="row no-gutters">
            <div class="cover-photo">
                <button onclick="addRemoveCoverPhoto()" class="dropdown-open"><i class="fa fa-camera"></i>Edit Cover Photo</button>
                <div id="dropdown-content" class="add-remove-cover-photo">
                    {!! Form::model($userprofile, ['method' => 'POST','id'=>'coverForm', 'route' => ['user.cover.update', $userprofile->id], 'data-parsley-validate','files'=>'true','enctype'=>'multipart/form-data']) !!}
                        <div class="upload-btn-wrappers upload-cover-photo">
                            <a class="upload-photo"><i class="fa fa-upload mr-2"></i>Upload Cover Photo</a>
                            {!! Form::file('coverphoto', [
                               'onchange'=>'coverpicURL(this,"cover_photo")','accept'=>'image/*','id'=>'coverphoto'
                            ]) !!}
                            @if($errors->has('coverphoto'))
                                <span class="help-block">
                                    {{ $errors->first('coverphoto') }}
                                </span>
                            @endif
                        </div>
                    {!! Form::close() !!}
    @if($userprofile['coverphoto']=='yes')
        <a class="remove-photo" onclick="removecoverphoto()"><i class="fa fa-trash mr-2"></i>Remove Cover Photo</a>
    @endif
</div>
</div>
</div>
</div>
</div>
@include('sports.js.cover')