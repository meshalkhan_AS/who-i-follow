<!DOCTYPE html>
<html>

@include('sports.head')

@php
    $userprofile = \App\Models\Admin\UserProfile::select('user_profiles.*','users.user_link as user_link')
            ->join('users', 'users.id', '=', 'user_profiles.user_id')
            ->where('user_profiles.user_id',Auth::user()->id)->first();
    $userprofileall = \App\Models\Admin\UserProfile::take(10)->get();
    $userNotifications = \App\Models\Admin\UserNotification::orderBy('created_at','desc')->where('user_id',Auth::user()->id)->take(20)->get();
    $count = \App\Models\Admin\UserNotification::where('user_id',Auth::user()->id)->sum('checked');
@endphp
<script>
    const SetTab=(no)=>{
localStorage.setItem('TabNum', no);
reloadDIV();

    }
</script>
<body class="main-body">
    <div class="wrapper">
        <div class="container">
        </div>
        @include('sports.head_main')
        {{--<section class="companies-info">--}}
            {{--<div class="container-fluid">--}}
                {{--<div>--}}
                    {{--@if($message = Session::get('success'))--}}
                        {{--<div id="mydiv">--}}
                            {{--<div  class="alert alert-success alert-dismissible fade show" role="alert" style=" margin: 0 10px 20px 16px;font-weight: 100">--}}
                                {{--<strong>{{$message}}</strong>--}}
                            {{--</div>--}}
                        {{--</div>--}}
                    {{--@endif--}}
                {{--</div>--}}
                {{--<div class="company-title ml-0 pt-1 mb-1">--}}
                    {{--<h3>Suggested Profiles</h3>--}}
                    {{--<input id="search" value="{{$search}}" hidden/>--}}
                {{--</div>--}}
                {{--<div class="companies-list">--}}
                    {{--@if(count($profiles)>0)--}}
                        {{--<div class="row" id="profile-data">--}}
                            {{--@include('sports.lists.profiles')--}}
                        {{--</div>--}}
                    {{--@else--}}
                        {{--<div class="row">--}}
                            {{--<div class="col-lg-12">--}}
                                {{--<div class="company_profile_info no-profile">--}}
                                    {{--<div class="company-up-info">--}}
                                        {{--<h3>No User Profile Found.</h3>--}}
                                    {{--</div>--}}
                                {{--</div>--}}
                            {{--</div>--}}
                        {{--</div>--}}
                    {{--@endif--}}
                    {{--<div class="ajax-load text-center" style="display:none">--}}
                        {{--<div class="process-comm">--}}
                            {{--<div class="spinner">--}}
                                {{--<div class="bounce1"></div>--}}
                                {{--<div class="bounce2"></div>--}}
                                {{--<div class="bounce3"></div>--}}
                            {{--</div>--}}
                            {{--<br/>--}}
                            {{--<br/>--}}
                        {{--</div>--}}
                    {{--</div>--}}
                    {{--<br/>--}}
                {{--</div>--}}
            {{--</div>--}}
        {{--</section>--}}

        <section id="tabs" class="project-tab">
            <div class="container">
                <div class="row">
                    <div class="col-md-3">
                        <div class="suggestions full-width">
                            <div class="sd-title">
                                <h3 style="font-weight: bold">Related Search</h3>
                            </div>
                            <div class="suggestions-list">

                                <div class="view-more">

                                </div>

                            </div>

                        </div>
                    </div>
                    <div class="col-md-6">
                        <nav class="w-100">
                            <div class="nav nav-tabs" id="nav-tab" role="tablist">
                                <a class="first nav-item nav-link active" id="nav-home-tab" onclick="SetTab('1')">All</a>
                                <a class="nav-item nav-link" id="nav-profile-tab"  onclick="SetTab('2')">People</a>
                                <a class="nav-item nav-link" id="nav-contact-tab"  onclick="SetTab('3')">Post</a>
                            </div>
                        </nav>
                        {{--<div class="tab-content" id="nav-tabContent">--}}
                            <div  id="nav-home">
                                <div class="companies-list bg-white p-2 search-results">
                                    <div class="search-results-title">
                                        <h3>Search Results</h3>
                                    </div>
                                    <div class="search-results-subtitle">
                                        <h3>People</h3>
                                        @if(count($profiles)>0)
                                        <a class="nav-item nav-link float-right" id="nav-profile-tab" onclick="window.scrollTo(0, 0);">View All</a>
                                        @endif
                                    </div>
                                    <div class="companies-list mb-2">
                                        @if(count($profiles)>0)
                                            <div class="row" id="profile-data" style="border-bottom: 1px solid #e4e4e4">
                                                @include('sports.lists.suggested-profiles')
                                            </div>
                                        @else
                                            <div class="row">
                                                <div class="col-lg-12">
                                                    <div class="company_profile_info no-profile">
                                                        <div class="company-up-info">
                                                            <h3>No User Profile Found.</h3>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        @endif
                                    </div>
                                    <br/>
                                    <div class="search-results-subtitle">
                                        <h3>Posts</h3>
                                        @if(count($posts)>0)
                                        <a class="nav-item nav-link float-right" id="nav-contact-tab" onclick="window.scrollTo(0, 0);">View All</a>
                                        @endif
                                    </div> 
                                        <div class="companies-list mb-2">
                                            @if(count($posts)>0)
                                                <div class="row" id="profile-data" style="border-bottom: 1px solid #e4e4e4">
                                                    @include('sports.lists.suggested-posts')
                                                </div>
                                            @else
                                                <div class="row">
                                                    <div class="col-lg-12">
                                                        <div class="company_profile_info no-profile">
                                                            <div class="company-up-info">
                                                                <h3>No Post Found.</h3>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            @endif
                                        </div>
                                    <br/>
                                </div>
                            </div>
                            <div id="nav-profile">
                                <div class="companies-list bg-white p-2 search-results">
                                    <div class="search-results-subtitle">
                                        <h3>People</h3>
                                    </div>
                                    @if(count($profiles)>0)
                                        <div class="row" id="profile-data">
                                            @include('sports.lists.all-profiles')
                                        </div>
                                    @else
                                        <div class="row">
                                            <div class="col-lg-12">
                                                <div class="company_profile_info no-profile">
                                                    <div class="company-up-info">
                                                        <h3>No User Profile Found.</h3>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    @endif
                                    <div class="ajax-load text-center d-none">
                                        <div class="process-comm">
                                            <div class="spinner">
                                                <div class="bounce1"></div>
                                                <div class="bounce2"></div>
                                                <div class="bounce3"></div>
                                            </div>
                                            <br/>
                                            <br/>
                                        </div>
                                    </div>
                                <br/>
                                </div>
                            </div>
                            <div id="nav-contact">
                                <div class="companies-list bg-white p-2 search-results">
                                    <div class="search-results-subtitle">
                                        <h3>Posts</h3>
                                    </div>
                                    @if(count($posts)>0)
                                        <div class="row" id="post-data">
                                            @include('sports.lists.all-posts')
                                        </div>
                                    @else
                                        <div class="row">
                                            <div class="col-lg-12">
                                                <div class="company_profile_info no-profile">
                                                    <div class="company-up-info">
                                                        <h3>No Posts Found.</h3>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    @endif
                                    <div class="ajax-load text-center d-none">
                                        <div class="process-comm">
                                            <div class="spinner">
                                                <div class="bounce1"></div>
                                                <div class="bounce2"></div>
                                                <div class="bounce3"></div>
                                            </div>
                                            <br/>
                                            <br/>
                                        </div>
                                    </div>
                                    <br/>
                                </div>
                            </div>
                        {{--</div>--}}
                    </div>
                    <div class="col-md-2"></div>
                </div>
            </div>
        </section>
        @include('sports.contact')
        @include('sports.modal.editpost')
    </div>

    <!-- Invites Modal -->
    @include('sports.modal.invites')

    {{-- Friends List--}}
    @include('sports.script')
    @include('sports.js.images')
    @include('sports.js.invites')
    <script>
        function reloadDIV() {
			 window.location.reload();
}
        if(localStorage.getItem('TabNum')=='1'){
        $('#nav-home-tab').addClass('active')
        $("#nav-home").css("display", "block");
    }
    if(localStorage.getItem('TabNum')=='2'){
        $('#nav-profile-tab').addClass('active')
        $('#nav-home-tab').removeClass('active')
        $("#nav-profile").css("display", "block");
        $("#nav-home").css("display", "none");
    }
    if(localStorage.getItem('TabNum')=='3'){
        $('#nav-contact-tab').addClass('active')
        $('#nav-home-tab').removeClass('active')
        $("#nav-contact").css("display", "block");
        $("#nav-home").css("display", "none");
    }
    </script>
</body>
</html>