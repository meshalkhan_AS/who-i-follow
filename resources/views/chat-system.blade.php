<!DOCTYPE html>
<html>

@include('sports.head')

<body>
<style>
    body {
        padding: 0px;
        overflow: hidden;
    }
    #no-chat-message{
        display: none;
    }
    #main-wrapper{
        padding: 0px;
    }
</style>

<!-- jQuery -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.1.0/jquery.min.js"></script>
<!-- Firebase -->
<script src="https://www.gstatic.com/firebasejs/8.8.0/firebase-app.js"></script>
<script src="https://www.gstatic.com/firebasejs/8.8.0/firebase-firestore.js"></script>

@include('sports.head_main')

<div class="wrapper" id="main-wrapper">


    <div class="wrapper">
        <!-- Side View -->
        <nav id="Sidebar-first" class="active">
            <div class="sidebar-header"></div>

            <ul class="list-unstyled components">
                <li>
                    <a href="#">
                        <i class="fas fa-comments"></i>
                    </a>
                </li>
            </ul>
        </nav>

        <!-- Chat List  -->
        <nav id="sidebar">
            <div class="top">
                <p>Chats</p>

                <div class="top-icons">
                    <a data-toggle="modal" data-target="#exampleModal">
                        <i class="fas fa-users"></i>
                    </a>

                    <a data-toggle="modal" data-target="#exampleModal">
                        <i class="fas fa-edit"></i>
                    </a>
                </div>
            </div>

            <div class="search-box">
                <input type="search" name="search" class="form-control" placeholder="search chats">
            </div>


            <div class="info-message" id="no-chat-message">
                <p>You don't have any chats yet. Create a group or start a direct message to get the conversations going!</p>
            </div>


            <ul class="list-unstyled components">
                <li>
                    <div class="list-group" id="list-tab" role="tablist">
                     
                    </div>
                </li>


            </ul>
        </nav>

        <!-- Page Content  -->
        <div id="content">

            <div class="right-side">
                <div class="tab-content" id="nav-tabContent">
                    <div class="tab-pane fade show active" id="list-home" role="tabpanel" aria-labelledby="list-home-list">

                        <!-- Chat Start -->
                        <div class="full-chat">

                            <!-- Chat Header -->
                            <div class="chat-header">
                                <div class="media">
                                    <button type="button" id="sidebarCollapse">
                                        <i class="fa fa-arrow-left arrow"></i></button>
                                    <img src="{{ asset('public/sports/images') }}/default-group.avatar.png" class="mr-3">
                                    <div class="media-body custom-class">
                                        <h5 class="mt-0">Group Name <i class="fas fa-caret-down"></i></h5>
                                        <a href="#"><i class="fas fa-times"></i></a>
                                    </div>
                                </div>
                            </div>

                            <!-- Add Member Area -->
                            <div class="add-member-box">
                                <a href="#" class="close"><i class="fas fa-times"></i></a>
                                <div class="btn-member">
                                    <a href="#"><i class="fas fa-user-plus"></i> Add Members</a>
                                </div>
                            </div>

                            <!-- Message Middle Area -->
                            <div class="full-message-box">

                                <div class="chat-layout">
                                    <div class="chat-middle">
                                        <div class="meesage-box">
                                            <div class="message-time">
                                                <p>1:09 AM</p>
                                            </div>
                                        </div>

                                        <div class="messages-area">
                                            <div class="media">
                                                <img src="{{ asset('public/sports/images') }}/default-group.avatar.png" class="mr-3">
                                                <div class="media-body">
                                                    <h4 class="mt-0">Member Name</h4>
                                                    <div class="content-and-icons">
                                                        <p class="recieve-message">Message</p>
                                                        <div class="icons">
                                                            <a href="#">
                                                                <i class="fas fa-ellipsis-h"></i>
                                                            </a>
                                                            <a href="#">
                                                                <i class="fas fa-reply"></i>
                                                            </a>
                                                            <a href="#">
                                                                <i class="fa fa-heart"></i>
                                                            </a>
                                                        </div>
                                                    </div>
                                                    <p class="message-time">Sent - 1:09 AM</p>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="meesage-box">
                                            <div class="message-time">
                                                <p>1:09 AM</p>
                                            </div>
                                        </div>

                                        <div class="messages-area-user">
                                            <div class="media">
                                                <div class="media-body">
                                                    <h4 class="mt-0">Member Name</h4>
                                                    <div class="content-and-icons">
                                                        <div class="icons">
                                                            <a href="#">
                                                                <i class="fa fa-heart"></i>
                                                            </a>
                                                            <a href="#">
                                                                <i class="fas fa-reply"></i>
                                                            </a>
                                                            <a href="#">
                                                                <i class="fas fa-ellipsis-h"></i>
                                                            </a>
                                                        </div>
                                                        <p class="recieve-message">Message</p>
                                                    </div>
                                                    <p class="message-time">Sent - 1:09 AM</p>
                                                </div>
                                                <img src="{{ asset('public/sports/images') }}/default-group.avatar.png" class="ml-3">
                                            </div>
                                        </div>


                                    </div>
                                </div>

                                <div class="input-message-field">
                                    <div class="field">
                                        <p class="lead emoji-picker-container">

                                            <!-- Button trigger modal -->
                                            <a class="show-popup" data-toggle="modal" data-target="#uploadPhotos">
                                              <img src="images/file-upload.png"/>
                                            </a>
                                            
                                            <textarea class="form-control textarea-control" rows="3" placeholder="Send Message" data-emojiable="true"></textarea>
                                        </p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>


                    <div class="tab-pane fade" id="list-profile" role="tabpanel" aria-labelledby="list-profile-list">
                        <!-- Chat Start -->
                        <div class="full-chat">

                            <!-- Chat Header -->
                            <div class="chat-header">
                                <div class="media">
                                    <img src="{{ asset('public/sports/images') }}/default-group.avatar.png" class="mr-3">
                                    <div class="media-body custom-class">
                                        <h5 class="mt-0">Single Chat <i class="fas fa-caret-down"></i></h5>
                                        <a href="#"><i class="fas fa-times"></i></a>
                                    </div>
                                </div>
                            </div>

                            <!-- Add Member Area -->
                            <div class="add-member-box">
                                <a href="#" class="close"><i class="fas fa-times"></i></a>
                                <div class="btn-member">
                                    <a href="#"><i class="fas fa-user-plus"></i> Add Members</a>
                                </div>
                            </div>

                            <!-- Message Middle Area -->
                            <div class="full-message-box">

                                <div class="chat-layout">
                                    <div class="chat-middle">
                                        <div class="meesage-box">
                                            <div class="message-time">
                                                <p>1:09 AM</p>
                                            </div>
                                        </div>

                                        <div class="messages-area">
                                            <div class="media">
                                                <img src="{{ asset('public/sports/images') }}/default-group.avatar.png" class="mr-3">
                                                <div class="media-body">
                                                    <h4 class="mt-0">Member Name</h4>
                                                    <div class="content-and-icons">
                                                        <p class="recieve-message">Message</p>
                                                        <div class="icons">
                                                            <a href="#">
                                                                <i class="fas fa-ellipsis-h"></i>
                                                            </a>
                                                            <a href="#">
                                                                <i class="fas fa-reply"></i>
                                                            </a>
                                                            <a href="#">
                                                                <i class="fa fa-heart"></i>
                                                            </a>
                                                        </div>
                                                    </div>
                                                    <p class="message-time">Sent - 1:09 AM</p>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="meesage-box">
                                            <div class="message-time">
                                                <p>1:09 AM</p>
                                            </div>
                                        </div>

                                        <div class="messages-area-user">
                                            <div class="media">
                                                <div class="media-body">
                                                    <h4 class="mt-0">Member Name</h4>
                                                    <div class="content-and-icons">
                                                        <div class="icons">
                                                            <a href="#">
                                                                <i class="fa fa-heart"></i>
                                                            </a>
                                                            <a href="#">
                                                                <i class="fas fa-reply"></i>
                                                            </a>
                                                            <a href="#">
                                                                <i class="fas fa-ellipsis-h"></i>
                                                            </a>
                                                        </div>
                                                        <p class="recieve-message">Message</p>
                                                    </div>
                                                    <p class="message-time">Sent - 1:09 AM</p>
                                                </div>
                                                <img src="{{ asset('public/sports/images') }}/default-group.avatar.png" class="ml-3">
                                            </div>
                                        </div>


                                    </div>
                                </div>

                                <div class="input-message-field">
                                    <div class="field">
                                        <p class="lead emoji-picker-container">

                                            <!-- Button trigger modal -->
                                            <a class="show-popup" data-toggle="modal" data-target="#uploadPhotos">
                                              <img src="images/file-upload.png"/>
                                            </a>
                                            
                                            <textarea class="form-control textarea-control" rows="3" placeholder="Send Message" data-emojiable="true"></textarea>
                                        </p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- Modal -->
    <div class="modal fade" id="exampleModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <form>

                        <select class="selectpicker form-control" multiple data-live-search="true">
                            <option>Mustard</option>
                            <option>Ketchup</option>
                            <option>Relish</option>
                        </select>

                        <div class="form-group">
                            <textarea class="form-control" id="exampleFormControlTextarea1" rows="3" placeholder="Write a message"></textarea>
                        </div>
                        <button type="submit" class="btn btn-primary">Send Message</button>
                    </form>
                </div>
            </div>
        </div>
    </div>


    <!-- Photos And Documents Upload Model -->
    <div class="modal fade" id="uploadPhotos" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
      <div class="modal-dialog modal-dialog-centered">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <div class="modal-body">
            <h2>Share Photos & Documents</h2>
            <a href="#">
                <div class="image-upload">
                  <label for="file-input">
                    <img src="images/file-upload.png"/>
                  </label>

                  <img id="blah" src="" />
                  <input name="file" id="file-input" ref="fileInput" type="file" onchange="getFileName()" /><span  id="fileName"></span>
                </div>
            </a>
          </div>
        </div>
      </div>
    </div>

</div>

<script src="https://cdn.jsdelivr.net/npm/lodash@4.17.10/lodash.min.js"></script>

<!-- Begin emoji-picker JavaScript -->
<script src="https://onesignal.github.io/emoji-picker/lib/js/config.js"></script>
<script src="https://onesignal.github.io/emoji-picker/lib/js/util.js"></script>
<script src="https://onesignal.github.io/emoji-picker/lib/js/jquery.emojiarea.js"></script>
<script src="https://onesignal.github.io/emoji-picker/lib/js/emoji-picker.js"></script>
<!-- End emoji-picker JavaScript -->

<script>
    $(function() {
        // Initializes and creates emoji set from sprite sheet
        window.emojiPicker = new EmojiPicker({
            emojiable_selector: '[data-emojiable=true]',
            assetsPath: 'http://onesignal.github.io/emoji-picker/lib/img/',
            popupButtonClasses: 'fa fa-smile-o'
        });
        // Finds all elements with `emojiable_selector` and converts them to rich emoji input fields
        // You may want to delay this step if you have dynamically created input fields that appear later in the loading process
        // It can be called as many times as necessary; previously converted input fields will not be converted again
        window.emojiPicker.discover();
    });

    function getFileName()
    {
    var x = document.getElementById('file-input')
    x.style.visibility = 'collapse'
    document.getElementById('fileName').innerHTML = x.value.split('\\').pop()
    }

    function readURL(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();
            
            reader.onload = function (e) {
                $('#blah').attr('src', e.target.result);
            }
            
            reader.readAsDataURL(input.files[0]);
        }
    }

    $("#file-input").change(function(){
        readURL(this);
    });
</script>


<script>
    const IMGURL = "{{ asset('public/sports/images') }}";
    // Your web app's Firebase configuration
    var firebaseConfig = {
        apiKey: "AIzaSyDqaohOWgKFDkmsXB8bajPiQdh8uJctSco",
        authDomain: "who-i-follow.firebaseapp.com",
        databaseURL: "https://who-i-follow-default-rtdb.firebaseio.com",
        projectId: "who-i-follow",
        storageBucket: "who-i-follow.appspot.com",
        messagingSenderId: "173439628160",
        appId: "1:173439628160:web:3a924148d67f259708c765"
    };
    // Initialize Firebase
    firebase.initializeApp(firebaseConfig);

    var user = {
        id: parseInt('{{auth()->user()->id}}'),
        name: '{{auth()->user()->name}}'
    };
    const db = firebase.firestore();
    var groups = [], currentGroup = undefined, messages = [];

    //create group
    var members = {};
    members[user.id] = user;
    members[3] = { id: 3, name: 'Sajid Ali'};
    //  createGroup(members, user.id, 'Sajid2', 'individual');

    //fetch all groups
    fetchGroupByUserID(user.id);

    function createGroup(userArray, createdBy, name, type) {
        const groupObj = {
            createdAt: new Date(),
            createdBy,
            members: userArray,
            name,
            type,
        }
        return new Promise((resolve, reject) => {
            db.collection('group')
                .add(groupObj)
                .then(function (docRef) {
                    groupObj.id = docRef.id
                    fetchGroupByUserID(user.id)
                    resolve(groupObj)
                })
                .catch(function (error) {
                    reject(error)
                })
        })
    }

    function filterGroup(userArray) {
        groups = []
        return new Promise((resolve, reject) => {
            let groupRef = db.collection('group')
            userArray.forEach((userId) => {
                groupRef = groupRef.where('members', '==', userId)
            })
            groupRef
                .get()
                .then(function (querySnapshot) {
                    const allGroups = []
                    querySnapshot.forEach((doc) => {
                        const data = doc.data()
                        data.id = doc.id
                        allGroups.push(data)
                    })
                    if (allGroups.length > 0) {
                        resolve(allGroups[0])
                    } else {
                        resolve(null)
                    }
                })
                .catch(function (error) {
                    reject(error)
                })
        })
    }

    function fetchGroupByUserID(uid) {
        return new Promise((resolve, reject) => {
            const groupRef = db.collection('group')
            groupRef.where('members.'+uid+'.id', '==', parseInt(uid)).onSnapshot((querySnapshot) => {
                const allGroups = []
                querySnapshot.forEach((doc) => {
                    const data = doc.data()
                    data.id = doc.id;
                    // if (data.recentMessage) allGroups.push(data)
                    allGroups.push(data);
                })
                groups = allGroups;

                //renderChatList when groups updated
                renderChatList();
            })
        })
    }

    function fetchGroupByIds(groupIds) {
        const groupsObj = []
        const groupRef = db.collection('group')
        groupIds.forEach(async (groupId) => {
            await groupRef
                .doc(groupId)
                .get()
                .then(function (doc) {
                    groupsObj.push(doc.data())
                })
                .catch(function (error) {
                    // eslint-disable-next-line no-console
                    console.error('Error get document: ', error)
                })
        })
        groups = groupsObj
    }

    function updateGroup(group) {
        db.collection('group')
            .doc(group.id)
            .set(group)
            .then(function (docRef) {})
            .catch(function (error) {
                // eslint-disable-next-line no-console
                console.error('Error writing document: ', error)
            })
    }

    function addNewGroupToUser(user, groupId) {
        const groupsObj = user.groups ? user.groups : []
        const existed = groupsObj.filter((group) => group === groupId)
        if (existed.length === 0) {
            groups.push(groupId)
            user.groups = groupsObj
            const userRef = db.collection('user')
            userRef.doc(user.uid).set(user)
        }
    }

    function saveMessage(messageText, sentAt, currentGroupId) {
        if (messageText.trim()) {
            const message = {
                messageText,
                sentAt,
                sentBy: this.user.uid,
            }
            return new Promise((resolve, reject) => {
                db.collection('message')
                    .doc(currentGroupId)
                    .collection('messages')
                    .add(message)
                    .then(function (docRef) {
                        resolve(message)
                    })
                    .catch(function (error) {
                        reject(error)
                    })
            })
        }
    }

    function fetchMessagesByGroupId(groupId) {
        messages = []
        db.collection('message')
            .doc(groupId.trim())
            .collection('messages')
            .orderBy('sentAt')
            .onSnapshot((querySnapshot) => {
                const allMessages = []
                querySnapshot.forEach((doc) => {
                    if (doc) allMessages.push(doc.data())
                })
                messages = allMessages
            })
    }


    /* Render chat list in the UI when groups are updated in firestore */
    function renderChatList()
    {
        if (groups.length <= 0) {
            $('#no-chat-message').css('display', 'block');
        } else {
            $('#no-chat-message').css('display', 'none');
        }

        var chatHtml = '';
        for (let index in groups) {
            var groupObj = groups[index];
            if (groupObj.type == 'group') {
                chatHtml += ' <a class="list-group-item list-group-item-action active" id="list-home-list" data-toggle="list" href="#list-home" role="tab" aria-controls="home">' +
                    ' <div class="chats">' +
                    '<div class="media">' +
                    ' <img src="'+IMGURL+'/default-group.avatar.png" class="mr-3" alt="...">' +
                    '  <div class="media-body">' +
                    '  <div class="chat-name-and-time">' +
                    '    <h4 class="chat-name">' +
                    '   <div class="chat-title">' +
                    '   <span>'+groupObj.name+'</span>' +
                    '  </div>' +
                    '   <div>1:09 AM</div>' +
                    '   </h4>' +
                    ' </div>' +
                    '  <div class="chat-content">' +
                    '   <p><span>member-name:</span> hello</p>' +
                    ' </div>' +
                    ' </div>' +
                    '  </div>' +
                    '  </div>' +
                    '   </a>';
            } else {
                var otherChatMember = getOtherUser(groupObj.members);
                chatHtml += '<a class="list-group-item list-group-item-action" id="list-profile-list" data-toggle="list" href="#list-profile" role="tab" aria-controls="profile">'+
                    '<div class="chats">'+
                    '<div class="media">'+
                    '<img src="'+IMGURL+'/default-group.avatar.png" class="mr-3" alt="...">'+
                    ' <div class="media-body">'+
                    ' <div class="chat-name-and-time">'+
                    '  <h4 class="chat-name">'+
                    '    <div class="chat-title">'+
                    '     <span>'+otherChatMember.name+'</span>'+
                    '   </div>'+
                    '     <div>1:30 AM</div>'+
                    '    </h4>'+
                    '    </div>'+
                    '    <div class="chat-content">'+
                    '    <p><span>Name:</span> Hi!</p>'+
                    '  </div>'+
                    ' </div>'+
                    ' </div>'+
                    '</div>'+
                    ' </a>';
            }
        }
        $('#list-tab').html(chatHtml);
    }

    function getOtherUser(members)
    {
        const otherMember = _.reject(members, user);
        return otherMember.length > 0 ? otherMember[0] : [];
    }
</script>

@include('sports.script')
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.bundle.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.13.1/js/bootstrap-select.min.js"></script>
</body>
</html>
