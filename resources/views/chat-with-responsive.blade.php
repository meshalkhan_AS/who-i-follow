<!DOCTYPE html>
<html>
@include('sports.head')
<body>
<style>
    body {
        padding: 0px;
        overflow: hidden;
    }
    #no-chat-message{
        display: none;
    }
    #main-wrapper{
        padding: 0px;
    }
    input#group-name {
        width: 100%;
    }
    .group-option {
        display: none;
    }

    .search-box
    {
        height: inherit;
    }

    .list-unstyled li{
        margin-top: 15px;
    }

    .make_admin
    {
        float: right !important;
        margin-left: 10px;
        color: #7c151f;
    }

    .make_admin_
    {
        float: right !important;
        margin-left: 10px;
        color: #c1c1c1;
    }
    .tooltip {
        position: relative;
        display: inline-block;
    }

    .tooltip .tooltiptext {
        visibility: hidden;
        width: 140px;
        background-color: #555;
        color: #fff;
        text-align: center;
        border-radius: 6px;
        padding: 5px;
        position: absolute;
        z-index: 1;
        bottom: 150%;
        left: 50%;
        margin-left: -75px;
        opacity: 0;
        transition: opacity 0.3s;
    }

    .tooltip .tooltiptext::after {
        content: "";
        position: absolute;
        top: 100%;
        left: 50%;
        margin-left: -5px;
        border-width: 5px;
        border-style: solid;
        border-color: #555 transparent transparent transparent;
    }

    .tooltip:hover .tooltiptext {
        visibility: visible;
        opacity: 1;
    }

    .group-option:hover {
        background-color: #385499;
        color: #fff;
    }

</style>
@include('sports.head_main')
<div class="container">

</div>

<div class="wrapper" id="main-wrapper" style="margin-top: 62px;">
    <div class="wrapper-chat">
        <!-- Side View -->
        <nav id="Sidebar-first" class="active">
            <div class="sidebar-header">
            </div>

            <ul class="list-unstyled components">
                <li>
                    <span>
                         <a href="#" onclick="viewChatlistSidebar()">
                        <i class="fa fa-comments"></i>
                    </a>
                    </span>

                </li>
                <li>
                    <span title="Friends">
                        <a href="#" onclick="viewFriendslistSidebar()">
                        <i class="fas fa-address-book"></i>
                        </a>
                    </span>
                </li>
                <li>
                    <span title="Archives">
                        <a href="#" onclick="viewArchiveslistSidebar()">
                        <i class="fas fa-archive"></i>
                    </a>
                    </span>

                </li>
                <li>
                    <a href="{{route('user.newChatSystem')}}">New Chat</a>
                </li>
            </ul>
        </nav>
        <!-- Side View -->

        <nav id="sidebar">
                <div class="top">
                    <p></p>

                    <div class="top-icons">
                        <span title="Start a new group chat">
                        <a data-toggle="modal" style="cursor: pointer;" data-target="#newGroupModal" onClick="clearGroupModel()">
                            <img src="{{ asset('public/sports/images') }}/chatting.png" class="mr-3">
                        </a>
                        </span>

                        <span title="Start a new chat">
                        <a data-toggle="modal" style="cursor: pointer;"  data-target="#newMessageModal" onClick="clearFriendModel()">
                            <img src="{{ asset('public/sports/images') }}/chat.png">
                        </a>
                        </span>
                    </div>
                </div>

            <!-- Chat List  -->

            <div class="chat-div">
                <div class="search-box">
                    <input type="text" name="search" class="form-control" id="search-chats" placeholder="Search Chats">
                    <input type="text" name="search" class="form-control" id="search-archived" placeholder="Search Archive" style="display: none">
                    <input type="text" name="search" class="form-control" id="search-friends" placeholder="Search Friends" style="display: none">
                </div>

                {{--No List--}}
                <div class="info-message" id="no-chat-message">
                    <p></p>
                </div>
            </div>

            {{--Loader--}}

            <div class="sideloader">

            </div>

            {{-- <!-- List  --> --}}
            <ul class="list-unstyled components chat-friends-list">
                <li>
                    <div class="list-group" id="list-tab" role="tablist">

                    </div>
                </li>
            </ul>
        </nav>

        <!-- Page Content  -->
        <div id="content">

            <div class="right-side">
                <div class="tab-content" id="nav-tabContent">
                    <div class="tab-pane fade show active" id="list-home" role="tabpanel" aria-labelledby="list-home-list">
                        <div class="loader_full" style="display: none">
                        </div>
                        <div class="full_first" style="display: none">
                        </div>

                        <!-- Chat Start -->
                        <div class="full-chat" style="display: none">
                            <!-- Chat Header -->
                            <div class="chat-header">
                                <div class="media">
                                    <button type="button" id="sidebarCollapse">
                                        <i class="fa fa-arrow-left arrow"></i></button>
                                    <span id="default-img"><img src="{{ asset('public/sports/images') }}/default-group.avatar.png" class="mr-3"></span>
                                    <div class="media-body custom-class">
                                        <h5 class="mt-0" id="chat-title"></h5>

                                        <div class="btn-group dropleft group-option" data-toggle="modal" data-target="#editGroupModal" onclick="editGroup()" style="cursor: pointer; border: 1px solid #385499; padding: 5px; font-size: 12px; border-radius: 100px;">
                                            <div class="edit_button_modal__" id="edit_button_modal"></div>
                                            Edit Group
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- Message Middle Area -->
                            <div class="full-message-box">

                                <div class="chat-layout">
                                    <div class="chat-middle">
                                        <div id="chat-msg-area"></div>
                                    </div>
                                    <div class="input-message-field">
                                        <div class="field">
                                            <p class="lead emoji-picker-container">
                                                <!-- File Upload -->
                                                <div class="image-upload">
                                                    <label for="file-input">
                                                        <img src="{{ asset('public/sports/images') }}/file-upload.png"/>
                                                    </label>
                                                    <input name="file" id="file-input" ref="fileInput" type="file" onchange="sendAttachment()" accept="image/*"/><span  id="fileName"></span>
                                                </div>

                                                <textarea class="form-control textarea-control" data-emoji-input="unicode" id="message-field" placeholder="Send Message..." data-emojiable="true"></textarea>
                                                <a href="javascript:void(0)" title="Send Message" class="send-msg">
                                                    <i class="fa fa-send" onclick="sendMessageToChat()"></i>
                                                </a>
                                            </p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div id="myModal" class="modal">
        <span class="close">&times;</span>
        <img class="img-modal-content" id="img01">
    </div>

    <!-- Button trigger modal -->

    <!-- New Message Modal -->
    <div class="modal fade friend-message-modal" id="newMessageModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel" style="text-align: center; font-weight: 600; font-size: 20px;">New Message</h5>
                    <button type="button" class="close position-absolute d-flex align-items-center justify-content-center" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <form class="new_message_settings">
                        <div class="show-message"></div>
                        <select class="form-control" style="width: 97%;" id="friends-list">
                            <option value="" selected>Select Friend</option>

                        </select>

                        <div class="form-group m-1 mt-3">
                            <textarea class="form-control friend_message" id="new_message_single" data-emoji-input="unicode" rows="12" placeholder="Write message" data-emojiable="true"></textarea>
                        </div>
                        <button type="button" class="btn btn-primary" onclick="sendMessageToFriend()">Send</button>
                    </form>
                </div>
            </div>
        </div>
    </div>

    <!-- Modal group -->
    <div class="modal fade group-message-modal" id="newGroupModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true" style="margin-top: 30px">
        <div class="modal-dialog modal-dialog-centered">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel" style="text-align: center; font-weight: 600; font-size: 20px;">Create New Team</h5>
                    <button type="button" class="close position-absolute d-flex align-items-center justify-content-center" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="header-group-edit">
                        <div class="row">
                            <img id="create-image" src="{{ asset('public/sports/images') }}/default-group.avatar.png" width="60" height="60">
                        </div>
                        <div class="form-group group_picture_update">
                            <div class="upload-btn-wrappers sub-group-field p-2">
                                <a class="btns">
                                    Add Avatar
                                    <i class="fas fa-camera"></i>
                                </a>
                                {!! Form::file('picture', [
                                    'onchange'=>'readCURL(this,)','accept'=>'image/*','id'=>'create_group_img'
                                ]) !!}
                            </div>
                            {{--<input type="file" id="edit_group_img" accept="image/*" onchange='readURL(this)'/>--}}
                        </div>
                    </div>
                    <form class="new_message_setting">
                        <div class="show-message"></div>
                        <div class="form-group m-2">
                            <select class="selectpicker form-control" style="width: 100%" multiple id="friends-list-group" data-live-search="true">
                                <option value="" selected>Select Friends</option>
                            </select>
                        </div>
                        <div class="form-group m-2">
                            <input type="text" class="form-control" id="group-name" placeholder="Enter team name"></input>
                        </div>
                        <div class="form-group m-2 textarea-emoji">
                            <textarea class="form-control group_message textarea-control" rows="9" data-emoji-input="unicode" placeholder="Write message" data-emojiable="true"></textarea>
                        </div>
                        <button type="button" class="btn btn-primary" onclick="createGroupOnButton()">Create</button>
                    </form>
                </div>
            </div>
        </div>
    </div>

    <!-- Edit Group Model -->
    <div class="modal fade" id="editGroupModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered modal-dialog-scrollable" style="height: 480px !important;">
            <div class="modal-content edit-model-content">
                <div class="modal-header">
                    <button type="button" class="close position-absolute d-flex align-items-center justify-content-center" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body edit-group-model-body">
                    <div class="header-group-edit">
                        <div class="row">
                            <img id="edit-image" src="{{ asset('public/sports/images') }}/default-group.avatar.png" width="60" height="60">
                        </div>
                        <div class="form-group group_picture_update">
                            <div class="upload-btn-wrappers sub-group-field p-2">
                                <a class="btns">
                                    Add Avatar
                                    <i class="fas fa-camera"></i>
                                </a>
                                {!! Form::file('picture', [
                                    'onchange'=>'readURL(this,)','accept'=>'image/*','id'=>'edit_group_img'
                                ]) !!}
                            </div>
                        </div>
                        <div class="row">
                            <input type="text" class="form-control group-name group_name_update"/>
                        </div>
                    </div>
                    <div class="show-message"></div>
                    <div class="edit-group">
                        <h5 class="member-heading">Members</h5>
                        <div class="members-list"></div>
                    </div>
                    <div class="btns-edit">
                        <button type="button" class="btn btn-primary update_group_update" onclick="updateGroupName()">Update</button>
                        <!-- Button trigger New Member modal -->
                        <button type="button" class="btn btn-primary update_group_member" data-toggle="modal" data-target="#addNewMember" data-dismiss="modal" aria-label="Close" onclick="getFriendList()">Add new member</button>

                        <div class="row "style="text-align: center">
                            <div class="col-md-1"></div>
                            <button type="button" class="col-md-3 btn btn-success archive_group_update" onclick="archiveGroup()">Archive</button>
                            <button type="button" class="col-md-3 btn btn-success unarchive_group_update" onclick="unarchiveGroup()">Unarchive</button>
                            <button type="button" class="col-md-3 btn btn-danger delete_group_update" onclick="deleteGroup()" style="display: none;margin-left: 10px">Delete</button>
                            <button type="button" class="col-md-3 btn btn-danger leave_group_update" onclick="leaveGroup()" style="display: none;margin-left: 10px">Leave</button>
                            <button type="button" class="col-md-3 btn btn-success invites_via_group" title="" style="margin-left: 10px" data-toggle="modal" data-target="#invites_friends"  onclick="via_invites_groups()">Invite Friends</button>
                            <div class="col-md-2"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- Add New Member Modal -->
    <div class="modal fade" id="addNewMember" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog  modal-dialog-centered">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel" style="text-align: center; font-weight: 600; font-size: 20px;">Add New Member</h5>
                    <button type="button" class="close position-absolute d-flex align-items-center justify-content-center" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body add-member">
                    <form>
                        <div class="show-message"></div>
                        <div class="form-group">
                            <select class="selectpicker form-control" style="width: 100%" multiple id="add-friend-group" data-live-search="true">
                                <option value="" selected>Select Friends</option>
                            </select>
                        </div>
                        <button type="button" class="btn btn-primary" onClick="addMemberToCurrentGroup()">Add Member</button>
                    </form>
                </div>
            </div>
        </div>
    </div>

</div>

@include('sports.script')
@include('chat.basejs')


<!-- Invites Modal -->
@include('sports.modal.invites')
@include('sports.modal.invites_group')
@include('sports.js.invites')

<script>
    $(document).on('click', function(e) {
        document.getElementById("edit_button_modal").style.display = "none";
    });
</script>
</body>
</html>
