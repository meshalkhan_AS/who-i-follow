<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

    Route::get('cache', function ()
    {
        /* php artisan migrate */
        \Artisan::call('cache:clear');
        \Artisan::call('config:clear');
        \Artisan::call('view:clear');
        dd("Done");
    });

    Route::get('/clear-cache', function()
    {
        $exitCode = Artisan::call('cache:clear');
        dd("Done");
    });

    Route::get('/', function ()
    {
        return redirect('/login');
    });
    Route::get('/', 'HomeController@news')->name('home');


//    ---------------------------------------------- Shop Routes -------------------------------------------------------

    Route::prefix('shop')->group(function () {

        Route::group(['middleware' => 'auth'], function () {

            Route::get('/', 'FrontHomeController@index')->name('front.shop');
            Route::get('/products/{cat_id}', 'FrontHomeController@products')->name('front.products');
            Route::get('/products-view/{id}', 'FrontHomeController@viewProduct')->name('front.viewProduct');

            Route::get('/cart-add', 'FrontCartController@cart')->name('front.cart.add');
            Route::get('/getCart', 'FrontCartController@getCart')->name('front.getCart');
            Route::get('/removeCart', 'FrontCartController@removeCart')->name('front.removeCart');
            Route::get('/cart', 'FrontHomeController@carFront')->name('front.carFront');
            Route::get('/payment', 'GlobalPayment@index')->name('front.globalPayment');
            Route::get('/curlPay', 'GlobalPayment@curlPay')->name('front.curlPay');
            Route::get('/checkout', 'FrontHomeController@checkOut')->name('front.cart.checkOut');
            Route::post('/checkout-store', 'FrontHomeController@processCheckout')->name('front.store.processCheckout');

            Route::get('/thank-you', 'ThankyouPageController@index')->name('thank.you');
            Route::get('/payment','FrontHomeController@payment')->name('payment');
            Route::post('/stripe/pay','StripeController@processStripe')->name('payment.stripe-process');

            Route::get('/order-history', 'FrontHomeController@orderHistory')->name('front.store.orderHistory');
            Route::get('/chat', 'HomeController@chatSystem')->name('chatSystem');


        });
    });

//   --------------------------------------------- Admin Panel Routes --------------------------------------------------

    Route::prefix('admin')->group(function ()
    {
        Route::get('/', 'LoginController@adminLogin')->name('admin.login');
        Route::post('/login', 'LoginController@admin_process_login')->name('admin.login.post');
        Route::get('/logout', 'LoginController@admin_process_logout')->name('admin.logout');
        Route::get('/categories/by/id', 'AdminCategoryController@categoryById')->name('admin.category.by.id');

        Route::group(['middleware' => 'verify.admin'], function () {

        // product
        Route::get('/product/list', 'AdminProductController@index')->name('admin.product.index');
        Route::get('/product/create', 'AdminProductController@create')->name('admin.product.create');
        Route::post('/product/store', 'AdminProductController@store')->name('admin.product.store');
        Route::get('/product-edit/{id}', 'AdminProductController@edit')->name('admin.product.edit');
        Route::get('/product-delete/{id}', 'AdminProductController@destroy')->name('admin.product.destroy');
        Route::post('/product-update/{id}', 'AdminProductController@update')->name('admin.product.update');

        //Users list Routes
        Route::get('/user/list/', 'Admin\UserslistController@index')->name('admin.users.list');
        Route::get('/user-edit/{id}', 'Admin\UserslistController@edit')->name('admin.users.edit');
        Route::post('/user-update/{id}', 'Admin\UserslistController@update')->name('admin.user.update');
        Route::get('/user-delete/{id}', 'Admin\UserslistController@destroy')->name('admin.user.destroy');
        Route::get('/post-manage', 'Admin\UserslistController@postdestroyview')->name('admin.post.destroy');
        Route::get('/postdel/{id}', 'Admin\UserslistController@postdelete')->name('admin.post.delete');
        Route::get('/post-view/{id}', 'Admin\UserslistController@singlepost')->name('admin.post.view');
        // category
        Route::get('/categories/list', 'AdminCategoryController@index')->name('admin.categories.list');
        Route::get('/categories/create', 'AdminCategoryController@create')->name('admin.categories.create');
        Route::post('/categories/store', 'AdminCategoryController@store')->name('admin.categories.store');
        Route::get('/categories-edit/{id}', 'AdminCategoryController@edit')->name('admin.categories.edit');
        Route::post('/categories-update/{id}', 'AdminCategoryController@update')->name('admin.categories.update');
        Route::get('/categories-delete/{id}', 'AdminCategoryController@destroy')->name('admin.categories.destroy');

        Route::get('/orders/list', 'AdminOrdersController@index')->name('admin.orders.list');
        Route::get('/orders/status/change', 'AdminOrdersController@ordersStatusChange')->name('admin.orders.status.change');
        Route::get('/orders/view/{id}', 'AdminOrdersController@show')->name('admin.orders.view');

        //feedback admin routes
        Route::get('/feedback/types', 'Admin\FeedBackController@showtypes')->name('admin.showtypes');
        Route::post('/feedtype/store', 'Admin\FeedBackController@storefeed')->name('admin.feedtype');
        Route::get('/type-edit/{id}', 'Admin\FeedBackController@edit')->name('admin.type.edit');
        Route::post('/type-update/{id}', 'Admin\FeedBackController@update')->name('admin.type.update');
        Route::get('/type-delete/{id}', 'Admin\FeedBackController@destroy')->name('admin.type.destroy');
        Route::get('/feedback/list', 'Admin\FeedBackController@showlist')->name('admin.showlist');
        Route::get('/feedback/{id}', 'Admin\FeedBackController@feeddelete')->name('admin.feed.delete');
        Route::get('/feedback-view/{id}', 'Admin\FeedBackController@singlefeed')->name('admin.feed.view');
      

        // Add Category CRUD - protected by middleware
        Route::group(['prefix' => '/cat'], function(){
            Route::get('/', 'ManageAdsController@index')->name('ads.index');
            // Add View
            Route::get('/add', 'ManageAdsController@catIndex')->name('cat.getAdd');
            // Add Post
            Route::post('/add', 'ManageAdsController@catAdd')->name('cat.postAdd');
            // Edit
            Route::get('/edit/{id}', 'ManageAdsController@catEdit')->name('cat.edit');
            // Update
            Route::post('/edit/{id}', 'ManageAdsController@catUpdate')->name('cat.update');
            // Delete
            Route::delete('/del/{id}', 'ManageAdsController@catDelete')->name('cat.delete');
        });

        // Add Banner CRUD - protected by middleware
        Route::group(['prefix' => '/banner'], function(){

            // Add
            Route::get('/', 'ManageAdsController@Bannerindex')->name('banner.index');
            Route::get('/add', 'ManageAdsController@bannerAddView')->name('add.banner.view');
            Route::post('/add', 'ManageAdsController@bannerAdd')->name('add.banner.post');
            Route::get('/banner/edit/{id}', 'ManageAdsController@bannerEdit')->name('banner.edit');
            Route::post('/banner/{id}', 'ManageAdsController@bannerUpdate')->name('banner.update');

            // Edit
            // Update
            // Delete
            Route::delete('/del/{id}', 'ManageAdsController@bannerDelete')->name('banner.delete');
        });
            //banner stats
            Route::get('/advertisement-stats', 'ManageAdsController@adsReport')->name('ad.report');

        });

    });

//    ----------------------------------------------- Auth Routes ------------------------------------------------------

    // Password Reset Routes...
    Route::get('/password/reset/auth', 'Auth\ForgotPasswordController@showlinkrequestform')->name('auth.password.reset');
    Route::post('password/email', 'auth\forgotpasswordcontroller@sendresetlinkemail')->name('auth.password.reset');
    Route::get('password/resets/{token}', 'auth\resetpasswordcontroller@showresetform2')->name('password.resets');

    // Auth
    Route::get('/login','LoginController@index')->name('login');
    Route::get('/register', "RegisterController@index")->name('register');
    Route::get('/{level}/{userid}/register/{custom}', "RegisterController@registerByInvite");
    Route::get('/verification','LoginController@check')->name('login.check');
    Route::get('/userlogout', 'LoginController@user_logout')->name('userlogout');
    Route::post('/login/checkemail','LoginController@checkemail')->name('login.checkemail');
    Route::post('/login/checkuserlink','LoginController@checkuserlink')->name('login.checkuserlink');
    Route::post('/login/checkmobile','LoginController@checkmobile')->name('login.checkmobile');
    Route::post('/userprofile/store','RegisterController@store')->name('register.store');
    Route::post('/register/verify}','RegisterController@verify')->name('register.verify');

//    ----------------------------------------------- Social Site Routes -----------------------------------------------

    Auth::routes();
    Route::group(['middleware' => ['auth'], 'prefix' => '', 'as' => 'user.'], function ()
    {
        Route::get('/', 'HomeController@myopinion')->name('news');
        Route::get('/{link}', 'HomeController@index')->name('home');
        Route::get('/userprofiles/user', 'HomeController@search')->name('search');

        Route::get('/ads/click/{adID}', 'HomeController@adClicked')->name('adClicked');
        Route::get('/chat/user', 'HomeController@chatSystem')->name('chatSystem');
        Route::post('/chat/upload-attachment', 'HomeController@uploadChatAttachment')->name('uploadChatAttachment');
        Route::post('/chat/groups/upload', 'HomeController@uploadGroupAttachment')->name('uploadGroupAttachment');

        // Route for new Chat
        Route::get('/newchat/user', 'HomeController@newChatSystem')->name('newChatSystem');

        // Route Resource for User
        // Route::resource('users', 'Admin\UsersController');

        // Route for User Profile
        Route::get('/profiles/user', ['uses' => 'Admin\UserProfileController@profiles', 'as' => 'profiles']);
        Route::get('/request/user', ['uses' => 'Admin\FriendsController@requestindex', 'as' => 'request']);
        Route::post('/invites/user', ['uses' => 'Admin\InvitesController@invites', 'as' => 'invites']);
        Route::get('/re_invites/user', ['uses' => 'Admin\InvitesController@reInvites', 'as' => 're_invites']);
        Route::post('/updateInvite/user', ['uses' => 'Admin\InvitesController@updateInvite', 'as' => 'updateInvite']);
        Route::get('/{id}', ['uses' => 'Admin\UserProfileController@individualprofile', 'as' => 'individualprofile']);

        Route::get('/bio/user', ['uses' => 'Admin\UserProfileController@bioindex', 'as' => 'bio']);
        Route::post('/bio/store', ['uses' => 'Admin\UpdateController@bioupdate', 'as' => 'bio.store']);
        Route::put('/about/update', ['uses' => 'Admin\UpdateController@aboutupdate', 'as' => 'about.update']);
        Route::post('/tab-update/{id}', ['uses' =>'Admin\UpdateController@tabupdate', 'as' => 'tab.update']);
        Route::get('/weburl/user/{id}', ['uses' => 'Admin\UserProfileController@weburlmodal', 'as' => 'weblink']);
        Route::get('/weburl/edit/{id}', ['uses' => 'Admin\UserProfileController@editurl', 'as' => 'weburl']);
        Route::post('/weburl/edit', ['uses' => 'Admin\UpdateController@editurlupdate', 'as' => 'weburl']);
      //feedback user routes
      Route::get('/user/feedback',['uses' => 'Admin\FeedBackController@userfeedback', 'as' => 'feedback']);
      Route::post('/feedback/uploadimages',['uses' =>'Admin\FeedBackController@uploadimages', 'as' => 'feedback.uploadimages']);
      Route::post('/feed/store',['uses' =>'Admin\FeedBackController@store', 'as' => 'feed.store']);
      Route::post('/feed/deleteimages',['uses' =>'Admin\FeedBackController@deleteimages', 'as' => 'feed.deleteimages']);
        //  Route for Friends
        Route::get('/friendsdata/list', ['uses' => 'Admin\FriendsController@friendList', 'as' => 'friendsListing']);
        Route::get('/users/list', ['uses' => 'Admin\FriendsController@userList', 'as' => 'usersListing']);
        Route::get('/friend/{id}', ['uses' => 'Admin\FriendsController@addfriends', 'as' => 'friend']);
        Route::get('/unfriend/{id}', ['uses' => 'Admin\FriendsController@unfriend', 'as' => 'unfriend']);
        Route::post('/friend/acceptrequest','Admin\FriendsController@acceptrequest')->name('acceptrequest');
        Route::post('/friend/rejectrequest','Admin\FriendsController@rejectrequest')->name('rejectrequest');
        Route::get('/cancelrequest/{id}','Admin\FriendsController@cancelrequest')->name('cancelrequest');
        Route::get('/confirmrequest/{id}','Admin\FriendsController@confirmrequest')->name('confirmrequest');
        Route::get('/ignorerequest/{id}','Admin\FriendsController@ignorerequest')->name('ignorerequest');
        Route::get('/friends/{id}', ['uses' => 'Admin\FriendsController@viewfriends', 'as' => 'friends']);

        // Route for Followers
        Route::get('/follow/{id}', ['uses' => 'Admin\FollowersController@addfollowers', 'as' => 'follow']);
        Route::get('/unfollow/{id}', ['uses' => 'Admin\FollowersController@removefollowers', 'as' => 'unfollow']);
        Route::get('/followers/{id}', ['uses' => 'Admin\FollowersController@viewfollowers', 'as' => 'followers']);
        Route::get('/following/{id}', ['uses' => 'Admin\FollowersController@viewfollowing', 'as' => 'following']);

        // Route for Notifications
        Route::get('/notification/user', ['uses' => 'NotificationController@view', 'as' => 'notification.view']);
        Route::post('/notification/read', ['uses' => 'NotificationController@read', 'as' => 'notification.read']);
        Route::post('/notification/checked', ['uses' => 'NotificationController@checked', 'as' => 'notification.checked']);
        Route::post('/sms/messageCheck', ['uses' => 'Admin\InvitesController@messageCheck', 'as' => 'sms.messageCheck']);
        Route::post('/sms/messagesChecked', ['uses' => 'Admin\InvitesController@messagesChecked', 'as' => 'sms.messagesChecked']);
        Route::post('/sms/sendSMS', ['uses' => 'Admin\InvitesController@sendSMS', 'as' => 'sms.sendSMS']);
        Route::post('/sms/updateSMSNotification', ['uses' => 'Admin\InvitesController@updateSMSNotification', 'as' => 'sms.updateSMSNotification']);


        // Route for Posts
        Route::post('/post/store','Admin\PostController@store')->name('post.store');
        Route::post('/post/delete/','Admin\PostController@delete')->name('post.delete');
        Route::post('/post/edit','Admin\PostController@edit')->name('post.edit');
        Route::post('/post/fetch','Admin\PostController@fetchdata')->name('post.fetchdata');
        Route::post('/post/expert','Admin\PostController@expertdata')->name('post.expertdata');
        Route::post('/post/uploadimages','Admin\PostController@uploadimages')->name('post.uploadimages');
        Route::post('/post/deleteimages','Admin\PostController@deleteimages')->name('post.deleteimages');
        Route::post('/post/clearimages','Admin\PostController@clearimages')->name('post.clearimages');
        Route::get('/post/single/{id}','Admin\PostController@singlepost')->name('single.post');
        Route::get('/post/all','HomeController@allexperts')->name('allexperts.post');

        // Update cover photo
        Route::get('/cover-photo/user', ['uses' => 'Admin\UserProfileController@coverindex', 'as' => 'cover']);
        Route::post('/coverphoto/update', ['uses' => 'Admin\UpdateController@coverupdate', 'as' => 'cover.update']);
        Route::post('/coverphoto/remove', ['uses' => 'Admin\UpdateController@coverremove', 'as' => 'cover.remove']);

        //payment routes
        Route::get('payment', ['uses' => 'PaymentController@index', 'as' => 'payment']);
        Route::post('charge', ['uses' => 'PaymentController@charge', 'as' => 'charge']);
        // Update Profile Photo
        Route::post('/profilephoto/update', ['uses' => 'Admin\UpdateController@profileupdate', 'as' => 'profile.update']);

        // Update Profile Link
        Route::post('/profilelink/update', ['uses' => 'Admin\UpdateController@profilelink', 'as' => 'profilelink.update']);


//      // Sports Update
        Route::post('/sport/update', ['uses' => 'Admin\UpdateController@sportupdate', 'as' => 'sport.update']);

        Route::post('/save-token/user', [App\Http\Controllers\NotificationController::class, 'saveToken'])->name('save-token');
        Route::get('/desktop/notification', [App\Http\Controllers\NotificationController::class, 'index']);
        Route::get('/send-notification/user', [App\Http\Controllers\NotificationController::class, 'sendNotification'])->name('send.notification');
    });

    Route::get('/update_link','RegisterController@updateUid')->name('update_link');

