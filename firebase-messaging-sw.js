importScripts('https://www.gstatic.com/firebasejs/3.9.0/firebase-app.js');
importScripts('https://www.gstatic.com/firebasejs/3.9.0/firebase-messaging.js');

// Initialize the Firebase app in the service worker by passing in the
// messagingSenderId.
 // Initialize the Firebase app in the service worker by passing the generated config
 const firebaseConfig = {  
   apiKey: "AIzaSyBsuLifKoAzNwy04dJ4mdoVtlOQgE9NCuA",
 authDomain: "whoifollow-9fa54.firebaseapp.com",
 projectId: "whoifollow-9fa54",
 storageBucket: "whoifollow-9fa54.appspot.com",
 messagingSenderId: "906041462214",
 appId: "1:906041462214:web:fe443721dc7b83bcd376c7",
 measurementId: "G-X50T8QF297"
};

firebase.initializeApp({
   'messagingSenderId': '906041462214'
});

// Retrieve an instance of Firebase Messaging so that it can handle background
// messages.
const messaging = getMessaging(firebaseApp);

messaging.setBackgroundMessageHandler(function(payload) {
  console.log('[firebase-messaging-sw.js] Received background message ', payload);
  // Customize notification here
  const notificationTitle = 'Background Message Title';
  const notificationOptions = {
    body: 'Background Message body.', //your logo here
  };

  return self.registration.showNotification(notificationTitle,
      notificationOptions);
});
if ('serviceWorker' in navigator) {
navigator.serviceWorker.register('../firebase-messaging-sw.js')
  .then(function(registration) {
    console.log('Registration successful, scope is:', registration.scope);
  }).catch(function(err) {
    console.log('Service worker registration failed, error:', err);
  });
}