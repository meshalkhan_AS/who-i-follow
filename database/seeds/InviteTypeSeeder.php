<?php

use Illuminate\Database\Seeder;

class InviteTypeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \App\Models\Admin\InviteType::insert([
            [
                'type' => 'email_register'
            ],
            [
                'type' => 'phone_register'
            ],
            [
                'type' => 'individual_chat'
            ],
            [
                'type' => 'open_chat'
            ],
        ]);
    }
}
