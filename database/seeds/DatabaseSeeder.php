<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        $this->call(RoleSeed::class);
        $this->call(PermissionSeed::class);
        $this->call(UserSeed::class);
        $this->call(SportSeeder::class);
        $this->call(InviteTypeSeeder::class);

        /*
         * Whatever happens with the world
         * Let this seeder at the last of seeders list for the admin
         *
         * */
        $this->call(AdminPermissionSeeder::class);
    }
}
