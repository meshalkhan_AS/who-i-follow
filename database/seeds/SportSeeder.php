<?php

use Illuminate\Database\Seeder;

class SportSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        \App\Models\Admin\Sport::insert([
            [
                'name' => 'American Football'
            ],
            [
                'name' => 'Baseball'
            ],
            [
                'name' => 'Basketball'
            ],
            [
                'name' => 'Ice Hockey'
            ],
            [
                'name' => 'Soccer'
            ],
            [
                'name' => 'Tennis'
            ],
            [
                'name' => 'Golf'
            ],
            [
                'name' => 'Wrestling'
            ],
            [
                'name' => 'Motor Sports'
            ],
            [
                'name' => 'Badminton'
            ]
        ]);
    }
}