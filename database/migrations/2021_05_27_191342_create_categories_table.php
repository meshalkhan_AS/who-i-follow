<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use App\Category;


class CreateCategoriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('categories', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name')->nullable();
            $table->string('slug')->nullable();
            $table->integer('parent_id')->nullable();
            $table->text('path')->nullable();
            $table->enum('is_active',['active','in-active'])->default('active');
            $table->boolean('is_archive')->default(0);
            $table->timestamps();
            $table->softDeletes();
        });

        $b = Category::create([
            'name' => "MAN'S",
            'parent_id' => 0,
            'slug' => "MAN-S"
        ]);

        $b = Category::create([
            'name' => "CHILDREN'S",
            'parent_id' => 0,
            'slug' => "CHILDREN-S"
            
        ]);

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('categories');
    }
}
