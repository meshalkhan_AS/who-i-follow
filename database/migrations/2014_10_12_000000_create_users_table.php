<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('email')->nullable();
            $table->integer('mob')->nullable();
            $table->string('user_link')->nullable();
            $table->unsignedTinyInteger('role_id')->default(0);
            $table->string('password')->nullable();
            $table->string('remember_token')->nullable();
            $table->string('device_token')->nullable();
            $table->unsignedTinyInteger('active')->default(0);
            $table->text('permissions')->nullable();
            $table->string('level')->nullable();
            $table->string('is_admin')->nullable();
            $table->string('weixin_session_key')->nullable();
            $table->unsignedInteger('created_by')->nullable();
            $table->unsignedInteger('updated_by')->nullable();
            $table->unsignedInteger('deleted_by')->nullable();

            $table->timestamps();
            $table->softDeletes();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
