<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name')->nullable();
            $table->float('price')->nullable();
            $table->string('image')->nullable();
            $table->string('is_archieve')->nullable();
            $table->string('vendor_name')->nullable();
            $table->text('product_description')->nullable();
            $table->integer('category_id')->nullable();
            $table->string('vendor_comission')->nullable();
            $table->boolean('is_archive')->default(0);
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('products');
    }
}
