<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAdvertisementStatsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('advertisement_stats', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedInteger('advertisement_banner_id');
            $table->unsignedInteger('advertisement_category_id');
            $table->unsignedInteger('user_id');
            $table->string('action_type');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('advertisement_stats');
    }
}
