<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMessageSmsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('message_sms', function (Blueprint $table) {
            $table->increments('id');
            $table->string('group_id')->nullable();
            $table->string('group_name')->nullable();
            $table->string('number')->nullable();
            $table->string('last_count')->nullable();
            $table->string('unread')->nullable();
            $table->string('user_id')->nullable();
            $table->string('date')->nullable();
            $table->string('notify')->nullable();
            $table->unsignedInteger('created_by')->nullable();
            $table->unsignedInteger('updated_by')->nullable();
            $table->unsignedInteger('deleted_by')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('message_sms');
    }
}
