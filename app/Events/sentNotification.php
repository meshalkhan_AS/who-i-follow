<?php

namespace App\Events;

use App\Models\Admin\UserNotification;
use App\Models\Admin\UserProfile;
use App\User;
use Illuminate\Queue\SerializesModels;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;
use Auth;

class sentNotification implements ShouldBroadcast
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    public $user_id;
    public $user_link;
    public $table_id;
    public $notification_id;
    public $message;
    public $username;
    public $counter;
    public $send_by;
    public $send_by_id;
    public $profile_exists;
    public $time;
    public $url;
    public $platform;

    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct($user_id, $message_type,$table_id)
    {

//        Receiver
        $user_receiver=User::findOrFail($user_id);
//      User data
        $user=UserProfile::select('user_profiles.*','users.user_link as user_link')
            ->join('users', 'users.id', '=', 'user_profiles.user_id')
            ->where('user_profiles.user_id',Auth::user()->id)->first();

        $username = $user['fname'].' '.$user['lname'];
        $current_date_time = \Carbon\Carbon::now();

        if($message_type=="followed")
        {
            $message = "followed you.";
            $url=route('user.followers',$user_receiver['user_link']);
        }
        else if($message_type=="unfollowed")
        {
            $message  = "unfollowed you.";
            $url=route('user.followers',$user_receiver['user_link']);
        }
        else if($message_type=="sentFriendRequest")
        {
            $message  = "sent you friend request.";
            $url=route('user.friends',['friendrequest'=>'yes','id'=>$user_receiver['user_link']]);
        }
        else if($message_type=="confirmFriendRequest")
        {
            $message  = "confirmed your friend request.";
            $url=route('user.friends',$user_receiver['user_link']);
        }
        else if($message_type=="addPost")
        {
            $message  = "added a new post.";
            $url=route('user.news',['random'=>$table_id]);
        }
        else if($message_type=="joinedByInvite")
        {
            $message  = "joined Who I follow and has been added to your friend list.";
            $url=route('user.friends',$user_receiver['user_link']);
        }
        else if($message_type=="AddedAsFriend")
        {
            $message  = "has been added to your friend list.";
            $url=route('user.friends',$user_receiver['user_link']);
        }
        else
        {
            $message  = "";
            $url="";
        }


//       Create Notification
        $notification_id=UserNotification::create([
            'user_id'=>$user_id,
            'send_by'=>Auth::user()->id,
            'message_type'=>$message_type,
            'message'=>$message,
            'table_id'=>$table_id,
            'created_at'=>$current_date_time,
        ])->id;

        //    Return
        $this->table_id=$table_id;
        $this->notification_id=$notification_id;
        $this->message=$message;
        $this->username=$username;
        $this->user_id = $user_id;
        $this->user_link = $user_id;
        $this->counter = '1';
        $this->send_by = $user['user_link'];
        $this->send_by_id = $user['user_id'];
        $this->profile_exists=$user['picture'];
        $this->time=date("Y-m-d H:i:s", strtotime('+5 hours', strtotime($current_date_time)));
        $this->url=$url;
        $this->platform=env('NOTIFICATION_PLATFORM');

    }




    /**
     * Get the channels the event should broadcast on.
     *
     * @return Channel|array
     */
    public function broadcastOn()
    {
        return ['notification'];
    }
}
