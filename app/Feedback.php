<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class feedback extends Model
{
    use SoftDeletes;
    protected $fillable = ['id',
        'discription',
        'user_id',
        'feed_type',
        'status',
        'created_by','updated_by','deleted_by'
    ];
    protected $table = 'feedback';
}
