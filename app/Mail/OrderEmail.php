<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class OrderEmail extends Mailable
{
    use Queueable, SerializesModels;
public $pp_id;
public $carttotal;
public $discount;
public $cartcontent;
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($data,$pp_id,$carttotal,$discount,$cartcontent)
    {
        //
        $this->data=$data;
        $this->pp_id=$pp_id;
        $this->carttotal=$carttotal;
        $this->discount=$discount;
        $this->cartcontent=$cartcontent;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        /**
         * Replace the "from" field with your valid sender email address.
         * The "email-template" is the name of the file present inside
         * "resources/views" folder. If you don't have this file, then
         * create it.
         */
        return $this->from("noreply@whoifollow.com")->view('order-mail')->with('data', $this->data);
    }
}