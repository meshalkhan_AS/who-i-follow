<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class ReturnsMail extends Mailable
{
    use Queueable, SerializesModels;
    public $details;
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($details)
    {
        $this->details=$details;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
     {
//        $path=$this->details['path'];
//        $subject=$this->details['subject'];
//       // dd($subject);
//        return $this->subject($subject)->view('emails.returns')->attach(public_path($path));

//        return $this->from('mail@example.com', 'Mailtrap')
//            ->subject('Mailtrap Confirmation')
////            ->markdown('mails.exmpl')
//            ->with([
//                'name' => 'New Mailtrap User',
//                'link' => 'https://mailtrap.io/inboxes'
//            ]);
    }
}
