<?php

namespace App\Helpers;

use App\User;
use App\Category;
use App\Product;
use Config;
use Gate;
use Illuminate\Support\Facades\Auth;
use Mail;

/**
* Class to store the entire group tree
*/
class StoreHelper
{
/**
* Initializer
*/

   public  function catgories()
   {
   	$category = Category::where('parent_id',0)->where('is_archive',0)->get();
    return $category;
   }

   public function topCategories()
   {
   		$catgories = Category::where('is_archive',0)->where('parent_id','!=',0)->get();
   		return $catgories;
   }

   public function lastProducts()
   {
   	 $lastProducts =  Product::where('is_archive',0)->take(15)->get();
   	 return $lastProducts;
   }

}