<?php


namespace App\Models\Admin;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Invite extends Model
{
    use SoftDeletes;
    protected $fillable = ['id',
        'invite_type',
        'user_link',
        'c_id',
        'group_id',
        'group_name',
        'group_image',
        'target',
        'level',
        'status',
        'created_by','updated_by','deleted_by'
    ];
    protected $table = 'invites';
}
