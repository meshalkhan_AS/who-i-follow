<?php

namespace App\Models\Admin;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class FriendList extends Model
{
    use SoftDeletes;
    protected $fillable = ['id',
        'friend_id',
        'user_id',
        'status',
        'created_by','updated_by','deleted_by'
    ];
    protected $table = 'friend_lists';

    public function user_friend()
    {
        return $this->belongsTo('App\Models\Admin\UserProfile', 'friend_id');
    }

    public function user()
    {
        return $this->belongsTo('App\Models\Admin\UserProfile', 'user_id','user_id');
    }
}
