<?php

namespace App\Models\Admin;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class PostImage extends Model
{
    use SoftDeletes;
    protected $fillable = ['id',
        'image',
        'post_id',
        'user_id',
        'status',
        'created_by','updated_by','deleted_by'
    ];
    protected $table = 'post_images';
}
