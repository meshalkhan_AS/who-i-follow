<?php

namespace App\Models\Admin;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Post extends Model
{
    use SoftDeletes;
    protected $fillable = ['id',
        'caption',
        'title',
        'privacy',
        'user_id',
        'exp_status',//expert status
        'status',
        'created_by','updated_by','deleted_by'
    ];
    protected $table = 'posts';
}
