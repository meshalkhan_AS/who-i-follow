<?php

namespace App\Models\Admin;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Sport extends Model
{
    use SoftDeletes;
    protected $fillable = ['id',
        'name',
        'type',
        'status',
        'created_by','updated_by','deleted_by'
    ];
    protected $table = 'sports';
}
