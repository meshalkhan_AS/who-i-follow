<?php

namespace App\Models\Admin;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class UserProfile extends Model
{
    use SoftDeletes;
    protected $fillable = ['id',
        'user_id',
        'picture',
        'coverphoto',
        'fname',
        'lname',
        'email',
        'password',
        'cpassword',
        'dob',
        'sports_id',
        'policy',
        'age',
        'bio',
        'country',
        'state',
        'zip_code',
        'city',
        'hometown',
        'gender',
        'religion',
        'exp_status', //status for expert 
        'default_tab', //status for tabs
        'mob',
        'status', 'created_by', 'updated_by', 'deleted_by'
    ];
    protected $table = 'user_profiles';

    public function user()
    {
        return $this->belongsTo('App\User', 'user_id');
    }
}