<?php

namespace App\Models\Admin;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Education extends Model
{
    use SoftDeletes;
    protected $fillable = ['id',
        'degree',
        'university',
        'start_date',
        'start_month',
        'start_year',
        'end_date',
        'end_month',
        'end_year',
        'user_id',
        'status',
        'created_by','updated_by','deleted_by'
    ];
    protected $table = 'educations';
}
