<?php

namespace App\Models\Admin;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class KeyType extends Model
{
    use SoftDeletes;
    protected $fillable = ['id',
        'type',
        'created_by','updated_by','deleted_by'
    ];
    protected $table = 'key_types';


}
