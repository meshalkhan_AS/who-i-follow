<?php

namespace App\Models\Admin;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class PostVideo extends Model
{
    use SoftDeletes;
    protected $fillable = ['id',
        'video',
        'post_id',
        'user_id',
        'status',
        'created_by','updated_by','deleted_by'
    ];
    protected $table = 'post_videos';
}
