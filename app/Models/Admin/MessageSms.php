<?php

namespace App\Models\Admin;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class MessageSms extends Model
{
    use SoftDeletes;
    protected $fillable = ['id',
        'group_id',
        'group_name',
        'number',
        'last_count',
        'unread',
        'user_id',
        'notify',
        'date',
        'created_by','updated_by','deleted_by'
    ];
    protected $table = 'message_sms';
}
