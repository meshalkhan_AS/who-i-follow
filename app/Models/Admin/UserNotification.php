<?php

namespace App\Models\Admin;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class UserNotification extends Model
{
    use SoftDeletes;
    protected $fillable = ['id',
        'user_id',
        'table_id',
        'send_by',
        'message_type',
        'message',
        'read',
        'checked',
        'created_by','updated_by','deleted_by'
    ];
    protected $table = 'user_notifications';
}
