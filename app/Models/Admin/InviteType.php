<?php


namespace App\Models\Admin;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class InviteType extends Model
{
    use SoftDeletes;
    protected $fillable = ['id',
        'type',
        'status',
        'created_by','updated_by','deleted_by'
    ];
    protected $table = 'invite_types';
}
