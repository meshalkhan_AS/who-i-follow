<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AdvertisementBanner extends Model
{

	/**
	 * [$fillable description]
	 * @var [type]
	 */
	protected $fillable = [
        'category_id',
        'title',
        'link',
        'sort',
        'desc',
        'is_featured',
        'is_visible',
        'image',
    ];

    protected static function boot()
    {
        parent::boot();

//        static::created(function ($banner) {
//            $banner->link = $banner->createLink($banner->title);
//            $banner->save();
//        });
    }


    /**
     * [user description]
     * @return [type] [description]
     */
    public function user()
    {
        return $this->belongsTo('App\User');
    }

    /**
     * [categories description]
     * @return [type] [description]
     */
    public function category()
    {
    	return $this->belongsTo('App\AdvertisementCategory', 'category_id');
    }


    private function createLink($title){
        if (static::whereLink($slug = \Str::slug($title))->exists()) {
            $max = static::whereTitle($title)->latest('id')->skip(1)->value('link');
            // dd($title);

            if (is_numeric($max[-1])) {
                return preg_replace_callback('/(\d+)$/', function ($mathces) {
                    return $mathces[1] + 1;
                }, $max);
            }

            return "{$slug}-2";
        }

        return $slug;
    }

}
