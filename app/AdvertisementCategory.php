<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AdvertisementCategory extends Model
{

	/**
	 * [$fillable description]
	 * @var [type]
	 */
    protected $fillable = [
        'name',
        'is_visible',
    ];

    /**
     * [user description]
     * @return [type] [description]
     */
    public function user()
    {
        return $this->belongsTo('App\User');
    }

    /**
     * [banners description]
     * @return [type] [description]
     */
	public function banners()
	{
    	return $this->hasMany('App\AdvertisementBanner', 'category_id', 'id');
    }

}
