<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Product extends Model
{
	use SoftDeletes;
    //
	protected $guarded = [];

	public function category()
	{
		return $this->belongsTo('App\Category');
	}

	public function gallery()
    {
        return $this->morphMany('App\Gallery', 'galleryable');
    }
    
}
