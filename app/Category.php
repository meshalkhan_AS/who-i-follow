<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;


class Category extends Model
{
	use SoftDeletes;
	protected $guarded = [];
    //

    public function children()
	{
	    return $this->hasMany(Category::class, 'parent_id');
	}

    public function parent()
	{
	    return $this->belongsTo(Category::class, 'parent_id');
	}

	public function product()
	{
	    return $this->hasMany('App\Product', 'category_id');
	}

	  // this is the recommended way for declaring event handlers
    public static function boot() {

        parent::boot();
        self::deleting(function($category) { 
        	// delete all parent categories and sub categories with products
             $category->children()->each(function($children) {
             	$children->product()->each(function($product) {
	                $product->delete(); 
	             });
                $children->delete(); // <-- direct deletion
             });

             // delete sub categories with products
             $category->product()->each(function($product) {
	                $product->delete(); 
	         });

        });
    }

}
