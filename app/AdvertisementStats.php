<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AdvertisementStats extends Model
{

	/**
	 * [$fillable description]
	 * @var [type]
	 */
	protected $fillable = [
        'advertisement_banner_id',
        'advertisement_category_id',
        'user_id',
        'action_type'
    ];

    /**
     * [user description]
     * @return [type] [description]
     */
    public function user()
    {
        return $this->belongsTo('App\User');
    }

    /**
     * [categories description]
     * @return [type] [description]
     */
    public function category()
    {
    	return $this->belongsTo('App\AdvertisementCategory', 'advertisement_category_id');
    }

    /**
     * [Banners description]
     * @return [type] [description]
     */
    public function banner()
    {
        return $this->belongsTo('App\AdvertisementBanner', 'advertisement_banner_id');
    }
}
