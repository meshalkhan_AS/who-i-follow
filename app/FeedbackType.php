<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class FeedbackType extends Model
{
    use SoftDeletes;
    protected $fillable = ['id',
        'type',
        'status',
        'created_by','updated_by','deleted_by'
    ];
    protected $table = 'feedback_types';
}
