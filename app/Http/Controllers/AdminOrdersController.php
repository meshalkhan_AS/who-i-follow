<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Storage;
use App\Category;
use App\Product;
use App\Order;
use Auth;

class AdminOrdersController extends Controller
{

    // folder path
    public $path = "store.admin.orders.";
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        if(Auth::user()->id!=1)
        {
            return redirect()->route('login');
        }
        $data['title'] = "Orders";
        $data['cols'] = [
            // 'Id',
            'User name',
            'Invoice id',
            'Billing name',
            'Billing email',
            'Billing phone',
            'Billing address',
            'Billing zip',
            // 'View',
            'Status'

        ];

        // if(!is_null($order->ordered_items)){
        //     $ordered_items=unserialize(base64_decode($order->ordered_items));
        // }

        $data['datas'] = Order::orderBy('id','desc')->get();

        return view($this->path.'list',$data);

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        if(Auth::user()->id!=1)
        {
            return redirect()->route('login');
        }
        $data['title'] = "Add Product";
        $data['cats'] = Category::orderBy('id','desc')
        ->where('parent_id','=',0)
        ->where('is_archive','=',0)
        ->get();

        // load view
        return view($this->path.'add',$data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if(Auth::user()->id!=1)
        {
            return redirect()->route('login');
        }

        $validated = $request->validate([
            'product_name'  => 'required',
            'price'  => 'required|regex:/^\d+(\.\d{1,2})?$/',
            'vendor_name'   => 'required',
            'product_description' => 'required',
            'category'            => 'required',
            'vendor_comission'    => 'required',
        ]);

        $input = $request->except(['product_name','category','_token']);

        $folder = "public/uploads/products-image/";
         // check if image set
        if ($request->file('product_image')) {

            unset($input['product_image']);
            $image      = $request->file('product_image');
            $image_name = time().'.'.$image->getClientOriginalExtension();
            $image->move($folder, $image_name);
            $input['image'] = $folder.$image_name;

        }


        $input['name']        = $request->product_name;
        $input['category_id'] = $request->category;

        // return $input;
        Product::create($input);

        return redirect(route('admin.product.index'))->with('message', 'Product added successfully!');
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        if(Auth::user()->id!=1)
        {
            return redirect()->route('login');
        }
        $data['title'] = "Orders";
        $data['product_detail'] = Order::find($id);
        //
        return view($this->path.'view',$data);

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        if(Auth::user()->id!=1)
        {
            return redirect()->route('login');
        }
        $data['title'] = "Edit Product";
        $data['data'] = Product::find($id);
        $data['cats'] = Category::where('parent_id',0)
        ->where('is_archive','=',0)
        ->orderBy('id','desc')
        ->get();


        return view($this->path.'edit',$data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

        $validated = $request->validate([
            'product_name'  => 'required',
            'vendor_name'   => 'required',
            'product_description' => 'required',
            'category'            => 'required',
            'vendor_comission'    => 'required',
            'price'    => 'required',
        ]);


        $input = $request->except(['product_name','category','_token']);

        $folder = "public/uploads/products-image/";
         // check if image set
        if ($request->file('product_image')) {
            $product = Product::find($id);

            if (file_exists($product->image)) {
                unlink($product->image);
            }


            unset($input['product_image']);
            $image      = $request->file('product_image');
            $image_name = time().'.'.$image->getClientOriginalExtension();
            $image->move($folder, $image_name);
            $input['image'] = $folder.$image_name;

        }

        if ($request->category != "Select") {
            $input['category_id'] = $request->category;
        }

            // dd($input);

        $input['name']        = $request->product_name;
        Product::find($id)->update($input);

        return redirect(route('admin.product.index'))->with('message', 'Product updated successfully!');

        //
        dd($request->all(),$id);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if(Auth::user()->id!=1)
        {
            return redirect()->route('login');
        }
        $product = Product::find($id);

        if (file_exists($product->image)) {
            unlink($product->image);
        }
        
        Product::find($id)->delete();
        return redirect(route('admin.product.index'))->with('message', 'Product delted successfully!');
    }

    public function ordersStatusChange(Request $request)
    {
        if(Auth::user()->id!=1)
        {
            return redirect()->route('login');
        }
        $order = Order::find($request->order_id);
        $order->status = $request->order_status;
        $order->save();
        return "Order status chnaged successfully";
    }
}
