<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Product;
use Cart;

class FrontCartController extends Controller
{
    
    public function cart(Request $request)
    {
         $product = Product::find($request->product_id);

         if (empty($product)) {

             return response()->json([
                    'status' => false,
                    'data'    => NULL,
                    'message' => 'failed',
                    'code'    => 422
                ], 422);
         }

         $checkCart = Cart::get($product->id);

         if (!empty($checkCart)) {

            if ($request->input_value) {
                $updateArray = [
                    'quantity' => array(
                      'relative' => false,
                      'value' => $request->qty
                  )
                ];
            }else{

            $updateArray = [
                'quantity' => $request->qty
            ];

            }

            Cart::update($product->id, $updateArray);

            

         }else{
            $imagess =  file_exists($product->image) ? asset($product->image) : asset("public/front/images/placeholder.png");
            Cart::add(array(
                'id' => $product->id, // inique row ID
                'name' => $product->name,
                'price' => $product->price,
                'quantity' => $request->qty,
                'attributes' => array('image'=> $imagess)
            ));
         }

         return response()->json([
                    'status' => true,
                    'data'    => NULL,
                    'message' => 'success',
                    'code'    => 200
                ], 200);

         
        
    }

    public function getCartById(Request $request)
    {
        $checkCart = Cart::get($request->product_id);

        return response()->json([
                    'status' => true,
                    'data'    => $checkCart,
                    'message' => 'success',
                    'code'    => 200
                ], 200);

        return $checkCart;
    }

    public function getCart()
    {
        $data['cat_content'] =  Cart::getContent();
        $data['total_count'] =  Cart::getTotalQuantity();
        $data['total_price'] =  Cart::getTotal();

        return response()->json([
                    'status' => true,
                    'data'    => $data,
                    'message' => 'success',
                    'code'    => 200
                ], 200);
    }


    public function removeCart(Request $request)
    {
        Cart::remove($request->product_id);

        return response()->json([
                    'status' => true,
                    'data'    => NULL,
                    'message' => 'success',
                    'code'    => 200
                ], 200);
    }


}
