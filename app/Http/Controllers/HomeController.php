<?php

namespace App\Http\Controllers;

use App\Models\Admin\Follower;
use App\Models\Admin\FriendList;
use App\Models\Admin\Post;
use App\Models\Admin\Sport;
use App\Models\Admin\UserProfile;
use Illuminate\Http\Request;
use App\User;
use Auth;
use App\Helpers\GeneralHelper;
use App\AdvertisementBanner;
use App\AdvertisementStats;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Storage;

class HomeController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index($user_link,Request $request)
    {
        if(Auth::user()->id==1)
        {
            return redirect()->route('admin.product.index');
        }
        if($user_link=='login')
        {
            Auth::logout();
            return redirect()->route('login');
        }
        if($user_link=='register')
        {
            Auth::logout();
            return redirect()->route('register');
        }
        if($user_link=='home')
        {
            Auth::logout();
            return redirect()->route('login');
        }

        if($user_link=='shop')
        {
            Auth::logout();
            return view('store.front.index');
        }
        if($request->random)
        {
            $random_post=$request->random;
        }
        else
        {
            $random_post=0;
        }
        if($user_link!=Auth::user()->user_link)
        {
            
            $user=User::where('user_link',$user_link)->first();
            if($user)
            {
                
                $id=$user['id'];
                $userprofile = UserProfile::where('user_id',$id)->first();
                $myfriends=FriendList::select('friend_lists.*','friend_lists.friend_id as user_profile',
                    'user_profiles.city as city',
                    'user_profiles.country as country',
                    'user_profiles.picture as picture',
                    'user_profiles.fname as fname',
                    'user_profiles.lname as lname','users.user_link as user_link')
                    ->join('users', 'users.id', '=', 'friend_lists.friend_id')
                    ->join('user_profiles', 'friend_lists.friend_id', '=', 'user_profiles.user_id')
                    ->where('friend_lists.user_id',$id)
                    ->where('friend_lists.status','1')
                    ->get()->toArray();

                $ifriends=FriendList::select('friend_lists.*','friend_lists.user_id as user_profile',
                    'user_profiles.city as city',
                    'user_profiles.country as country',
                    'user_profiles.picture as picture',
                    'user_profiles.fname as fname',
                    'user_profiles.lname as lname','users.user_link as user_link')
                    ->join('users', 'users.id', '=', 'friend_lists.user_id')
                    ->join('user_profiles', 'friend_lists.user_id', '=', 'user_profiles.user_id')
                    ->where('friend_lists.friend_id',$id)
                    ->where('friend_lists.status','1')
                    ->get()->toArray();

                $friends_merge=array_merge($myfriends,$ifriends);
                $friendsCount=(count($friends_merge)>0)?count($friends_merge):'';
                $friends = array_slice($friends_merge, 0, 5, true);

                $followers =Follower::where('follower_id',$id)->get();
                $following =Follower::where('user_id',$id)->get();

                $posts =Post::select('posts.*',
                    'user_profiles.picture as propicture',
                    'user_profiles.fname as fname',
                    'user_profiles.lname as lname','users.user_link as user_link',
                    'user_profiles.user_id as user_id')
                    ->orderBy('created_at','desc')
                    ->where('posts.user_id',$id)->where('posts.id','!=',$random_post)
                    ->join('user_profiles', 'user_profiles.user_id', '=', 'posts.user_id')
                    ->join('users', 'users.id', '=', 'posts.user_id')
                    ->paginate(3);

                //get random ad for news feed
                $ads = GeneralHelper::getRandomAdBanner(Auth::user()->id);
                if ($request->ajax()) {
                    $view = view('sports.lists.profilepost',compact('userprofile','posts','ads','random_post'))->render();
                    return response()->json(['html'=>$view]);
                }
                else
                {
                    
                    return view('sports.userprofile',compact('userprofile','random_post','friends','friendsCount','followers','ads','following','posts','id','user_link'));
                }
            }
            else
            {
                return redirect()->route('login');
            }

        }
        else
        {
            $userprofile = UserProfile::where('user_id',Auth::user()->id)->first();

            $myfriends=FriendList::select('friend_lists.*','friend_lists.friend_id as user_profile',
                'user_profiles.city as city',
                'user_profiles.user_id as user_id',
                'user_profiles.country as country',
                'user_profiles.picture as picture',
                'user_profiles.fname as fname',
                'user_profiles.lname as lname','users.user_link as user_link')
                ->join('users', 'friend_lists.friend_id', '=', 'users.id')
                ->join('user_profiles', 'friend_lists.friend_id', '=', 'user_profiles.user_id')
                ->where('friend_lists.user_id',Auth::user()->id)
                ->where('friend_lists.status','1')
                ->get()->toArray();

            $ifriends=FriendList::select('friend_lists.*','friend_lists.user_id as user_profile',
                'user_profiles.city as city',
                'user_profiles.user_id as user_id',
                'user_profiles.country as country',
                'user_profiles.picture as picture',
                'user_profiles.fname as fname',
                'user_profiles.lname as lname','users.user_link as user_link')
                ->join('user_profiles', 'friend_lists.user_id', '=', 'user_profiles.user_id')
                ->join('users', 'friend_lists.user_id', '=', 'users.id')
                ->where('friend_lists.friend_id',Auth::user()->id)
                ->where('friend_lists.status','1')
                ->get()->toArray();

            $friends_merge=array_merge($myfriends,$ifriends);
            $friendsCount=(count($friends_merge)>0)?count($friends_merge):'';
            $friends = array_slice($friends_merge, 0, 5, true);

            $followerslist =Follower::where('follower_id',Auth::user()->id)->get();
            $followers=count($followerslist);
            $followinglist =Follower::where('user_id',Auth::user()->id)->get();
            $following=count($followinglist);
            $posts =Post::select('posts.*',
                'user_profiles.picture as propicture',
                'user_profiles.fname as fname',
                'user_profiles.lname as lname',
                'user_profiles.user_id as user_id','users.user_link as user_link')
                ->orderBy('created_at','desc')
                ->where('posts.user_id',Auth::user()->id)->where('posts.id','!=',$random_post)
                ->join('user_profiles', 'user_profiles.user_id', '=', 'posts.user_id')
                ->join('users', 'users.id', '=', 'posts.user_id')
                ->paginate(3);
            $sports=array();

            if($userprofile)
            {
                if($userprofile['sports_id']!='')
                {
                    $sports_=explode(",",$userprofile['sports_id']);
                    foreach ($sports_ as $sport)
                    {
                        $sport_name =Sport::where('id',$sport)->first();
                        array_push($sports,$sport_name['name']);
                    }
                }
            }

            //get random ad for news feed
            $ads = GeneralHelper::getRandomAdBanner(Auth::user()->id);

            if ($request->ajax()) {
                $view = view('sports.lists.opinion',compact('userprofile','posts','ads'))->render();
                return response()->json([
                    'html'=>$view,
                    'count'=>count($posts)
                ]);
            }
            else
            {
                //get random ad for news feed
                $ads = GeneralHelper::getRandomAdBanner(Auth::user()->id);
                $data=array();
                return view('sports.index',compact('userprofile','random_post','friends','friendsCount','followers','following','sports','posts', 'ads','data'));
            }
        }
    }

    public function admin_token($token){
        $user_id = Auth::user()->id;
        User::where('id', $user_id)->update(['device_token' => $token ]);
        return response()->json( ['result' => true] );
    }

    public function search(Request $request)
    {
        $search=$request->search;

        $posts=array();
        $allposts=array();

        $search= preg_replace('/[^A-Za-z0-9\-]/', ' ', $search);

        if(str_word_count($search)>1)
        {
            $arr=explode(' ',$search);
            $first=$arr[0];
            $last=$arr[1];
            $profiles=UserProfile::select('user_profiles.*','users.user_link as user_link')
                ->join('users', 'users.id', '=', 'user_profiles.user_id')
                ->where('user_profiles.fname', 'LIKE',"%{$first}%")
                ->orwhere('user_profiles.fname', 'LIKE',"%{$last}%")
                ->orwhere('user_profiles.lname', 'LIKE',"%{$first}%")
                ->orwhere('user_profiles.lname', 'LIKE',"%{$last}%")->paginate(5);
            $allprofiles=UserProfile::select('user_profiles.*','users.user_link as user_link')
                ->join('users', 'users.id', '=', 'user_profiles.user_id')
                ->where('user_profiles.fname', 'LIKE',"%{$first}%")
                ->orwhere('user_profiles.fname', 'LIKE',"%{$last}%")
                ->orwhere('user_profiles.lname', 'LIKE',"%{$first}%")
                ->orwhere('user_profiles.lname', 'LIKE',"%{$last}%")->get();
            $user_id = $allprofiles->pluck('user_id');

            if(count($user_id)>0)
            {
                $posts = Post::select('posts.*',
                    'user_profiles.picture as propicture',
                    'user_profiles.fname as fname',
                    'user_profiles.lname as lname',
                    'user_profiles.user_id as user_id', 'users.user_link as user_link')
                    ->orderBy('created_at', 'desc')
                    ->join('users', 'users.id', '=', 'posts.user_id')
                    ->join('user_profiles', 'user_profiles.user_id', '=', 'posts.user_id')
                    ->where('posts.caption', 'LIKE', "%{$search}%")
                    ->orwhere('posts.user_id', $user_id)
                    ->orwhere('user_profiles.fname', 'LIKE',"%{$first}%")
                    ->orwhere('user_profiles.fname', 'LIKE',"%{$last}%")
                    ->orwhere('user_profiles.lname', 'LIKE',"%{$first}%")
                    ->orwhere('user_profiles.lname', 'LIKE',"%{$last}%")
                    ->orwhere('posts.title', 'LIKE', "%{$search}%")->get();

                $allposts = Post::select('posts.*',
                    'user_profiles.picture as propicture',
                    'user_profiles.fname as fname',
                    'user_profiles.lname as lname',
                    'user_profiles.user_id as user_id', 'users.user_link as user_link')
                    ->orderBy('created_at', 'desc')
                    ->join('users', 'users.id', '=', 'posts.user_id')
                    ->join('user_profiles', 'user_profiles.user_id', '=', 'posts.user_id')
                    ->where('posts.caption', 'LIKE', "%{$search}%")
                    ->orwhere('user_profiles.fname', 'LIKE',"%{$first}%")
                    ->orwhere('user_profiles.fname', 'LIKE',"%{$last}%")
                    ->orwhere('user_profiles.lname', 'LIKE',"%{$first}%")
                    ->orwhere('user_profiles.lname', 'LIKE',"%{$last}%")
                    ->orwhere('posts.user_id', $user_id)
                    ->orwhere('posts.title', 'LIKE', "%{$search}%")->get();
            }
            else
            {
                $posts = Post::select('posts.*',
                    'user_profiles.picture as propicture',
                    'user_profiles.fname as fname',
                    'user_profiles.lname as lname',
                    'user_profiles.user_id as user_id', 'users.user_link as user_link')
                    ->orderBy('created_at', 'desc')
                    ->join('users', 'users.id', '=', 'posts.user_id')
                    ->join('user_profiles', 'user_profiles.user_id', '=', 'posts.user_id')
                    ->where('posts.caption', 'LIKE', "%{$search}%")
                    ->orwhere('user_profiles.fname', 'LIKE',"%{$first}%")
                    ->orwhere('user_profiles.fname', 'LIKE',"%{$last}%")
                    ->orwhere('user_profiles.lname', 'LIKE',"%{$first}%")
                    ->orwhere('user_profiles.lname', 'LIKE',"%{$last}%")
                    ->orwhere('posts.title', 'LIKE', "%{$search}%")->get();

                $allposts = Post::select('posts.*',
                    'user_profiles.picture as propicture',
                    'user_profiles.fname as fname',
                    'user_profiles.lname as lname',
                    'user_profiles.user_id as user_id', 'users.user_link as user_link')
                    ->orderBy('created_at', 'desc')
                    ->join('users', 'users.id', '=', 'posts.user_id')
                    ->join('user_profiles', 'user_profiles.user_id', '=', 'posts.user_id')
                    ->where('posts.caption', 'LIKE', "%{$search}%")
                    ->orwhere('user_profiles.fname', 'LIKE',"%{$first}%")
                    ->orwhere('user_profiles.fname', 'LIKE',"%{$last}%")
                    ->orwhere('user_profiles.lname', 'LIKE',"%{$first}%")
                    ->orwhere('user_profiles.lname', 'LIKE',"%{$last}%")
                    ->orwhere('posts.title', 'LIKE', "%{$search}%")->get();
            }
        }
        else
        {
            $profiles=UserProfile::select('user_profiles.*','users.user_link as user_link')
                ->join('users', 'users.id', '=', 'user_profiles.user_id')
                ->where('user_profiles.fname', 'LIKE',"%{$search}%")
                ->orwhere('user_profiles.lname', 'LIKE',"%{$search}%")->paginate(5);

            $allprofiles=UserProfile::select('user_profiles.*','users.user_link as user_link')
                ->join('users', 'users.id', '=', 'user_profiles.user_id')
                ->where('user_profiles.fname', 'LIKE',"%{$search}%")
                ->orwhere('user_profiles.lname', 'LIKE',"%{$search}%")->get();

            $user_id = $allprofiles->pluck('user_id');

            if(count($user_id)>0)
            {
                $posts =Post::select('posts.*',
                    'user_profiles.picture as propicture',
                    'user_profiles.fname as fname',
                    'user_profiles.lname as lname',
                    'user_profiles.user_id as user_id','users.user_link as user_link')
                    ->orderBy('created_at','desc')
                    ->join('users', 'users.id', '=', 'posts.user_id')
                    ->join('user_profiles', 'user_profiles.user_id', '=', 'posts.user_id')
                    ->where('posts.caption', 'LIKE',"%{$search}%")
                    ->orwhere('posts.user_id',$user_id)
                    ->orwhere('posts.title', 'LIKE',"%{$search}%")->get();


                $allposts =Post::select('posts.*',
                    'user_profiles.picture as propicture',
                    'user_profiles.fname as fname',
                    'user_profiles.lname as lname',
                    'user_profiles.user_id as user_id','users.user_link as user_link')
                    ->orderBy('created_at','desc')
                    ->join('users', 'users.id', '=', 'posts.user_id')
                    ->join('user_profiles', 'user_profiles.user_id', '=', 'posts.user_id')
                    ->where('posts.caption', 'LIKE',"%{$search}%")
                    ->orwhere('posts.user_id',$user_id)
                    ->orwhere('posts.title', 'LIKE',"%{$search}%")->get();
            }
            else
            {
                $posts =Post::select('posts.*',
                    'user_profiles.picture as propicture',
                    'user_profiles.fname as fname',
                    'user_profiles.lname as lname',
                    'user_profiles.user_id as user_id','users.user_link as user_link')
                    ->orderBy('created_at','desc')
                    ->join('users', 'users.id', '=', 'posts.user_id')
                    ->join('user_profiles', 'user_profiles.user_id', '=', 'posts.user_id')
                    ->where('posts.caption', 'LIKE',"%{$search}%")
                    ->orwhere('user_profiles.fname', 'LIKE',"%{$first}%")
                    ->orwhere('user_profiles.fname', 'LIKE',"%{$last}%")
                    ->orwhere('user_profiles.lname', 'LIKE',"%{$first}%")
                    ->orwhere('user_profiles.lname', 'LIKE',"%{$last}%")
                    ->orwhere('posts.title', 'LIKE',"%{$search}%")->get();


                $allposts =Post::select('posts.*',
                    'user_profiles.picture as propicture',
                    'user_profiles.fname as fname',
                    'user_profiles.lname as lname',
                    'user_profiles.user_id as user_id','users.user_link as user_link')
                    ->orderBy('created_at','desc')
                    ->join('users', 'users.id', '=', 'posts.user_id')
                    ->join('user_profiles', 'user_profiles.user_id', '=', 'posts.user_id')
                    ->where('posts.caption', 'LIKE',"%{$search}%")
                      ->orwhere('user_profiles.fname', 'LIKE',"%{$first}%")
                    ->orwhere('user_profiles.fname', 'LIKE',"%{$last}%")
                    ->orwhere('user_profiles.lname', 'LIKE',"%{$first}%")
                    ->orwhere('user_profiles.lname', 'LIKE',"%{$last}%")
                    ->orwhere('posts.title', 'LIKE',"%{$search}%")->get();
            }



        }
        $data=array();
        $userprofile = UserProfile::where('user_id',Auth::user()->id)->first();
        if ($request->ajax()) {
            $view = view('sports.profiles',compact('profiles','allprofiles','posts','allposts'))->render();
            return response()->json([
                'html'=>$view,
                'count'=>count($allprofiles)
            ]);
        }
        else
        {
            return view('sports.profiles', compact( 'profiles','search','allprofiles','posts','allposts','userprofile','data'));
        }
    }
    public function myopinion(Request $request)
    {
        if(Auth::user()->id==1)
        {
            Auth::logout();
            return redirect()->route('login');
        }
        if(!Auth::check())
        {
            return redirect()->route('login');
        }
        $myfriends=FriendList::select('user_profiles.user_id as friends_id')
            ->join('user_profiles', 'friend_lists.friend_id', '=', 'user_profiles.user_id')
            ->where('friend_lists.user_id',Auth::user()->id)
            ->where('friend_lists.status','1')
            ->get()->toArray();


        $ifriend=FriendList::select('user_profiles.user_id as friends_id')
            ->join('user_profiles', 'friend_lists.user_id', '=', 'user_profiles.user_id')
            ->where('friend_lists.friend_id',Auth::user()->id)
            ->where('friend_lists.status','1')
            ->get()->toArray();


        $friends_array=array_merge($myfriends,$ifriend);
        $following=Follower::select('user_profiles.user_id as friends_id')
            ->join('user_profiles', 'followers.follower_id', '=', 'user_profiles.user_id')
            ->where('followers.user_id',Auth::user()->id)
            ->where('followers.status','1')
            ->get()->toArray();

        $final_array=array_merge($friends_array,$following);
        $final_array=array_unique($final_array,SORT_REGULAR);
        array_push($final_array,Auth::user()->id);
        array_push($final_array,16);

        if($request->random)
        {
            $random_post=$request->random;
        }
        else
        {
            $random_post=0;
        }

        $posts =Post::select('posts.*',
            'user_profiles.picture as propicture',
            'user_profiles.fname as fname',
            'user_profiles.lname as lname',
            'user_profiles.user_id as user_id','users.user_link as user_link')
            ->orderBy('created_at','desc')
            ->whereIn('posts.user_id',$final_array)
            ->where('posts.id','!=',$random_post)->where('posts.exp_status', NULL)
            ->join('users', 'users.id', '=', 'posts.user_id')
            ->join('user_profiles', 'user_profiles.user_id', '=', 'posts.user_id')->paginate(3);
        $exp_posts =Post::select('posts.*',
            'user_profiles.picture as propicture',
            'user_profiles.fname as fname',
            'user_profiles.lname as lname',
            'user_profiles.user_id as user_id','users.user_link as user_link')
            ->orderBy('created_at','desc')
            ->where('posts.exp_status', 1)
            ->join('users', 'users.id', '=', 'posts.user_id')
            ->join('user_profiles', 'user_profiles.user_id', '=', 'posts.user_id')->get();
        $userprofile = UserProfile::where('user_id',Auth::user()->id)->first();
        //get random ad for news feed
        $ads = GeneralHelper::getRandomAdBanner(Auth::user()->id);
        $data=array();
        if ($request->ajax()) {
            $view = view('sports.lists.profilepost',compact('userprofile','posts','ads','data'))->render();
            return response()->json([
                'html'=>$view,
                'count'=>count($posts)
            ]);
        }
        else
        {

            return view('sports.news',compact('userprofile','posts', 'ads','data','random_post','exp_posts'));
        }
    }

    public function allexperts(Request $request)
    {

        if(Auth::user()->id==1)
        {
            Auth::logout();
            return redirect()->route('login');
        }
        if(!Auth::check())
        {
            return redirect()->route('login');
        }
        $myfriends=FriendList::select('user_profiles.user_id as friends_id')
            ->join('user_profiles', 'friend_lists.friend_id', '=', 'user_profiles.user_id')
            ->where('friend_lists.user_id',Auth::user()->id)
            ->where('friend_lists.status','1')
            ->get()->toArray();


        $ifriend=FriendList::select('user_profiles.user_id as friends_id')
            ->join('user_profiles', 'friend_lists.user_id', '=', 'user_profiles.user_id')
            ->where('friend_lists.friend_id',Auth::user()->id)
            ->where('friend_lists.status','1')
            ->get()->toArray();


        $friends_array=array_merge($myfriends,$ifriend);
        $following=Follower::select('user_profiles.user_id as friends_id')
            ->join('user_profiles', 'followers.follower_id', '=', 'user_profiles.user_id')
            ->where('followers.user_id',Auth::user()->id)
            ->where('followers.status','1')
            ->get()->toArray();

        $final_array=array_merge($friends_array,$following);
        $final_array=array_unique($final_array,SORT_REGULAR);
        array_push($final_array,Auth::user()->id);
        array_push($final_array,16);

        if($request->random)
        {
            $random_post=$request->random;
        }
        else
        {
            $random_post=0;
        }
        $posts =Post::select('posts.*',
            'user_profiles.picture as propicture',
            'user_profiles.fname as fname',
            'user_profiles.lname as lname',
            'user_profiles.user_id as user_id','users.user_link as user_link')
            ->orderBy('created_at','desc')
            ->where('posts.exp_status', 1)
            ->join('users', 'users.id', '=', 'posts.user_id')
            ->join('user_profiles', 'user_profiles.user_id', '=', 'posts.user_id')->paginate(5);
        $userprofile = UserProfile::where('user_id',Auth::user()->id)->first();
        //get random ad for news feed
        $ads = GeneralHelper::getRandomAdBanner(Auth::user()->id);
        $data=array();
        if ($request->ajax()) {
            $view = view('sports.lists.profilepost',compact('userprofile','posts','ads','data'))->render();
            return response()->json([
                'html'=>$view,
                'count'=>count($posts)
            ]);
        }
        else
        {

            return view('sports.expertposts',compact('userprofile', 'ads','data','random_post','posts'));
        }

    }

    public function adClicked(Request $request, $adID)
    {
        $banner = AdvertisementBanner::find($adID);

        if (!$banner) return redirect(route('user.news'));

        //create click
        AdvertisementStats::create([
            'advertisement_banner_id' => $banner->id,
            'advertisement_category_id' => $banner->category_id,
            'action_type' => 'click',
            'user_id' => Auth::user()->id
        ]);

        return redirect()->away($banner->link);
    }

    public function chatSystem(Request $request)
    {
        if(Auth::user()->id==1)
        {
            Auth::logout();
            return redirect()->route('login');
        }
        if(!Auth::check())
        {
            return redirect()->route('login');
        }
        $myfriends=FriendList::select('user_profiles.user_id as friends_id')
            ->join('user_profiles', 'friend_lists.friend_id', '=', 'user_profiles.user_id')
            ->where('friend_lists.user_id',Auth::user()->id)
            ->where('friend_lists.status','1')
            ->get()->toArray();


        $ifriend=FriendList::select('user_profiles.user_id as friends_id')
            ->join('user_profiles', 'friend_lists.user_id', '=', 'user_profiles.user_id')
            ->where('friend_lists.friend_id',Auth::user()->id)
            ->where('friend_lists.status','1')
            ->get()->toArray();


        $friends_array=array_merge($myfriends,$ifriend);
        $following=Follower::select('user_profiles.user_id as friends_id')
            ->join('user_profiles', 'followers.follower_id', '=', 'user_profiles.user_id')
            ->where('followers.user_id',Auth::user()->id)
            ->where('followers.status','1')
            ->get()->toArray();

        $final_array=array_merge($friends_array,$following);
        $final_array=array_unique($final_array,SORT_REGULAR);
        array_push($final_array,Auth::user()->id);
        array_push($final_array,16);

        if($request->random)
        {
            $random_post=$request->random;
        }
        else
        {
            $random_post=0;
        }

        $posts =Post::select('posts.*',
            'user_profiles.picture as propicture',
            'user_profiles.fname as fname',
            'user_profiles.lname as lname',
            'user_profiles.user_id as user_id','users.user_link as user_link')
            ->orderBy('created_at','desc')
            ->whereIn('posts.user_id',$final_array)
            ->where('posts.id','!=',$random_post)->where('posts.exp_status', NULL)
            ->join('users', 'users.id', '=', 'posts.user_id')
            ->join('user_profiles', 'user_profiles.user_id', '=', 'posts.user_id')->paginate(3);
        $exp_posts =Post::select('posts.*',
            'user_profiles.picture as propicture',
            'user_profiles.fname as fname',
            'user_profiles.lname as lname',
            'user_profiles.user_id as user_id','users.user_link as user_link')
            ->orderBy('created_at','desc')
            ->where('posts.exp_status', 1)
            ->join('users', 'users.id', '=', 'posts.user_id')
            ->join('user_profiles', 'user_profiles.user_id', '=', 'posts.user_id')->paginate(5);
        $userprofile = UserProfile::where('user_id',Auth::user()->id)->first();
        //get random ad for news feed
        $ads = GeneralHelper::getRandomAdBanner(Auth::user()->id);
        $data=array();
        if ($request->ajax()) {
            $view = view('newchat.teamexp',compact('userprofile','exp_posts','ads','data'))->render();
            return response()->json([
                'html'=>$view,
                'count'=>count($posts)
            ]);
        }
        else
        {

            return view('newchat.newchatindex',compact('userprofile','posts', 'ads','data','random_post','exp_posts'));
        }
        //get random ad for news feed

        return view('newchat.newchatindex');
    }

    public function newChatSystem(Request $request)
    {
        if(Auth::user()->id==1)
        {
            Auth::logout();
            return redirect()->route('login');
        }
        //get random ad for news feed

        return view('newchat.newchatindex');
    }

    public function uploadChatAttachment(Request $request)
    {
        //  Store File
        $picture = file_get_contents($request->attachment);
        $extension = $request->file('attachment')->getClientOriginalExtension();
        $name = $request->file('attachment')->getClientOriginalName();
        $name= preg_replace('/[^A-Za-z0-9\-]/', '_', $name);
        $dateTime = date('Ymd_His');
        $filename = uniqid().$name.".".$extension;
        $path='public/chat_attachment/';
        Storage::put($path. $filename, $picture);

        return response()->json([ 'path' => '/public/storage/chat_attachment/'.$filename ,'file_name'=>$name], 200);

    }

    public function uploadGroupAttachment(Request $request)
    {
        //  Store File
        $picture = file_get_contents($request['attachment']);
        $dateTime = date('Ymd_His');
        $filename = uniqid().$dateTime.".jpg";
        $path='public/chat_groups/';
        Storage::put($path. $filename, $picture);

        return response()->json([ 'path' => '/public/storage/chat_groups/'.$filename ], 200);
    }
}
