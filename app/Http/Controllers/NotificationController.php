<?php

namespace App\Http\Controllers;
use App\Models\Admin\UserNotification;
use Illuminate\Http\Request;
use Auth;
use App\User;

class NotificationController extends Controller
{
    public function saveToken(Request $request)
    {
        auth()->user()->update(['device_token'=>$request->token]);
        return response()->json(['token saved successfully.']);
    }
    public function index(){
        return view('sports.js.firebase');
    }

    public function read(Request $request)
    {
        UserNotification::where('id',$request->id)->update(['read'=>'0']);
    }

    public function checked(Request $request)
    {
        UserNotification::where('user_id',Auth::user()->id)->update(['checked'=>'0']);
    }

    public function view(Request $request)
    {
        if(Auth::user()->id==1)
        {
            Auth::logout();
            return redirect()->route('login');
        }
        $userNotifications=UserNotification::
        select('user_notifications.*','users.user_link as user_link')
            ->join('users', 'users.id', '=', 'user_notifications.user_id')
            ->orderBy('user_notifications.created_at','desc')->where('user_notifications.user_id',Auth::user()->id)->paginate(9);

        if ($request->ajax()) {
            $view = view('sports.lists.notification',compact('userNotifications'))->render();
            return response()->json([
                'html'=>$view,
                'count'=>count($userNotifications)
            ]);
        }
        else
        {

            return view('sports.notifications',compact('userNotifications'));
        }

    }

    public function sendNotification()
    {

//        Notification
$id = Auth::User()->id;
$gettoken = User::where('id',$id)->value('device_token');
        $token =$gettoken; 
        $from = "AAAA0vRHjcY:APA91bFlCuQE0r_c4yAh--sp53IIxkvwifGJY9UxS_gzCPvgEm0X7x_qGL1DUHQlk-ZmsWxWqfMzV_e-VEwIHqtiw3NqbBWMvi_dKWIYtnMymykWaVnpBwTLXughSEKAwFh5igLIFCf0";

        //API URL of FCM
        $url = 'https://fcm.googleapis.com/fcm/send';

        $fields = array (
            'registration_ids' => array (
                    $token
            ),
            'notification' => array (
                "title" => "test from server",
                "body" => "test lorem ipsum"
            )
        );

        //header includes Content type and api key
        $headers = array(
            'Content-Type:application/json',
            'Authorization:key='.$from
        );

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));
        $result = curl_exec($ch);
        if ($result === FALSE) {
            die('FCM Send Error: ' . curl_error($ch));
        }
        curl_close($ch);
        return $result;
    }
}
