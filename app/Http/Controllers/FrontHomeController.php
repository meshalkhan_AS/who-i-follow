<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Product;
use App\Order;
use Cart;
use App\Models\Admin\UserProfile;
use Twilio\Rest\Client;
use PragmaRX\Countries\Package\Countries;
use Auth;

class FrontHomeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        if(Auth::user()->id==1)
        {
            Auth::logout();
            return redirect()->route('login');
        }

        return view('store.front.index');
    }

  
    public function products($catId)
    {
        //
        if(Auth::user()->id==1)
        {
            Auth::logout();
            return redirect()->route('login');
        }

        $data['products'] = Product::where('category_id',$catId)->where('is_archive',0)->paginate(10);
        return view('store.front.products',$data);

    }


    public function viewProduct($id)
    {
        if(Auth::user()->id==1)
        {
            Auth::logout();
            return redirect()->route('login');
        }

        $data['product'] = Product::find($id);
        //
        return view('store.front.productDetail',$data);
    }


    public function orderHistory()
    {
        if(Auth::user()->id==1)
        {
            Auth::logout();
            return redirect()->route('login');
        }

        $data['orders'] = Order::where('user_id',\Auth::user()->id)->where('status','!=','Rejected')->orderBy('created_at','DESC')->get();
        $datapast['orderspast'] = Order::where('user_id',\Auth::user()->id)->where('status','=','rejected')->orwhere('status','=','delivered')->orderBy('created_at','DESC')->get();
        //dd($datapast);//
        return view('store.front.order-history',$data,$datapast);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function carFront()
    {
        if(Auth::user()->id==1)
        {
            Auth::logout();
            return redirect()->route('login');
        }

        $data['cart_content'] = Cart::getContent();
        $data['total_price'] =  Cart::getTotal();
        
        return view('store.front.cart',$data);
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function checkOut()
    {
        if(Auth::user()->id==1)
        {
            Auth::logout();
            return redirect()->route('login');
        }

        $countries = new Countries();
        $userprofile = UserProfile::where('user_id',Auth::user()->id)->first();
        $data['countries'] = $countries->all()->pluck('name.common');
        $data['cart_content'] = Cart::getContent();
        $data['total_price'] =  Cart::getTotal();
        //dd($userprofile);
        return view('store.front.checkout',$data,compact('userprofile'));
        //
    }


    public function payment(){

        if(Auth::user()->id==1)
        {
            Auth::logout();
            return redirect()->route('login');
        }

        if(!session()->has('success')){
            return redirect('shop/cart');
        }
        // dd(1);
        $totalamount=Cart::getTotal();
        return view('store.front.payment',compact('totalamount'));
    }


    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function processCheckout(Request $request)
    {
        
        if(Auth::user()->id==1)
        {
            Auth::logout();
            return redirect()->route('login');
        }

        if(Cart::isEmpty()){
            return redirect('/cart');
        }
        // dd(1);
        $request->validate([
            'name'=> 'required',
            'email'=>'required|email',
            'address'=>'required',
            'city'=>'required',
            'state'=>'required',
            'zip'=>'required',
            'shipping_address' => 'required_if:shipping_address_different,==,on',
            'shipping_city'=>'required_if:shipping_address_different,==,on',
            'shipping_state'=>'required_if:shipping_address_different,==,on',
            'shipping_zip'=>'required_if:shipping_address_different,==,on',
        ]);

        $data=collect([]);
        if($request->shipping_address_different=='on'){
            $shipping_country = $request->shipping_country;
            $shipping_address = $request->shipping_address;
            $shipping_city=$request->shipping_city;
            $shipping_state=$request->shipping_state;
            $shipping_zip=$request->shipping_zip;
            $ship_to_diff='on';
        }
        else{
            $ship_to_diff='off';
            $shipping_country = NULL;
            $shipping_address = NULL;
            $shipping_city=NULL;
            $shipping_state=NULL;
            $shipping_zip=NULL;
        }
        
        $data->push([
            'billing_country' => $request->country,
            'billing_address' => $request->address,
            'billing_city'=>$request->city,
            'billing_state'=>$request->state,
            'billing_zip'=>$request->zip,
            'billing_phone'=>$request->number,
            'billing_phone_code'=>$request->country_code,
            'billing_name' => $request->name,
            'billing_email' => $request->email,
            'shipping_address_different'=>$ship_to_diff,
            'shipping_country' => $shipping_country,
            'shipping_address' => $shipping_address,
            'shipping_city'=>$shipping_city,
            'shipping_state'=>$shipping_state,
            'shipping_zip'=>$shipping_zip,
            'shipping_address_different'=>'on',
        ]);
        session()->forget('checkout_data');
        session()->put('checkout_data',$data);
        
        // return redirect(route('payment'))->with('success','pay');
        return redirect(route('payment'))->with('success','pay');
        

        // return redirect(route('thank.you'))->with('success','pay');

        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function twilloMsg()
    {

        $account_sid = "AC44a6613349ba0f541f0813a0529d39d9";
        $auth_token = "5ecd1134399694bc449962ea5b71bcc9";
        $twilio_phone_number = "+14159916929";

        $client = new Client($account_sid, $auth_token);

        $json = $client->messages->create(
            '+923422764737',
            array(
                "from" => $twilio_phone_number,
                "body" => "Whaddup from PHP!",
                "statusCallback" => "http://postb.in/1234abcd"
            )
        );

    }
}
