<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

// use GlobalPayments\Api\ServicesConfig;
use GlobalPayments\Api\PaymentMethods\CreditCardData;
use GlobalPayments\Api\ServicesContainer;
use GlobalPayments\Api\Entities\Address;
use GlobalPayments\Api\ServiceConfigs\Gateways\PorticoConfig;
use GlobalPayments\Api\ServiceConfigs\Gateways\GpEcomConfig;
use GlobalPayments\Api\Entities\Exceptions\ApiException;
use GlobalPayments\Api\HostedPaymentConfig;
use GlobalPayments\Api\Entities\Enums\HppVersion;
use GlobalPayments\Api\Services\HostedService;
use GlobalPayments\Api\Entities\HostedPaymentData;


class GlobalPayment extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('store.front.payment');

        // $config = new GpEcomConfig();
        // $config->merchantId = "MER_7e3e2c7df34f42819b3edee31022ee3f";
        // $config->accountId = "TRA_c9967ad7d8ec4b46b6dd44a61cde9a91";
        // $config->sharedSecret ="Po8lRRT67a";
        // // $config->timestamp =time();

        // // TIMESTAMP
        // ServicesContainer::configureService($config);

        // $card = new CreditCardData();
        // $card->number = "4111111111111111";
        // $card->expMonth = "12";
        // $card->expYear = "2025";
        // $card->cvn = "123";


        // try {
        //     $response = $card->charge(129.99)
        //         ->withCurrency("EUR")
        //         ->execute();

        //     $result = $response->responseCode; // 00 == Success
        //     $message = $response->responseMessage; // [ test system ] AUTHORISED
        // } catch (ApiException $e) {
        //     $bac = json_encode($e);
        //     return json_decode($bac,true);
        //     dd($e);
        //     // handle errors
        // }

        // $config = new GpEcomConfig();
        // $config->merchantId = "MerchantId";
        // $config->accountId = "internet";
        // $config->sharedSecret = "secret";
        // $config->serviceUrl = "https://pay.sandbox.realexpayments.com/pay";
        // $config->hostedPaymentConfig = new HostedPaymentConfig();
        // $config->hostedPaymentConfig->version = HppVersion::VERSION_2;
        // $config->hostedPaymentConfig->cardStorageEnabled = "1";

        // $card = new CreditCardData();
        // $card->number = "4263970000005262";
        // $card->expMonth = 12;
        // $card->expYear = 2025;
        // $card->cvn = 123;
        // $card->cardHolderName = "James";

        // $service = new HostedService($config);

        // data to be passed to the HPP along with transaction level settings
        // $hostedPaymentData = new HostedPaymentData();
        // $hostedPaymentData->offerToSaveCard = true; // display the save card tick box
        // $hostedPaymentData->customerExists = false; // new customer
        // supply your own references for the customer and payment method
        // $hostedPaymentData->customerKey = "a7960ada-3da9-4a5b-bca5-7942085b03c6";
        // $hostedPaymentData->paymentKey = "48fa69fe-d785-4c27-876d-6ccba660fa2b";

        // try {
        //    $hppJson = $card->charge(19.99)
        //       ->withCurrency("EUR")
        //       // ->withHostedPaymentData($hostedPaymentData)
        //       ->execute();

        //       // $response = $card->charge(19.99)
        //       // ->withCurrency("EUR")
        //       // ->execute();
        //       dd($hppJson);
        //    // TODO: pass the HPP JSON to the client-side
        // } catch (ApiException $e) {
        //     dd($e);
        //    // TODO: Add your error handling here
        // }



        // $config = new GpEcomConfig();
        // $config->merchantId = "MER_7e3e2c7df34f42819b3edee31022ee3f";
        // $config->accountId = "TRA_c9967ad7d8ec4b46b6dd44a61cde9a91";
        // $config->sharedSecret ="Po8lRRT67a";

        // // $config->serviceUrl = "https://api.sandbox.realexpayments.com/epage-remote.cg";

        // // $config = new PorticoConfig();
        // // $config->secretApiKey = 'ZgDItUsm7dPOfKPR';
        // // $config->serviceUrl = 'https://api.sandbox.realexpayments.com/epage-remote.cgi';

        // ServicesContainer::configureService($config);

        // // create the card object
        // $card = new CreditCardData();
        // $card->number = "4263970000005262";
        // $card->expMonth = 12;
        // $card->expYear = 2025;
        // $card->cvn = 123;
        // $card->cardHolderName = "James";

        // try {
        //    // process an auto-capture authorization
        //    $response = $card->charge(19.99)
        //       ->withCurrency("EUR")
        //       ->execute();

        //       dd($response);
        // } catch (ApiException $e) {
        //     dd($e);
        //    // TODO: Add your error handling here
        // }

        // if (isset($response)) {
        //    $result = $response->responseCode; // 00 == Success
        //    $message = $response->responseMessage; // [ test system ] AUTHORISED

        //    // get the details to save to the DB for future requests
        //    $orderId = $response->orderId; // N6qsk4kYRZihmPrTXWYS6g
        //    $authCode = $response->authorizationCode; // 12345
        //    $paymentsReference = $response->transactionId; // pasref: 14610544313177922
        //    $schemeReferenceData = $response->schemeId; // MMC0F00YE4000000715
        // }

        // $_POST = filter_input_array(INPUT_POST, FILTER_SANITIZE_STRING);
        // $_GET = filter_input_array(INPUT_GET, FILTER_SANITIZE_STRING);

        // $config = new PorticoConfig();
        // $config->secretApiKey = 'skapi_cert_MYl2AQAowiQAbLp5JesGKh7QFkcizOP2jcX9BrEMqQ';
        // $config->serviceUrl = 'https://cert.api2.heartlandportico.com';

        // ServicesContainer::configureService($config);

        // $card = new CreditCardData();
        // $card->number = "5425230000004415";
        // $card->expMonth = "12";
        // $card->expYear = "2025";
        // $card->cvn = "123";

        // try {
        //     $response = $card->charge(129.99)
        //         ->withCurrency("EUR")
        //         ->execute();

        //         dd($response);

        //     $result = $response->responseCode; // 00 == Success
        //     $message = $response->responseMessage; // [ test system ] AUTHORISED
        // } catch (ApiException $e) {
        //     // handle errors
        // }


        // $card = new CreditCardData();
        // $card->token = $_GET['token_value'];

        // $address = new Address();
        // $address->streetAddress1 = $_GET["Address"];
        // $address->city = $_GET["City"];
        // $address->state = $_GET["State"];
        // $address->postalCode = preg_replace('/[^0-9]/', '', $_GET["Zip"]);
        // $address->country = "United States";

        /*
          $validCardHolder = new HpsCardHolder();
          $validCardHolder->firstName = $_GET["FirstName"];
          $validCardHolder->lastName = $_GET["LastName"];
          $validCardHolder->address = $address;
          $validCardHolder->phoneNumber = preg_replace('/[^0-9]/', '', $_GET["PhoneNumber"]);
         */

        // try {
        //     $response = $card->charge(15)
        //             ->withCurrency('USD')
        //             ->withAddress($address)
        //             ->withInvoiceNumber($_GET["invoice_number"])
        //             ->withAllowDuplicates(true)
        //             ->execute();

        //     // $body = '<h1>Success!</h1>';
        //     // $body .= '<p>Thank you, ' . $_GET['FirstName'] . ', for your order of $' . $_GET["payment_amount"] . '.</p>';

        //     // echo "Transaction Id: " . $response->transactionId;
        //     // echo "<br />Invoice Number: " . $_GET["invoice_number"];

        //     // i'm running windows, so i had to update this:
        //     //ini_set("SMTP", "my-mail-server");

        //     // sendEmail($_GET['EMAIL'], 'donotreply@e-hps.com', 'Successful Charge!', $body, true);
        // } catch (Exception $e) {
        //     echo 'Failure: ' . $e->getMessage();
        //     exit;
        // }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function curlPay(Request $request)
    {


      // $config = new ServicesConfig();
      // $config->secretApiKey = "skapi_cert_MY9OAgAbJ2IAueJZrqBiZqcccA73RhJNgs7ufsm5Hg";
      // $config->developerId = "000000";
      // $config->versionNumber  = "0000";
      // $config->serviceUrl = "https://cert.api2.heartlandportico.com";
      // ServicesContainer::configure($config);

      $config = new PorticoConfig();
      $config->secretApiKey = "skapi_cert_MY9OAgAbJ2IAueJZrqBiZqcccA73RhJNgs7ufsm5Hg";
      $config->developerId = "000000";
      $config->versionNumber = "0000";
      $config->serviceUrl = "https://cert.api2.heartlandportico.com";
      // ServicesContainer::configure($config);
      ServicesContainer::configureService($config);


      



      $card = new CreditCardData();
      $card->token = $request->payment_token;

      // return  $request->all();

      $address = new Address();
      $address->postalCode = "75024";


      try {
                $card->charge(129.99)
                ->withCurrency("EUR")
                ->execute();
      }

      catch (ApiException $e) {

        return $e;

        dd($e);
                      // handle error
      }



      // $card = new CreditCardData();
      // $card->token = $request->input('token_value');
      // $address = new Address();
      // $address->postalCode = '12345';
      // try {
      //     $response = $card->charge(10)
      //       ->withCurrency("USD")
      //       ->withAddress($address)
      //       ->execute();
      // } catch (ApiException $e) {

      //   // return $e;

      //   // return response()->json($e);

      //   dd($e);
      //     // handle error
      // }



     
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
