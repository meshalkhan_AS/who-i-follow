<?php

namespace App\Http\Controllers;

use App\Helpers\GeneralHelper;
use App\Models\Admin\Invite;
use App\Models\Admin\Sport;
use App\Models\Admin\UserProfile;
use App\User;
use Auth;
use Lang;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;

class LoginController
{
    public function index()
    {
        return view('auth.login');
    }


    public function check(Request $request)
    {
        $user_invite_chat_share=Session::get('user_invite_chat_share');
        $email=$request->email;
        $email=trim( $email );
        $user=User::where('email',$email)->get();

        if(count($user)>=1)
        {
            if(Auth::attempt([ 'email'=> $email, 'password'=> $request->password]))
            {
//                dd($user);
                $user=User::where('email',$email)->first();
                if($user->is_admin())
                {
                    return redirect()->route('user.news');
                }

                if($user_invite_chat_share)
                {
                    $findInvitation=Invite::where('level',$user_invite_chat_share['level'])->first();
                    $findInvitation['id']=($findInvitation)?$findInvitation['id']:0;
                    $findInvitation['level']=$user_invite_chat_share['level'];
                    //get random ad for news feed
                    $ads = GeneralHelper::getRandomAdBanner(Auth::user()->id, 'Messages');
                    $userprofile = \App\Models\Admin\UserProfile::where('user_id',Auth::user()->id)->first();
                    return view('chat.invitation',compact('findInvitation','ads','userprofile'));

                }
                else
                {
                    $usertab = UserProfile::where('user_id',Auth::user()->id)->value('default_tab');
                    if($usertab==1){
                        return redirect()->route('user.news');
                    }
                    else{
                        return redirect()->route('user.chatSystem');
                    }
                    
                }
            }
            else
            {
                return redirect()->back()->withErrors([
                    'error' => 'Incorrect Email or Password',
                ]);
            }
        }
        else
        {
            return redirect()->back()->withErrors([
                'error' => 'Incorrect Email or Password',
            ]);
        }
    }


    public function checkemail(Request $request)
    {
        $data['email']=$request->email;
        if($request->id=='')
        {
            $useremail = User::where('email', $data['email'])->get();
        }
        else
        {
            $useremail = User::where('id','!=',$request->id)->where('email', $data['email'])->get();
        }

        if(count($useremail)>=1)
        {
            $response='repeat';
        }
        else
        {
            $response = 'success';

        }
        return response()->json($response);
    }

    public function checkuserlink(Request $request)
    {
        $data['newlink']=$request->newlink;
            $usernewlink = User::where('user_link', $data['newlink'])->where('id','!=',Auth::user()->id)->get();

        if(count($usernewlink)>=1)
        {
            $response='repeat';
        }
        else
        {
            $response = 'success';

        }
        return response()->json($response);
    }

    public function checkmobile(Request $request)
    {
        $data['mob']=$request->mob;
        if($request->id=='')
        {
            $usermob = UserProfile::where('mob', $data['mob'])->orwhere('mob', '0'.$data['mob'])->get();
        }
        else
        {
            $usermob = UserProfile::where('user_id','!=',$request->id)->where('mob', $data['mob'])->orwhere('mob', '0'.$data['mob'])->get();
        }

        if(count($usermob)>=1)
        {
            $response='repeat';
        }
        else
        {
            $response = 'success';

        }
        return response()->json($response);
    }

    public function adminLogin()
    {
        return view('store.admin.login');
    }

    public function admin_process_login(Request $request)
    {
        // return $request->all();
        $request->validate([
            'email' => 'required',
            'password' => 'required'
        ]);

        $credentials = $request->except(['_token']);

        $user = User::where('email',$request->email)->first();

        $credentials['role_id'] = 1;

        if (auth()->attempt($credentials)) {

            return redirect()->route('admin.product.index');

        }else{
            session()->flash('message', 'Invalid credentials');
            return redirect()->back();
        }
    }

    public function admin_process_logout(Request $request)
    {
        Auth::logout();

        $request->session()->invalidate();

        $request->session()->regenerateToken();

        return redirect('/admin');
    }

    public function user_logout(Request $request)
    {

        if(Auth::check())
        {
            $user = User::where('id',Auth::user()->id)->first();
            $user->device_token=NULL;
            $user->save();
            Auth::logout();

            $request->session()->invalidate();
            $request->session()->regenerateToken();
        }

        return redirect('/login');
    }
}
