<?php

namespace App\Http\Controllers\Admin;

use App\Helpers\PermissionHelper as Per;
use Spatie\Permission\Models\Permission;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\StorePermissionsRequest;
use App\Http\Requests\Admin\UpdatePermissionsRequest;
use Session;

class PermissionsController extends Controller
{

    public function index()
    {
        if (! Per::has_permission('users_manage')) {
            return abort(401);
        }

        $Permissions = Permission::all();
        $Roles = array();

        return view('admin.permissions.index', compact('Permissions', 'Roles'));
    }


    public function create()
    {


        $Permissions = ['' => 'Select a Parent Group', 0 => 'This is Parent Group'];
        $PermissionsData = Permission::where('main_group', 1)->OrderBy('name', 'asc')->get();
        if($PermissionsData) {
            foreach ($PermissionsData as $permission) {
                $Permissions[$permission->id] = $permission->name;
            }
        }

        return view('admin.permissions.create', compact('Permissions'));
    }


    public function store(StorePermissionsRequest $request)
    {

        $data = $request->all();
        if(!$data['parent_id']) {
            $data['main_group'] = 1;
        }
        Permission::create($data);
        Session::flash('success', 'Record has been store successfully.');
        return redirect()->route('admin.permissions.index');
    }



    public function edit($id)
    {

        $Permissions = ['' => 'Select a Parent Group', 0 => 'This is Parent Group'];
        $PermissionsData = Permission::where('main_group', 1)->OrderBy('name', 'asc')->get();
        if($PermissionsData) {
            foreach ($PermissionsData as $permission) {
                $Permissions[$permission->id] = $permission->name;
            }
        }

        $permission = Permission::findOrFail($id);

        return view('admin.permissions.edit', compact('permission', 'Permissions'));
    }

    public function update(UpdatePermissionsRequest $request, $id)
    {
        $permission = Permission::findOrFail($id);
        $permission->update($request->all());

        Session::flash('success', 'Record has been updated successfully.');

        return redirect()->route('admin.permissions.index');
    }



    public function destroy($id)
    {
        $permission = Permission::findOrFail($id);
        $permission->delete();

        Session::flash('success', 'Record has been deleted successfully.');
        return redirect()->route('admin.permissions.index');
    }


    public function active($id)
    {
        $Permission = Permission::findOrFail($id);
        $Permission->update(['status' => 1]);

        Session::flash('success', 'Record has been active successfully.');

        return redirect()->route('admin.permissions.index');
    }


    public function inactive($id)
    {

        $Permission = Permission::findOrFail($id);
        $Permission->update(['status' => 0]);
        Session::flash('success', 'Record has been inactive successfully.');

        return redirect()->route('admin.permissions.index');
    }

    public function massDestroy(Request $request)
    {
        if ($request->input('ids')) {
            $entries = Permission::whereIn('id', $request->input('ids'))->get();

            foreach ($entries as $entry) {
                $entry->delete();
            }
        }
    }

}
