<?php

namespace App\Http\Controllers\Admin;
use App\Models\Admin\Invite;
use App\Models\Admin\MessageSms;
use App\Models\Admin\UserProfile;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Auth;
use DOMDocument;
use App\user;
use Illuminate\Support\Facades\Mail;

class InvitesController extends Controller
{
    public function invites(Request $request)
    {
        $url=$request->url;
    //  Check Invite Social and Group
        if($url=='site')
        {
            //  Create link
            $milliseconds = round(microtime(true) * 1000);
            $string= substr($milliseconds, -7);
            $data_['c_id']=$string;
            $urlFinal=url('/').'/'.$string.'/'.Auth::user()->user_link.'/register/'.$string;
        }
        else
        {
            $urlFinal=$request->urlInvite;
            $string=$request->level;
            $data_['c_id']=$request->c_id;
            $data_['group_id']=$request->group_id;
            $data_['group_name']=$request->group_name;
            $data_['group_image']=$request->group_image;
        }

    //  Assign ID
        $data_['level']=$string;

       if($request->email_!='')
       {
           $emailCheck=UserProfile::where('email',$request->email_)->first();

           if($emailCheck)
           {
               $emailTextforGroup='Please Login and Join Group. ';
               $emailTextforSocial='Please Login. ';
               $firstCheck='login';

           }
           else
           {
               $emailTextforGroup='Please click on the below button to complete your registration and Join Group. ';
               $emailTextforSocial='Please click on the below button to complete your registration.';
               $firstCheck='register';
           }

           if($url=='site')
           {
               $data_['invite_type']='1';
               $check=Invite::where('target',$request->email_)->where('invite_type','1')->first();

            //  Create message for social
               $body='You have been invited to Join Who I Follow. '.$emailTextforSocial;
           }
           else
           {
               $data_['invite_type']='4';
               $check=Invite::where('target',$request->email_)->where('group_id',$request->group_id)->first();

           //  Create message for group
               $subGroup='You have been invited to Join Who I Follow Group Chat Created by '.Auth::user()->name.'. '.$emailTextforGroup;
               $body=$subGroup;
           }

           $data = [
               'subject' => 'Welcome to Who I Follow.!',
               'email' => $request->email_,
               'level' => $string,
               'c_id' => ($url=='site')?$string:$request->c_id,
               'firstCheck' => $firstCheck,
               'url' => $url,
               'body' => $body,
           ];

           Mail::send('email-template', $data, function($message) use ($data) {
               $message->to($data['email'])
                   ->subject($data['subject']);
           });


           $data_['target']=$request->email_;
           $data_['user_link']=Auth::user()->id;
           if(!$check)
           {
               Invite::create($data_);
           }

       }
        if($request->mob_!='')
        {
            $mobileCheck=UserProfile::where('mob',$request->mob_)->first();
            if($mobileCheck)
            {
                $mobileTextforGroup='Please login to Join Group. ';
                $mobileTextforSocial='Please login.';
            }
            else
            {
                $mobileTextforSocial='Please click on the link to complete your registration.';
                $mobileTextforGroup='Please click on the link to complete your registration and Join Group. ';

            }
           if($url=='site')
           {
               $data_['invite_type']='2';
               $body='You have been invited to Join Who I Follow. '.$mobileTextforSocial.' '.$urlFinal;
               $checkMob=Invite::where('target',$request->mob_)->where('invite_type','2')->first();
           }
           else
           {
               $data_['invite_type']='4';
               $subGroup='You have been invited to Join Who I Follow Group Chat Created by '.Auth::user()->name.'. '.$mobileTextforGroup;
               $checkMob=Invite::where('target',$request->mob_)->where('group_id',$request->group_id)->first();
               $body=$subGroup.$urlFinal;
           }

           $mob=$request->mob_;
           $payload = [
               'From' => '+16182283747',
               'To' => $request->code.ltrim($mob, '0'),
               'Body'   => $body
           ];

           $ch = curl_init();

           curl_setopt($ch, CURLOPT_URL, "https://api.twilio.com/2010-04-01/Accounts/AC44a6613349ba0f541f0813a0529d39d9/Messages.json");
           curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
           curl_setopt($ch, CURLOPT_POST, 1);
           curl_setopt($ch, CURLOPT_USERPWD, "AC44a6613349ba0f541f0813a0529d39d9" . ':' . "5ecd1134399694bc449962ea5b71bcc9");

           curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($payload));

           $result = curl_exec($ch);
           if (curl_errno($ch)) {
               echo 'Error:' . curl_error($ch);
           }

           json_decode($result,true);
           curl_close ($ch);
           $data_['target']=$request->mob_;
           $data_['user_link']=Auth::user()->id;


           if(!$checkMob)
           {
               Invite::create($data_);
           }

       }

    }

    public function updateInvite(Request $request)
    {
        Invite::where('id',$request['id'])->update(['level'=>'0']);
    }

    public function reInvites(Request $request)
    {
        $id=$request->id;
        $invites=Invite::where('id',$id)->first();
        $urlFinal=url('/').'/'.$invites['level'].'/'.Auth::user()->user_link.'/register/'.$invites['c_id'];
        $url='';

        //  Check Invite type
        if($invites['group_id']=='')
        {
            $url='site';
        }

        if(!ctype_digit($invites['target']))
        {
            $emailCheck=UserProfile::where('email',$invites['target'])->first();
            if($emailCheck)
            {
                $emailTextforGroup='Please Login and Join Group.';
                $emailTextforSocial='Please Login.';
                $firstCheck='login';

            }
            else
            {
                $emailTextforGroup='Please click on the below button to complete your registration and Join Group.';
                $emailTextforSocial='Please click on the below button to complete your registration.';
                $firstCheck='register';
            }

            if($url=='site')
            {
                //  Create message for social
                $body='You have been invited to Join Who I Follow. '.$emailTextforSocial;
            }
            else
            {
                //  Create message for group
                $subGroup='You have been invited to Join Who I Follow Group Chat Created by '.Auth::user()->name.'. '.$emailTextforGroup;
                $body=$subGroup;
            }

            $data = [
                'subject' => 'Welcome to Who I Follow.!',
                'email' => $invites['target'],
                'level' => $invites['level'],
                'c_id' => $invites['c_id'],
                'firstCheck' => $firstCheck,
                'url' => $url,
                'body' => $body,
            ];

            Mail::send('email-template', $data, function($message) use ($data) {
                $message->to($data['email'])
                    ->subject($data['subject']);
            });

        }
        else
        {
            $mobileCheck=UserProfile::where('mob',$invites['target'])->first();
            if($mobileCheck)
            {
                $mobileTextforGroup='Please login to Join Group.';
                $mobileTextforSocial='Please login.';
            }
            else
            {
                $mobileTextforSocial='Please click on the link to complete your registration.';
                $mobileTextforGroup='Please click on the link to complete your registration and Join Group. ';

            }
            if($url=='site')
            {
                $body='You have been invited to Join Who I Follow. '.$mobileTextforSocial.' '.$urlFinal;
            }
            else
            {
                $subGroup='You have been invited to Join Who I Follow Group Chat Created by '.Auth::user()->name.'. '.$mobileTextforGroup;
                $body=$subGroup.$urlFinal;
            }
            $payload = [
                'From' => '+16182283747',
                'To' => '+92'.ltrim($invites['target'],0),
                'Body'   => $body
            ];

            $ch = curl_init();

            curl_setopt($ch, CURLOPT_URL, "https://api.twilio.com/2010-04-01/Accounts/AC44a6613349ba0f541f0813a0529d39d9/Messages.json");
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
            curl_setopt($ch, CURLOPT_POST, 1);
            curl_setopt($ch, CURLOPT_USERPWD, "AC44a6613349ba0f541f0813a0529d39d9" . ':' . "5ecd1134399694bc449962ea5b71bcc9");

            curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($payload));

            $result = curl_exec($ch);
            if (curl_errno($ch)) {
                echo 'Error:' . curl_error($ch);
            }

            json_decode($result,true);
            curl_close ($ch);
        }

    }


    public function sendSMS()
    {
        $current_date_time = \Carbon\Carbon::now();
        $allsms=MessageSms::where('notify','on')->get();

        foreach ($allsms as $sms)
        {
            $hourdiff = round((strtotime($current_date_time) - strtotime($sms->date))/3600, 1);
            $data['last_count']=$sms->last_count;
            $data['unread']=$sms->unread;
            $data['group_name']=$sms->group_name;
            $data['group_id']=$sms->group_id;

            $userMobile=UserProfile::where('user_id',$sms->user_id)->first();
            $result = substr($userMobile['mob'], 0, 2);
            if($result=='+92')
            {
                $data['number']=$userMobile['mob'];
            }
            else if($result=='03')
            {
                $data['number']= '+92'.ltrim($userMobile['mob'], '0');
            }
            else
            {
                $data['number']= $userMobile['mob'];
            }

            if($hourdiff>3 && $data['last_count']<$data['unread'])
            {
                //   update
                $data['date']= $current_date_time;

                if($data['group_name']!='no-group')
                {
                    $subbody='You have an unread messages in the team '.$data['group_name'].'.';
                }
                else
                {
                    $subbody='You have an unread messages on Who I Follow.';
                }


                $body=$subbody.' Please Login to check. '.url('/');
                $payload = [
                    'From' => '+16182283747',
                    'To' => $data['number'],
                    'Body'   => $body
                ];

                $ch = curl_init();

                curl_setopt($ch, CURLOPT_URL, "https://api.twilio.com/2010-04-01/Accounts/AC44a6613349ba0f541f0813a0529d39d9/Messages.json");
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
                curl_setopt($ch, CURLOPT_POST, 1);
                curl_setopt($ch, CURLOPT_USERPWD, "AC44a6613349ba0f541f0813a0529d39d9" . ':' . "5ecd1134399694bc449962ea5b71bcc9");

                curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($payload));

                $result = curl_exec($ch);
                if (curl_errno($ch)) {
                    echo 'Error:' . curl_error($ch);
                }

                json_decode($result,true);
                curl_close ($ch);
                $data['last_count']=$data['unread'];
                $findUser= MessageSms::where('user_id',$sms->user_id)->where('group_id', $sms->group_id)->first();
                $findUser->update($data);
            }
        }

    }

    public function messageCheck(Request $request)
    {
        $current_date_time = \Carbon\Carbon::now();
        $data=$request->all();
        $data['last_count']=0;
        $data['group_id']=$request->group_id;
        $data['group_name']=$request->group_name;
        $data['notify']=($request->group_name=='no-group')?'on':$request->notify;
        $findUser= MessageSms::where('user_id',$data['user_id'])->where('group_id',$data['group_id'])->first();

        if($findUser)
        {
            $datanew['unread']=$findUser['unread']+1;
            $findUser->update($datanew);
        }
        else
        {
            $userMobile=UserProfile::where('user_id',$data['user_id'])->first();
            $result = substr($userMobile['mob'], 0, 2);
            if($result=='+92')
            {
                $data['number']=$userMobile['mob'];
            }
            else if($result=='03')
            {
                $data['number']= '+92'.ltrim($userMobile['mob'], '0');
            }
            else
            {
                $data['number']= $userMobile['mob'];
            }
            $data['unread']=$data['last_count'];

            if($data['group_name']!='no-group')
            {
                $subbody='You have an unread messages in the team '.$data['group_name'].'.';
            }
            else
            {
                $subbody='You have an unread messages on Who I Follow.';
            }

//            if($data['unread']==1)
            $body=$subbody.' Please Login to check. '.url('/');
            $data['date']= $current_date_time;

            $payload = [
                'From' => '+16182283747',
                'To' => $data['number'],
                'Body'   => $body
            ];
            MessageSms::create($data);

            $ch = curl_init();

            curl_setopt($ch, CURLOPT_URL, "https://api.twilio.com/2010-04-01/Accounts/AC44a6613349ba0f541f0813a0529d39d9/Messages.json");
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
            curl_setopt($ch, CURLOPT_POST, 1);
            curl_setopt($ch, CURLOPT_USERPWD, "AC44a6613349ba0f541f0813a0529d39d9" . ':' . "5ecd1134399694bc449962ea5b71bcc9");

            if($data['notify']=='on')
            {
                curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($payload));
            }

            $result = curl_exec($ch);
            if (curl_errno($ch)) {
                echo 'Error:' . curl_error($ch);
            }

            json_decode($result,true);
            curl_close ($ch);
        }

  //    User Desktop Notification -----------------------------------------------
        $user_id=$data['user_id'];
        $messageReceived=$data['messageReceived'];
        if($messageReceived!='((left_group_undefined))'  && $messageReceived!='((update_group_name))'  && $messageReceived!='((make_admin_group))'
            && $messageReceived!='((remove_admin_group))' && $messageReceived!='((removed_group_undefined))' && $messageReceived!='((joined_group_undefined))'
            && $messageReceived!='((added_group_undefined))' )

        {
           // dd($messageReceived);
            $body= $messageReceived;

        }
        if(preg_match('/<img/', $body)){
       preg_match_all('/\<img[^\>]*\>/', $body, $matches);
$matchesarray = $matches[0];

foreach ($matchesarray as $match)
{
   $replacement = 'image';

   if (preg_match('/alt=\"(.*?)\"(.*?)/', $match, $matches2)){
      $replacement = $matches2[1];
    }

   $body = str_replace($match, ' '.$replacement, $body);
}
        }
        $heading=$data['heading'];
        $this->sendNotification($user_id,$body,$heading);     
    }
    public function sendNotification($user_id,$body,$heading)
    {
        //        Notification
        $id = $user_id;
        $gettoken = User::where('id',$id)->value('device_token');
        $token =$gettoken; 
        $from = env('serverkey');

        //API URL of FCM
        $url = 'https://fcm.googleapis.com/fcm/send';

        $msg = array
              (
                'body'  => $body,
                'title' => $heading,
              );

        $fields = array
                (
                    'to'        => $token,
                    'notification'  => $msg
                );

        $dataString = json_encode($fields);
        //header includes Content type and api key
        $headers = array(
            'Content-Type:application/json',
            'Authorization:key='.$from
        );

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $dataString);
        $result = curl_exec($ch);
        if ($result === FALSE) {
            die('FCM Send Error: ' . curl_error($ch));
        }
        curl_close($ch);
        return $result;
    }

    public function messagesChecked(Request $request)
    {
        $findUser= MessageSms::where('group_id',$request->group_id)->where('user_id',Auth::user()->id)->first();

        if($findUser)
        {
            $datanew['last_count']=0;
            $datanew['unread']=0;
            $findUser->update($datanew);
        }
    }

    public function updateSMSNotification(Request $request)
    {
        $findUser= MessageSms::where('group_id',$request->group_id)->where('user_id',auth()->user()->id)->first();
        if($findUser)
        {
            $datanew['notify']=$request->notify;
            $findUser->update($datanew);
        }
    }
}
