<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Feedback;
use App\FeedbackType;
use App\Models\Admin\UserProfile;
use App\FeedbackImages;
use Artisan;
use App\User;
use Illuminate\Support\Facades\Storage;
use Session;
use Auth;

class FeedBackController extends Controller
{
    public function showlist()
    {  $data['title'] = "Feedback list";
        $data['cols'] = [
            'First Name',
            'Last Name',
            'Description',
            'Type',
            'Images',
            'Date',
            'Action'
        ];

        $data['datas'] = Feedback::orderBy('id','desc')->get();
        return view('store.admin.feedbacks.list',$data);
    }
    public function feeddelete(Request $request){
        Feedback::where('id',$request->id)->forceDelete();
        FeedbackImages::where('feedback_id',$request->id)->forceDelete();
        return redirect()->route('admin.showlist')->with('message', 'Feedback has been deleted sucessfully!');
      }


public function singlefeed($id)
  {  
      Session::put('images', array());
    // $id=$request->id;
    $feed= Feedback::find($id);
    $userdetail = UserProfile::where('user_id',$feed->user_id)->get();
    $feedimage= FeedbackImages::where('feedback_id',$feed->id)->get();
    $dataimage = array();
    Session::put('edit', 'yes');

    //   If Images Exists, add on Session
    if(count($feedimage)>0)
    {
        foreach ($feedimage as $image)
        {
                array_push($dataimage, $image->image);
        }

        Session::put('images', $dataimage);
    }
    return view('store.admin.feedbacks.feedview',compact('feedimage','feed','userdetail'));
  }
    
    public function showtypes()
    {  $data['title'] = "Feedback Types";
        $data['cols'] = [
            'Type',
            'Status',
            'Action'
        ];

        $data['datas'] = FeedbackType::orderBy('id','desc')->get();
        return view('store.admin.feedbacks.types',$data);
    }
    public function edit($id)
    {
        $data['data'] = FeedbackType::find($id);
        return view('store.admin.feedbacks.edittype',['data' => $data['data'] ]);
    }
    public function update(Request $request)
    {
        $validated = $request->validate([
            'feedtype'  => 'required',
        ]);
        FeedbackType::where('id', $request->id)->update([
            'type' => $request->feedtype,
            'status' => $request->typestatus,
        ]);
        // $user = UserProfile::find($id);
        return redirect()->route('admin.showtypes')->with('message', 'Feedback Type updated Successfully!');
    }
    public function storefeed(Request $request)
    {
        $user_id = Auth::user()->id;
        $feedtype=$request->feedtype;
        $firstValue = array_filter($feedtype);
        if($firstValue!=null){
            foreach($firstValue as $feed) {
                FeedbackType::create([
                    'created_by' => $user_id,
                    'status' => 1,
                    'type' => $feed
                ]);
            }
    }
    return redirect()->route('admin.showtypes')->with('message', 'Feedback Type Added Successfully!');
    }
    public function destroy($id)
    {
       
        FeedbackType::find($id)->delete();
        
        return redirect()->back()->with('message', 'Type has been Deleted successfully!');
    }
     public function userfeedback()
    {
        if(Auth::user()->id==1)
        {
            Auth::logout();
            return redirect()->route('login');
        }
        $data=array();
		//Session::put('images',array());
        $types=FeedbackType::orderBy('id','desc')->get();
        return view('sports.feedback', compact('data','types'));
    }
    public function uploadimages(Request $request)
    {
        $data=array();
        if ($request->totalfiles>0)
        {
            $totalfiles=$request->totalfiles;

        //  Store New Images
            for($i=0;$i<$totalfiles;$i++)
            {
                $picture=file_get_contents($request['file-'.$i]);
                $filename=$this->storeimage($picture);
                array_push($data,$filename);

            }
            $edit='no';
        }
        if ($request->totalfiles_>0)
        {
        //  Store Edit Images
            $totalfiles=$request->totalfiles_;

            for($i=0;$i<$totalfiles;$i++)
            {
                $picture=file_get_contents($request['fileedit-'.$i]);
                $filename=$this->storeimage($picture);
                array_push($data,$filename);

            }
            $edit='yes';
        }

        Session::put('edit', $edit);

        $view = view('sports.modal.feedbackimg',compact('data'))->render();
        return response()->json([
            'html'=>$view,
            'edit'=>$edit,
            'count'=>count($data),
        ]);
    }

    public function deleteimages(Request $request)
    {

        //   Delete Images from Session
        $data = Session::get('images');
        if (($key = array_search($request->id, $data)) !== false) {
            unset($data[$key]);
            Session::put('images', $data);
        }

        $images = Session::get('images');
        $edit = Session::get('edit');
        return response()->json([
            'count'=>count($images),
            'data'=>$data,
        ]);
    }

    public function clearimages()
    {
        //   Clear array from Session
        Session::put('images', array());
    }

    public function storeimage($picture)
    {

        
        //  Generate Name
        $dateTime = date('Ymd_His');
        $filename = uniqid().$dateTime.".jpg";
        $path='public/feeds/';
        Storage::put($path. $filename, $picture);

        //   Put Images on Session
        $images = Session::get('images');
         if(empty($images) || $images == null){
			 $images = array();
		 }
		
        //   Check Images on Session
        array_push($images, $filename);
		// dd($images);
        // dump($images);
        Session::put('images', $images);
      
        return $filename;
    }
    public function store(Request $request)
    {
		
        $data=$request->all();
        $current_date_time = \Carbon\Carbon::now();
        $data['user_id']= Auth::user()->id;
        $data['status']= '1';
        $data['feed_type']= $request->feedtype;
        $data['discription']= $request->caption;
        $data['created_at']= $current_date_time;
        $images = Session::get('images');
       
		//   Check if any caption or image receive
        if(!empty($images) || $request->caption!='')
        {
            $feedback_id=feedback::create($data)->id;
			   $images = Session::get('images');

            //   Store Images Name from Session
            if(!empty($images))
            {
                foreach ($images as $image)
                {
                    $dataimage['images']=$image;
                    FeedbackImages::create([
                        'image'=>$dataimage['images'],
                        'user_id'=>Auth::user()->id,
                        'feedback_id'=>$feedback_id,
                        'created_at'=>$current_date_time,
                    ]);
                }
				 session()->flash('success', 'Feedback submitted successfully.');
            }else{
			 session()->flash('success', 'Feedback submitted successfully.');	 
			}
           
        }
        else
        {
            session()->flash('success', 'No Feedback Added.');
        }

        Artisan::call('cache:clear');
        //   Clear array from Session
        Session::put('images', array());

        
        return redirect()->back();
    }
}
