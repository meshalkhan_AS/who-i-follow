<?php

namespace App\Http\Controllers\Admin;


use App\Events\sentNotification;
use App\Models\Admin\Follower;
use App\Http\Controllers\Controller;
use App\Models\Admin\UserProfile;
use App\User;
use Auth;
use Illuminate\Http\Request;

class FollowersController extends Controller
{
    public function addfollowers($follower_id)
    {
        $current_date_time = \Carbon\Carbon::now();
        $data['user_id']= Auth::user()->id;
        $data['status']= '1';
        $data['follower_id']= $follower_id;
        $data['created_at']= $current_date_time;

        $follower= Follower::where('user_id',Auth::user()->id)->where('follower_id',$follower_id)->get();
        $followers= Follower::where('user_id',Auth::user()->id)->where('follower_id',$follower_id)->first();

        if(count($follower)>=1)
        {
            $followerupdate=Follower::findOrFail($followers['id']);
            $followerupdate->update($data);
            $table_id=$followers['id'];
        }
        else
        {
            $table_id=Follower::create($data)->id;
        }

        $user=UserProfile::where('user_id',$follower_id)->first();

//        Sent Notification
        event(new sentNotification($follower_id,'followed',$table_id));

        session()->flash('success', 'You have successfully followed '.ucfirst($user['fname']).' '.ucfirst($user['lname']));
        return redirect()->back();
    }

    public function removefollowers($follower_id)
    {
        Follower::where('user_id',Auth::user()->id)->where('follower_id',$follower_id)->forceDelete();
        $user=UserProfile::where('user_id',$follower_id)->first();

        session()->flash('success', 'You have successfully unfollowed '.ucfirst($user['fname']).' '.ucfirst($user['lname']));
        return redirect()->back();
    }

    public function viewfollowers($userlink,Request $request)
    {
        $user=User::where('user_link',$userlink)->first();
        $follower_id=$user['id'];

        $user=UserProfile::where('user_id',$follower_id)->first();
        $profiles=Follower::select('followers.*','user_profiles.user_id as user_id',
        'user_profiles.city as city',
        'user_profiles.country as country',
        'user_profiles.picture as picture',
        'user_profiles.fname as fname',
        'user_profiles.lname as lname','users.user_link as user_link')
        ->join('users', 'users.id', '=', 'followers.user_id')
        ->join('user_profiles', 'followers.user_id', '=', 'user_profiles.user_id')
        ->where('followers.follower_id',$follower_id)
        ->where('followers.status','1')
            ->paginate(18);


        if ($request->ajax()) {
            $view = view('sports.lists.profiles',compact('profiles'))->render();
            return response()->json(['html'=>$view]);
        }
        else
        {
            return view('sports.followerslist',compact('profiles','user'));
        }

    }

    public function viewfollowing($userlink,Request $request)
    {
        if(Auth::user()->id==1)
        {
            Auth::logout();
            return redirect()->route('login');
        }
        $user=User::where('user_link',$userlink)->first();
        $user_id=$user['id'];

        $user=UserProfile::where('user_id',$user_id)->first();
        $profiles=Follower::select('followers.*','user_profiles.user_id as user_id',
        'user_profiles.city as city',
        'user_profiles.country as country',
        'user_profiles.picture as picture',
        'user_profiles.fname as fname',
        'user_profiles.lname as lname','users.user_link as user_link')
        ->join('users', 'followers.follower_id', '=', 'users.id')
        ->join('user_profiles', 'followers.follower_id', '=', 'user_profiles.user_id')
        ->where('followers.user_id',$user_id)
        ->where('followers.status','1')
            ->paginate(18);

        if ($request->ajax()) {
            $view = view('sports.lists.profiles',compact('profiles'))->render();
            return response()->json(['html'=>$view]);
        }
        else
        {
            return view('sports.followinglist',compact('profiles','user'));
        }

    }
}
