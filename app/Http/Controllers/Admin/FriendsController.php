<?php

namespace App\Http\Controllers\Admin;

use App\Events\sentNotification;
use App\Models\Admin\FriendList;
use App\Models\Admin\Invite;
use App\Models\Admin\UserNotification;
use App\Models\Admin\UserProfile;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Auth;

class FriendsController extends Controller
{
    public function requestindex()
    {
        $active='request';
        return view('sports.bio',compact('active'));
    }

    public function rejectrequest(Request $request)
    {
        $id=$request->id;
        FriendList::where('id',$id)->forceDelete();
        FriendList::where('user_id',$request->friend_id)->where('friend_id',Auth::user()->id)->forceDelete();
        FriendList::where('friend_id',$request->friend_id)->where('user_id',Auth::user()->id)->forceDelete();
    }

    public function acceptrequest(Request $request)
    {
        $id=$request->id;
        FriendList::where('id',$id)->update(['status'=>'1']);
        FriendList::where('friend_id',$request->friend_id)->where('user_id',Auth::user()->id)->forceDelete();

        //        Sent Notification
        event(new sentNotification($request->friend_id,'confirmFriendRequest',$id));
    }

    public function confirmrequest($friend_id)
    {
        FriendList::where('friend_id',Auth::user()->id)->where('user_id',$friend_id)->update(['status'=>'1']);
        $table=FriendList::where('friend_id',Auth::user()->id)->where('user_id',$friend_id)->first();
        FriendList::where('friend_id',$friend_id)->where('user_id',Auth::user()->id)->forceDelete();

        //        Sent Notification
        event(new sentNotification($friend_id,'confirmFriendRequest',$table['id']));

        return redirect()->back();
    }

    public function cancelrequest($friend_id)
    {

        $friendRow=FriendList::where('friend_id',$friend_id)->where('user_id',Auth::user()->id)->where('status','0')->first();
        FriendList::where('friend_id',$friend_id)->where('user_id',Auth::user()->id)->where('status','0')->forceDelete();

        //   Delete Notification
        UserNotification::where('message_type','sentFriendRequest')->where('table_id',$friendRow['id'])->forceDelete();
        session()->flash('success', 'Friend request has been cancelled successfully.');
        return redirect()->back();
    }

    public function ignorerequest($friend_id)
    {
        FriendList::where('user_id',$friend_id)->where('friend_id',Auth::user()->id)->forceDelete();
        FriendList::where('friend_id',$friend_id)->where('user_id',Auth::user()->id)->forceDelete();

        session()->flash('success', 'Friend request has been cancelled successfully.');
        return redirect()->back();
    }

    public function addfriends($friend_id)
    {
        $current_date_time = \Carbon\Carbon::now();
        $data['user_id']= Auth::user()->id;
        $data['status']= '0';
        $data['friend_id']= $friend_id;
        $data['created_at']= $current_date_time;

        $friend= FriendList::where('user_id',Auth::user()->id)->where('friend_id',$friend_id)->get();
        $friends= FriendList::where('user_id',Auth::user()->id)->where('friend_id',$friend_id)->first();

        if(count($friend)>=1)
        {
            $followerupdate=FriendList::findOrFail($friends['id']);
            $followerupdate->update($data);
            $table_id=$friends['id'];
        }
        else
        {
            $table_id=FriendList::create($data)->id;
        }

        //        Sent Notification
        event(new sentNotification($friend_id,'sentFriendRequest',$table_id));

        session()->flash('success', 'Friend request has been sent successfully');
        return redirect()->back();
    }

    public function unfriend($friend_id)
    {
        FriendList::where('friend_id',$friend_id)->where('user_id',Auth::user()->id)->where('status','1')->forceDelete();
        FriendList::where('user_id',$friend_id)->where('friend_id',Auth::user()->id)->where('status','1')->forceDelete();

        session()->flash('success', 'Friend has been removed successfully.');
        return redirect()->back();
    }


    public function viewfriends(Request $request,$userlink)
    {
        if(Auth::user()->id==1)
        {
            Auth::logout();
            return redirect()->route('login');
        }

        $user=User::where('user_link',$userlink)->first();
        $friend_id=$user['id'];

        if($request->friendrequest)
        {
            $request_='yes';
        }
        else
        {
            $request_='no';
        }

        $user=UserProfile::select('user_profiles.*','users.user_link as user_link')
            ->join('users', 'users.id', '=', 'user_profiles.user_id')->where('user_profiles.user_id',$friend_id)->first();

        if($request->ajax())
        {
            if($request->filterID=='1')
            {
                $data=$this->myFriends($friend_id);
            }
            else if($request->filterID=='2')
            {
                $data=FriendList::select('friend_lists.*','friend_lists.friend_id as user_id',
                    'user_profiles.picture as picture',
                    'user_profiles.fname as fname',
                    'user_profiles.lname as lname','users.user_link as user_link')
                    ->join('users', 'friend_lists.friend_id', '=', 'users.id')
                    ->join('user_profiles', 'friend_lists.friend_id', '=', 'user_profiles.user_id')
                    ->where('friend_lists.user_id',Auth::user()->id)
                    ->where('friend_lists.status','0')->get()->toArray();
            }
            else if($request->filterID=='3')
            {
                $data=FriendList::select('friend_lists.*','friend_lists.user_id as user_id',
                    'user_profiles.picture as picture',
                    'user_profiles.fname as fname',
                    'user_profiles.lname as lname','users.user_link as user_link')
                    ->join('users', 'friend_lists.user_id', '=', 'users.id')
                    ->join('user_profiles', 'friend_lists.user_id', '=', 'user_profiles.user_id')
                    ->where('friend_lists.friend_id',Auth::user()->id)
                    ->where('friend_lists.status','0')->get()->toArray();
            }

            $view = view('sports.lists.friends',compact('data','user'))->render();
            return response()->json(['html'=>$view]);
        }
        else
        {
            $data=$this->myFriends($friend_id);
        }
        $request_received=FriendList::where('friend_id',Auth::user()->id)->where('status','0')->get();
        $request_sent=FriendList::where('user_id',Auth::user()->id)->where('status','0')->get();
        $reqReveivedCount=count($request_received);
        $invites=Invite::where('user_link',Auth::user()->id)->where('level','!=','0')->get();
        $reqSentCount=count($request_sent);

        return view('sports.friendslist',compact('data','user','reqReveivedCount','reqSentCount','request_','invites'));

    }

    public function friendList(Request $request)
    {
        $search=$request->name;
        $data = $this->myFriends(Auth::user()->id);
        $i=0;
        foreach ($data as $val) {
            $data[$i]['fname']=ucfirst($val['fname']);
            $i++;
        }
        usort($data, array('app\Http\Controllers\Admin\FriendsController','compareByName'));

        if($search!='null')
        {
            $newarray=array();
            foreach ($data as $key => $val) {

                if (stripos($val['fname'].' '.$val['lname'], $search) !== FALSE) {
                    array_push($newarray,$val);
                }
            }
            $data=$newarray;
        }

        return response()->json($data, 200);

    }

    public function userList(Request $request)
    {
        $data=UserProfile::get()->toArray();
        return response()->json($data, 200);
    }

    public static function compareByName($a, $b) {
        return strcmp($a["fname"], $b["fname"]);
    }


    public function myFriends($friend_id)
    {
        $friends=FriendList::select('friend_lists.*','friend_lists.friend_id as user_id',
            'user_profiles.picture as picture',
            'user_profiles.fname as fname',
            'user_profiles.lname as lname','users.user_link as user_link')
            ->join('users', 'friend_lists.friend_id', '=', 'users.id')
            ->join('user_profiles', 'friend_lists.friend_id', '=', 'user_profiles.user_id')
            ->where('friend_lists.user_id',$friend_id)
            ->where('friend_lists.status','1')
            ->get()->toArray();

        $friends_=FriendList::select('friend_lists.*','friend_lists.user_id as user_id',
            'user_profiles.picture as picture',
            'user_profiles.fname as fname',
            'user_profiles.lname as lname','users.user_link as user_link')
            ->join('users', 'friend_lists.user_id', '=', 'users.id')
            ->join('user_profiles', 'friend_lists.user_id', '=', 'user_profiles.user_id')
            ->where('friend_lists.friend_id',$friend_id)
            ->where('friend_lists.status','1')
            ->get()->toArray();
         return $data = array_merge($friends, $friends_);
    }
}
