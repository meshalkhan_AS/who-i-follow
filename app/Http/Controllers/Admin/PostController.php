<?php

namespace App\Http\Controllers\Admin;

use App\Events\sentNotification;
use App\Models\Admin\Follower;
use App\Models\Admin\FriendList;
use App\Models\Admin\Post;
use App\Models\Admin\PostImage;
use App\Models\Admin\UserNotification;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Auth;
use Session;
use Artisan;
use Illuminate\Support\Facades\Storage;

class PostController extends Controller
{
    public function store(Request $request)
    {
        
        $data=$request->all();
        $current_date_time = \Carbon\Carbon::now();
        $data['user_id']= Auth::user()->id;
        $data['status']= '1';
        $data['exp_status']= $request->exp;
        $data['created_at']= $current_date_time;
        $images = Session::get('images');

        //   Check if any caption or image receive
        if(!empty($images) || $request->caption!='')
        {
            $post_id=Post::create($data)->id;

            //   Store Images Name from Session
            if(!empty($images))
            {
                foreach ($images as $image)
                {
                    $dataimage['images']=$image;
                    PostImage::create([
                        'image'=>$dataimage['images'],
                        'user_id'=>Auth::user()->id,
                        'post_id'=>$post_id,
                        'created_at'=>$current_date_time,
                    ]);
                }
            }
            session()->flash('success', 'Post has been added successfully.');
        }
        else
        {
            session()->flash('success', 'No Post Added.');
        }

        Artisan::call('cache:clear');
        //   Clear array from Session
        Session::put('images', array());

        //        Sent Notification
        $myfriends=FriendList::select('user_profiles.user_id as friends_id')
            ->join('user_profiles', 'friend_lists.friend_id', '=', 'user_profiles.user_id')
            ->where('friend_lists.user_id',Auth::user()->id)
            ->where('friend_lists.status','1')
            ->get()->toArray();


        $ifriend=FriendList::select('user_profiles.user_id as friends_id')
            ->join('user_profiles', 'friend_lists.user_id', '=', 'user_profiles.user_id')
            ->where('friend_lists.friend_id',Auth::user()->id)
            ->where('friend_lists.status','1')
            ->get()->toArray();


        $friends_array=array_merge($myfriends,$ifriend);
        $following=Follower::select('user_profiles.user_id as friends_id')
            ->join('user_profiles', 'followers.follower_id', '=', 'user_profiles.user_id')
            ->where('followers.user_id',Auth::user()->id)
            ->where('followers.status','1')
            ->get()->toArray();

        $final_array=array_merge($friends_array,$following);
        $final_array=array_unique($final_array,SORT_REGULAR);

        foreach ($final_array as $array)
        {
            event(new sentNotification($array['friends_id'],'addPost',$post_id));
        }
        return redirect()->back();
    }

    public function edit(Request $request)
    {
        $data=$request->all();

        $current_date_time = \Carbon\Carbon::now();
        $data['user_id']= Auth::user()->id;
        $data['status']= '1';
        $data['created_at']= $current_date_time;
        $images = Session::get('images');

        //   Store Images Name from Session
        if(!empty($images))
        {
            PostImage::where('post_id', $data['id'])->forceDelete();
            foreach ($images as $image)
            {
                $dataimage['images']=$image;
                PostImage::create([
                    'image'=>$dataimage['images'],
                    'user_id'=>Auth::user()->id,
                    'post_id'=>$data['id'],
                    'created_at'=>$current_date_time,
                ]);
            }
        }
        else
        {
            PostImage::where('post_id', $data['id'])->forceDelete();
        }

        //   Update Post
        Post::where('user_id', $data['user_id'])->where('id', $data['id'])->update([
            'title' => $data['title'],
            'caption' => $data['caption'],

        ]);

        session()->flash('success', 'Post has been updated successfully.');
        return redirect()->back();
    }

    public function delete(Request $request)
    {
        //   Delete Post and Images from db
        Post::where('id',$request->id)->forceDelete();
        PostImage::where('post_id',$request->id)->forceDelete();

        //   Delete Notification
        UserNotification::where('message_type','addPost')->where('table_id',$request->id)->forceDelete();

        session()->flash('success', 'Post has been deleted sucessfully.');
        return redirect()->back();
    }

    public function fetchdata(Request $request)
    {
        Session::put('images', array());
        $id=$request->id;
        $post= Post::where('id',$id)->get();
        $postimage= PostImage::where('post_id',$id)->get();
        $dataimage = array();
        Session::put('edit', 'yes');

        //   If Images Exists, add on Session
        if(count($postimage)>0)
        {
            foreach ($postimage as $image)
            {
                    array_push($dataimage, $image->image);
            }

            Session::put('images', $dataimage);
        }
        return response()->json(['post'=>$post,'post_image'=>$postimage,'count'=>count($postimage),'edit'=>'yes']);
    }
    public function singlepost($id)
    {
        Session::put('images', array());
        // $id=$request->id;
        $post= Post::find($id);
        $postimage= PostImage::where('post_id',$id)->get();
        $dataimage = array();
        Session::put('edit', 'yes');

        //   If Images Exists, add on Session
        if(count($postimage)>0)
        {
            foreach ($postimage as $image)
            {
                    array_push($dataimage, $image->image);
            }

            Session::put('images', $dataimage);
        }
        return view('sports.expertview',compact('postimage','post'));
    }

    public function uploadimages(Request $request)
    {
        $data=array();
        if ($request->totalfiles>0)
        {
            $totalfiles=$request->totalfiles;

        //  Store New Images
            for($i=0;$i<$totalfiles;$i++)
            {
                $picture=file_get_contents($request['file-'.$i]);
                $filename=$this->storeimage($picture);
                array_push($data,$filename);

            }
            $edit='no';
        }
        if ($request->totalfiles_>0)
        {
        //  Store Edit Images
            $totalfiles=$request->totalfiles_;

            for($i=0;$i<$totalfiles;$i++)
            {
                $picture=file_get_contents($request['fileedit-'.$i]);
                $filename=$this->storeimage($picture);
                array_push($data,$filename);

            }
            $edit='yes';
        }

        Session::put('edit', $edit);

        $view = view('sports.modal.images',compact('data'))->render();
        return response()->json([
            'html'=>$view,
            'edit'=>$edit,
            'count'=>count($data),
        ]);
    }

    public function deleteimages(Request $request)
    {

        //   Delete Images from Session
        $data = Session::get('images');
        if (($key = array_search($request->id, $data)) !== false) {
            unset($data[$key]);
            Session::put('images', $data);
        }

        $images = Session::get('images');
        $edit = Session::get('edit');
        return response()->json([
            'count'=>count($images),
            'edit'=>$edit,
        ]);
    }

    public function clearimages()
    {
        //   Clear array from Session
        Session::put('images', array());
    }

    public function storeimage($picture)
    {
        //  Generate Name
        $dateTime = date('Ymd_His');
        $filename = uniqid().$dateTime.".jpg";
        $path='public/posts/';
        Storage::put($path. $filename, $picture);

        //   Put Images on Session
        $images = Session::get('images');

        //   Check Images on Session
        array_push($images, $filename);
        Session::put('images', $images);

        return $filename;
    }
}
