<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Admin\UserProfile;
use App\Models\Admin\Follower;
use App\Models\Admin\Post;
use App\Models\Admin\PostImage;
use App\Models\Admin\PostVideo;
use App\Models\Admin\UserNotification;
use App\Models\Admin\FriendList;
use App\User;
use Carbon\Carbon;
use Session;

class UserslistController extends Controller
{
    public $path = "store.admin.users.";

    public function index()
    {
        //
        $data['title'] = "Users List";
        $data['cols'] = [
            'First Name',
            'Last Name',
            'Email',
            'Status',
            'Action'
        ];

        $data['datas'] = UserProfile::orderBy('id','desc')->get();

        return view($this->path.'list',$data);
    }

    public function edit($id)
    {
        
        // $user = UserProfile::find($id);
        $data['data'] = UserProfile::find($id);
        return view($this->path.'edit',['data' => $data['data'] ]);
    }
    public function update(Request $request, $id)
    {
        $validated = $request->validate([
            'first_name'  => 'required',
            'email' => 'required|unique:users,email,'.$request->user_id,
            'last_name' =>'required',
            'dob' => 'before_or_equal:now',


        ]);
$age = Carbon::parse($request->dob)->diff(Carbon::now())->y;
        $user_id=$request->user_id;
        User::where('id',$user_id)->update([
            'name'=>$request->first_name,
            'email'=>$request->email,
        ]);
        UserProfile::where('user_id', $user_id)->update([
            'fname' => $request->first_name,
            'lname' => $request->last_name,
            'dob' => $request->dob,
            'age' => $age,
            'email' => $request->email,
            'mob' => $request->mobile,
            'bio' => $request->bio,
            'hometown' => $request->hometown,
            'city' => $request->city,
            'country' => $request->country,
            'gender' => $request->gender,
            'exp_status' => $request->exp,
        ]);
        // $user = UserProfile::find($id);
        return redirect()->route('admin.users.list')->with('message', 'User has been updated successfully!');
    }
    public function destroy($id)
    {
        $userprofile = UserProfile::where('id',$id)->first();
        $user_id = $userprofile->user_id;
        Follower::where('follower_id', $user_id)->delete();
        FriendList::where('friend_id', $user_id)->delete();
        Post::where('user_id', $user_id)->delete();
        UserNotification::where('send_by', $user_id)->delete();
        PostVideo::where('user_id', $user_id)->delete();
        PostImage::where('user_id', $user_id)->delete();
        User::find($user_id)->delete();
        UserProfile::find($id)->delete();
        
        return redirect()->back()->with('message', 'User has been Deleted successfully!');
    }
  public function postdestroyview()
  {
    
        //
        $data['title'] = "Posts List";
        $data['cols'] = [
            'First Name',
            'Last Name',
            'Caption',
            'Image(s)',
            'Status',
            'Action'
        ];
        $data['datas']= Post::select('posts.*',
        'user_profiles.picture as propicture',
        'user_profiles.fname as fname',
        'user_profiles.lname as lname',
        'user_profiles.user_id as user_id','users.user_link as user_link')
        ->orderBy('created_at','desc')
        ->join('users', 'users.id', '=', 'posts.user_id')
        ->join('user_profiles', 'user_profiles.user_id', '=', 'posts.user_id')->get();


        return view($this->path.'posts',$data);
  }
  public function postdelete(Request $request){
    Post::where('id',$request->id)->forceDelete();
    PostImage::where('post_id',$request->id)->forceDelete();
    PostImage::where('post_id',$request->id)->forceDelete();

    //   Delete Notification
    UserNotification::where('message_type','addPost')->where('table_id',$request->id)->forceDelete();
    return redirect()->route('admin.post.destroy')->with('message', 'Post has been deleted sucessfully!');
  }
  public function singlepost($id)
  {  Session::put('images', array());
    // $id=$request->id;
    $post= Post::find($id);
    $userdetail = UserProfile::where('user_id',$post->user_id)->get();
    $postimage= PostImage::where('post_id',$id)->get();
    $dataimage = array();
    Session::put('edit', 'yes');

    //   If Images Exists, add on Session
    if(count($postimage)>0)
    {
        foreach ($postimage as $image)
        {
                array_push($dataimage, $image->image);
        }

        Session::put('images', $dataimage);
    }
    return view('store.admin.users.postview',compact('postimage','post','userdetail'));
  }
}
