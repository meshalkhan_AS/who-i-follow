<?php

namespace App\Http\Controllers\Admin;
use App\Models\Admin\UserProfile;
use App\User;
use App\WebUrl;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Auth;
use Illuminate\Support\Facades\Storage;


class UpdateController extends Controller
{
    public function aboutupdate(Request $request)
    {
        $request->all();
        $user_id=$request->user_id;
        $datauser['name']=$request->fname;
        $datauser['lname']=$request->lname;

        // Evaluate Age through DOB
        $age = (date("md",
                            date("U", mktime(0, 0, 0,
                            date('m',strtotime($request->dob)),
                            date('d',strtotime($request->dob)),
                            date('Y',strtotime($request->dob))))
                    ) > date("md")
                    ? ((date("Y") - date('Y',strtotime($request->dob))) - 1)
                    : (date("Y") - date('Y',strtotime($request->dob))));

        $datauser['age']=$age;
        $datauser['dob']=$request->dob;
        $datauser['mob']=$request->mob;
        $datauser['email']=trim($request->email);


        // Update values on User Table
        User::where('id',$user_id)->update([
            'name'=>$datauser['name'],
            'email'=>$datauser['email'],
        ]);

        // Update User Profile Table if picture
        if ($request->file('picture'))
        {
            //  Store File
            $picture=file_get_contents($request->picture);
            $filename='profile_picture.jpg';
            $path='public/users/'.$user_id.'/';
            Storage::put($path. $filename, $picture);
            $datauser['picture']='yes';

            UserProfile::where('user_id', $user_id)->update([
                'fname' => $datauser['name'],
                'lname' => $datauser['lname'],
                'dob' => $datauser['dob'],
                'age' => $datauser['age'],
                'email' => $datauser['email'],
                'picture' => 'yes',
            ]);
        }
        else
        {
        // Update User Profile Table if picture
            UserProfile::where('user_id', $user_id)->update([
                'fname' => $datauser['name'],
                'lname' => $datauser['lname'],
                'dob' => $datauser['dob'],
                'age' => $datauser['age'],
                'email' => $datauser['email'],
                'mob' => $datauser['mob'],
            ]);
        }

        /* php artisan migrate */
        \Artisan::call('cache:clear');
        \Artisan::call('config:clear');
        \Artisan::call('view:clear');
        session()->flash('about', 'Your information has been updated successfully.');
        return redirect()->back()->withInput(['tab' => 'nav-status']);
    }

    public function coverupdate(Request $request)
    {

        $user_id=Auth::user()->id;

        // Update Cover Photo if picture
        if ($request->file('coverphoto'))
        {
            //  Store File
            $picture=file_get_contents($request->coverphoto);
            $filename='cover_photo.jpg';
            $path='public/users/'.$user_id.'/';
            Storage::put($path. $filename, $picture);
            $datauser['picture']='yes';

            UserProfile::where('user_id',$user_id)->update([
                'coverphoto'=>'yes',
            ]);
        }

        /* php artisan migrate */
        \Artisan::call('cache:clear');
        \Artisan::call('config:clear');
        \Artisan::call('view:clear');

        session()->flash('success', 'Cover Photo has been updated Successfully.');
        return redirect()->route('user.home',Auth::user()->user_link);
    }
    public function coverremove()
    {

        $user_id=Auth::user()->id;
        UserProfile::where('user_id',$user_id)->update([
            'coverphoto'=>'no',
        ]);

    }
    public function profileupdate(Request $request)
    {

        $user_id=Auth::user()->id;

        // Update Cover Photo if picture
        if ($request->file('picture'))
        {
            //  Store File
            $picture=file_get_contents($request->picture);
            $filename='profile_picture.jpg';
            $path='public/users/'.$user_id.'/';
            Storage::put($path. $filename, $picture);
            $datauser['picture']='yes';

            UserProfile::where('user_id',$user_id)->update([
                'picture'=>'yes',
            ]);
        }

        /* php artisan migrate */
        \Artisan::call('cache:clear');
        \Artisan::call('config:clear');
        \Artisan::call('view:clear');

        session()->flash('success', 'Profile Photo has been updated Successfully.');
        return redirect()->route('user.home',Auth::user()->user_link);
    }


    public function profilelink(Request $request)
    {

        $user_id=Auth::user()->id;

        // Update Cover Photo if picture
        if ($request->user_link)
        {
            User::where('id',$user_id)->update([
                'user_link'=>$request->user_link,
                'level'=>'1',
            ]);
        }
        session()->flash('success', 'Profile Link has been updated Successfully.');
        return redirect()->route('user.home',$request->user_link);
    }
    public function tabupdate(Request $request, $id){
        
        UserProfile::where('user_id',$id)->update([
            'default_tab'=>$request->page,
        ]);

        session()->flash('defaulttab', 'Your Default page configuration has been updated successfully.');
        return redirect()->back()->withInput(['tab' => 'page']);
    }
    public function editurlupdate(Request $request){
        WebUrl::where('id', $request->editUrlID)->update([
            'urls' => $request->weburl,
        ]);
        session()->flash('bio', 'Your URL has been updated successfully.');
        return redirect()->back();
    }
    public function bioupdate(Request $request)
    {
        $validated = $request->validate([
            'Url'  => 'required'
        ]);
        $request->all();
        $user_id=$request->user_id;
        $datauser['bio']=$request->bio;
        $datauser['country']=$request->country;
        $datauser['city']=$request->city;
        $datauser['hometown']=$request->hometown;
        $datauser['gender']=$request->gender;
        $datauser['zip_code']=$request->zip_code;
        $datauser['state']=$request->state;
        $user_url=$request->Url;
        UserProfile::where('user_id',$user_id)->update([
            'bio'=>$datauser['bio'],
            'country'=>$datauser['country'],
            'city'=>$datauser['city'],
            'hometown'=>$datauser['hometown'],
            'gender'=>$datauser['gender'],
            'zip_code'=>$datauser['zip_code'],
            'state'=>$datauser['state'],
        ]);
        $firstValue = array_filter($user_url);
        if($firstValue!=null){
            foreach($firstValue as $url) {
                WebUrl::create([
                    'user_id' => $user_id,
                    'urls' => $url
                ]);
            }
    }

        session()->flash('bio', 'Your information has been updated successfully.');
        return redirect()->back();
    }

    public function sportupdate(Request $request)
    {
        $user_id = Auth::user()->id;
        $insert_data ='';
        $line_items_array_start = $request['sport_id'];

        if($request['sport_id'])
        {

            if($line_items_array_start[0]!=null)
            {
                foreach ($line_items_array_start as $datasport)
                {
                    $insert_data.=','.$datasport;
                }
            }
            $datauser['sports_id']=$insert_data=ltrim($insert_data,',') ;

            UserProfile::where('user_id',$user_id)->update([
                'sports_id'=>$datauser['sports_id'],
            ]);
            session()->flash('sport', 'Your Sports Has Been Updated Successfully.');
        }
        else
        {
            session()->flash('sportnot', 'Not Updated.! Please select atleast one Sport.');
        }

        return redirect()->back();
    }
}
