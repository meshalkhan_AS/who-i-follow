<?php

namespace App\Http\Controllers\Admin;

use App\Helpers\GeneralHelper;
use App\Models\Admin\Follower;
use App\Models\Admin\FriendList;
use App\Models\Admin\Post;
use App\Models\Admin\UserProfile;
use PragmaRX\Countries\Package\Countries;
use App\User;
use App\WebUrl;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Auth;

class UserProfileController extends Controller
{

    public function editurl($id)
    {
    	$weburl = WebUrl::find($id);

	    return response()->json([
	      'data' => $weburl
	    ]);
    }
    public function weburlmodal($id)
    {
        $user_modal = WebUrl::where('id',$id)->delete();
        return redirect()->back();
    }
    public function bioindex(Request $request)
    {
        if(Auth::user()->id==1)
        {
            Auth::logout();
            return redirect()->route('login');
        }
        $countries = new Countries();
        $allcountries = $countries->all()->pluck('name.common');
        $active='bio';
        $user_urls = WebUrl::where('user_id',Auth::user()->id)->simplePaginate(10);
        $userprofile = UserProfile::select('user_profiles.*','users.user_link as user_link','users.level as level')
            ->join('users', 'users.id', '=', 'user_profiles.user_id')
            ->where('user_profiles.user_id',Auth::user()->id)->first();

        $friend_request=FriendList::select('friend_lists.*','users.user_link as user_link')
            ->join('users', 'users.id', '=', 'friend_lists.user_id')
            ->where('friend_lists.friend_id',Auth::user()->id)->where('friend_lists.status','0')->paginate(18);

        if ($request->ajax()) {
            $view = view('sports.lists.request',compact('friend_request'))->render();
            return response()->json(['html'=>$view]);
        }
        else {
            return view('sports.bio', compact('active', 'userprofile', 'friend_request','user_urls','allcountries'));
        }
    }

    public function coverindex()
    {
        if(Auth::user()->id==1)
        {
            Auth::logout();
            return redirect()->route('login');
        }
        return view('sports.cover');
    }

    public function profiles(Request $request)
    {
        if(Auth::user()->id==1)
        {
            Auth::logout();
            return redirect()->route('login');
        }
        $profiles=UserProfile::select('user_profiles.*','users.user_link as user_link')
        ->join('users', 'users.id', '=', 'user_profiles.user_id')
        ->where('user_profiles.user_id','!=',Auth::user()->id)->paginate(18);
        $search='';
        if ($request->ajax()) {
            $view = view('sports.lists.profiles',compact('profiles'))->render();
            return response()->json(['html'=>$view]);
        }
        else
        {
            return view('sports.profiles', compact( 'profiles','search'));
        }
    }

    public function individualprofile($user_link,Request $request)
    {
        if(Auth::user()->id==1)
        {
            Auth::logout();
            return redirect()->route('login');
        }
        $user=User::where('user_link',$user_link)->first();
        $id=$user['id'];
        if($id==Auth::user()->id)
        {
            return redirect()->route('user.home');
        }
        if($request->random)
        {
            $random_post=$request->random;
        }
        else
        {
            $random_post=0;
        }
        
        $userprofile = UserProfile::where('user_id',$id)->first();

        $myfriends=FriendList::select('friend_lists.*','friend_lists.friend_id as user_profile',
            'user_profiles.city as city',
            'user_profiles.country as country',
            'user_profiles.picture as picture',
            'user_profiles.fname as fname',
            'user_profiles.lname as lname','users.user_link as user_link')
            ->join('users', 'users.id', '=', 'friend_lists.friend_id')
            ->join('user_profiles', 'friend_lists.friend_id', '=', 'user_profiles.user_id')
            ->where('friend_lists.user_id',$id)
            ->where('friend_lists.status','1')
            ->get()->toArray();

        $ifriends=FriendList::select('friend_lists.*','friend_lists.user_id as user_profile',
            'user_profiles.city as city',
            'user_profiles.country as country',
            'user_profiles.picture as picture',
            'user_profiles.fname as fname',
            'user_profiles.lname as lname','users.user_link as user_link')
            ->join('users', 'users.id', '=', 'friend_lists.user_id')
            ->join('user_profiles', 'friend_lists.user_id', '=', 'user_profiles.user_id')
            ->where('friend_lists.friend_id',$id)
            ->where('friend_lists.status','1')
            ->get()->toArray();

        $friends_merge=array_merge($myfriends,$ifriends);
        $friendsCount=(count($friends_merge)>0)?count($friends_merge):'';
        $friends = array_slice($friends_merge, 0, 5, true);

        $followers =Follower::where('follower_id',$id)->get();
        $following =Follower::where('user_id',$id)->get();

        $posts =Post::select('posts.*',
        'user_profiles.picture as propicture',
        'user_profiles.fname as fname',
        'user_profiles.lname as lname','users.user_link as user_link',
        'user_profiles.user_id as user_id')
        ->orderBy('created_at','desc')
        ->where('posts.user_id',$id)->where('posts.id','!=',$random_post)
        ->join('user_profiles', 'user_profiles.user_id', '=', 'posts.user_id')
        ->join('users', 'users.id', '=', 'posts.user_id')
            ->paginate(3);

        //get random ad for news feed
        $ads = GeneralHelper::getRandomAdBanner(Auth::user()->id);
        if ($request->ajax()) {
            $view = view('sports.lists.profilepost',compact('userprofile','posts','ads'))->render();
            return response()->json(['html'=>$view]);
        }
        else
        {
            return view('sports.userprofile',compact('userprofile','random_post','friends','friendsCount','followers','ads','following','posts','id','user_link'));
        }


    }

}