<?php

namespace App\Http\Controllers\Admin;

use App\Models\Admin\TaxPayer;
use App\Models\Admin\UserProfile;
use App\User;
use Spatie\Permission\Models\Role;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Admin\RolePermission;
use App\Helpers\PermissionHelper as Per;
use Session;
use Auth;
use DB;
use App\Helpers\GeneralHelper;


class UsersController extends Controller
{

    public function index()
    {

        if (! Per::has_permission('users_manage')) {
            return abort(401);
        }
        $users = User::get();
        return view('admin.users.index', compact('users'));
    }


    public function create()
    {
        if (! Per::has_permission('users_manage')) {
            return abort(401);
        }
        $roles = Role::where('name','<>', 'administrator')->get()->pluck('name', 'name');

        $roles->prepend('Select a Role', '');
        return view('admin.users.create', compact('roles'));
    }

    public function store(Request $request)
    {
        if (! Per::has_permission('users_manage'))
        {
            return abort(401);
        }
        $data = $request->all();

        $email = $request->email;
        $emailcheck = User::where('email',$email)->get();
        if(count($emailcheck)>=1)
        {
            session()->flash('success', 'Email Already Exists.');
            return redirect()->route('admin.users.create');
        }
        else
            {
            $roles = $request->input('roles') ? $request->input('roles') : [];
            $role_name = $roles[0];
            $role_id = Role::where('name', $role_name)->first()->id;

            $perm = RolePermission::where('id', $role_id)->pluck('permissions')->first();

            $data['role_id'] = $role_id;
            $data['permissions'] = $perm;
            $data['password'] = bcrypt($data['password']);
            $data['created_by'] = Auth::user()->id;
            $data['updated_by'] = Auth::user()->id;

            $user = User::create($data);

            session()->flash('success', 'Record has been created successfully.');
            return redirect()->route('admin.users.index');
        }
    }


    public function edit($id)
    {
        if (! Per::has_permission('users_manage')) {
            return abort(401);
        }
        $roles = Role::where('name','<>', 'administrator')->get()->pluck('name', 'name');

        $user = User::findOrFail($id);
        return view('admin.users.edit', compact('user', 'roles'));
    }


    public function update(Request $request, $id)
    {
        if (! Per::has_permission('users_manage')) {
            return abort(401);
        }

        $user = User::findOrFail($id);

        $data = $request->all();
        $email = $request->email;

        $cniccheck = User::where('email',$email)->where('id', '!=', $id)->get();
        if(count($cniccheck)>=1)
        {
            session()->flash('success', 'Email Already Exists.');
            return redirect()->route('admin.users.index');
        }
        else
        {
            $roles = $request->input('roles') ? $request->input('roles') : [];
            $role_name = $roles[0];
            $role_id = Role::where('name', $role_name)->first()->id;

            $perm = RolePermission::where('id', $role_id)->pluck('permissions')->first();
            $data['role_id'] = $role_id;
            $data['permissions'] = $perm;
            $datas['password'] = $request->password;

            if (isset($datas['password']))
            {
                $data['password'] = bcrypt($datas['password']);
            }
            $data['updated_by'] = Auth::user()->id;

            $user->update($data);
            Session::flash('success', 'Record has been updated successfully.');
            return redirect()->route('admin.users.index');
        }
    }

    public function destroy($id)
    {
        if (! Per::has_permission('users_manage')) {
            return abort(401);
        }
        $user = User::findOrFail($id);

        $user->delete();
        Session::flash('success', 'Record has been deleted successfully.');
        return redirect()->route('admin.users.index');
    }



}
