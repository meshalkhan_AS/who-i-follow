<?php
namespace App\Http\Controllers;
  
use Illuminate\Http\Request;
use Omnipay\Omnipay;
use App\Payment;
use Auth;  
class PaymentController extends Controller
{
    public $gateway;
  
    public function __construct()
    {
        $this->gateway = Omnipay::create('AuthorizeNetApi_Api');
        $this->gateway->setAuthName(env('ANET_API_LOGIN_ID'));
        $this->gateway->setTransactionKey(env('ANET_TRANSACTION_KEY'));
        //$this->gateway->setTestMode(true); //comment this line when move to 'live'
    }
  
    public function index()
    {
        $totalamount=Cart::getTotal();
        return view('payment',compact($totalamount));
    }
  
    public function charge(Request $request)
    {
        if ($request->input('opaqueDataDescriptor') && $request->input('opaqueDataValue')) {
     
            try {
                // Generate a unique merchant site transaction ID.
                $transactionId = rand(100000000, 999999999);
                $costumerdata=session()->get('checkout_data')->first();
                $user_id=Auth::user()->id;
                //dd($costumerdata);
                $response = $this->gateway->authorize([
                    'amount' => $request->input('amount'),
                    'currency' => 'USD',
                    'transactionId' => $transactionId,
                    'customerId' => $user_id,
                    'card' => [
                        'billingFirstName' => $costumerdata['billing_name'],
                        'billingAddress1' => $costumerdata['billing_address'],
                        'billingCity' => $costumerdata['billing_city'],
                        'billingState' => $costumerdata['billing_state'],
                        'billingCountry' => $costumerdata['billing_country'],
                        'billingPostcode' => $costumerdata['billing_zip'],
                        'billingPhone' => $costumerdata['billing_phone'],
                        'billingEmail' => $costumerdata['billing_email'],
                        //... may include shipping info but do not include card (number, cvv or expiration)
                        'shippingFirstName' => $costumerdata['billing_name'],
                        'shippingAddress1' => $costumerdata['billing_address'],
                        'shippingCity' => $costumerdata['billing_city'],
                        'shippingState' => $costumerdata['billing_state'],
                        'shippingCountry' => $costumerdata['billing_country'],
                        'shippingPostcode' => $costumerdata['billing_zip'],
                        'shippingPhone' => $costumerdata['billing_phone'],
                    ],
                    'opaqueDataDescriptor' => $request->input('opaqueDataDescriptor'),
                    'opaqueDataValue' => $request->input('opaqueDataValue'),
                ])->send();
         
                if($response->isSuccessful()) {
         
                    // Captured from the authorization response.
                    $transactionReference = $response->getTransactionReference();
         
                    $response = $this->gateway->capture([
                        'amount' => $request->input('amount'),
                        'currency' => 'USD',
                        'transactionReference' => $transactionReference,
                        ])->send();
         
                    $transaction_id = $response->getTransactionReference();
                    $amount = $request->input('amount');
     
                    // Insert transaction data into the database
                    $isPaymentExist = Payment::where('transaction_id', $transaction_id)->first();
     
                    if(!$isPaymentExist)
                    {
                        $payment = new Payment;
                        $payment->transaction_id = $transaction_id;
                        $payment->amount = $request->input('amount');
                        $payment->currency = 'USD';
                        $payment->payment_status = 'Captured';
                        $payment->save();
                    }
                    
            $data=([
                'name'=>'Auth.net',
                'id'=>$transaction_id
            ]);
            session()->put('payment_method',$data);
                    session()->put('invoice',$transaction_id);
                    return redirect(route('thank.you'))->with('success','Payment has been charged successfully!');
     
                    echo "Your payment transaction id: ". $transaction_id;
                } else {
                    // not successful
                    return redirect('shop/cart')->withErrors($response->getMessage());
                    echo $response->getMessage();
                }
            } catch(Exception $e) {
                return redirect('/shop/cart')->withErrors($e->getMessage());
                echo $e->getMessage();
            }
        }
    }
}