<?php

namespace App\Http\Controllers;

use App\Events\sentNotification;
use App\Helpers\GeneralHelper;
use App\Models\Admin\FriendList;
use App\Models\Admin\Invite;
use App\Models\Admin\UserProfile;
use App\User;
use Auth;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Storage;

class RegisterController extends Controller
{
    public function index()
    {
        // User data empty for new user
        Session::put('user_data', array());
        Session::put('user_invite_site', array());
        Session::put('user_invite_chat_share', array());
        return view('auth.register');
    }

    public function registerByInvite($level,$user_link,$custom)
    {
        // User data empty for new user
        Session::put('user_data', array());
        Session::put('user_invite_site', array());
        Session::put('user_invite_chat_share', array());

        // Find User ID
        $user=User::where('user_link',$user_link)->first();

        // Find If its in DB for site invitation
        $findInvitation=Invite::where('level',$level)->where('invite_type','!=','4')->first();
        if($findInvitation)
        {
            $user_invite['user_id']=$findInvitation['user_link'];
            $user_invite['level']=$level;
            //  Store on Session
            Session::put('user_invite_site', $user_invite);
        }
        else
        {
            $user_invite_chat_share['user_id']=$user['id'];
            $findInvitation=Invite::where('level',$level)->first();
            if($findInvitation)
            {
                $findInvitation['level']=$user_invite_chat_share['level']=$level;
                if (Auth::check()) {
                    if ($user['id'] != Auth::user()->id) {
                        $friend = FriendList::where('user_id', Auth::user()->id)->where('friend_id', $user['id'])->get();
                        $friend_ = FriendList::where('user_id', $user['id'])->where('friend_id', Auth::user()->id)->get();

                        if (count($friend) >0 || count($friend_) >0) {

                        } else {
                            $current_date_time = \Carbon\Carbon::now();
                            $data['user_id'] = $user['id'];
                            $data['status'] = '1';
                            $data['friend_id'] = Auth::user()->id;
                            $data['created_at'] = $current_date_time;
                            $table_id = FriendList::create($data)->id;

                            //        Sent Notification
                            event(new sentNotification($user['id'], 'AddedAsFriend', $table_id));
                        }
                    }
                    //get random ad for news feed
                    $ads = GeneralHelper::getRandomAdBanner(Auth::user()->id, 'Messages');

                    $userprofile = \App\Models\Admin\UserProfile::where('user_id', Auth::user()->id)->first();
                    return view('chat.invitation', compact('findInvitation', 'ads', 'userprofile'));
                }
            }
            else
            {
                $user_invite_chat_share['level']=$level;
            }

            Session::put('user_invite_chat_share', $user_invite_chat_share);
            Session::put('user_invite_site', array());

        }
        $findNew=Invite::where('level',$level)->first();

        if($findNew)
        {
            $findUser=UserProfile::where('email',$findNew['target'])->orWhere('mob',$findNew['target'])->first();
            if($findUser)
            {
                $message='';
                return view('auth.login',compact('message'));
            }
            else
            {

                return view('auth.register');
            }
        }
        else
        {
            return view('auth.register');
        }

    }

    public function store(Request $request)
    {
        $data=$request->all();
        $data['dob']=$request->day.'-'.$request->month.'-'.$request->year;
        $time = strtotime($data['dob']);
        $data['dob'] = date('Y-m-d',$time);

        // Evaluate Age through DOB
        $age = (date("md", date("U", mktime(0, 0, 0,date('m',strtotime($request->month)), $request->day, $request->year))) > date("md")
            ? ((date("Y") - $request->year) - 1)
            : (date("Y") - $request->year));

        $data['age']=$age;

        // Assign values for User Table
        $datauser['name']=$request->fname;
        $datauser['email']=trim($request->email);
        $datauser['password']=bcrypt(trim($request->password));

        // Add data on User Table
        $id=User::create($datauser)->id;
        $data['user_id'] = $id;
        $data['exp_status'] = NULL;

        //  Store File
        if ($request->file('picture'))
        {
            $picture=file_get_contents($data['picture']);
            $filename='profile_picture.jpg';
            $path='public/users/'.$id.'/';
            if (!file_exists($path)) {
                Storage::put($path. $filename, $picture);
                $data['picture']='yes';
            }
        }
        else
        {
            $data['picture']='no';
        }

        // Add data on User Profile Table
        UserProfile::create($data);

        // Add User link
        $string=$this->getUId($id);


        $userUpdate=User::findOrFail($id);
        $userUpdate->user_link=$string;
        $userUpdate->save();

        // Store Registered User data on Session
        Session::put('user_data',$data);
        $user=User::where('email',$datauser['email'])->get();
        Invite::where('target',$datauser['email'])->update(['level'=>'0']);

        if(count($user)>0)
        {
            if(Auth::attempt([ 'email'=> $datauser['email'], 'password'=> $data['password']]))
            {
                // Get User Invite data for Site on Session
                $user_invite_site=Session::get('user_invite_site');
                $user_invite_chat_share=Session::get('user_invite_chat_share');

//                dd($user_invite_site);

                if($user_invite_site)
                {
                    $findInvitation=Invite::where('level',$user_invite_site['level'])->first();
                    if($findInvitation)
                    {
                        Auth::loginUsingId($id);
                        $current_date_time = \Carbon\Carbon::now();
                        $data['user_id'] = $user_invite_site['user_id'];
                        $data['status'] = '1';
                        $data['friend_id'] = $id;
                        $data['created_at'] = $current_date_time;
                        $table_id = FriendList::create($data)->id;

                        //        Sent Notification
                        event(new sentNotification($user_invite_site['user_id'], 'joinedByInvite', $table_id));
                        Invite::where('level',$findInvitation['level'])->update(['level'=>'0']);
                    }
                    return redirect()->route('user.chatSystem');
                }
                else if($user_invite_chat_share)
                {
                    $findInvitation=Invite::where('level',$user_invite_chat_share['level'])->first();
                    Auth::loginUsingId($id);
                    $current_date_time = \Carbon\Carbon::now();
                    $data['user_id'] = $user_invite_chat_share['user_id'];
                    $data['status'] = '1';
                    $data['friend_id'] = $id;
                    $data['created_at'] = $current_date_time;
                    $table_id = FriendList::create($data)->id;

                    //        Sent Notification
                    event(new sentNotification($user_invite_chat_share['user_id'], 'joinedByInvite', $table_id));
                    $findInvitation['id']=($findInvitation)?$findInvitation['id']:0;
                    $findInvitation['level']=$user_invite_chat_share['level'];


                    //get random ad for news feed
                    $ads = GeneralHelper::getRandomAdBanner($id, 'Messages');
                    $userprofile = \App\Models\Admin\UserProfile::where('user_id',$id)->first();
                    return view('chat.invitation',compact('findInvitation','ads','userprofile'));

                }
                else
                {
                    return redirect()->route('user.chatSystem');
                }
            }
        }

    }

    public function verify(Request $request)
    {

        // Get Registered User data on Session
        $data=Session::get('user_data');

        //  Add Mobile Number
        $profile = UserProfile::where('user_id',$data['user_id'])->first();
        if($profile) {
            $profile->mob = $request->mob;
            $profile->save();
        }

        //  To login into system
        $email=$data['email'];
        $email=trim( $email );
        $user=User::where('email',$email)->get();


        if(count($user)>0)
        {
            if(Auth::attempt([ 'email'=> $email, 'password'=> $data['password']]))
            {
                Session::put('user_data','');
                return redirect()->route('user.chatSystem');

            }
            else
            {
                return redirect()->back()->withErrors([
                    'error' => 'Credentials not Match.',
                ]);
            }
        }
        else
        {
            return redirect()->back()->withErrors([
                'password' => Lang::get('auth.failed'),
            ]);
        }

    }

    public function getUId($id)
    {
        $user=UserProfile::where('user_id',$id)->first();
        $nums = range(0, 9);
        shuffle($nums);
        $nums = array_slice($nums, 0, 4);
        $string= implode($nums);

        $name=trim($user['fname']).trim($user['lname']).$string;
        $finalName=strtolower($name);
        $key = User::where('user_link',$finalName)->first();
        if(!empty($key))
        {
            return $this->getUId();
        }
        return $finalName;
    }

    // Updating User Link

    public function updateUid()
    {
        $users = User::where('user_link', null)->where('id', '!=',1)->get();
        foreach ($users as $user)
        {
            $user->user_link = $this->getUId($user->id);
            $user->save();
        }
    }
}
