<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Category;
use Auth;

class AdminCategoryController extends Controller
{
    // folder path
    public $path = "store.admin.category.";

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        //
        if(Auth::user()->id!=1)
        {
            return redirect()->route('login');
        }
        $data['title'] = "Main Categories";
        $data['cols'] = ['Name'];


            if ($request->cat == "sub") {
                $data['title'] = "Sub categories";
                $data['cols'][] = 'Main category';
                
               $cat = Category::where('parent_id','!=',0)
               ->where('is_archive','=',0)
               ->orderBy('id','desc')
               ->get();
            }else{
               $cat = Category::where('parent_id','=',0)
               ->where('is_archive','=',0)
               ->orderBy('id','desc')
               ->get(); 
            }

        $data['cats'] = $cat;
        $data['request'] = $request;


        // load view
        return view($this->path.'list',$data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        //
        if(Auth::user()->id!=1)
        {
            return redirect()->route('login');
        }
        $data['title'] = "Add Category";
        $data['request'] = $request;

        $data['cats'] = Category::orderBy('id','desc')
        ->where('parent_id','=',0)
        ->where('is_archive','=',0)
        ->get();

        // load view
        return view($this->path.'add',$data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if(Auth::user()->id!=1)
        {
            return redirect()->route('login');
        }
        $validated = $request->validate([
            'main_category' => 'required',
            'category_title' => 'required|unique:categories,name,NULL,deleted_at,deleted_at,NULL',
        ]);

        $cat = Category::where('name',$request->category_title)->first();

        if (empty($cat)) {
            $cat =  new Category;
        }

        $string = preg_replace('/[^a-zA-Z0-9\']/', ' ', $request->category_title);
        $string = trim(preg_replace('/\s+/', ' ', $string));

        $cat->name = $request->category_title;
        $cat->slug = str_replace(' ', '-', $string);
        $cat->parent_id = $request->main_category;
        $cat->save();

        if ($request->main_category == 0) {
            $catUrl = "?cat=main";
            $msg = "Main category added successfully";
        }else{
            $catUrl = "?cat=sub";
            $msg = "Sub category added successfully";

        }


        return redirect(route('admin.categories.list').$catUrl)->with('message', $msg.'!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request,$id)
    {
        if(Auth::user()->id!=1)
        {
            return redirect()->route('login');
        }
        $data['title'] = "Edit Category";

        $data['data'] = Category::find($id);
        $data['request'] = $request;

        $data['cats'] = Category::orderBy('id','desc')
        ->where('parent_id','=',0)
        ->where('is_archive','=',0)
        ->get();

        return view($this->path.'edit',$data);
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        if(Auth::user()->id!=1)
        {
            return redirect()->route('login');
        }
        $validated = $request->validate([
            'main_category' => 'required',
            // 'category_title' => 'required',
            'category_title' => 'unique:categories,name,'.$id,
        ]);

        $cat = Category::find($id);

        $string = preg_replace('/[^a-zA-Z0-9\']/', ' ', $request->category_title);
        $string = trim(preg_replace('/\s+/', ' ', $string));

        $cat->name = $request->category_title;
        $cat->slug = str_replace(' ', '-', $string);
        $cat->parent_id = $request->main_category;
        $cat->save();


        // if ($request->main_category == 0) {
        //     $catUrl = "?cat=main";
        // }else{
        //     $catUrl = "?cat=sub";

        // }

        if ($request->main_category == 0) {
            $catUrl = "?cat=main";
            $msg = "Main category updated successfully";
        }else{
            $catUrl = "?cat=sub";
            $msg = "Sub category updated successfully";

        }



        return redirect(route('admin.categories.list').$catUrl)->with('message', $msg.'!');
    }

    public function categoryById(Request $request)
    {
        if(Auth::user()->id!=1)
        {
            return redirect()->route('login');
        }

        $data = Category::where('parent_id',$request->parent_id)
        ->where('is_archive','=',0)
        ->get();

        return response()->json([
          'status_code' => 200,
          'data'    => $data,
          'status'  => true,
          'message'     => 'successfully'
        ],200);

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request , $id)
    {

        if(Auth::user()->id!=1)
        {
            return redirect()->route('login');
        }
        if ($request->cat == "main") {
            $catUrl = "?cat=main";
            $msg = "Main category deleted successfully";
        }else{
            $catUrl = "?cat=sub";
            $msg = "Sub category deleted successfully";
        }

        // Category::find($id)->update(['is_archive' => 1]);
        Category::find($id)->delete();


         return redirect(route('admin.categories.list').$catUrl)->with('message', $msg.'!');

        // return redirect(route('admin.categories.list'))->with('message', 'Category delted successfully!');
        //
    }
}
