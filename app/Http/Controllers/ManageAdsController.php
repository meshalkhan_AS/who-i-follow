<?php

namespace App\Http\Controllers;

use App\AdvertisementBanner;
use App\AdvertisementCategory;
use App\AdvertisementStats;
use Illuminate\Http\Request;
use Auth;

class ManageAdsController extends Controller
{
	/**
	 * To display categories and banners with forms to fill
     * new entries for both models
	 * @return view with banners and categories model
	 */
    public $path = "store.admin.ads.";

    public function index()
    {
        if(Auth::user()->id!=1)
        {
            return redirect()->route('login');
        }
    	$banners = AdvertisementBanner::all();
        $categories = AdvertisementCategory::all();
        return view($this->path.'category.table', [
            'banners' => $banners,
            'categories' => $categories
        ]);
    }




    /* Category */

    /**
     * [catIndex description]
     * @return [type] [description]
     */
    public function catIndex()
    {
        if(Auth::user()->id!=1)
        {
            return redirect()->route('login');
        }
        $data['title'] = "Add Advertisement Category";
    	return view($this->path.'category.add',$data);
    }

    /**
     * [catAdd description]
     * @return [type] [description]
     */
    public function catAdd(Request $request)
    {
        if(Auth::user()->id!=1)
        {
            return redirect()->route('login');
        }
        // Validation
        $rules = [
            'cat' => 'required',
            'visible' => 'required',
        ];
        // dd($request->all());
        if($this->validate($request, $rules)) {
            try {
                $new_cat = new AdvertisementCategory();
                $new_cat->name = $request['cat'];
                $new_cat->is_visible = $request['visible'];
                // $new_cat->user_id = auth()->user()->id;
                $new_cat->save();
                return redirect()->route('ads.index');
            } catch(Exception $e) {
                print_r($e);
            }
        } else {
            // Print Message for Filling the required once
        }
    }

    /**
     * [catEdit description]
     * @return [type] [description]
     */
    public function catEdit($id)
    {
        if(Auth::user()->id!=1)
        {
            return redirect()->route('login');
        }
        $edit_cat = AdvertisementCategory::find($id);

        // dd($edit_cat->name_ar);
        return view($this->path.'category.edit', [
            'category' => $edit_cat,
            'title'    => 'Add Advertisement Category'
        ]);
    }

    /**
     * [catUpdate description]
     * @return [type] [description]
     */
    public function catUpdate(Request $request, $id)
    {
        if(Auth::user()->id!=1)
        {
            return redirect()->route('login');
        }
    	$rules = [
            'cat' => 'required',
            // 'cat_en' => 'required',
        ];
        if($this->validate($request, $rules)) {
            try {
                $cat = AdvertisementCategory::find($id);
                $cat->name = $request['cat'];
                // $cat->name_en = $request['cat_en'];
                $cat->is_visible = $request['visible'];
                $cat->save();
                return redirect()->route('ads.index');
            } catch(Exception $e) {
                print_r($e);
            }
        } else {
            // Error Messages
        }
    }

    /**
     * [catDelete description]
     * @return [type] [description]
     */
    public function catDelete($id)
    {
        if(Auth::user()->id!=1)
        {
            return redirect()->route('login');
        }
        $cat = AdvertisementCategory::find($id);
        $cat->delete();
        return redirect()->route('ads.index');
    }

    /* Banner */

    /**
     * [bannerIndex description]
     * @return [type] [description]
     */
    public function bannerIndex()
    {
        $banners = AdvertisementBanner::all();
        $categories = AdvertisementCategory::all();
        return view($this->path.'banner.table', [
            'banners' => $banners,
            'categories' => $categories
        ]);
    	# code...
    }

    /**
     * [bannerAdd description]
     * @return [type] [description]
     */
    public function bannerAddView()
    {
        if(Auth::user()->id!=1)
        {
            return redirect()->route('login');
        }
        $data['categories'] = AdvertisementCategory::all();
        $data['title'] = "Add Advertisement Banner";
        return view($this->path.'banner.add',$data);
    	# code...
    }

    public function bannerAdd(Request $request)
    {
        if(Auth::user()->id!=1)
        {
            return redirect()->route('login');
        }

        $rules = [
            'category'    => 'required',
            'name'        => 'required',
            'link'        => 'required|string|url',
            'description' => 'required',
            'image'       => 'mimes:jpeg,jpg,png,gif|required'
        ];
        // dd($request->all());
        if($this->validate($request, $rules)) {

            try {

                if ($files = $request->file('image'))
                {
                    $dateTime = date('Ymd_His');
                    $imageName = $dateTime . ".jpg";
                    $files->getClientOriginalName();
                    $savePath = 'public/uploads/banners/';
                    $files->move($savePath, $imageName);
                }


                $banner = new AdvertisementBanner();
                $banner->category_id = $request->category;
                $banner->title = $request->name;
                $banner->link = $request->link;
                $banner->desc = $request->description;
                $banner->image = $savePath.$imageName;
                $banner->is_featured = 0;
                $banner->is_visible = 1;
                $banner->sort = 5;
                $banner->save();
                return redirect()->route('banner.index');

            } catch (Exception $e) {

            }

        }




    }

    /**
     * [bannerEdit description]
     * @return [type] [description]
     */
    public function bannerEdit($id)
    {
        if(Auth::user()->id!=1)
        {
            return redirect()->route('login');
        }
        $data['categories'] = AdvertisementCategory::all();
        $data['banner'] = AdvertisementBanner::find($id);

        return view($this->path.'banner.edit', $data);

    	# code...
    }

    /**
     * [bannerUpdate description]
     * @return [type] [description]
     */
    public function bannerUpdate(Request $request , $id)
    {
        if(Auth::user()->id!=1)
        {
            return redirect()->route('login');
        }
        $rules = [
            'category'    => 'required',
            'name'        => 'required',
            'description' => 'required',
            'link'        => 'required|string|url',
            // 'image'       => 'mimes:jpeg,jpg,png,gif|required'
        ];

        if ($files = $request->file('image'))
            {

                $rules['image'] = 'mimes:jpeg,jpg,png,gif|required';

            }

        if($this->validate($request, $rules)) {
            try {

                $banner =  AdvertisementBanner::find($id);

                if ($files = $request->file('image'))
                {
                    $dateTime = date('Ymd_His');
                    $imageName = $dateTime . ".jpg";
                    $files->getClientOriginalName();
                    $savePath = 'public/uploads/banners/';
                    $files->move($savePath, $imageName);

                    $banner->image = $savePath.$imageName;
                }

                $banner->category_id = $request->category;
                $banner->title = $request->name;
                $banner->desc = $request->description;
                $banner->link = $request->link;
                $banner->is_featured = 0;
                $banner->is_visible = 1;
                $banner->sort = 5;
                $banner->save();
                return redirect()->route('banner.index');

            } catch(Exception $e) {
                print_r($e);
            }
        } else {
            // Error Messages
        }

    	# code...
    }

    /**
     * [bannerDelete description]
     * @return [type] [description]
     */
    public function bannerDelete($id)
    {
        if(Auth::user()->id!=1)
        {
            return redirect()->route('login');
        }
        $banner =  AdvertisementBanner::find($id);
        $banner->forceDelete();

        $adsstates=AdvertisementStats::where('advertisement_banner_id',$id)->get();

        if(count($adsstates)>0)
        {
            foreach ($adsstates as $adsstate)
            {
                $adsstates_=AdvertisementStats::where('id',$adsstate->id)->first();
                $adsstates_->forceDelete();
            }
        }

        return redirect()->route('banner.index');
    }

    public function adsReport()
    {
        if(Auth::user()->id!=1)
        {
            return redirect()->route('login');
        }
        $data['count_stats'] =  AdvertisementStats::with('banner')->where('action_type','click')->selectRaw('advertisement_banner_id,COUNT(*) as click_count')->groupBy('advertisement_banner_id')->get();
        $data['adsStats'] = AdvertisementStats::with('banner', 'category', 'user')->orderBy('id', 'desc')->get();
        return view($this->path.'report.table', $data);
    }
}
