<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class WebUrl extends Model
{
    protected $fillable = ['id',
    'user_id',
    'urls',
    'created_by','updated_by','deleted_by'
];
}
