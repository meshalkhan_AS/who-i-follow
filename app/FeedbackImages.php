<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class FeedbackImages extends Model
{
    use SoftDeletes;
    protected $fillable = ['id',
        'image',
        'feedback_id',
        'user_id',
        'status',
        'created_by','updated_by','deleted_by'
    ];
    protected $table = 'feedback_images';
}
